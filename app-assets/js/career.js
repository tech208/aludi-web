
var work_e = [];
var edit_mode = false;
var active_id = null;
$('#btn_add_experience').click(function() {
    var company = $('#work_e_company').val();
    var jabatan = $('#work_e_jabatan').val();
    var gaji = $('#work_e_gaji').val();
    var start_month = $('#work_e_start_month').val();
    var start_year = $('#work_e_start_year').val();
    var end_month = $('#work_e_end_month').val();
    var end_year = $('#work_e_end_year').val();

    if (!company || company == '') {
        $('#txt_help_company').css('visibility', 'visible');
        return;
    }

    if (!jabatan || jabatan == '') {
        $('#txt_help_jabatan').css('visibility', 'visible');
        return;
    }

    if (!gaji || gaji == '') {
        $('#txt_help_gaji').css('visibility', 'visible');
        return;
    }

    if(edit_mode === true) {
        var data = work_e[active_id];
        $('#company_title_'+active_id).text(company);
        $('#company_item_'+active_id).text(company);
        $('#jabatan_item_'+active_id).text(jabatan);
        $('#gaji_item_'+active_id).text('Gaji: '+ gaji);
        $('#period_item_'+active_id).text(start_month +` `+ start_year +` - `+ end_month +` `+ end_year);

        $('#work_e_company').val('');
        $('#work_e_jabatan').val('');
        $('#work_e_gaji').val('');

        work_e[active_id] = {
            company:company,
            jabatan:jabatan,
            gaji:gaji,
            start_month:start_month,
            start_year:start_year,
            end_month:end_month,
            end_year:end_year,
        };

        edit_mode = false;
        return;
    }

    work_e.push({
        company:company,
        jabatan:jabatan,
        gaji:gaji,
        start_month:start_month,
        start_year:start_year,
        end_month:end_month,
        end_year:end_year,
    });
    var cur_id = work_e.length - 1;

    var wrapper = $('#job_experience_wrapper');
    wrapper.append(`
        <div class="card mb-0" id="card_company_`+cur_id+`" data-height>
            <input type="hidden" name="work[`+cur_id+`][company]" value="`+ company +`">
            <input type="hidden" name="work[`+cur_id+`][jabatan]" value="`+ jabatan +`">
            <input type="hidden" name="work[`+cur_id+`][gaji]" value="`+ gaji +`">
            <input type="hidden" name="work[`+cur_id+`][period_start]" value="`+ start_month +` `+ start_year +`">
            <input type="hidden" name="work[`+cur_id+`][period_end]" value="`+ end_month +` `+ end_year +`">
            <div class="card-header">
                <h4 class="card-title" id="company_title_`+cur_id+`">`+ company +`</h4>
                <a class="heading-elements-toggle"><i class="la la-ellipsis font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li>
                            <a data-toggle="collapse" href="#work_e_`+cur_id+`" role="button" aria-expanded="false" aria-controls="work_e_`+cur_id+`">
                                <i class="ft-plus"></i>
                            </a>
                        </li>
                        <li>
                            <a data-id="work_e_`+cur_id+`" data-action="edit-company"><i class="ft-edit"></i></a>
                        </li>
                        <li>
                            <a class="danger" data-id="work_e_`+cur_id+`" data-action="delete-company" ><i class="ft-x"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="card-content collapse" id="work_e_`+cur_id+`">
                <div class="card-body">
                    <ul class="list-group">
                        <li class="list-group-item" id="company_item_`+cur_id+`">`+ company +`</li>
                        <li class="list-group-item" id="jabatan_item_`+cur_id+`">`+ jabatan +`</li>
                        <li class="list-group-item" id="gaji_item_`+cur_id+`">Gaji: `+ gaji +`</li>
                        <li class="list-group-item" id="period_item_`+cur_id+`">`+ start_month +` `+ start_year +` - `+ end_month +` `+ end_year +`</li>
                    </ul>
                </div>
            </div>
        </div>
    `);

    $('#work_e_company').val('');
    $('#work_e_jabatan').val('');
    $('#work_e_gaji').val('');
});//End tambah company

$(document).on('click', 'a[data-action=edit-company]', function() {
    edit_mode = true;
    var id = $(this).data('id').split('_');
    active_id = parseInt(id[2]);
    var data = work_e[active_id];
    $('#work_e_company').val(data.company);
    $('#work_e_jabatan').val(data.jabatan);
    $('#work_e_gaji').val(data.gaji);
    $('#work_e_start_month').val(data.start_month);
    $('#work_e_start_year').val(data.start_year);
    $('#work_e_end_month').val(data.end_month);
    $('#work_e_end_year').val(data.end_year);
});

$(document).on('click', 'a[data-action=delete-company]', function() {
    if(edit_mode) {
        return;
    }
    var id = $(this).data('id').split('_');
    active_id = parseInt(id[2]);
    work_e.splice(active_id, 1);
    $('#card_company_'+ active_id).remove();
});


/**
 * Study block
 */


var studies = [];
var study_edit_mode = false;
var study_active_id = null;
$('#btn_add_study').click(function() {
    var tingkat = $('#study_tingkat').val();
    var institusi = $('#study_institusi').val();
    var jurusan = $('#study_jurusan').val();
    var tahun_lulus = $('#study_tahun').val();

    if (!tingkat || tingkat == '') {
        $('#txt_help_tingkat').css('visibility', 'visible');
        return;
    }

    if (!institusi || institusi == '') {
        $('#txt_help_institusi').css('visibility', 'visible');
        return;
    }

    if (!jurusan || jurusan == '') {
        $('#txt_help_jurusan').css('visibility', 'visible');
        return;
    }

    if (!tahun_lulus || tahun_lulus == '') {
        $('#txt_help_tahun').css('visibility', 'visible');
        return;
    }

    if(study_edit_mode === true) {
        var data = studies[study_active_id];
        $('#study_title_'+study_active_id).text(tingkat +'-'+ jurusan);
        $('#tingkat_item_'+study_active_id).text(tingkat);
        $('#institusi_item_'+study_active_id).text(institusi);
        $('#jurusan_item_'+study_active_id).text(jurusan);
        $('#tahun_item_'+study_active_id).text(tahun_lulus);

        $('#study_institusi').val('');
        $('#study_jurusan').val('');
        $('#study_tahun').val('');

        studies[study_active_id] = {
            tingkat:tingkat,
            institusi:institusi,
            jurusan:jurusan,
            tahun_lulus:tahun_lulus,
        };

        study_edit_mode = false;
        return;
    }

    studies.push({
        tingkat:tingkat,
        institusi:institusi,
        jurusan:jurusan,
        tahun_lulus:tahun_lulus,
    });
    var cur_id = studies.length - 1;

    var wrapper = $('#study_list_wrapper');
    wrapper.append(`
        <div class="card mb-0" id="card_study_`+cur_id+`" data-height>
            <input type="hidden" name="study[`+cur_id+`][tingkat]" value="`+ tingkat +`">
            <input type="hidden" name="study[`+cur_id+`][jurusan]" value="`+ jurusan +`">
            <input type="hidden" name="study[`+cur_id+`][institusi]" value="`+ institusi +`">
            <input type="hidden" name="study[`+cur_id+`][tahun_lulus]" value="`+ tahun_lulus +`">
            <div class="card-header">
                <h4 class="card-title" id="study_title_`+cur_id+`">`+ tingkat +'-'+ jurusan +`</h4>
                <a class="heading-elements-toggle"><i class="la la-ellipsis font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li>
                            <a data-toggle="collapse" href="#study_`+cur_id+`" role="button" aria-expanded="false" aria-controls="study_`+cur_id+`">
                                <i class="ft-plus"></i>
                            </a>
                        </li>
                        <li>
                            <a data-id="study_`+cur_id+`" data-action="edit-study"><i class="ft-edit"></i></a>
                        </li>
                        <li>
                            <a class="danger" data-id="study_`+cur_id+`" data-action="delete-study" ><i class="ft-x"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="card-content collapse" id="study_`+cur_id+`">
                <div class="card-body">
                    <ul class="list-group">
                        <li class="list-group-item" id="tingkat_item_`+cur_id+`">`+ tingkat +'-'+ jurusan +`</li>
                        <li class="list-group-item" id="institusi_item_`+cur_id+`">`+ institusi +`</li>
                        <li class="list-group-item" id="tahun_item_`+cur_id+`">`+ tahun_lulus +`</li>
                    </ul>
                </div>
            </div>
        </div>
    `);

    $('#study_institusi').val('');
    $('#study_jurusan').val('');
    $('#study_tahun').val('');
});//End tambah study

$(document).on('click', 'a[data-action=edit-study]', function() {
    study_edit_mode = true;
    var id = $(this).data('id').split('_');
    study_active_id = parseInt(id[1]);
    var data = studies[study_active_id];
    $('#study_tingkat').val(tingkat +'-'+ jurusan);
    $('#study_institusi').val(data.institusi);
    $('#study_tahun').val(data.tahun_lulus);
});

$(document).on('click', 'a[data-action=delete-study]', function() {
    if (study_edit_mode) {
        return;
    }

    var id = $(this).data('id').split('_');
    study_active_id = parseInt(id[1]);
    studies.splice(study_active_id, 1);
    $('#card_study_'+ study_active_id).remove();
});

/**
 * Organization block
*/


var orgs = [];
var org_edit_mode = false;
var org_active_id = null;
$('#btn_add_org').click(function() {
    var org_name = $('#org_name').val();
    var jabatan = $('#org_jabatan').val();
    var start_month = $('#org_start_month').val();
    var start_year = $('#org_start_year').val();
    var end_month = $('#org_end_month').val();
    var end_year = $('#org_end_year').val();

    if (!org_name || org_name == '') {
        $('#txt_help_orgname').css('visibility', 'visible');
        return;
    }

    if (!jabatan || jabatan == '') {
        $('#txt_help_orgjabatan').css('visibility', 'visible');
        return;
    }

    if(org_edit_mode === true) {
        var data = orgs[org_active_id];
        $('#org_title_'+org_active_id).text(org_name);
        $('#org_name_item_'+org_active_id).text(org_name);
        $('#org_jabatan_item_'+org_active_id).text(jabatan);
        $('#org_period_item_'+org_active_id).text(start_month +` `+ start_year +` - `+ end_month +` `+ end_year);

        $('#org_name').val('');
        $('#org_jabatan').val('');

        orgs[org_active_id] = {
            org_name:org_name,
            jabatan:jabatan,
            start_month:start_month,
            start_year:start_year,
            end_month:end_month,
            end_year:end_year,
        };

        org_edit_mode = false;
        return;
    }

    orgs.push({
        org_name:org_name,
        jabatan:jabatan,
        start_month:start_month,
        start_year:start_year,
        end_month:end_month,
        end_year:end_year,
    });
    var cur_id = orgs.length - 1;

    var wrapper = $('#org_experience_wrapper');
    wrapper.append(`
        <div class="card mb-0" id="card_org_`+cur_id+`" data-height>
            <input type="hidden" name="org[`+cur_id+`][org_name]" value="`+ org_name +`">
            <input type="hidden" name="org[`+cur_id+`][jabatan]" value="`+ jabatan +`">
            <input type="hidden" name="org[`+cur_id+`][period_start]" value="`+ start_month +` `+ start_year +`">
            <input type="hidden" name="org[`+cur_id+`][period_end]" value="`+ end_month +` `+ end_year +`">
            <div class="card-header">
                <h4 class="card-title" id="org_title_`+cur_id+`">`+ org_name +`</h4>
                <a class="heading-elements-toggle"><i class="la la-ellipsis font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li>
                            <a data-toggle="collapse" href="#org_`+cur_id+`" role="button" aria-expanded="false" aria-controls="org_`+cur_id+`">
                                <i class="ft-plus"></i>
                            </a>
                        </li>
                        <li>
                            <a data-id="org_`+cur_id+`" data-action="edit-org"><i class="ft-edit"></i></a>
                        </li>
                        <li>
                            <a class="danger" data-id="org_`+cur_id+`" data-action="delete-org" ><i class="ft-x"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="card-content collapse" id="org_`+cur_id+`">
                <div class="card-body">
                    <ul class="list-group">
                        <li class="list-group-item" id="org_name_item_`+cur_id+`">`+ org_name +`</li>
                        <li class="list-group-item" id="org_jabatan_item_`+cur_id+`">`+ jabatan +`</li>
                        <li class="list-group-item" id="org_period_item_`+cur_id+`">`+ start_month +` `+ start_year +` - `+ end_month +` `+ end_year +`</li>
                    </ul>
                </div>
            </div>
        </div>
    `);

    $('#org_name').val('');
    $('#org_jabatan').val('');
});//End tambah org

$(document).on('click', 'a[data-action=edit-org]', function() {
    org_edit_mode = true;
    var id = $(this).data('id').split('_');
    org_active_id = parseInt(id[1]);
    var data = orgs[org_active_id];
    $('#org_name').val(data.org_name);
    $('#org_jabatan').val(data.jabatan);
    $('#org_start_month').val(data.start_month);
    $('#org_start_year').val(data.start_year);
    $('#org_end_month').val(data.end_month);
    $('#org_end_year').val(data.end_year);
});

$(document).on('click', 'a[data-action=delete-org]', function() {
    if(org_edit_mode) {
        return;
    }
    var id = $(this).data('id').split('_');
    org_active_id = parseInt(id[1]);
    orgs.splice(org_active_id, 1);
    $('#card_org_'+ org_active_id).remove();
});


/**
 * Stepper Block
 */

var current_view = 1;
var total_view = 2;
$(document).on('click', '[data-action=next-view]', function() {
    var required = $('#f_apply').find('[required]');
    var validated = 0;
    for(var i=0;i<required.length;i++) {
        if($(required[i]).is(':visible')) {
            if(!$(required[i]).val() || $(required[i]).val() == '') {
                validated += 1;
                $('small#txt_help-'+ $(required[i]).attr('name')).remove();
                $([required[i]]).after(`
                <small id="txt_help-`+ $(required[i]).attr('name') +`" class="form-text text-danger" style="font-size:10px;visibility:visible;">
                    This field is required
                </small>
                `);
            } else {
                $('small#txt_help-'+ $(required[i]).attr('name')).css('visibility', 'hidden');
            }
        }
    }

    if(validated == 0) {
        if(current_view != total_view) {
            $('#step_view_'+current_view).hide("slide", { direction: "left" }, 500, function() {
                current_view++;
                $('#step_view_'+current_view).show("slide", { direction: "right" }, 250);
                $('#step_view_'+current_view).css("display", 'flex');
            });
        } else {
            $('#f_apply').submit();
        }
    }
});

$(document).on('click', '[data-action=back-view]', function() {
    $('#step_view_'+current_view).hide("slide", { direction: "left" }, 250, function() {
        current_view--;
        $('#step_view_'+current_view).show("slide", { direction: "right" }, 250);
        $('#step_view_'+current_view).css("display", 'flex');
    });
});
