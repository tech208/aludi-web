<?php $this->load->view('core_home/header_info') ?>
    <!-- ==================================================
                        welcome-area
    ================================================== -->
    <section class="blog-intro" data-scroll-index="1">
        <div class="overlay-bg-75 sec-padding flex-center">
            <div class="container">
                <div>
                    <h1 class="mb-20px color-fff fw-400">Info Terbaru</h1>
                    <a href="<?=site_url('')?>" class="color-fff color-orange-hvr fs-25">Home</a> <span class="color-aaa fs-18">/ </span>
                    <span class="color-aaa fs-18">Info Terbaru</span>
                </div>
                
            </div>
        </div>
    </section>
    <!-- ==================================================
                        End welcome-area
    ================================================== -->

   
    <!-- ==================================================
                        intro-sec
    ================================================== -->
    <section class="intro-sec sec-padding text-center">
        <div class="container">
            <div class="card  wow fadeInUp" data-wow-delay="0.3s">
                <div class="card-header">
                    <h3>Headlines</h3>
                </div>
                <div class="card-body">
                    
                    <div class="row">
                        <div class="card-deck">
                            <?php
                                foreach ($artikel as $key => $value) :
                                    if (@$value->grup=="Headlines"):
                            ?>
                            <div class="card  wow fadeInUp" data-wow-delay="0.3s">
                                <div class="card-body">
                                    <div class="row"style="margin:auto; text-align:center; display:block;"><a  href="<?=site_url('core_home/info_terbaru_detail/'.$value->id)?>" title="<?=@$value->name?>">
                                                <h6 class="card-title"><?=$value->name?></h6>
                                            </a>
                                            <?php if (!empty(@$value->lampiran) AND file_exists("uploads/artikel/".@$value->lampiran)) :?>
											
											<object width="1000" height="900" type="application/pdf" data="<?=base_url("uploads/artikel/".@$value->lampiran)?>?#zoom=85&scrollbar=0&toolbar=0&navpanes=0">
</object>
<!--<embed src="<?=base_url("uploads/artikel/".@$value->lampiran)?>" />-->
											<!--<iframe src="<?=base_url("uploads/artikel/".@$value->lampiran)?>" style="width: 1000px;height: 900px;border: none;"></iframe>-->
                                            <?php else: ?>
                                                <img src="<?=base_url("app-assets-depan/images/cloud-download.png")?>" class="card-img-top" alt="image">
                                            <?php endif; ?>
                                                                           </div>
                                </div>
                                <div class="card-footer">
                                <small class="text-muted"><?=MasTanggal($value->created_at,1)?></small>
                                </div>
                            </div> 
                            <hr>
                            <?php
                                    endif;
                                endforeach;
                            ?>
                        </div>
                    </div>

                </div>
            </div>
            
            <br>
            <br>
            <hr>
            <br>
            <div class="row">
                <div class="col-md-4" >
                    
                    <div class="card  wow fadeInUp" data-wow-delay="0.3s">
                        <div class="card-header">
                            <h5>Informasi Terbaru</h5>
                        </div>
                        <div class="card-body">
                            <?php
                                foreach ($artikel as $key => $value) :
                                    if (@$value->grup!="Headlines" AND @$value->grup!="Penting"):
                            ?>
                            <div class="card  wow fadeInUp" data-wow-delay="0.3s">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <?php if (!empty(@$value->lampiran) AND file_exists("uploads/artikel/".@$value->lampiran)) :?>
                                                <a href="<?=base_url("uploads/artikel/".@$value->lampiran)?>" target="_blank" title="download">
                                                    <img src="<?=base_url("app-assets-depan/images/cloud-download.png")?>" class="card-img-top" alt="image">
                                                </a>
                                            <?php else: ?>
                                                <img src="<?=base_url("app-assets-depan/images/cloud-download.png")?>" class="card-img-top" alt="image">
                                            <?php endif; ?>
                                        </div>
                                        <div class="col-md-10">
                                            <a href="<?=site_url('core_home/info_terbaru_detail/'.$value->id)?>" title="<?=@$value->name?>">
                                                <h6 class="card-title"><?=$value->name?></h6>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                <small class="text-muted"><?=MasTanggal($value->created_at,1)?></small>
                                </div>
                            </div>
                            <hr>
                            <?php
                                    endif;
                                endforeach;
                            ?>
                        </div>
                    </div>
                    
                    
                </div>
				<div class="col-md-4" >
                    
                    <div class="card  wow fadeInUp" data-wow-delay="0.3s">
                        <div class="card-header">
                            <h5>Berita</h5>
                        </div>
                        <div class="card-body">
                            <?php
                                foreach ($berita as $key => $value) :
                                    
                            ?>
                            <div class="card  wow fadeInUp" data-wow-delay="0.3s">
                                <div class="card-body">
                                    <div class="row">
                                        
                                        <div class="col-md-10">
                                            <a href="<?=site_url('core_home/info_terbaru_detail2/'.$value->id)?>" title="<?=@$value->name?>">
                                                <h6 class="card-title"><?=$value->name?></h6>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                <small class="text-muted"><?=MasTanggal($value->created_at,1)?></small>
                                </div>
                            </div>
                            <hr>
                            <?php
                                endforeach;
                            ?>
                        </div>
                    </div>
                    
                    
                </div>
                <div class="col-md-4" >
                    
                    <div class="card  wow fadeInUp" data-wow-delay="0.3s">
                        <div class="card-header">
                            <h5>Informasi Penting</h5>
                        </div>
                        <div class="card-body">
                            <?php
                                foreach ($artikel as $key => $value) :
                                    if (@$value->grup=="Penting"):
                            ?>
                            <div class="card  wow fadeInUp" data-wow-delay="0.3s">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <?php if (!empty(@$value->lampiran) AND file_exists("uploads/artikel/".@$value->lampiran)) :?>
                                                <a href="<?=base_url("uploads/artikel/".@$value->lampiran)?>" target="_blank" title="download">
                                                    <img src="<?=base_url("app-assets-depan/images/cloud-download.png")?>" class="card-img-top" alt="image">
                                                </a>
                                            <?php else: ?>
                                                <img src="<?=base_url("app-assets-depan/images/cloud-download.png")?>" class="card-img-top" alt="image">
                                            <?php endif; ?>
                                        </div>
                                        <div class="col-md-10">
                                            <a href="<?=site_url('core_home/info_terbaru_detail/'.$value->id)?>" title="<?=@$value->name?>">
                                                <h6 class="card-title"><?=$value->name?></h6>                    
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                <small class="text-muted"><?=MasTanggal($value->created_at,1)?></small>
                                </div>
                            </div>
                            <hr>
                            <?php
                                    endif;
                                endforeach;
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
            

            </div>
        </div>
    </section>
    <!-- ==================================================
                        End intro-sec
    ================================================== -->


<?php $this->load->view('core_home/footer') ?>