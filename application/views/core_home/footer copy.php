
    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>

    <!-- BEGIN: Footer-->
    <footer class="footer footer-static footer-light navbar-shadow">
        <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-12"><span class="float-md-center d-block d-md-inline-block">Copyright &copy; <?=date('Y')?> <a class="text-bold-800 grey darken-2" href="<?= site_url('/') ?>" >ALUDI</a></span></p>
    </footer>
    <!-- END: Footer-->


    <!-- BEGIN: Vendor JS-->
    <script src="<?= base_url('/')?>app-assets/vendors/js/vendors.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="<?= base_url('/')?>app-assets/vendors/js/ui/jquery.sticky.js"></script>
    <script src="<?= base_url('/')?>app-assets/vendors/js/charts/jquery.sparkline.min.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="<?= base_url('/')?>app-assets/js/core/app-menu.js"></script>
    <script src="<?= base_url('/')?>app-assets/js/core/app.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="<?= base_url('/')?>app-assets/js/scripts/ui/breadcrumbs-with-stats.js"></script>
    <!-- END: Page JS-->

</body>
<!-- END: Body-->


    <!-- Global alert -->
    <?php if(flashdata('success')): ?>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
        <script>
            Swal.fire({
                title: 'Berhasil',
                text: '<?= flashdata('success') ?>',
                icon: 'success',
                confirmButtonText: 'Ok'
            });
        </script>
    <?php endif ?>

    <?php if(flashdata('error')): ?>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
        <script>
            Swal.fire({
                title: 'Gagal',
                text: '<?= flashdata('error') ?>',
                icon: 'error',
                confirmButtonText: 'Ok'
            });
        </script>
    <?php endif ?>
    
<script src="<?= base_url('/')?>app-assets/js/career.js"></script>
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js"></script>
<script>
    $(document).ready(function() {
        $('#modal-sccs').modal('<?= flashdata("success") ? "show":"hide" ?>');
    });

    $('#tgl_lahir').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        zIndexOffset: 1500,
        uiLibrary: 'bootstrap4',
        icons: {
            rightIcon: false
        },
        modal: false, header: true, footer: true
    });
</script>

</html>