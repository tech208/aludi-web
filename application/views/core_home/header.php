<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="deskripsi">
    <meta name="keywords" content="Platform Investasi">
    <meta name="author" content="ALUDI">
    <link rel="apple-touch-icon" href="<?= base_url() ?>app-assets/images/aludi/logo.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?= base_url() ?>app-assets/images/aludi/logo.png">
	<title>ALUDI - Asosiasi Layanan Urun Dana Indonesia</title>
	  <!-- Favicon -->
		<link rel="shortcut icon" href="images/fav.jpg" />
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?= base_url()?>app-assets-depan/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url()?>app-assets-depan/css/et-line-icons.css"/>
    <link rel="stylesheet" href="<?= base_url()?>app-assets-depan/css/font-awesome.min.css"/>
	<link rel="stylesheet" href="<?= base_url()?>app-assets-depan/css/animated-headline.css"/>
    <link rel="stylesheet" href="<?= base_url()?>app-assets-depan/css/lightbox.css"/>
    <link rel="stylesheet" href="<?= base_url()?>app-assets-depan/css/animate.css"/>
    <link rel="stylesheet" href="<?= base_url()?>app-assets-depan/css/owl.carousel.min.css"/>
    <link rel="stylesheet" href="<?= base_url()?>app-assets-depan/css/owl.theme.default.min.css"/>
	<link rel="stylesheet" href="<?= base_url()?>app-assets-depan/css/main.css">
	<link rel="stylesheet" href="<?= base_url()?>app-assets-depan/css/responsive.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700,800,900%7cPoppins:100,200,300,400,500,600,700,800,900" rel="stylesheet">
  </head>
  <body>
        <!-- ==================================================
                            load-wrapp
        ================================================== -->
        <div class="load-wrapp">
            <div class="wrap">
                <ul class="dots-box">
                <li class="dot"><span></span></li>
                <li class="dot"><span></span></li>
                <li class="dot"><span></span></li>
                <li class="dot"><span></span></li>
                <li class="dot"><span></span></li>
                </ul>
            </div>
        </div> 
        <!-- ==================================================
                            End load-wrapp
        ================================================== -->
	  
        <!-- ==================================================
                            navbar
        ================================================== -->
        <nav class="navbar navbar-expand-md">
            <div class="container">
                <a class="navbar-brand color-fff color-orange-hvr fs-25 fw-500 mr-auto ml-auto" href="#">
                    <!-- ALUDI <span class="bg-orange d-inline-block radius-50"></span>  -->
                    <img class="brand-logo" alt="Aludi" style="width: 100px; " src="<?= base_url() ?>app-assets/images/logo/logo_aludi.png">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-navbar" aria-controls="main-navbar" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="fa fa-bars"></span>
                </button>
                <div class="collapse navbar-collapse" id="main-navbar">
                    <ul class="navbar-nav ml-auto">  
                        <li class="nav-item">
                        <a class="nav-link" href="<?=site_url('core_home/index/'.$_SESSION['language'])?>" ><?= $arrlanguage['header_more_1'] ?></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#" data-scroll-nav="2"><?= $arrlanguage['header_more_2'] ?></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#" data-scroll-nav="3"><?= $arrlanguage['header_more_3'] ?></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#" data-scroll-nav="4"><?= $arrlanguage['header_more_4'] ?></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#" data-scroll-nav="5"><?= $arrlanguage['header_more_5'] ?></a>
                        </li>
						<li class="nav-item"> 
							<select id="language" class="form-control" style="width:100px;" OnChange="location.href = '<?= site_url().'/core_home/selengkapnya/' ?>'+$(this).val();">
								<option <?= ($_SESSION['language'] == "ind")?'selected':'';?> value="ind"> BAHASA</option>
								<option <?= ($_SESSION['language'] == "eng")?'selected':'';?> value="eng">ENGLISH</option>
							</select>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- ==================================================
                            End navbar
        ================================================== -->
	  