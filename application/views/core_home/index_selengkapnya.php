<?php $this->load->view('core_home/header') ?>

 <!-- ==================================================
                            welcome-area
================================================== -->
<section class="welcome-area-2 p-relative" data-scroll-index="1">
    <div class="overlay-bg-75 flex-center">
    
        <div class="container">
            <div class="welcome-text">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 mb-2 color-ccc">
                        <h1 class="mb-15px color-fff fw-100 text-center"><?= $arrlanguage['more_of_header'] ?>
                            <span class="fw-600">ALUDI</span>
                        </h1>                    
                        <!-- <?=MasProfile("syarat")->deskripsi?> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="pattern-bottom p-absolute"></div>
</section>
<!-- ==================================================
                    End welcome-area
================================================== -->


 <!-- ==================================================
                    about-area
================================================== -->
<section class="about-area text-center sec-padding" data-scroll-index="2">
    <div class="container">
        <h3 class="mb-25px cd-headline clip">
            <span class="fw-300"><?= $arrlanguage['term_of_service_header'] ?></span>
            <span class="cd-words-wrapper">
                <b class="is-visible fw-600">ALUDI</b>
                <b class="fw-600">ALUDI</b>
                <b class="fw-600">ALUDI</b>
            </span>
        </h3>
        <?= $arrlanguage['term_of_service_content'] ?>
        
    </div>
</section>
<!-- ==================================================
                    End about-area
================================================== -->


<!-- ==================================================
                    skills-area
================================================== -->
<section class="skills-area sec-padding bg-f4f4f4 p-relative pt-150px" data-scroll-index="3">
    <div class="container">
        <h3 class="mb-25px cd-headline clip">
            <span class="fw-300"><?= $arrlanguage['term_of_service_header2'] ?></span>
            <span class="cd-words-wrapper">
                <b class="is-visible fw-600">ALUDI</b>
                <b class="fw-600">ALUDI</b>
                <b class="fw-600">ALUDI</b>
            </span>
        </h3>
        <div class="row">
            <div class="col-md-12">
                <?= $arrlanguage['term_of_service_content2'] ?>
            </div>
        </div>
    </div>
    <div class="pattern-top p-absolute"></div>
</section>
<!-- ==================================================
                    End skills-area
================================================== -->


 <!-- ==================================================
                    about-area
================================================== -->
<section class="about-area text-center sec-padding" data-scroll-index="4">
    <div class="container">
        <h3 class="mb-25px cd-headline clip">
            <span class="fw-300"> <?= $arrlanguage['policy_header'] ?></span>
            <span class="cd-words-wrapper">
                <b class="is-visible fw-600">ALUDI</b>
                <b class="fw-600">ALUDI</b>
                <b class="fw-600">ALUDI</b>
            </span>
        </h3>
        <?= $arrlanguage['policy_content'] ?>
        
    </div>
</section>
<!-- ==================================================
                    End about-area
================================================== -->


<!-- ==================================================
                    skills-area
================================================== -->
<section class="skills-area sec-padding bg-f4f4f4 p-relative pt-150px" data-scroll-index="5">
    <div class="container">
        <h3 class="mb-25px cd-headline clip">
            <span class="fw-300"><?= $arrlanguage['registration_flow_header'] ?></span>
            <span class="cd-words-wrapper">
                <b class="is-visible fw-600">ALUDI</b>
                <b class="fw-600">ALUDI</b>
                <b class="fw-600">ALUDI</b>
            </span>
        </h3>
    </div>
    <div class="pattern-top p-absolute"></div>
    <div class="container">
        <div class="row">
            <div class="col">
                <img class="card-img-top img-fluid" src="<?= base_url() ?>uploads/selengkapnya/langkah1-registrasi.png" alt="Card image cap" />
            </div>
            <div class="col">
                <?= $arrlanguage['registration_flow_step_1'] ?>
                <!--<button type="submit" class="btn btn-primary col-6 ">Registrasi Sekarang</button>-->
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row mt-5">
            <div class="col">
                <?= $arrlanguage['registration_flow_step_2'] ?>
                <!-- <button type="submit" class="btn btn-outline-secondary col-6 ">Isi Formulir</button> -->
            </div>
            <div class="col">
                <img class="card-img-top img-fluid" src="<?= base_url() ?>uploads/selengkapnya/langkah2-tahap1.png" alt="Card image cap" />
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row mt-5">
            <div class="col">
                <img class="card-img-top img-fluid" src="<?= base_url() ?>uploads/selengkapnya/langkah3-pembayaran.png" alt="Card image cap" />
            </div>
            <div class="col">
                <?= $arrlanguage['registration_flow_step_3'] ?>
                <!-- <button type="submit" class="btn btn-outline-secondary col-6 ">Bayar Sekarang</button> -->
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row mt-5">
            <div class="col">
                <?= $arrlanguage['registration_flow_step_4'] ?>
                <!-- <button type="submit" class="btn btn-outline-secondary col-6 ">Isi Formulir</button> -->
            </div>
            <div class="col">
                <img class="card-img-top img-fluid" src="<?= base_url() ?>uploads/selengkapnya/langkah4-tahap2.png" alt="Card image cap" />
            </div>
        </div>
    </div>    
    <div class="container">
        <div class="row mt-5">
            <div class="col">
                <img class="card-img-top img-fluid" src="<?= base_url() ?>uploads/selengkapnya/langkah5-sitevisit.png" alt="Card image cap" />
            </div>
            <div class="col">
                <?= $arrlanguage['registration_flow_step_5'] ?>
                <!-- <button type="submit" class="btn btn-outline-secondary col-6 ">Lihat Status</button> -->
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row mt-5">
            <div class="col">
                <?= $arrlanguage['registration_flow_step_6'] ?>
                <!-- <button type="submit" class="btn btn-outline-secondary col-6 ">Lihat Status</button> -->
            </div>
            <div class="col">
                <img class="card-img-top img-fluid" src="<?= base_url() ?>uploads/selengkapnya/langkah6-rekomendasi.png" alt="Card image cap" />
            </div>
        </div>
    </div>           
</section>

<!-- ==================================================
End skills-area
================================================== -->


<?php $this->load->view('core_home/footer') ?>