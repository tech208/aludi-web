        
        <!-- ==================================================
                            scroll-top-btn
        ================================================== -->
        <div class="scroll-top-btn text-center">
            <i class="fa fa-angle-up fs-20 color-fff bg-333 bg-orange-hvr radius-50"></i>          
        </div>
        <!-- ==================================================
                            End scroll-top-btn
        ================================================== -->
	  
	  

    <!-- Optional JavaScript -->


    <!-- Global alert -->
    <?php if(flashdata('success')): ?>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
        <script>
            Swal.fire({
                title: 'Berhasil',
                text: '<?= flashdata('success') ?>',
                icon: 'success',
                confirmButtonText: 'Ok'
            });
        </script>
    <?php endif ?>

    <?php if(flashdata('error')): ?>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
        <script>
            Swal.fire({
                title: 'Gagal',
                text: '<?= flashdata('error') ?>',
                icon: 'error',
                confirmButtonText: 'Ok'
            });
        </script>
    <?php endif ?>

    <!-- google captcha -->
    <script src='https://www.google.com/recaptcha/api.js'></script>

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="<?= base_url()?>app-assets-depan/js/jquery-3.3.1.min.js"></script>
    <script src="<?= base_url()?>app-assets-depan/js/jquery-migrate-3.0.0.min.js"></script>
    <script src="<?= base_url()?>app-assets-depan/js/popper.min.js"></script>
    <script src="<?= base_url()?>app-assets-depan/js/bootstrap.min.js"></script>
	<script src="<?= base_url()?>app-assets-depan/js/jquery.counterup.min.js"></script>
	<script src="<?= base_url()?>app-assets-depan/js/jquery.waypoints.min.js"></script>
	<script src="<?= base_url()?>app-assets-depan/js/lightbox.js"></script>
	<script src="<?= base_url()?>app-assets-depan/js/scrollIt.min.js"></script>
	<script src="<?= base_url()?>app-assets-depan/js/owl.carousel.min.js"></script>
	<script src="<?= base_url()?>app-assets-depan/js/animated-headline.js"></script>
	<script src="<?= base_url()?>app-assets-depan/js/validator.js"></script>
	<script src="<?= base_url()?>app-assets-depan/js/isotope.pkgd.min.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBn-q0VMtMOum5A7HVG86duHeJApbVDv7o"></script>
  	<script src="<?= base_url()?>app-assets-depan/js/map-white.js"></script>
	<script src="<?= base_url()?>app-assets-depan/js/main.js"></script>
	<script src="<?= base_url()?>app-assets-depan/js/wow.min.js"></script>
	  
	  
  </body>
</html>