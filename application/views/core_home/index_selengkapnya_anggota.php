<?php $this->load->view('core_home/header_info') ?>
    <!-- ==================================================
                        welcome-area
    ================================================== -->
    <section class="blog-intro" data-scroll-index="1">
        <div class="overlay-bg-75 sec-padding flex-center">
            <div class="container">
                <div>
                    <h1 class="mb-20px color-fff fw-400">Selengkapnya Anggota</h1>
                    <a href="<?=site_url('')?>" class="color-fff color-orange-hvr fs-25">Home</a> <span class="color-aaa fs-18">/ </span>
                    <span class="color-aaa fs-18">Info Terbaru</span>
                </div>
            </div>
        </div>
    </section>
    <!-- ==================================================
                        End welcome-area
    ================================================== -->
    <style>
        @media (max-width: 767px) {
            .column-1,
            .column-2 {
                width: 100%;
            }
        }
        @media (min-width: 768px) and (max-width: 991px) {
            .column-1 {
                width: 50%;
            }
            .column-2 {
                width: 100%;
            }
        }
        @media (min-width: 992px) {
            .column-2,
            .column-1 {
                width: 33%;
            }
        }

        .c {
          white-space: nowrap;
        }


    </style>

    <section class="intro-sec sec-padding text-center">
        <h4 class="card-title white-text">Anggota Berizin</h4>
        <!-- Grid row -->
        <div class="containera px-5" >
            <div class="row " >

                <!-- di foreach -->
                <?php foreach(@$anggota as $val): ?>
                    <?php if($val->tipe == "berizin"): ?>
                        <div class="col-md-3 amx-auto mt-4 px-4 mb-r wow fadeInUp" data-wow-delay="0.3s">
                            <div class="card default-color-dark">
                                <div class="card-header  text-center">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <i class="fas fa-check-circle text-success"></i>
                                            <span >Berizin OJK</span>
                                        </div>
                                        <div class="col-md-6">
                                            <i class="fas fa-check-circle text-success"></i>
                                            <span >Terverifikasi</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body text-center">
                                    <img src="<?=base_url("uploads/anggota/".$val->logo)?>" alt="image" >
                                    <h5 class="title white-text c"><?= (!empty(@$val->nama)) ? @$val->nama : "&nbsp;" ;  ?></h5>
                                    <p class="title white-text c"><?= (!empty(@$val->nama_perusahaan)) ? @$val->nama_perusahaan : "&nbsp;" ; ?></p>
                                    <a href="<?= @$val->url_site?>" target="_blank"> <button class="btn"><i class="fas fa-globe"></i> Kunjungi Website</button></a>
                                </div>
                                <div class="card-footer c">
                                    <a href="https://www.instagram.com/<?= @$val->instagram?>" target="_blank"><img src="https://img.icons8.com/cute-clipart/32/000000/instagram-new.png"/> @<?= @$val->instagram ?></a>
                                </div>
                            </div>
                        </div>
                    <?php endif ?>
                <?php  endforeach ?>
                <!-- end di foreach -->

            </div>
        </div>
        <!-- Grid row -->
    </section>

    <hr>
    <section class="intro-sec sec-padding text-center">
        <h4 class="card-title white-text">Anggota Lainnya</h4>
        <!-- Grid row -->
        <div class="containera px-5" >
            <div class="row" >


                <!-- Grid column -->
                <?php foreach(@$anggota as $val): ?>
                    <?php if($val->tipe == "lainya"): ?>
                        <div class="col-md-3 amx-auto mt-4 px-4 mb-r wow fadeInUp" data-wow-delay="0.3s">
                            <div class="card default-color-dark">
                                <div class="card-header  text-center">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <i class="fas fa-check-circle text-success"></i>
                                            <span >Terverifikasi</span>
                                        </div>
                                    </div>
                                </div>
								<?php if($val->id == "39"){ ?>
									<div class="card-body text-center">
										<br>
										<br>
										<div style="height:70px;">
										<img src="<?=base_url("uploads/anggota/".$val->logo)?>"  alt="image" style="max-height:120px;">
										</div>
										<h5 class="title white-text c"><?= (!empty(@$val->nama)) ? @$val->nama : "&nbsp;" ;  ?></h5>
										<p class="title white-text c"><?= (!empty(@$val->nama_perusahaan)) ? @$val->nama_perusahaan : "&nbsp;" ; ?></p>
										<a href="<?= @$val->url_site?>" target="_blank"> <button class="btn"><i class="fas fa-globe"></i> Kunjungi Website</button></a>
									</div>
								<?php }else{ ?>
									<div class="card-body text-center">
										<div style="height:120px;">
										<img src="<?=base_url("uploads/anggota/".$val->logo)?>"  alt="image" style="max-height:120px;">
										</div>
										<h5 class="title white-text c"><?= (!empty(@$val->nama)) ? @$val->nama : "&nbsp;" ;  ?></h5>
										<p class="title white-text c"><?= (!empty(@$val->nama_perusahaan)) ? @$val->nama_perusahaan : "&nbsp;" ; ?></p>
										<a href="<?= @$val->url_site?>" target="_blank"> <button class="btn"><i class="fas fa-globe"></i> Kunjungi Website</button></a>
									</div>
								<?php } ?>
                                <div class="card-footer c">
                                    <a href="https://www.instagram.com/<?= @$val->instagram?>" target="_blank"><img src="https://img.icons8.com/cute-clipart/32/000000/instagram-new.png"/> @<?= @$val->instagram ?></a>
                                </div>
                            </div>
                        </div>
                    <?php endif ?>
                <?php  endforeach ?>

                <!-- Grid column -->


            </div>
        </div>
        <!-- Grid row -->
    </section>



<?php $this->load->view('core_home/footer') ?>