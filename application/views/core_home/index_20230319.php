<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="deskripsi">
    <meta name="keywords" content="Platform Investasi">
    <meta name="author" content="ALUDI">
    <link rel="apple-touch-icon" href="<?= base_url() ?>app-assets/images/aludi/logo.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?= base_url() ?>app-assets/images/aludi/logo.png">
	<title>ALUDI - Asosiasi Layanan Urun Dana Indonesia</title>
	  <!-- Favicon -->
		<link rel="shortcut icon" href="images/fav.jpg" />
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?= base_url()?>app-assets-depan/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url()?>app-assets-depan/css/et-line-icons.css"/>
    <link rel="stylesheet" href="<?= base_url()?>app-assets-depan/css/font-awesome.min.css"/>
	<link rel="stylesheet" href="<?= base_url()?>app-assets-depan/css/animated-headline.css"/>
    <link rel="stylesheet" href="<?= base_url()?>app-assets-depan/css/lightbox.css"/>
    <link rel="stylesheet" href="<?= base_url()?>app-assets-depan/css/animate.css"/>
    <link rel="stylesheet" href="<?= base_url()?>app-assets-depan/css/owl.carousel.min.css"/>
    <link rel="stylesheet" href="<?= base_url()?>app-assets-depan/css/owl.theme.default.min.css"/>
	<link rel="stylesheet" href="<?= base_url()?>app-assets-depan/css/main.css">
	<link rel="stylesheet" href="<?= base_url()?>app-assets-depan/css/responsive.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700,800,900%7cPoppins:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <!-- <link href='https://fonts.googleapis.com/css?family=Nunito Sans' rel='stylesheet'> -->
  </head>
  <body style="font-family: 'Montserrat';">
        <!-- ==================================================
                            load-wrapp
        ================================================== -->
        <div class="load-wrapp">
            <div class="wrap">
                <ul class="dots-box">
                <li class="dot"><span></span></li>
                <li class="dot"><span></span></li>
                <li class="dot"><span></span></li>
                <li class="dot"><span></span></li>
                <li class="dot"><span></span></li>
                </ul>
            </div>
        </div> 
        <!-- ==================================================
                            End load-wrapp
        ================================================== -->
	  
        <!-- ==================================================
                            navbar
        ================================================== -->
        <nav class="navbar navbar-expand-md">
            <div class="container">
                <a class="navbar-brand color-fff color-orange-hvr fs-25 fw-500 mr-auto ml-auto" href="#">
                    <!-- ALUDI <span class="bg-orange d-inline-block radius-50"></span>  -->
                    <img class="brand-logo" alt="Aludi" style="width: 100px; " src="<?= base_url() ?>app-assets/images/logo/logo_aludi.png">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-navbar" aria-controls="main-navbar" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="fa fa-bars"></span>
                </button>
                <div class="collapse navbar-collapse" id="main-navbar">
                    <ul class="navbar-nav ml-auto">  
                        <li class="nav-item">
                        <a class="nav-link" href="" data-scroll-nav="home"><?= $arrlanguage['header_1'] ?></a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="#"  data-scroll-nav="2"><?= $arrlanguage['header_2'] ?></a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="#"  data-scroll-nav="6"><?= $arrlanguage['header_3'] ?></a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="<?= site_url("core_home/selengkapnya_anggota") ?>" ><?= $arrlanguage['header_4'] ?></a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="<?= site_url("core_home/info_terbaru") ?>" ><?= $arrlanguage['header_5'] ?></a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="#" data-scroll-nav="5"><?= $arrlanguage['header_6'] ?></a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="#" data-scroll-nav="3"><?= $arrlanguage['header_7'] ?></a>
                        </li>
						<li class="nav-item"> 
							<select id="language" class="form-control" style="width:100px;" OnChange="location.href = '<?= site_url().'/core_home/index/' ?>'+$(this).val();">
								<option <?= ($_SESSION['language'] == "ind")?'selected':'';?> value="ind"> BAHASA</option>
								<option <?= ($_SESSION['language'] == "eng")?'selected':'';?> value="eng">ENGLISH</option>
							</select>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- ==================================================
                            End navbar
        ================================================== -->
        <style>	  
            .size-logo{
                width:175px ;
                height:75px;
            }
            .cls_name{
            flex-wrap: nowrap;
            }
            .custom{
                width:160px;
                height:50px;
                border-radius: 30px;
                /* box-shadow: 2px 2px 4px #000000; */
                position: fixed;
                /* position: relative ; */
                right: 15px;
                top:100px;
                z-index: 10 !important;
            }

            .mail{
                position:absolute;
                left:15px;
                top:10px;
                font-size:30px;
                /* font-weight: bold; */
                color:rgb(68, 156, 170);
            }

            .tul {
                line-height: 1.2;
                color: black;
                font-size: 9px;
                font-weight: bold;
                text-align: left;
                position:absolute;
                left:35px;
                top:5px;
            }
            #owl-demo .item img{
                display: block;
                width: 100%;
                height: auto;
            }

        </style>
        
        
        <button href="#" class="btn custom" data-scroll-nav="5" title="">
            <i class="fa fa-envelope-o mail" aria-hidden="true">
                <h5 class="tul">
                    <?= $arrlanguage['box_right_1'] ?>
                </h5>
            </i>            
        </button>


        <script>
            $(document).ready(function() {
                $("#owl-demo").owlCarousel({

                    navigation : true, // Show next and prev buttons
                    slideSpeed : 300,
                    paginationSpeed : 400,
                    singleItem:true

                });
            });
        </script>
        
        <!-- ==================================================
                            banner
        ================================================== -->
        <section class="testimonials text-center">
            <div class="overlay-bg-75 sec-padding" >
                <div class="container">
                    <div id="owl-demo" class="owl-carousel owl-theme">
                        <?php
                            if (!empty($banner)) :
                                foreach ($banner as $key => $value) :
								if($value['jenis'] == "1"){
                        ?>
                            <div class="item"><img src="<?= base_url('uploads/banner_lampiran/'.$value['lampiran'])?>" alt="The Last of us"></div>
                        <?php
								}else if($value['jenis'] == "2"){
									if(@$value['url'] != ""){
						?>
                            <div class="item"><iframe width="1000" height="515" src="https://www.youtube.com/embed/<?= $value['url']?>?start=2&loop=1&playlist=<?= $value['url']?>" frameborder="0" allowfullscreen></iframe></div>
                        <?php
									}else{
						?>
							<div class="item"><video width="1000" height="515" controls>
  <source src="<?= base_url('uploads/banner_lampiran/'.$value['lampiran'])?>" type="video/mp4">
  Error Message
</video></div>
						<?php
									}
								}
                                endforeach;
                            endif;
                        ?>
                    </div>
                </div>
            </div>
        </section>
        <!-- ==================================================
                            End banner
        ================================================== -->


        <!-- ==================================================
                            welcome-area
        ================================================== -->
        <section class="welcome-area-1 p-relative " data-scroll-index="1" style="margin-top: -100px;">
            <div class="overlay-bg-75 " style="background-image: linear-gradient(#068094, #4BA489);">
                <div class="container ">
                        <div class="row ">
                            <div class="col-xl-6 col-lg-12 mb-2 color-ccc">
                                <h1 class="mb-15px color-fff fw-100"><?= $arrlanguage['about_us_header'] ?> <span class="fw-600">ALUDI</span></h1>
                                <div class="color-fff">
                                    <?= $arrlanguage['about_us_content'] ?>
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-12">
                                <img class="card-img-top img-fluid"  src="<?= base_url() ?>app-assets/images/aludi/logo-putih.png" alt="Card image cap" />
                            </div>
                        </div>
                                    <p>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        &nbsp;
                                    </p>
                </div>
            </div>
            <div class="pattern-bottom p-absolute"></div>
        </section>
        <!-- ==================================================
                            End welcome-area
        ================================================== -->
	  
        <!-- ==================================================
                            about-area
        ================================================== -->
        <section class="about-area text-center  sec-padding" data-scroll-index="2">
            <div class="container">
                <h3 class="mb-25px cd-headline clip">
                    <span class="fw-300"><?= $arrlanguage['vision_header'] ?></span>
                    <span class="cd-words-wrapper">
                        <b class="is-visible fw-600">ALUDI</b>
                        <b class="fw-600">ALUDI</b>
                        <b class="fw-600">ALUDI</b>
                    </span>
                </h3>
                <?= $arrlanguage['vision_content'] ?>

            </div>
        </section>
        <!-- ==================================================
                            End about-area
        ================================================== -->
	  
        <!-- ==================================================
                            skills-area
        ================================================== -->
        <section class="skills-area sec-padding  p-relative pt-150px">
            <div class="container">
                <h3 class="mb-25px cd-headline clip text-center">
                    <span class="fw-300 text-center"><?= $arrlanguage['mission_header'] ?></span>
                    <span class="cd-words-wrapper">
                        <b class="is-visible fw-600">ALUDI</b>
                        <b class="fw-600">ALUDI</b>
                        <b class="fw-600">ALUDI</b>
                    </span>
                </h3>
                <div class="row">
                    <div class="col-xl-6 col-lg-12">
                        <?= $arrlanguage['mission_content'] ?>
                    </div>
                </div>
            </div>
            <div class="pattern-top p-absolute"></div>
        </section>
        <!-- ==================================================
							End skills-area
        ================================================== -->
	  
        <!-- ==================================================
                            experience-area
        ================================================== -->
        <section class="experience-area p-relative">
            <div class="overlay-bg-75 sec-padding pt-150px pb-120px"  style="background-image: linear-gradient(#068094, #4BA489);">
                <div class="container">
                    <h1 class="title-h p-relative color-fff">
                        <span class="fw-200 color-fff"><?= $arrlanguage['member_header'] ?></span> ALUDI
                        <span class="line p-absolute bg-orange"></span>
                    </h1>
                    <div class="color-fff">
                        <?= $arrlanguage['member_content'] ?>
                    </div>
                </div>
            </div>
            <div class="pattern-top  p-absolute"></div>
            <div class="pattern-bottom p-absolute"></div>
        </section>
        <!-- ==================================================
                            End experience-area
        ================================================== -->
	    
        <!-- ==================================================
                            services-area
        ================================================== -->
        <section class="contact-area sec-padding text-center" data-scroll-index="3">
            <div class="container">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            
                            <h1 class="title-h p-relative">
                                <span class="fw-200"><?= $arrlanguage['sign_up_header'] ?> </span> 
                                <span class="line p-absolute bg-orange"></span>
                            </h1>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-xl-6 col-lg-12 mb-2">
                                        <img class="card-img-top img-fluid" src="<?= base_url() ?>app-assets/images/aludi/logo.png" alt="Card image cap" />
                                    </div>
                                    <div class="col-xl-6 col-lg-12">
                                        <form enctype="multipart/form-data" method="POST" action="<?= site_url('po_pendaftaran/save') ?>" class="form form-horizontal" required>
                                            <div class="form-group">
                                                <!-- <label for="exampleInputEmail1">Nama Lengkap</label> -->
                                                <input type="text" class="form-control" id="nama" name="nama" placeholder="<?= $arrlanguage['place_holder_form_sign_up_1'] ?>" required>
                                            </div>
                                            <div class="form-group">
                                                <!-- <label for="exampleInputEmail1">Email</label> -->
                                                <input type="email" class="form-control" id="email" name="email"  placeholder="<?= $arrlanguage['place_holder_form_sign_up_2'] ?>" required>
                                            </div>
                                            <div class="form-group">
                                                <!-- <label for="exampleInputEmail1">Nomor Handphone</label> -->
                                                <input type="nomor" class="form-control" id="nomor_handphone" name="nomor_handphone"  placeholder="<?= $arrlanguage['place_holder_form_sign_up_3'] ?>" required>
                                            </div>
                                            <br>
                                            <!-- <div class="form-group">
                                                <input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
                                            </div> -->
                                            <div class="input-group">
                                                <input type="password" name="password" id="user_password" class="form-control" placeholder="<?= $arrlanguage['place_holder_form_sign_up_4'] ?>" required data-toggle="password">
                                                <div class="input-group-append">
                                                <span class="input-group-text"><i class="fa fa-eye"></i></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <!-- <label for="exampleInputPassword1">Konfirmasi Password</label> -->
                                                <input type="password" class="form-control" id="password_ulangi" name="password_ulangi" placeholder="<?= $arrlanguage['place_holder_form_sign_up_5'] ?>" required>
                                            </div>
                                            <hr>
                                            <div class="form-group form-check">
                                                <input type="checkbox" class="form-check-input" id="setuju" name="setuju" value="1" required>
                                                <label class="form-check-label" for="exampleCheck1"><?= $arrlanguage['sign_up_terms_condition'] ?></label>
                                            </div>
                                            
                                            <br>
                                            <div class="form-group px-1">
                                                <div class="g-recaptcha " data-sitekey="<?php echo $this->config->item('google_key') ?>"></div> 
                                            </div>
                                            <hr>
                                            <div class="row px-1">
                                                <button type="submit" class="btn btn-primary col-12 "><?= $arrlanguage['button_sign_up'] ?></button>
                                            </div>
                                        </form>
                                        <br>
                                        <div class="border-top card-body text-center"><?= $arrlanguage['text_sign_up_1'] ?> <a href="<?= site_url("auth/login")?>">Log In</a></div>
                                    </div>
                                    <div class="card-body text-center"> <a href="<?= site_url("core_home/selengkapnya/".$_SESSION['language']) ?>" target="_blank"><?= $arrlanguage['text_sign_up_2'] ?></a></div>
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>   
        </section>
        <!-- ==================================================
                            End services-area
        ================================================== -->
        
        <!-- ==================================================
                            offer-area
        ================================================== -->
        <section class="offer-area text-center p-relative" data-scroll-index="5">
            <div class="overlay-bg-75 sec-padding pt-150px pb-120px" style="background-image: linear-gradient(#068094, #4BA489);">
                <div class="container">
                    

                <h1 class="title-h p-relative" style="color:white  !important;">
                    <?= $arrlanguage['complaint_header'] ?>
                    <span class="line p-absolute bg-orange"></span>
                </h1>
                <p class="title-p color-fff"><?= $arrlanguage['complaint_text_1'] ?></p>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <form enctype="multipart/form-data" method="POST" action="<?= site_url('po_pendaftaran/save_aduan_aludi') ?>" class="form form-horizontal" required>
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="nama" name="nama" placeholder="<?= $arrlanguage['place_holder_form_complaint_1'] ?>" required>
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <input type="text" class="form-control" id="no_kontak" name="no_kontak"  placeholder="<?= $arrlanguage['place_holder_form_complaint_2'] ?>" required>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <input type="email" class="form-control" id="email" name="email"  placeholder="<?= $arrlanguage['place_holder_form_complaint_3'] ?>" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <select class="form-control" id="id_nama_platform" name="id_nama_platform" required>
                                                        <option >--<?= $arrlanguage['place_holder_form_complaint_4'] ?>--</option>
                                                        <?php
                                                            foreach ($nama_platform as $key => $value):
                                                        ?>
                                                        <option value="<?=$value['nama']?>"><?=$value['nama']?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <textarea class="form-control" id="kronologis" name="kronologis"  placeholder="<?= $arrlanguage['place_holder_form_complaint_5'] ?>" required style="height: 150px;" ></textarea>
                                                </div>
												
                                                <div class="form-group">
													<!--<label for="email">adas</label>-->
                                                    <input type="file" class="form-control" id="lampiran" name="lampiran"  placeholder="<?= $arrlanguage['place_holder_form_complaint_6'] ?>" required>
                                                </div>
                                                <div class="form-group px-1">
                                                    <div class="g-recaptcha " data-sitekey="<?php echo $this->config->item('google_key') ?>"></div> 
                                                </div>
                                                <hr>
                                                <div class="row px-1">
                                                    <button type="reset" class="btn  col-2 "><?= $arrlanguage['button_complaint_1'] ?></button>
                                                    <button type="submit" class="btn btn-primary col-2 "><?= $arrlanguage['button_complaint_2'] ?></button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                </div>
            </div>
            <div class="pattern-top p-absolute"></div>
            <div class="pattern-bottom p-absolute"></div>
        </section>
        <!-- ==================================================
                            End offer-area
        ================================================== -->
        
        
        <!-- ==================================================
                            brands-area
        ================================================== -->
        <section class="brands-area text-center sec-padding p-relative pt-150px pb-120px " data-scroll-index="6">
            <div class="container">
                <h3 class="mb-25px cd-headline clip">
                    <!-- <span class="fw-300">Media </span> -->
                    <span class="cd-words-wrapper">
                        <b class="is-visible fw-600">Partner</b>
                        <b class="fw-600">Partner</b>
                        <b class="fw-600">Partner</b>
                    </span>
                </h3>

                <div class="row">
                    <!-- di foreach -->
                    <?php foreach(@$media as $val_media): ?>
                            <div class="size-logo col-sm-4 col-md-2 mb-25px mt-25px">
                                <img alt="brand" src="<?=base_url("uploads/po_media/".$val_media->logo)?>" width="200" height="110" class="transition-3"> 
                            </div>
                    <?php  endforeach ?>
                    <!-- end di foreach -->
                </div>
            </div>

            <div class="pattern-top p-absolute"></div>
            <div class="pattern-bottom p-absolute"></div>
        </section>
        <!-- ==================================================
                            End brands-area
        ================================================== -->
        

        <!-- ==================================================
                            footer-area
        ================================================== -->
        <div class="footer-area text-center footer-padding bg-f4f4f4">
            <div class="container">
                <div class="row">
                    <div class="col-md-5">
                        <div class="row">
                            <div class="col-md-4">
                                <img class="card-img-top img-fluid" src="<?= base_url() ?>app-assets/images/aludi/logo.png" alt="Card image cap" style="width: 100px;"/>
                            </div>
                            <div class="col-md-8" align="left">
                                <h3>Address</h3>
                                <p>Kantorkuu coworking & office space, citywalk sudirman lantai 2, Jl.KH.Mas Mansyur no.121 RT 10/RW 11, Kel.Karet Tengsin Kec. Tanah Abang, Jakarta Pusat 10220</p>
                            </div>
                        </div>
                    </div>
					<div class="col-md-4">
                        <div class="row">
                            
                            <div class="col-md-12">
                                <h3 align="left">Contact</h3>
                                <p align="left">Phone : <?=MasProfile("wa")->deskripsi?></p>
                                <p align="left">Email : - hello@aludi.id</p>
                                <p align="left">     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - partnership@aludi.id</p>
								
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                            <a href="https://www.youtube.com/channel/UCadSOP7caGU7sjMEY9TqIpg" target="_blank" class="d-inline-block color-333 mr-20px mb-10px color-orange-hvr transition-3"><i class="fa fa-youtube-play"></i></a>
                            <a href="https://www.instagram.com/aludi.id" target="_blank" class="d-inline-block color-333 mr-20px mb-10px color-orange-hvr transition-3"><i class="fa fa-instagram"></i></a>
                            <a href="mailto:hello@aludi.id" target="_blank" class="d-inline-block color-333 mr-20px mb-10px color-orange-hvr transition-3"><i class="fa fa-envelope"></i></a>
                            <a href="https://open.spotify.com/show/47w0HvU0JEeh99k7Whu5Db" target="_blank" class="d-inline-block color-333 mr-20px mb-10px color-orange-hvr transition-3"><i class="fa fa-spotify"></i></a>
                            <a href="https://www.linkedin.com/company/asosiasi-layanan-urun-dana-indonesia/mycompany/" target="_blank" class="d-inline-block color-333 color-orange-hvr transition-3"><i class="fa fa-linkedin"></i></a>
                        <p class="mt-10px mb-0px color-333 fw-500">© <?=date("Y")?> <strong class="color-orange fw-600">Aludi</strong> </p>
                    </div>
                    <!-- <div class="col-md-12">
                    </div> -->
                </div>
            </div>
        </div>
        <!-- ==================================================
                            End footer-area
        ================================================== -->
        
        <!-- ==================================================
                            scroll-top-btn
        ================================================== -->
        <div class="scroll-top-btn text-center">
             <i class="fa fa-angle-up fs-20 color-fff bg-333 bg-orange-hvr radius-50"></i>
        </div>
        <!-- ==================================================
                            End scroll-top-btn
        ================================================== -->
	  
	  

    <!-- Optional JavaScript -->


    <!-- Global alert -->
    <?php if(flashdata('success')): ?>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
        <script>
            Swal.fire({
                title: 'Berhasil',
                text: '<?= flashdata('success') ?>',
                icon: 'success',
                confirmButtonText: 'Ok'
            });
        </script>
    <?php endif ?>

    <script>
        // $(document).ready(function(){
        //     $('#titleee a').trigger('click');
        // });
    </script>
    <?php if(flashdata('error')): ?>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
        <script>
            Swal.fire({
                title: 'Gagal',
                text: '<?= flashdata('error') ?>',
                icon: 'error',
                confirmButtonText: 'Ok'
            });
        </script>
		<script>
		</script>
    <?php endif ?>

    
    <link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script type="text/javascript" src="<?= base_url() ?>app-assets/bootstrap-show-password.js"></script>

    <!-- google captcha -->
    <script src='https://www.google.com/recaptcha/api.js'></script>

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="<?= base_url()?>app-assets-depan/js/jquery-3.6.0.min.js"></script>
    <script src="<?= base_url()?>app-assets-depan/js/jquery-migrate-3.0.0.min.js"></script>
    <script src="<?= base_url()?>app-assets-depan/js/popper.min.js"></script>
    <script src="<?= base_url()?>app-assets-depan/js/bootstrap.min.js"></script>
	<script src="<?= base_url()?>app-assets-depan/js/jquery.counterup.min.js"></script>
	<script src="<?= base_url()?>app-assets-depan/js/jquery.waypoints.min.js"></script>
	<script src="<?= base_url()?>app-assets-depan/js/lightbox.js"></script>
	<script src="<?= base_url()?>app-assets-depan/js/scrollIt.min.js"></script>
	<script src="<?= base_url()?>app-assets-depan/js/owl.carousel.min.js"></script>
	<script src="<?= base_url()?>app-assets-depan/js/animated-headline.js"></script>
	<script src="<?= base_url()?>app-assets-depan/js/validator.js"></script>
	<script src="<?= base_url()?>app-assets-depan/js/isotope.pkgd.min.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBn-q0VMtMOum5A7HVG86duHeJApbVDv7o"></script>
  	<script src="<?= base_url()?>app-assets-depan/js/map-white.js"></script>
	<script src="<?= base_url()?>app-assets-depan/js/main.js"></script>
	<script src="<?= base_url()?>app-assets-depan/js/wow.min.js"></script>
	  
	  
  </body>
</html>
