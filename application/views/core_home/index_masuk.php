<?php $this->load->view('core_home/header_info') ?>
    <!-- ==================================================
                        welcome-area
    ================================================== -->
    <section class="blog-intro" data-scroll-index="1">
        <div class="overlay-bg-75 sec-padding flex-center">
            <div class="container">
                <div>
                    <h1 class="mb-20px color-fff fw-400">Daftar</h1>
                    <a href="<?=site_url('')?>" class="color-fff color-orange-hvr fs-25">Home</a> <span class="color-aaa fs-18">/ </span>
                    <span class="color-aaa fs-18">Daftar</span>
                </div>
            </div>
        </div>
    </section>
    <!-- ==================================================
                        End welcome-area
    ================================================== -->
    <style>
        @media (max-width: 767px) {
            .column-1,
            .column-2 {
                width: 100%;
            }
        }
        @media (min-width: 768px) and (max-width: 991px) {
            .column-1 {
                width: 50%;
            }
            .column-2 {
                width: 100%;
            }
        }
        @media (min-width: 992px) {
            .column-2,
            .column-1 {
                width: 33%;
            }
        }

        .c {
          white-space: nowrap;
        }


    </style>

    <section class="intro-sec sec-padding text-center">
        <!-- <h4 class="card-title white-text">Daftar</h4> -->
        <!-- Grid row -->
        <div class="containera px-5" >
            <div class="card">
                <div class="card-header">
                    
                    <h1 class="title-h p-relative">
                        <span class="fw-200">Daftar </span> 
                        <span class="line p-absolute bg-orange"></span>
                    </h1>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xl-6 col-lg-12 mb-2">
                                <img class="card-img-top img-fluid" src="<?= base_url() ?>app-assets/images/aludi/logo.png" alt="Card image cap" />
                            </div>
                            <div class="col-xl-6 col-lg-12">
                                <form enctype="multipart/form-data" method="POST" action="<?= site_url('po_pendaftaran/save') ?>" class="form form-horizontal" required>
                                    <div class="form-group">
                                        <!-- <label for="exampleInputEmail1">Nama Lengkap</label> -->
                                        <input type="text" class="form-control" id="nama" name="nama" placeholder="Masukan Nama Lengkap" required>
                                    </div>
                                    <div class="form-group">
                                        <!-- <label for="exampleInputEmail1">Email</label> -->
                                        <input type="email" class="form-control" id="email" name="email"  placeholder="Masukan Email" required>
                                    </div>
                                    <div class="form-group">
                                        <!-- <label for="exampleInputEmail1">Nomor Handphone</label> -->
                                        <input type="nomor" class="form-control" id="nomor_handphone" name="nomor_handphone"  placeholder="Masukan Nomor Handphone" required>
                                    </div>
                                    <br>
                                    <!-- <div class="form-group">
                                        <input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
                                    </div> -->
                                    <div class="input-group">
                                        <input type="password" name="password" id="user_password" class="form-control" placeholder="Password" required data-toggle="password">
                                        <div class="input-group-append">
                                        <span class="input-group-text"><i class="fa fa-eye"></i></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <!-- <label for="exampleInputPassword1">Konfirmasi Password</label> -->
                                        <input type="password" class="form-control" id="password_ulangi" name="password_ulangi" placeholder="Ulangi Password" required>
                                    </div>
                                    <hr>
                                    <div class="form-group form-check">
                                        <input type="checkbox" class="form-check-input" id="setuju" name="setuju" value="1" required>
                                        <label class="form-check-label" for="exampleCheck1">Saya Menyetujui Ketentuan Layanan, Kebijakan Privasi Dan Kebijakan ALUDI</label>
                                    </div>
                                    
                                    <br>
                                    <div class="form-group px-1">
                                        <div class="g-recaptcha " data-sitekey="<?php echo $this->config->item('google_key') ?>"></div> 
                                    </div>
                                    <hr>
                                    <div class="row px-1">
                                        <button type="submit" class="btn btn-primary col-12 ">Daftar</button>
                                    </div>
                                </form>
                                <br>
                                <div class="border-top card-body text-center">Sudah Punya Akun? <a href="<?= site_url("auth/login")?>">Log In</a></div>
                            </div>
                            <div class="card-body text-center"> <a href="<?= site_url("core_home/selengkapnya") ?>" target="_blank">Pelajari Lebih Lanjut</a></div>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
        <!-- Grid row -->
    </section>




<?php $this->load->view('core_home/footer') ?>