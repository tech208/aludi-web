<?php $this->load->view('core_home/header_info') ?>
    <!-- ==================================================
                        welcome-area
    ================================================== -->
    <section class="blog-intro" data-scroll-index="1">
        <div class="overlay-bg-75 sec-padding flex-center">
            <div class="container">
                <div>
                    <h1 class="mb-20px color-fff fw-400">Info Terbaru</h1>
                    <a href="<?=site_url('')?>" class="color-fff color-orange-hvr fs-25">Home</a> <span class="color-aaa fs-18">/ </span>
                    <span class="color-aaa fs-18">Info Terbaru</span>
                </div>
            </div>
        </div>
    </section>
    <!-- ==================================================
                        End welcome-area
    ================================================== -->

    <script type="text/javascript" src="https://platform-api.sharethis.com/js/sharethis.js#property=5f2ccdb52684e00014234564&product=inline-share-buttons" async="async"></script>


    <div class="row">
        <div class="col-md-12">
            <br>
            <div class="sharethis-inline-share-buttons"></div>
        </div>
    </div>
    <!-- ==================================================
                        intro-sec
    ================================================== -->
    <section class="intro-sec sec-padding text-center">
        <div class="container">
            <div class="row">
            


               
                <div class="col-md-12">
                    <div class="mb-25px mt-25px">
                        <h5 class="fw-500 mt-20px"><?=$data->name?></h5> 
                    </div> 
                    <p class="text-right">
                        <?=$data->deskripsi?>
                    </p> 
                </div>


            </div>
        </div>
    </section>
    <!-- ==================================================
                        End intro-sec
    ================================================== -->


<?php $this->load->view('core_home/footer') ?>