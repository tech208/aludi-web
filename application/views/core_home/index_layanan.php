<?php $this->load->view('core_home/header_info') ?>
    <!-- ==================================================
                        welcome-area
    ================================================== -->
    <section class="blog-intro" data-scroll-index="1">
        <div class="overlay-bg-75 sec-padding flex-center">
            <div class="container">
                <div>
                    <h1 class="mb-20px color-fff fw-400">Layanan Aduan Masyarakat </h1>
                    <a href="<?=site_url('')?>" class="color-fff color-orange-hvr fs-25">Home</a> <span class="color-aaa fs-18">/ </span>
                    <span class="color-aaa fs-18">Layanan Aduan Masyarakat</span>
                </div>
            </div>
        </div>
    </section>
    <!-- ==================================================
                        End welcome-area
    ================================================== -->
    <style>
        @media (max-width: 767px) {
            .column-1,
            .column-2 {
                width: 100%;
            }
        }
        @media (min-width: 768px) and (max-width: 991px) {
            .column-1 {
                width: 50%;
            }
            .column-2 {
                width: 100%;
            }
        }
        @media (min-width: 992px) {
            .column-2,
            .column-1 {
                width: 33%;
            }
        }

        .c {
          white-space: nowrap;
        }


    </style>

    <section class="intro-sec sec-padding text-center">
        <h4 class="card-title white-text">Layanan Aduan Masyarakat </h4>
        <!-- Grid row -->
        <div class="containera px-5" >
            <div class="row " >
                <div class="col-1"></div>
                <div class="col-10">
                    <div class="card">
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <form enctype="multipart/form-data" method="POST" action="<?= site_url('po_pendaftaran/save_aduan_aludi') ?>" class="form form-horizontal" required>
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="nama" name="nama" placeholder="Masukan Nama Lengkap" required>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control" id="no_kontak" name="no_kontak"  placeholder="Masukan Nomor Kontak" required>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input type="email" class="form-control" id="email" name="email"  placeholder="Masukan Email" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <select class="form-control" id="id_nama_platform" name="id_nama_platform" required>
                                                    <option >--pilih--</option>
                                                    <?php
                                                        foreach ($nama_platform as $key => $value):
                                                    ?>
                                                    <option value="<?=$value['nama']?>"><?=$value['nama']?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <textarea class="form-control" id="kronologis" name="kronologis"  placeholder="Masukan Bentuk Aduan" required style="height: 150px;" ></textarea>
                                            </div>
                                            <div class="form-group">
                                                <input type="file" class="form-control" id="lampiran" name="lampiran"  placeholder="Masukan Nama Platform" required>
                                            </div>
                                            <div class="form-group px-1">
                                                <div class="g-recaptcha " data-sitekey="<?php echo $this->config->item('google_key') ?>"></div> 
                                            </div>
                                            <hr>
                                            <div class="row px-1">
                                                <button type="reset" class="btn  col-2 ">Reset</button>
                                                <button type="submit" class="btn btn-primary col-2 ">Kirim</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Grid row -->
    </section>




<?php $this->load->view('core_home/footer') ?>