<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Modern admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities with bitcoin dashboard.">
    <meta name="keywords" content="admin template, modern admin template, dashboard template, flat admin template, responsive admin template, web app, crypto dashboard, bitcoin dashboard">
    <meta name="author" content="PIXINVENT">
    <title>PT SANTARA DAYA INSPIRATAMA</title>
    <link rel="apple-touch-icon" href="<?= base_url() ?>app-assets/images/logo/Logo.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?= base_url() ?>app-assets/images/logo/Logo.png">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i%7CQuicksand:300,400,500,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/fonts/material-icons/material-icons.css">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/vendors/css/material-vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/vendors/css/charts/apexcharts.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/css/material.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/css/components.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/css/material-extended.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/css/material-colors.css">
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/css/core/menu/menu-types/material-vertical-menu-modern.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/vendors/css/charts/jquery-jvectormap-2.0.3.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/vendors/css/charts/morris.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/fonts/simple-line-icons/style.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/css/core/colors/material-palette-gradient.css">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/style.css">
    <!-- END: Custom CSS-->

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern material-vertical-layout material-layout vertical-collapsed-menu 2-columns   menu-collapsed fixed-navbar" data-open="click" data-menu="vertical-menu-modern" data-col="2-columns">

    <!-- BEGIN: Header-->
    <nav class="header-navbar navbar-expand-lg navbar navbar-with-menu navbar-without-dd-arrow fixed-top navbar-semi-dark navbar-shadow">
        <div class="navbar-wrapper">
            <div class="navbar-header">
                <ul class="nav navbar-nav flex-row">
                    <li class="nav-item mobile-menu d-lg-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu font-large-1"></i></a></li>
                    <li class="nav-item mr-auto"><a class="navbar-brand" href="index.html"><img class="brand-logo" alt="modern admin logo" src="<?= base_url() ?>app-assets/images/logo/Logo.png">
                            <h3 class="brand-text">Santara</h3>
                        </a></li>
                    <li class="nav-item d-none d-lg-block nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="toggle-icon ft-toggle-right font-medium-3 white" data-ticon="ft-toggle-right"></i></a></li>
                    <li class="nav-item d-lg-none"><a class="nav-link open-navbar-container" data-toggle="collapse" data-target="#navbar-mobile"><i class="material-icons mt-50">more_vert</i></a></li>
                </ul>
            </div>
            <div class="navbar-container content">
                <div class="collapse navbar-collapse" id="navbar-mobile">
                    <ul class="nav navbar-nav mr-auto float-left">
                        <li class="nav-item"><a class="nav-link nav-link-expand" href="#"><i class="ficon ft-maximize"></i></a></li>
                        <li class="nav-item nav-search"><a class="nav-link nav-link-search" href="#"><i class="material-icons">search</i></a>
                            <div class="search-input">
                                <input class="input round form-control search-box" type="text" placeholder="Explore Modern Admin" tabindex="0" data-search="template-list">
                                <div class="search-input-close"><i class="ft-x"></i></div>
                                <ul class="search-list"></ul>
                                <div class="dropdown-menu arrow">
                                    <div class="dropdown-item">
                                        <input class="round form-control" type="text" placeholder="Search Here">
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav float-right">
                        <li class="dropdown dropdown-notification nav-item"><a class="nav-link nav-link-label" href="#" data-toggle="dropdown"><i class="material-icons">notifications_none</i><span class="badge badge-pill badge-danger badge-up badge-glow">5</span></a>
                            <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                                <li class="dropdown-menu-header">
                                    <h6 class="dropdown-header m-0"><span class="grey darken-2">Notifications</span></h6><span class="notification-tag badge badge-danger float-right m-0">5 New</span>
                                </li>
                                <li class="scrollable-container media-list w-100"><a href="javascript:void(0)">
                                        <div class="media">
                                            <div class="media-left align-self-center"><i class="material-icons icon-bg-circle bg-cyan mr-0">add_box</i></div>
                                            <div class="media-body">
                                                <h6 class="media-heading">You have new order!</h6>
                                                <p class="notification-text font-small-3 text-muted">Lorem ipsum dolor sit amet, consectetuer elit.</p><small>
                                                    <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">30 minutes ago</time></small>
                                            </div>
                                        </div>
                                    </a></li>
                                <li class="dropdown-menu-footer"><a class="dropdown-item text-muted text-center" href="javascript:void(0)">Read all notifications</a></li>
                            </ul>
                        </li>
                        <li class="dropdown dropdown-notification nav-item"><a class="nav-link nav-link-label" href="#" data-toggle="dropdown"><i class="material-icons">mail_outline</i></a>
                            <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                                <li class="dropdown-menu-header">
                                    <h6 class="dropdown-header m-0"><span class="grey darken-2">Messages</span></h6><span class="notification-tag badge badge-warning float-right m-0">4 New</span>
                                </li>
                                <li class="scrollable-container media-list w-100"><a href="javascript:void(0)">
                                        <div class="media">
                                            <div class="media-left"><span class="avatar avatar-sm avatar-online rounded-circle"><img src="<?= base_url() ?>app-assets/images/portrait/small/avatar-s-19.png" alt="avatar"><i></i></span></div>
                                            <div class="media-body">
                                                <h6 class="media-heading">Margaret Govan</h6>
                                                <p class="notification-text font-small-3 text-muted">I like your portfolio, let's start.</p><small>
                                                    <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">Today</time></small>
                                            </div>
                                        </div>
                                        <div class="media">
                                            <div class="media-left"><span class="avatar avatar-sm avatar-away rounded-circle"><img src="<?= base_url() ?>app-assets/images/portrait/small/avatar-s-6.png" alt="avatar"><i></i></span></div>
                                            <div class="media-body">
                                                <h6 class="media-heading">Eric Alsobrook</h6>
                                                <p class="notification-text font-small-3 text-muted">We have project party this saturday.</p><small>
                                                    <time class="media-meta text-muted" datetime="2015-06-11T18:29:20+08:00">last month</time></small>
                                            </div>
                                        </div>
                                    </a></li>
                                <li class="dropdown-menu-footer"><a class="dropdown-item text-muted text-center" href="javascript:void(0)">Read all messages</a></li>
                            </ul>
                        </li>
                        <li class="dropdown dropdown-user nav-item"><a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown"><span class="mr-1 user-name text-bold-700">Anggi Dian Anggraini</span><span class="avatar avatar-online"><img src="<?= base_url() ?>app-assets/images/portrait/small/avatar-s-1.png" alt="avatar"><i></i></span></a>
                            <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="user-profile.html"><i class="material-icons"></i> Edit Profile</a><a class="dropdown-item" href="app-kanban.html"><i class="material-icons"></i> Todo</a><a class="dropdown-item" href="user-cards.html"><i class="material-icons"></i> Task</a>
                                <div class="dropdown-divider"></div><a class="dropdown-item" href="login-with-bg-image.html"><i class="material-icons"></i> Logout</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <!-- END: Header-->


    <!-- BEGIN: Main Menu-->

    <div class="main-menu material-menu menu-fixed menu-dark menu-accordion menu-shadow" data-scroll-to-active="true">
        <div class="user-profile">
            <div class="user-info text-center pb-2"><img class="user-img img-fluid rounded-circle w-25 mt-2" src="<?= base_url() ?>app-assets/images/portrait/small/avatar-s-1.png" alt="" />
                <div class="name-wrapper d-block dropdown mt-1"><a class="white dropdown-toggle" id="user-account" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="user-name">Anggi Dian Anggraini</span></a>
                    <div class="text-light">HRIS</div>
                    <div class="dropdown-menu arrow" aria-labelledby="dropdownMenuLink"><a class="dropdown-item"><i class="material-icons align-middle mr-1">person</i><span class="align-middle">Profile</span></a><a class="dropdown-item"><i class="material-icons align-middle mr-1">message</i><span class="align-middle">Messages</span></a><a class="dropdown-item"><i class="material-icons align-middle mr-1">attach_money</i><span class="align-middle">Balance</span></a><a class="dropdown-item"><i class="material-icons align-middle mr-1">settings</i><span class="align-middle">Settings</span></a><a class="dropdown-item"><i class="material-icons align-middle mr-1">power_settings_new</i><span class="align-middle">Log Out</span></a></div>
                </div>
            </div>
        </div>
        <div class="main-menu-content">
            <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
                <li class=" nav-item"><a href="index.html"><i class="material-icons">settings_input_svideo</i><span class="menu-title" data-i18n="Dashboard">Dashboard</span></a>
                </li>
                <li class=" nav-item"><a href="#"><i class="material-icons">tv</i><span class="menu-title" data-i18n="Personal Data">Personal Data</span></a>
                    <ul class="menu-content">
                        <li><a class="menu-item" href="dashboard-ecommerce.html"><i class="material-icons"></i><span data-i18n="Profile">Profile</span></a>
                        </li>
                        <li><a class="menu-item" href="dashboard-ecommerce.html"><i class="material-icons"></i><span data-i18n="Hubungan Kerja">Hubungan Kerja</span></a>
                        </li>
                        <li><a class="menu-item" href="dashboard-ecommerce.html"><i class="material-icons"></i><span data-i18n="Benefit">Benefit</span></a>
                        </li>
                        <li><a class="menu-item" href="dashboard-ecommerce.html"><i class="material-icons"></i><span data-i18n="Tunjangan">Tunjangan</span></a>
                        </li>
                        <li><a class="menu-item" href="dashboard-ecommerce.html"><i class="material-icons"></i><span data-i18n="Pajak">Pajak</span></a>
                        </li>
                        <li><a class="menu-item" href="dashboard-ecommerce.html"><i class="material-icons"></i><span data-i18n="Berkas">Berkas</span></a>
                        </li>
                    </ul>
                </li>
                <li class=" nav-item"><a href="#"><i class="material-icons">tv</i><span class="menu-title" data-i18n="Performance">Performance</span></a>
                    <ul class="menu-content">
                        <li><a class="menu-item" href="dashboard-ecommerce.html"><i class="material-icons"></i><span data-i18n="Performance Appraisal">Performance Appraisal</span></a>
                        </li>
                        <li><a class="menu-item" href="dashboard-ecommerce.html"><i class="material-icons"></i><span data-i18n="Culture Fit">Culture Fit</span></a>
                        </li>
                        <li><a class="menu-item" href="dashboard-ecommerce.html"><i class="material-icons"></i><span data-i18n="Berikan Penilaian">Berikan Penilaian</span></a>
                        </li>
                    </ul>
                </li>
                <li class=" nav-item"><a href="index.html"><i class="material-icons">settings_input_svideo</i><span class="menu-title" data-i18n="Training">Training</span></a>
                </li>
                <li class=" nav-item"><a href="#"><i class="material-icons">tv</i><span class="menu-title" data-i18n="Self-services">Self-services</span></a>
                    <ul class="menu-content">
                        <li><a class="menu-item" href="dashboard-ecommerce.html"><i class="material-icons"></i><span data-i18n="Cuti">Cuti</span></a>
                        </li>
                        <li><a class="menu-item" href="dashboard-ecommerce.html"><i class="material-icons"></i><span data-i18n="Claim">Claim</span></a>
                        </li>
                        <li><a class="menu-item" href="dashboard-ecommerce.html"><i class="material-icons"></i><span data-i18n="Overtime">Overtime</span></a>
                        </li>
                    </ul>
                </li>
                <li class=" nav-item"><a href="index.html"><i class="material-icons">settings_input_svideo</i><span class="menu-title" data-i18n="Payroll">Payroll</span></a>
                </li>
                <li class=" navigation-header"><span data-i18n="Admin Panels">Admin Panels</span><i class="material-icons nav-menu-icon" data-toggle="tooltip" data-placement="right" data-original-title="Admin Panels">more_horiz</i>
                </li>
                <li class=" nav-item"><a href="<?= base_url() ?>material-ecommerce-menu-template" target="_blank"><i class="material-icons">add_shopping_cart</i><span class="menu-title" data-i18n="User Management">User Management</span></a>
                </li>
                <li class=" nav-item"><a href="<?= base_url() ?>material-travel-menu-template" target="_blank"><i class="material-icons">call_merge</i><span class="menu-title" data-i18n="Recruitment Management">Recruitment Management</span></a>
                </li>
                <li class=" nav-item"><a href="<?= base_url() ?>material-hospital-menu-template" target="_blank"><i class="material-icons">add_circle_outline</i><span class="menu-title" data-i18n="Resource Management">Resource Management</span></a>
                </li>
                <li class=" nav-item"><a href="<?= base_url() ?>material-crypto-menu-template" target="_blank"><i class="material-icons">attach_money</i><span class="menu-title" data-i18n="Tunjangan &amp; Benefit Management">Tunjangan &amp; Benefit Management</span></a>
                </li>
                <li class=" nav-item"><a href="<?= base_url() ?>material-support-menu-template" target="_blank"><i class="material-icons">label_outline</i><span class="menu-title" data-i18n="Performance Management">Performance Management</span></a>
                </li>
                <li class=" nav-item"><a href="<?= base_url() ?>material-bank-menu-template" target="_blank"><i class="material-icons">account_balance</i><span class="menu-title" data-i18n="Training Management">Training Management</span></a>
                </li>
                <li class=" nav-item"><a href="#"><i class="material-icons">tv</i><span class="menu-title" data-i18n="Self-services Management">Self-services Management</span></a>
                    <ul class="menu-content">
                        <li><a class="menu-item" href="dashboard-ecommerce.html"><i class="material-icons"></i><span data-i18n="Cuti Management">Cuti Management</span></a>
                        </li>
                        <li><a class="menu-item" href="dashboard-ecommerce.html"><i class="material-icons"></i><span data-i18n="Claim Management">Claim Management</span></a>
                        </li>
                        <li><a class="menu-item" href="dashboard-ecommerce.html"><i class="material-icons"></i><span data-i18n="Overtime Management">Overtime Management</span></a>
                        </li>
                    </ul>
                </li>
                <li class=" nav-item"><a href="<?= base_url() ?>material-bank-menu-template" target="_blank"><i class="material-icons">account_balance</i><span class="menu-title" data-i18n="Payroll Management">Payroll Management</span></a>
                </li>
            </ul>
        </div>
    </div>
    <!-- END: Main Menu-->
    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-body">
                <!-- Apexcharts Start -->
                <section class="charts-apexcharts">
                    <div class="row">
                        <div class="col-lg-3 col-12">
                            <div class="card pull-up">
                                <div class="card-content">
                                    <div class="card-body">
                                        <div class="media d-flex">
                                            <div class="media-body text-left">
                                                <h6 class="text-muted">Cuti Tahunan</h6>
                                                <h3>12 Hari</h3>
                                            </div>
                                            <div class="align-self-center">
                                                <i class="icon-trophy success font-large-2 float-right"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-12">
                            <div class="card pull-up">
                                <div class="card-content">
                                    <div class="card-body">
                                        <div class="media d-flex">
                                            <div class="media-body text-left">
                                                <h6 class="text-muted">Masa Kerja</h6>
                                                <h3>1 Tahun</h3>
                                            </div>
                                            <div class="align-self-center">
                                                <i class="icon-call-in danger font-large-2 float-right"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-12">
                            <div class="card pull-up">
                                <div class="card-content">
                                    <div class="card-body">
                                        <div class="media d-flex">
                                            <div class="media-body text-left">
                                                <h6 class="text-muted">Overtime</h6>
                                                <h3>0 Hari</h3>
                                            </div>
                                            <div class="align-self-center">
                                                <i class="icon-call-in danger font-large-2 float-right"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-12">
                            <div class="card pull-up">
                                <div class="card-content">
                                    <div class="card-body">
                                        <div class="media d-flex">
                                            <div class="media-body text-left">
                                                <h6 class="text-muted">Calls</h6>
                                                <h3>3,568</h3>
                                            </div>
                                            <div class="align-self-center">
                                                <i class="icon-call-in danger font-large-2 float-right"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <!-- Line Basic Chart Start -->
                            <div class="card">
                                <div class="card-body">
                                    <div class="card-title">Performance Appraisal</div>
                                    <div id="line-basic-chart"></div>
                                </div>
                            </div>
                            <!-- line basic chart end -->
                        </div>
                        
                        <div class="col-12 col-md-6">
                            <!-- Column Basic Chart Start -->
                            <div class="card">
                                <div class="card-body">
                                    <div class="card-title">Culture Fit</div>
                                    <div id="column-basic-chart"></div>
                                </div>
                            </div>
                            <!-- column basic chart end -->
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12 col-md-6">
                            <div class="card">
                                <div class="card-body">
                                    <div class="card-title">This Year Revenue</div>
                                    <canvas id="thisYearRevenue"></canvas>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-6">
                            <div class="card">
                                <div class="card-body">
                                    <div class="card-title">Last Year Revenue</div>
                                    <canvas id="lastYearRevenue"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- apexcharts end -->

            </div>
        </div>
    </div>
    <!-- END: Content-->

    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>

    <!-- BEGIN: Footer-->
    <footer class="footer footer-static footer-light navbar-border navbar-shadow">
        <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2"><span class="float-md-left d-block d-md-inline-block">Copyright &copy; 2019 <a class="text-bold-800 grey darken-2" href="https://1.envato.market/modern_admin" target="_blank">SANTARA</a></span><span class="float-md-right d-none d-lg-block">Hand-crafted & Made with<i class="ft-heart pink"></i><span id="scroll-top"></span></span></p>
    </footer>
    <!-- END: Footer-->


    <!-- BEGIN: Vendor JS-->
    <script src="<?= base_url() ?>app-assets/vendors/js/material-vendors.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="<?= base_url() ?>app-assets/vendors/js/charts/chart.min.js"></script>
    <script src="<?= base_url() ?>app-assets/vendors/js/charts/raphael-min.js"></script>
    <script src="<?= base_url() ?>app-assets/vendors/js/charts/morris.min.js"></script>
    <script src="<?= base_url() ?>app-assets/vendors/js/charts/jvector/jquery-jvectormap-2.0.3.min.js"></script>
    <script src="<?= base_url() ?>app-assets/vendors/js/charts/jvector/jquery-jvectormap-world-mill.js"></script>
    <script src="<?= base_url() ?>app-assets/data/jvector/visitor-data.js"></script>
    <script src="<?= base_url() ?>app-assets/vendors/js/charts/apexcharts/apexcharts.min.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="<?= base_url() ?>app-assets/js/core/app-menu.js"></script>
    <script src="<?= base_url() ?>app-assets/js/core/app.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="<?= base_url() ?>app-assets/js/scripts/pages/material-app.js"></script>
    <script src="<?= base_url() ?>app-assets/js/scripts/pages/dashboard-sales.js"></script>
    <script src="<?= base_url() ?>app-assets/js/scripts/charts/apexcharts/charts-apexcharts.js"></script>
    <!-- END: Page JS-->

</body>
<!-- END: Body-->

</html>