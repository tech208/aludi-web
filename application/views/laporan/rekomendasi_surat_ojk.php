<html>
    <head>
        <style>
            /** 
                Set the margins of the pdf page to 0, so the footer and the header
                can be of the full height and width !
             **/
            @page {
                margin: 1cm 0cm 1cm 0cm;
            }
            /** Define now the real margins of every pdf page in the PDF **/
            body {
                margin-top: 3cm;
                margin-left: 2cm;
                margin-right: 2cm;
                margin-bottom: 2cm;
            }
            /** Define the header rules **/
            header {
                position: fixed;
                top: 0cm;
                left: 0cm;
                right: 0cm;
                height: 6cm;
                /** Extra personal styles **/
                text-align: center;
                line-height: 0.5cm;
            }
        </style>
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    </head>
	<body>

	<header>
	<div >
	<table width="500" height="100" align="center" border="0">
	<tr>
		<td width="100" style="text-align: left;">
 			<img src="<?php echo 'app-assets/images/aludi/logo.png'; ?>" width="100px" height="100px"/>
		</td>
		<td width="">
			<b>ASOSIASI LAYANAN URUN DANA INDONESIA (ALUDI)</b> <br>
			PERKUMPULAN EQUITY CROWDFUNDING INDONESIA<br>
			MULA Kota Tua - Blok Cipta Niaga<br>
			Gedung Rotterdamsche Lloyd, JL. Kalibesar timur 4, No-37<br>
			Kec. Tamansari, Jakarta Barat, DKI Jakarta
			
		</td>
	</tr>
	</table>
	</div>
	</header>
<table width="500" align="center" style="line-height: 0.5cm;">
	<tr>
		<td colspan=2><hr /></td>
	</tr>
	<tr>
		<td colspan="2">
			<table border="0" width='100%'>
				<tr>
					<td colspan="3" align="right">Jakarta, <?=MasTanggal(date("Y-m-d"))?></td>
				</tr>
				<tr>
					<td width='100'>Nomor</td>
					<td width='10'>:</td>
					<td><?=@$tahap->tahap_pembayaran->sertifikat_no?></td>
				</tr>
				<tr>
					<td width='100'>Lampiran</td>
					<td width='10'>:</td>
					<td>2 (Dua) berkas</td>
				</tr>
				<tr>
					<td width='100'>Perihal</td>
					<td width='10'>:</td>
					<td>Surat Rekomendasi atas <?=@$tahap->tahap_satu->nama_perusahaan?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">
			Kepada Yth.<br>
			<b>Bapak Hoesen</b><br>
			<b>Kepala Eksekutif Pengawas Pasar Modal Otoritas Jasa Keuangan (OJK)</b><br>
			Gd. Soemitro Djojohadikusumo Lantai 3.<br>
			Jln. Lapangan Banteng Timur No.2-4, Jakarta 10710<br>
		</td>
	</tr>
	<tr>
		<td colspan=2>
			<br><br><br>
			<p>Dengan hormat,</p>
			<br>
			<p>
				Pertama-tama kami ingin menyampaikan terima kasih atas dukungan yang telah diberikan oleh Otoritas Jasa Keuangan (OJK) terhadap industri Lauanan Urun Dana di Indonesia selama ini. 
				Sebagai mitra OJK di Industri, Asosiasi Layanan Urun Dana Indonesia (ALUDI) akan selalu memegang komitmen untuk dapat bekerjasama dengan regilator dalam mengembangkan 
				Fintech ( Equity <?=@$tahap->tahap_satu->jenis_perusahaan?> ) untuk mencapai target inklusi keuangan di Indonesia.
			</p>
			<p>
				Sehubungan dengan telah dilaksanakanya verifikasi kembali atas calon penyelenggara <?=@$tahap->tahap_satu->nama_perusahaan?> Pada tanggal <?=MasTanggal(date("Y-m-d"))?>, 
				maka atas hasil pemeriksaan dokumen sebagaimana hasil terlampir serta hasil kunjungan yang telah dilakukan secara langsung kepada 
				<?=@$tahap->tahap_satu->nama_perusahaan?>, dengan ini bahwa Asosiasi Layanan Teknologi Gotong Royong Bersama (ALUDI) menyatakan <?=@$tahap->tahap_satu->nama_perusahaan?>
				telah LAYAK untuk melanjutkan proses permohonan perizinan kepada Otoritas Jasa Keuangan (OJK) dan telah memenuhi kaidah yang diamanatkan dalam ( POJK 57 Tahun 2020/ POJK 37 Tahun 2018 ) tentang Penawaran Efek Melalui Layanan Urun Dana Berbasis Teknologi Informasi.
			</p>
			<p>
				Adapun bilamana di kemudian hari pada saat proses permohonan perizinan kepada OJK ditemukan ketidaksesuaian dokumen, 
				model bisnis serta kegiatan sebagaimana yang telah diterima oleh ALUDI pada saat pemeriksaan maka hal tersebut menjadi tanggung jawab calon penyelenggara dan OJK memiliki 
				wewenang penuh untuk melanjutkan atau menolak proses permohonan izin <?=@$tahap->tahap_satu->nama_perusahaan?>.
			</p>
			<p>
				Demikian surat rekomendasi ini disampaikan untuk dapat dipergunakan sebagai mestinya dan ditindak lanjuti.  Atas perhatian dan kerjasamanya, kami mengucapkan terima kasih.
			</p>
		</td>
	</tr>
	<tr>
		<td><br></td>
	</tr>
	
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">Hormat kami,</td>
	</tr>
	<tr>
		<td colspan="2">Layanan Teknologi Gotong Royong Bersama (ALUDI)</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">
			<table border="0" width="100%">
				<tr>
					<td align="center"><u>Muhammad Reza Avesena</u></td>
					<td align="center"><u>Heinrich Vincent</u></td>
				</tr>
				<tr>
					<td align="center">Ketua Umum</td>
					<td align="center">Wakil Ketua Umum</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

	</body>
</html>
