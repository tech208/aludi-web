<table>
	<tr>
    	<td align="center">Data Anggota Asosiasi - ALUDI <?=date("Y")?></td>
    </tr>
	<tr>
    	<td align="center">&nbsp;</td>
    </tr>
    <tr>
    	<td >
            <table border='1'>
                <tr>
                    <td rowspan=2>No</td>
                    <td rowspan=2>Nama Perusahaan</td>
                    <td rowspan=2>Nama Platform</td>
                    <td rowspan=2>Nama CEO</td>
                    <td rowspan=2>Status Keanggotaan di ALUDI</td>
                    <td rowspan=2>Status Keanggotaan di OJK</td>
                    <td rowspan=2>No Sertifikat Keanggotaan</td>
                    <td rowspan=2>Periode Keanggotaan</td>
                    <td rowspan=2>Sertifikat Keanggotaan</td>
                    <td rowspan=2>Surat Rekomendasi</td>
                    <td rowspan=2>Sertifikat Keanggotaan</td>
                    <td colspan=3 align="center">Keterangan Detail Pembayaran</td>
                </tr>
                <tr>
                    <td>Keterangan</td>
                    <td>Waktu / Jam</td>
                    <td>Bukti Bayar</td>
                </tr>
                <?php
                    $no=1; foreach ($data as $key => $value) :
                ?>
                <tr>
                    <td><?=$no;?></td>
                    <td><?=@$value->tahap_satu->nama_perusahaan;?></td>
                    <td><?=@$value->tahap_satu->nama_platform;?></td>
                    <td><?=@$value->personals->name;?></td>
                    <td><?=@$value->statuse->name;?></td>
                    <td><?=(@$value->status==32) ? "Berizin" : "Belum Berizin" ;?></td>
                    <td><?=@$value->tahap_pembayaran->sertifikat_no;?></td>
                    <td></td>
                    <td></td>
                    <td>
                        <?php
                            if (!empty(@$value->tahap_rekomendasi->rekomendasis->nama_file)) {
                                echo "<a href='".base_url('uploads/rekomendasi_ojk/'.@@$value->tahap_rekomendasi->rekomendasis->nama_file)."'>Download</a>";
                            }
                        ?>
                    </td>
                    <td>
                        <?php
                            if (!empty(@$value->tahap_pembayaran->sertifikat)) {
                                echo "<a href='".base_url('uploads/pembayaran_sertifikat/'.@$value->tahap_pembayaran->sertifikat)."'>Download</a>";
                            }
                        ?>
                    </td>
                    <td><?=date('Y-m-d', strtotime(@$value->tahap_pembayaran->created_at));?></td>
                    <td><?=date('H:i:s', strtotime(@$value->tahap_pembayaran->created_at));?></td>
                    <td>
                        <?php
                            if (!empty(@$value->tahap_pembayaran->bukti)) {
                                echo "<a href='".base_url('uploads/bukti/'.@$value->tahap_pembayaran->bukti)."'>Download</a>";
                            }
                        ?>
                    </td>
                </tr>
                <?php $no++; endforeach; ?>
            </table>
        </td>
 	</tr>
</table>
