<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">


<br>

<div id="content" style="background-color: white;">
	<table class="table" border=0 width="900" align="center">
		<tr>
			<td width="450"><h1>INVOICE</h1></td>
			<td width="450" style="text-align: right;"><img class="brand-logo" alt="Aludi" style="width: 150px; " src="<?= base_url() ?>app-assets/images/aludi/logo.png"></td>
		</tr>
		<tr>
			<td colspan=2><hr /></td>
		</tr>
		<tr>
			<td><h4>INFO KAMI</h4></td>
			<td><h4>KEPADA</h4></td>
		</tr>
		<tr>
			<td>ALUDI ( Asosiasi Layanan Urun Indonesia)</td>
			<td><?=@$tahap_satu->nama_perusahaan?></td>
		</tr>
		<tr>
			<td>
				MULA Kota Tua - Blok Cipta Niaga
				Gedung Rotterdamcha Lioyd, JL. Kalibeber Timur
				No-37 Kec. Tamansari, Jakarta Barat, DKI Jakarta
			</td>
			<td>
				<?=@$tahap_satu->alamat_perusahaan?>
			</td>
		</tr>
		<tr>
			<td colspan=2>
				<br><br><br>
				<table border="1" width="100%">
					<tr>
						<th>Keterangan</th>
						<th>Periode</th>
						<th>Keterangan</th>
					</tr>
					<tr>
						<td>Pelunasan Membership Fee</td>
						<td><?=@$tahap_pembayaran->periode?></td>
						<td>Rp. 20.000.000</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan=2>
				<br>
				<table border="0" width="100%">
					<tr>
						<th width="70%">Total</th>
						<th>Rp. 20.000.000</th>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td><br></td>
		</tr>
		<tr>
			<td>INFO TRANSFER <br>
			Rek Bank BCA -0123037895 <br>
			PERK LAYANAN TEKNOLOGI KCU GAJAH MADA <br>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>Jakarta, <?=MasTanggal(date("Y-m-d"))?></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>Ratu Pebrianti Komala Dewi</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>Ketua Harian ALUDI</td>
		</tr>
	</table>
</div>


<button class="btn btn-success" style="margin: 10px;" onclick="printDiv('content','Title')">Cetak</button>

<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script src = "https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.4/jspdf.min.js"></script>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#cmd').click(function() {
            
            var options = {
            };
            var pdf = new jsPDF('p', 'pt', 'a4');
            pdf.addHTML($("#content"), 15, 15, options, function() {
                pdf.save('pageContent.pdf');
            });
        });
    });

    
function printDiv(divId,title) {

  let mywindow = window.open('', 'PRINT', 'height=650,width=900,top=100,left=150');

  mywindow.document.write(`<html><head><title>${title}</title>`);
  mywindow.document.write('</head><body >');
  mywindow.document.write(document.getElementById(divId).innerHTML);
  mywindow.document.write('</body></html>');

  mywindow.document.close(); // necessary for IE >= 10
  mywindow.focus(); // necessary for IE >= 10*/

  mywindow.print();
  mywindow.close();

  return true;
}
</script>