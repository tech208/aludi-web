<table>
	<tr>
    	<td align="center">Data Penyelenggara Pulling Asosiasi - ALUDI <?=date("Y")?></td>
    </tr>
	<tr>
    	<td align="center">&nbsp;</td>
    </tr>
    <tr>
    	<td >
            <table border='1'>
                <tr>
                    <td >No</td>
                    <td >Nama PT</td>
                    <td >Nama Platform</td>
                    <td >Nama Brand</td>
                    <td >Jumlah Penerbit</td>
                    <td >Jumlah Pendanaan</td>
                    <td >Jumlah Dividen</td>
                    <td >Jumlah Investor</td>
                    <td >Jenis / Kategori</td>
                    <td >Lokasi Pemodal</td>
                    <td >Lokasi Penerbit</td>
                </tr>
                <?php
                    $no=1; foreach ($data as $key => $value) :
                ?>
                <tr>
                    <td><?=$no;?></td>
                    <td><?=@$value->nama_pt;?></td>
                    <td><?=@$value->nama_brand;?></td>
                    <td><?=MasUang(@$value->jumlah_penerbit,0);?></td>
                    <td><?=MasUang(@$value->jumlah_investor,0);?></td>
                    <td><?=MasUang(@$value->jumlah_investor_dividen,0);?></td>
                    <td><?=MasUang(@$value->jumlah_dana,0);?></td>
                    <td><?=masuang(@$value->jumlah_dividen,0);?></td>
                    <td>
                        <?php
                            foreach ($value->jenise as $key_jenise => $value_jenise):
                        ?>
                        <ul>
                            <li>Jenis : <?=$value_jenise->jenis?>, Jumlah : <?=$value_jenise->jumlah ?></li>
                        </ul>
                        <?php
                            endforeach;
                        ?>
                    </td>
                    <td>
                        <?php
                            foreach ($value->lokasi_pemodals as $key_pemodals => $value_pemodals):
                        ?>
                        <ul>
                            <li>Negara <?=@$value_pemodals->negaras->nama?> Di Provinsi <?=@$value_pemodals->provinsis->nama?>, Jumlah : <?=$value_jenise->jumlah ?></li>
                        </ul>
                        <?php
                            endforeach;
                        ?>
                    </td>
                    <td>
                        <?php
                            foreach ($value->lokasi_penerbits as $key_penerbits => $value_penerbits):
                        ?>
                        <ul>
                            <li>Negara <?=@$value_penerbits->negaras->nama?> Di Provinsi <?=@$value_penerbits->provinsis->nama?>, Jumlah : <?=$value_penerbits->jumlah ?></li>
                        </ul>
                        <?php
                            endforeach;
                        ?>
                    </td>
                </tr>
                <?php $no++; endforeach; ?>
            </table>
        </td>
 	</tr>
</table>
