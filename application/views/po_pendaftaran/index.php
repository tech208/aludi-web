<?php $this->load->view('core_home/header') ?>
<script type="text/javascript">var delay = 1000; setTimeout(function() {$('#menu-pendaftaran').addClass('active');}, delay); </script> <!-- Set menu aktif -->


<div class="content-body">
    <!-- card actions section start -->
    <section id="card-actions">
        
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h1 style="text-align: center"><b>Mengapa kita menggunakannya?</b></h1>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xl-6 col-lg-12 mb-2">
                                <img class="card-img-top img-fluid" src="<?= base_url() ?>app-assets/images/banner/banner-22.jpg" alt="Card image cap" />
                            </div>
                            <div class="col-xl-6 col-lg-12">
                                <form enctype="multipart/form-data" method="POST" action="<?= site_url('po_pendaftaran/save') ?>" class="form form-horizontal" required>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Nama Lengkap</label>
                                        <input type="text" class="form-control" id="nama" name="nama" placeholder="Masukan Nama Lengkap" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Email</label>
                                        <input type="email" class="form-control" id="email" name="email"  placeholder="Masukan Email" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Nomor Handphone</label>
                                        <input type="nomor" class="form-control" id="nomor_handphone" name="nomor_handphone"  placeholder="Masukan Nomor Handphone" required>
                                    </div>
                                    <br>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Password</label>
                                        <input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Konfirmasi Password</label>
                                        <input type="password" class="form-control" id="password_ulangi" name="password_ulangi" placeholder="Ulangi Password" required>
                                    </div>
                                    <hr>
                                    <div class="form-group form-check">
                                        <input type="checkbox" class="form-check-input" id="setuju" name="setuju" value="1" required>
                                        <label class="form-check-label" for="exampleCheck1">Saya Menyetujui Ketentuan Layanan, Kebijakan Privasi Dan Kebijakan ALUDI</label>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Daftar</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            
    </section>
    <!-- // card-actions section end -->
</div>

<?php $this->load->view('core_home/footer') ?>

</body>
<!-- END: Body-->

</html>