<!doctype html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title>Login ALUDI</title>
        <link rel="apple-touch-icon" href="<?= base_url() ?>app-assets/images/aludi/logo.png">
        <link rel="shortcut icon" type="image/x-icon" href="<?= base_url() ?>app-assets/images/aludi/logo.png">
        <link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
        <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/4.1.3/darkly/bootstrap.min.css"> -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">


        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/css/material.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/css/components.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/css/bootstrap-extended.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/css/material-extended.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/css/material-colors.css">
        <!-- END: Theme CSS-->
        

    </head>

    <body>
        <!-- BEGIN: Content-->
            <div class="app-content content">
                <div class="content-header row">
                </div>
                <div class="content-overlay"></div>
                <div class="content-wrapper">
                    <div class="content-body">
                        <section class="row flexbox-container">
                            <div class="col-12 d-flex align-items-center justify-content-center">
                                <div class="col-lg-4 col-md-8 col-10 box-shadow-2 p-0">
                                    <div class="card border-grey border-lighten-3 px-1 py-1 m-0">
                                        <div class="card-header border-0">
                                            <div class="card-title text-center">
                                                <img src="<?= base_url() ?>app-assets/images/aludi/logo.png" alt="branding logo" width="50%">
                                            </div>
                                            <div class="card-body">
                                                <?php
                                                    if($this->session->flashdata('error_device')):
                                                ?>
                                                    <div class="alert alert-success">
                                                        <?= $this->session->flashdata('error_device') ?>
                                                    </div>
                                                <?php
                                                    endif;
                                                ?>
                                                <?php
                                                    if($this->session->flashdata('error')):
                                                ?>
                                                    <div class="alert alert-danger">
                                                        <?= $this->session->flashdata('error') ?>
                                                    </div>
                                                <?php
                                                    endif;
                                                ?>
                                                <form class="form-horizontal" action="<?= site_url('auth/login')?>" method="POST" autocomplete="off">
                                                    

                                                    <div class="form-group">
                                                    <!-- <label for="exampleInputEmail1">Email address:</label> -->
                                                    <input type="email" class="form-control" value="<?= set_value('email') ?>" name="email" placeholder="Email Perusahaan" required>
                                                    </div>
                                                    <fieldset class="form-group position-relative has-icon-left">
                                                        <div class="form-group">
                                                        <!-- <label for="user_password">Password:</label> -->
                                                        <div class="input-group">
                                                            <input type="password" name="password" id="user_password" class="form-control" placeholder="Password" required data-toggle="password">
                                                            <div class="input-group-append">
                                                            <span class="input-group-text"><i class="fa fa-eye"></i></span>
                                                            </div>
                                                        </div>
                                                        </div>
                                                        <small class="form-text text-danger">
                                                            <?= form_error('password') ?>
                                                        </small>
                                                    </fieldset>

                                                    <div class="form-group row">
                                                        <div class="g-recaptcha col-md-12" data-sitekey="<?php echo $this->config->item('google_key') ?>"></div> 
                                                    </div>
                                                    <?php if(isset($_GET['redirect']) && !empty($_GET['redirect'])): ?>
                                                        <input type="hidden" name="redirect" value="<?= $_GET['redirect'] ?>">
                                                    <?php endif ?>
                                                    <div class="form-group row">
                                                        <div class="col-sm-12 col-12 float-sm-right text-center text-sm-right"><a href="<?= site_url('/auth/forgotpassword') ?>" class="card-link">Lupa Password?</a></div>
                                                    </div>
                                                    <button type="submit" name="login" class="btn  btn-block" style="background-color: #009EC8;"><i class="ft-unlock"></i> Login</button>
                                                </form>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>

                    </div>
                </div>
            </div>
            <!-- END: Content-->

        </div>
        
        <!-- google captcha -->
        <script src='https://www.google.com/recaptcha/api.js'></script>

        <!-- BEGIN: Vendor JS-->
        <script src="<?= base_url() ?>app-assets/vendors/js/material-vendors.min.js"></script>
        <!-- BEGIN Vendor JS-->

        <!-- BEGIN: Page Vendor JS-->
        <script src="<?= base_url() ?>app-assets/vendors/js/forms/validation/jqBootstrapValidation.js"></script>
        <script src="<?= base_url() ?>app-assets/vendors/js/forms/icheck/icheck.min.js"></script>
        <!-- END: Page Vendor JS-->

        <!-- BEGIN: Theme JS-->
        <script src="<?= base_url() ?>app-assets/js/core/app-menu.js"></script>
        <script src="<?= base_url() ?>app-assets/js/core/app.js"></script>
        <!-- END: Theme JS-->

        <!-- BEGIN: Page JS-->
        <script src="<?= base_url() ?>app-assets/js/scripts/pages/material-app.js"></script>
        <script src="<?= base_url() ?>app-assets/js/scripts/forms/form-login-register.js"></script>
        <!-- END: Page JS-->

        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script type="text/javascript" src="<?= base_url() ?>app-assets/bootstrap-show-password.js"></script>
    </body>
</html>
