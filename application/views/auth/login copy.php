<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title>Login ALUDI</title>
    <link rel="apple-touch-icon" href="<?= base_url() ?>app-assets/images/aludi/logo.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?= base_url() ?>app-assets/images/aludi/logo.png">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i%7CQuicksand:300,400,500,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/fonts/material-icons/material-icons.css">

    <!-- BEGIN: Vendor CSS-->
    <!-- <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/vendors/css/material-vendors.min.css"> -->
    <!-- <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/vendors/css/forms/icheck/icheck.css"> -->
    <!-- <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/vendors/css/forms/icheck/custom.css"> -->
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/css/material.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/css/components.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/css/material-extended.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/css/material-colors.css">
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/css/core/menu/menu-types/material-vertical-menu-modern.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/css/pages/login-register.css">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/style.css">
    <!-- END: Custom CSS-->

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern material-vertical-layout material-layout vertical-collapsed-menu 1-column  menu-collapsed blank-page" data-open="click" data-menu="vertical-menu-modern" data-col="1-column">
    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-header row">
        </div>
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-body">
                <section class="row flexbox-container">
                    <div class="col-12 d-flex align-items-center justify-content-center">
                        <div class="col-lg-4 col-md-8 col-10 box-shadow-2 p-0">
                            <div class="card border-grey border-lighten-3 px-1 py-1 m-0">
                                <div class="card-header border-0">
                                    <div class="card-title text-center">
                                        <img src="<?= base_url() ?>app-assets/images/aludi/logo.png" alt="branding logo" width="50%">
                                    </div>
                                    <div class="card-body">
                                        <?php
                                            if($this->session->flashdata('error_device')):
                                        ?>
                                            <div class="alert alert-success">
                                                <?= $this->session->flashdata('error_device') ?>
                                            </div>
                                        <?php
                                            endif;
                                        ?>
                                        <?php
                                            if($this->session->flashdata('error')):
                                        ?>
                                            <div class="alert alert-danger">
                                                <?= $this->session->flashdata('error') ?>
                                            </div>
                                        <?php
                                            endif;
                                        ?>
                                        <form>
                                            <div class="form-group">
                                                <label>Password</label>
                                                <div class="input-group" id="show_hide_password">
                                                    <input class="form-control" type="password">
                                                    <div class="input-group-addon">
                                                        <a href=""><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        <form class="form-horizontal" action="<?= site_url('auth/login')?>" method="POST">
                                            <fieldset class="form-group position-relative has-icon-left">
                                                <input type="text" class="form-control" value="<?= set_value('email') ?>" name="email" placeholder="Email Perusahaan" required>
                                                <div class="form-control-position">
                                                    <i class="la la-user"></i>
                                                </div>
                                            </fieldset>
                                            <fieldset class="form-group position-relative has-icon-left">
                                                <input type="password" class="form-control" name="password" id="myInput" placeholder="Password" required>
                                                <div class="form-control-position">
                                                    <i class="la la-key"></i>
                                                </div>
                                                <small class="form-text text-danger">
                                                    <?= form_error('password') ?>
                                                </small>
                                            </fieldset>
                                            <div class="form-check">
                                                <input type="checkbox" class="form-check-input" onclick="myFunction()">
                                                <label class="form-check-label" for="exampleCheck1">Show Password</label>
                                            </div>
                                            <div class="form-group row">
                                                <div class="g-recaptcha col-md-12" data-sitekey="<?php echo $this->config->item('google_key') ?>"></div> 
                                            </div>
                                            <?php if(isset($_GET['redirect']) && !empty($_GET['redirect'])): ?>
                                                <input type="hidden" name="redirect" value="<?= $_GET['redirect'] ?>">
                                            <?php endif ?>
                                            <div class="form-group row">
                                                <div class="col-sm-12 col-12 float-sm-right text-center text-sm-right"><a href="<?= site_url('/auth/forgotpassword') ?>" class="card-link">Lupa Password?</a></div>
                                            </div>
                                            <button type="submit" name="login" class="btn  btn-block" style="background-color: #009EC8;"><i class="ft-unlock"></i> Login</button>
                                        </form>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
    <!-- END: Content-->


    <script>
    function myFunction() {
        var x = document.getElementById("myInput");
        if (x.type === "password") {
            x.type = "text";
        } else {
            x.type = "password";
        }
    }
    </script>

    <!-- google captcha -->
    <script src='https://www.google.com/recaptcha/api.js'></script>

    <!-- BEGIN: Vendor JS-->
    <script src="<?= base_url() ?>app-assets/vendors/js/material-vendors.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="<?= base_url() ?>app-assets/vendors/js/forms/validation/jqBootstrapValidation.js"></script>
    <script src="<?= base_url() ?>app-assets/vendors/js/forms/icheck/icheck.min.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="<?= base_url() ?>app-assets/js/core/app-menu.js"></script>
    <script src="<?= base_url() ?>app-assets/js/core/app.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="<?= base_url() ?>app-assets/js/scripts/pages/material-app.js"></script>
    <script src="<?= base_url() ?>app-assets/js/scripts/forms/form-login-register.js"></script>
    <!-- END: Page JS-->

</body>
<!-- END: Body-->

</html>