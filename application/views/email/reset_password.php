<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
 
        <title>Email Notif</title>
        <style>
            @import url('https://fonts.googleapis.com/css2?family=Nunito:wght@300;400;600;700');
        </style>
    </head>
    <body style="width: 650px; font-family: 'Nunito'; color: #616161;">
        <h1 style="font-size: 20px; margin-top: 17px;">Reset Password Request</h1>
        <div style="font-size: 15px;">

            <p>You receive this email cause the reset password was requested. If you not request it, please ignore this email</p>
            <p>If you want to reset your password, click link below <small>(link valid 1x24 hour)</small></p>

            <p style="text-align:center">
                <a href="<?= site_url('auth/new_password') ?>?t=<?= $token ?>">Reset Password</a>
            </p>
            <p style="margin-top: 3px;"><strong>nb* Mohon tidak membalas notice email ini</strong></p>
        </div>
    </body>
</html>
