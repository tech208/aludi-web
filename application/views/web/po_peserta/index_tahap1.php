<section id="number-tabs">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <form method="POST" enctype="multipart/form-data" action="<?= site_url('po_tahap_pendaftaran/saveTahap1') ?>" id="form-tahap1" class="form form-horizontal" >
                    <input type="hidden" id="id" name="id" value="<?=@$data->tahap_satu->id;?>">
                    <input type="hidden" id="id_personal" name="id_personal" value="<?=empty(@$data->tahap_satu->id_personal)?MasKaryawan()->id:@$data->tahap_satu->id_personal;?>" >
                    
                    <div class="card-header">
                        <h4 class="card-title">Formulir Tahap 1</h4>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="firstName1">Nama Platform*</label>
                                        <input type="text" class="form-control" id="nama_platform" name="nama_platform" value="<?=@$data->tahap_satu->nama_platform?>" required>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="lastName1">Nama Perusahaan*</label>
                                        <input type="text" class="form-control" id="nama_perusahaan" name="nama_perusahaan" value="<?=@$data->tahap_satu->nama_perusahaan?>" required>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="location1">Jenis Perusahaan</label>
                                        <select class="custom-select form-control" id="jenis_perusahaan" name="jenis_perusahaan" >
                                            <option value="">--pilih--</option>
                                            <option value="konvensional" <?=( "konvensional"==@$data->tahap_satu->jenis_perusahaan) ? "selected" : "" ;?>>Konvensional</option>
                                            <option value="syariah" <?=("syariah"==@$data->tahap_satu->jenis_perusahaan) ? "selected" : "" ;?>>Syariah</option>
                                            <option value="hybrid" <?=("hybrid"==@$data->tahap_satu->jenis_perusahaan) ? "selected" : "" ;?>>Hybrid</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="phoneNumber1">Tahun Berdiri</label>
                                        <input type="text" class="form-control" id="tahun_berdiri" name="tahun_berdiri" value="<?=@$data->tahap_satu->tahun_berdiri?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="phoneNumber1">Tahun Beroperasi</label>
                                        <input type="text" class="form-control" id="tahun_beroperasi" name="tahun_beroperasi" value="<?=@$data->tahap_satu->tahun_berdiri?>">
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="date1">Paid Up Capital/Modal</label>
                                        <input type="number" class="form-control" id="paid_up_capital" name="paid_up_capital" value="<?=@$data->tahap_satu->paid_up_capital?>">
                                    </div>
                                </div>
                            </div>
                            <br>
                            <h3>Alamat Perusahaan</h3>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="proposalTitle1">Alamat Perusahaan</label>
                                        <textarea class="form-control" id="alamat_perusahaan" name="alamat_perusahaan"><?=@$data->tahap_satu->alamat_perusahaan?></textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="eventType1">Negara</label>
                                        <select class="select2 form-control" id="id_negara" name="id_negara">
                                            <option value="1">Indonesia</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="eventType1">Provinsi</label>
                                        <select class="select2 form-control" id="id_provinsi" name="id_provinsi" onchange="getProvinsi(this);">
                                            <!-- <option value=" ">--pilih--</option> -->
                                            <?php foreach ($refProvinsi as $key => $value) : ?>
                                                <option value="<?=$value->id?>"><?=$value->nama?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="eventType1">Kabupaten/Kota</label>
                                        <select class="form-control select2" id="id_kota" name="id_kota">
                                            <!-- <option value=" ">--pilih--</option> -->
                                            <?php foreach ($refKabupaten as $key => $value) : ?>
                                                <option value="<?=$value->id?>"><?=$value->nama?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="jobTitle2">Kode Pos</label>
                                        <div class='input-group'>
                                            <input type='text' class="form-control " id="kode_pos" name="kode_pos" value="<?=@$data->tahap_satu->kode_pos?>"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <h3>Kontak Perusahaan</h3>
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="proposalTitle1">Nomor Telepon Perusahaan</label>
                                        <input type="text" class="form-control" id="nomor_telepon_perusahaan" name="nomor_telepon_perusahaan" value="<?=@$data->tahap_satu->nomor_telepon_perusahaan?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="proposalTitle1">Email Perusahaan</label>
                                        <input type="email" class="form-control" id="email_perusahaan" name="email_perusahaan" value="<?=@$data->tahap_satu->email_perusahaan?>"> 
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="proposalTitle1">Website (domain)</label>
                                        <input type="text" class="form-control" id="website" name="website" value="<?=@$data->tahap_satu->website?>">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="proposalTitle1">Instagram</label>
                                        <input type="text" class="form-control" id="instagram" name="instagram" value="<?=@$data->tahap_satu->instagram?>">
                                    </div>
                                </div>
                            </div>
                            <br>
                            <h3>Surat Perizinan</h3>
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="proposalTitle1">Nomor KBLI</label>
                                        <input type="text" class="form-control" id="nomor_kbli" name="nomor_kbli" value="<?=@$data->tahap_satu->nomor_kbli?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="proposalTitle1">Akta Pendirian</label>
                                        <input type="file" class="form-control" id="akta_pendirian" name="files[akta_pendirian]">
                                        <?php if (!empty(@$data->tahap_satu->akta_pendirian) AND file_exists("uploads/akta_pendirian/".@$data->tahap_satu->akta_pendirian)) :?>
                                        <div class=mt-1>
                                            <a href="<?=base_url("uploads/akta_pendirian/".@$data->tahap_satu->akta_pendirian)?>" target="_blank" class="btn btn-success ">Lihat</a> <?=@$data->tahap_satu->akta_pendirian?>
                                        </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="proposalTitle1">Akta Perubahan</label>
                                        <input type="file" class="form-control" id="akta_perubahan" name="files[akta_perubahan]">
                                        <?php if (!empty(@$data->tahap_satu->akta_perubahan) AND file_exists("uploads/akta_perubahan/".@$data->tahap_satu->akta_perubahan)) : ?>
                                        <div class=mt-1>
                                            <a href="<?=base_url("uploads/akta_perubahan/".@$data->tahap_satu->akta_perubahan)?>" target="_blank" class="btn btn-success ">Lihat</a> <?=@$data->tahap_satu->akta_perubahan?>
                                        </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="proposalTitle1">SK Kemenkumham Akta Pendirian</label>
                                        <input type="file" class="form-control" id="sk_kemenkumham_akta_pendirian" name="files[sk_kemenkumham_akta_pendirian]">
                                        <?php if (!empty(@$data->tahap_satu->sk_kemenkumham_akta_pendirian) AND file_exists("uploads/sk_kemenkumham_akta_pendirian/".@$data->tahap_satu->sk_kemenkumham_akta_pendirian)) : ?>
                                        <div class=mt-1>
                                            <a href="<?=base_url("uploads/sk_kemenkumham_akta_pendirian/".@$data->tahap_satu->sk_kemenkumham_akta_pendirian)?>" target="_blank" class="btn btn-success ">Lihat</a> <?=$data->sk_kemenkumham_akta_pendirian?>
                                        </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="proposalTitle1">Salinan Dokumen NPWP Perusahaan</label>
                                        <input type="file" class="form-control" id="salinan_dokumen_npwp_perusahaan" name="files[salinan_dokumen_npwp_perusahaan]">
                                        <?php if (!empty(@$data->tahap_satu->salinan_dokumen_npwp_perusahaan) AND file_exists("uploads/salinan_dokumen_npwp_perusahaan/".@$data->tahap_satu->salinan_dokumen_npwp_perusahaan)) : ?>
                                        <div class=mt-1>
                                            <a href="<?=base_url("uploads/salinan_dokumen_npwp_perusahaan/".@$data->tahap_satu->salinan_dokumen_npwp_perusahaan)?>" target="_blank" class="btn btn-success ">Lihat</a> <?=@$data->tahap_satu->salinan_dokumen_npwp_perusahaan?>
                                        </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="proposalTitle1">Struktur Organisasi Perusahaan</label>
                                        <input type="file" class="form-control" id="struktur_organisasi_perusahaan" name="files[struktur_organisasi_perusahaan]">
                                        <?php if (!empty(@$data->tahap_satu->struktur_organisasi_perusahaan) AND file_exists("uploads/struktur_organisasi_perusahaan/".@$data->tahap_satu->struktur_organisasi_perusahaan)) : ?>
                                        <div class=mt-1>
                                            <a href="<?=base_url("uploads/struktur_organisasi_perusahaan/".@$data->tahap_satu->struktur_organisasi_perusahaan)?>" target="_blank" class="btn btn-success ">Lihat</a> <?=@$data->tahap_satu->struktur_organisasi_perusahaan?>
                                        </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="proposalTitle1">Surat Pernyataan Kepatuhan</label>
                                        <input type="file" class="form-control" id="surat_pernyataan_kepatuhan" name="files[surat_pernyataan_kepatuhan]">
                                        <?php if (!empty(@$data->tahap_satu->surat_pernyataan_kepatuhan) AND file_exists("uploads/surat_pernyataan_kepatuhan/".@$data->tahap_satu->surat_pernyataan_kepatuhan)) : ?>
                                        <div class=mt-1>
                                            <a href="<?=base_url("uploads/surat_pernyataan_kepatuhan/".@$data->tahap_satu->surat_pernyataan_kepatuhan)?>" target="_blank" class="btn btn-success ">Lihat</a> <?=@$data->tahap_satu->surat_pernyataan_kepatuhan?>
                                        </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <?php if($data->statuse->grup == "6" AND !empty(@$data->tahap_satu->id)): ?>
                <div class="card-footer text-center">
                    <div class="form-group row ">
                        <div class="col-md-12">
                            <a href="<?=site_url("po_peserta/status/19/".@$data->tahap_satu->id.'/tahap1');//Verifikasi Tahap 1 Ditolak?>" class="btn btn-outline-danger mr-1" onclick="return confirm('Apakah kamu yakin ditolak?')">Tolak</a>
                            <a href="<?=site_url("po_peserta/status/18/".@$data->tahap_satu->id.'/tahap1');//Verifikasi Tahap 1 Diterima?>" class="btn btn-success  mr-1" onclick="return confirm('Apakah kamu yakin diterima?')">Terima</a>
                        </div>
                    </div>
                </div>
                <?php endif; ?>
            </div>
            
        </div>
    </div>
</section>


<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript">
    $("#form-tahap1 :input").prop("disabled", true);
</script>