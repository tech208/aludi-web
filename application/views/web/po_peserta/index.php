
<div class="row">
    <div class="col-12">
        <div class="card p-1">
            <div class="row">
                <div class="col-sm-6">
                    <a href="<?=site_url("po_peserta/printxlx")?>" class="btn btn-success" >Laporan excel</a>
                </div>
                <div class="col-sm-6 pull-right">
                    <form action="<?=site_url('/po_peserta/')?>" method="POST">
                        <div class="row">
                            <div class="col-md-6">
                            </div>
                            <div class="col-md-4">
                                <select name="status" id="status" class="form-control square select2" >
                                    <option value="" selected disabled>Pilih Status..</option>
                                    <?php foreach($refStatus as $l): ?>
                                        <option value="<?= $l['id'] ?>" <?=(@$status==$l['id'])?"selected":" ";?>><?= $l['name'] ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                            <div class="col-md-2">
                                <button class="btn btn-primary"><i class="la la-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div><hr>
            <div class="row">
                <div class="col-sm-12">
                    <table id="tbl_inventaris" class="table table-striped data_list">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Nama PT</th>
                                <th>Tanggal Mendaftar</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no=1; foreach($data as $val): ?>
                                <tr>
                                    <td><?= $no ?></td>
                                    <td><?= @$val->personals->name ?></td>
                                    <td><?= @$val->tahap_satu->nama_perusahaan ?></td>
                                    <td><?= MasTanggal(@$val->personals->pendaftarans->created_at,1); ?></td>
                                    <td><?= $val->statuse->name ?></td>
                                    <td>
                                        <a href="<?= site_url('po_peserta/detail/'. $val->id) ?>" class="btn btn-icon btn-pure dark" title="Mengirimkan data" >
                                            <i class="la la-file"></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php $no++; endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?= base_url('app-assets/vendors/js/forms/select/select2.full.min.js') ?>"></script>
<script src="<?= base_url('app-assets/vendors/js/tables/jquery.dataTables.min.js') ?>"></script>
<script src="<?= base_url('app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js') ?>"></script>
<script>
    $('.select2').select2();
    $('#tbl_inventaris').dataTable();
</script>