<?php
    // MasDebugPree($data->status,1);

    $css = "";
    $icon = "";
    if ($data->status=="16" OR $data->status=="20" OR $data->status=="24" OR $data->status=="29") {
        $css    = " alert-primary ";
        $icon   = " la-exclamation ";
    }
    elseif ($data->status=="18" OR $data->status=="22" OR $data->status=="26" OR $data->status=="29" OR $data->status=="31") {
        $css    = " alert-success ";
        $icon   = " la-check ";
    }
    elseif ($data->status=="17" OR $data->status=="21" OR $data->status=="25" OR $data->status=="28" OR $data->status=="30") {
        $css    = " alert-warning ";
        $icon   = " la-warning ";
    }
    elseif ($data->status=="19" OR $data->status=="23" OR $data->status=="27" ) {
        $css    = " alert-danger ";
        $icon   = " la-minus-circle ";
    }

?>

<div class="alert alert-icon-right alert-arrow-right <?=$css?> alert-dismissible mb-2 col-md-12" role="alert">
    <span class="alert-icon"><i class="la <?=$icon?>"></i></span>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    <strong><?=$data->statuse->name?></strong> Pada <a href="#" class="alert-link"><?=(!empty($data->updated_at)) ? MasTanggal($data->updated_at,1) : MasTanggal($data->created_at,1) ; ?></a>.
</div>
