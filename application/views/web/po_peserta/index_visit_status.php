<div class="row">
    <div class="col-md-4">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Jadwal Site Visit</h4>
            </div>
            <div class="card-content">
                <table border="0" class="table" >
                    <tr>
                        <td width='20%'><b>Tanggal</b></td>
                        <td>17/05/21 - 20 /05/21</td>
                    </tr>
                    <tr>
                        <td width='20%' valign='top'><b>Lokasi</b></td>
                        <td>UII, Jl. Taman Siswa No.158, RW.08, Wirogunan, Kec. Mergangsan, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55151</td>
                    </tr>
                </table>
                <hr/>
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Visitor</h4>
                    </div>
                    <div class="card-body">
                        <table border="0" class="table" >
                            <tr>
                                <th colspan="2">Visitor 1</th>
                            </tr>
                            <tr>
                                <td width='20%'><b>Nama </b></td>
                                <td>M. Reza Avesena</td>
                            </tr>
                            <tr>
                                <td width='20%'><b>Jabatan</b></td>
                                <td>Ketua Visitor</td>
                            </tr>
                        </table>
                        <hr/>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="row">
            <div class="col-12">
                <div class="card p-1">
                    <ul class="nav nav-pills nav-active-bordered-pill nav-justified">
                        <li class="nav-item">
                            <a href="<?= site_url('hr_aset/') ?>" class="nav-link active">Unduh Rundown</a>
                        </li>
                        <li class="nav-item">
                            <a href="<?= site_url('hr_aset/index_aset') ?>" class="nav-link ">Unduh Hasil Site Visit</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Site Visit</h4>
            </div>
            <div class="card-content collapse show">
            
                <div class="card-header">
                    <h4 class="card-title text-center">Langkah Pelaksanaan Site Visit</h4>
                </div>
                <div class="card-body">
                    <p class="text-muted">
                        1. Diskusi penentuan tanggal site visit
                            <br>* Diskusi dapat dilakukan melalui chat / email pihak ALUDI yang tercantum dibawah
                    </p>
                    <p class="text-muted">
                        2. ALUDI membuat jadwal dan rundown site visit sesuai dengan hasil diskusi dengan calon penyelenggara SCF dan mengunggah jadwal dan rundown tersebut pada website
                    </p>
                    <p class="text-muted">
                        3. Pelaksanaan Site Visit
                    </p>
                    <p class="text-muted">
                        4. Aludi mengunggah hasil site visit dalam platform
                            <br>* Jika penyelenggara dinyatakan tidak lolos site visit maka penyelenggara dapat mengajukan site visit kembali saat seluruh hasil ketidaksesuaian (temuan) telah diselesaikan, diskusi pengajuan dapat dilakukan melalui chat / email pihak ALUDI yang tercantum dibawah
                    </p>
                    <div class="title">
                        <b>Kontak Admin</b>
                        <br>
                        Wa/Call : 12312312<br>
                        Email : asa@email
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>