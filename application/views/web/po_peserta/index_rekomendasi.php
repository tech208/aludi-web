<?php
    if (@$data->status!=31 AND @$data->statuse->grup < 11 ):
?>
<div class="row d-flex">
    <div class="col-md-2">
    </div>
    <div class="col-md-8">

        <div class="card">
            <div class="card-content collapse show">
                <div class="card-body">

                        <div class="row text-center">
                            <div class="col-md-12 ">
                            <a href="<?=site_url("po_peserta/status/31/".@$data->id.'/selesai');//Selesai?>"  class="btn btn-success" onclick="return confirm('Apakah Anda Sudah Yakin?');">Selesai</a>
                            </div>
                        </div>
                        
                </div>
            </div>
        </div>
    </div>
</div>
<?php
    endif;
?>

<?php
    if (@$data->status==31 ):
?>
<div class="row d-flex">
    <div class="col-md-12">

        <div class="card">
            <div class="card-content collapse show">
                <div class="card-body">
                    <div class="row text-center">
                        <form method="POST" enctype="multipart/form-data" action="<?= site_url('po_peserta/save_rekomendasi_ojk') ?>" class="form form-horizontal" >
                            <input type="hidden" id="id" name="id" value="<?=@$data->id;?>">
                            <input type="hidden" id="id_rekomendasi" name="id_rekomendasi" value="<?=@$data->tahap_rekomendasi->id;?>">
                            <div class="card-content collapse show">
                                <?php if (!empty(@$data->tahap_rekomendasi->rekomendasis->nama_file) AND file_exists("uploads/rekomendasi_ojk/".@$data->tahap_rekomendasi->rekomendasis->nama_file)) : ?>
                                    <div class="row no-gutters mt-1">
                                        <div class="col-12 ">
                                            <span class="badge badge-success">Sudah Upload File Rekomendasi</span>
                                        </div>
                                    </div>

                                <?php else: ?>
                                <div class="card-footer">
                                    <div class="row ">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="proposalTitle1">File Rekomendasi</label>
                                                <input type="file" class="form-control" id="rekomendasi_ojk" name="files[rekomendasi_ojk]" required>
                                            </div>
                                        </div>
                                        <?= csrf_field(); ?>
                                        <div class="col-md-12">
                                            <button type="submit" class="btn btn-primary pull-right mr-1">Simpan</button>
                                        </div>
                                    </div>
                                </div>
                                <?php endif ?>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
    endif;
?>


<div class="row">
    <div class="col-12">
        <div class="card p-1">
            <div class="row">
                <div class="col-sm-12">
                </div>
            </div><hr>
            <div class="row">
                <div class="col-sm-12 table-responsive">
                    <table class="table table-striped data_list" width=100%>
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Deskripsi</th>
                                <th>File</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (!empty($data->tahap_rekomendasi->details) ):
                            $no=1; foreach(@$data->tahap_rekomendasi->details as $val): ?>
                                <tr>
                                    <td><?= $no ?></td>
                                    <td><?= @$val->nama ?></td>
                                    <td><?= @$val->deskripsi ?></td>
                                    <td>
                                        <a href="<?=base_url("uploads/rekomendasi_log/".@$val->nama_file)?>" target="_blank" class="btn btn-danger ">Download</a>
                                    </td>
                                    <td>
                                        <a href="<?=site_url('po_peserta/delete_visit/'. @$val->id )?>" class="btn btn-icon btn-pure dark" title="Menghapus data" onclick="return confirm('Yakin Menghapus?');">
                                            <i class="la la-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php $no++; endforeach;  endif;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
