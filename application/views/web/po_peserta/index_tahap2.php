<section id="number-tabs">
    <div class="row">
        <div class="col-12">
            <form method="POST" enctype="multipart/form-data" action="<?= site_url('po_tahap_pendaftaran/saveTahap2') ?>" id="form-tahap2" class="form form-horizontal" >
                <div class="card">
                    <input type="hidden" id="id" name="id" value="<?=@@$data->tahap_dua->id;?>">
                    <input type="hidden" id="id_personal" name="id_personal" value="<?=empty(@@$data->tahap_dua->id_personal)?MasKaryawan()->id:@@$data->tahap_dua->id_personal;?>" >

                    <div class="card-header">
                        <h4 class="card-title">Formulir Tahap 2</h4>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-header">
                            <h4 class="card-title text-center">Instruksi: Akan diisi oleh perwakilan platform</h4>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="firstName1">Nama Platform</label>
                                        <input type="text" class="form-control" id="nama_platform" name="nama_platform" value="<?=@@$data->tahap_dua->nama_platform?>" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="firstName1">Tanggal</label>
                                        <input type="date" class="form-control" id="tanggal" name="tanggal" value="<?=@@$data->tahap_dua->tanggal?>">
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="lastName1">1. Mohon tuliskan nama Domain (Domain wajib menggunakan “.ID”)</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" id="domain_nama" name="domain_nama" value="<?=@@$data->tahap_dua->domain_nama?>">
                                            </div>
                                            <div class="col-md-6">
                                                <input type="file" class="form-control" id="domain_bukti" name="files[domain_bukti]" >
                                                <?php if (!empty(@$data->tahap_dua->domain_bukti) AND file_exists("uploads/domain_bukti/".@$data->tahap_dua->domain_bukti)) : ?>
                                                <div class=mt-1>
                                                    <a href="<?=base_url("uploads/domain_bukti/".@$data->tahap_dua->domain_bukti)?>" target="_blank" class="btn btn-success ">Download</a> <?=@$data->tahap_dua->domain_bukti?>
                                                </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="lastName1">2. Apakah sudah mempunyai SOP APU PPT ? </label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="logo_ojk_nama" id="logo_ojk_nama_ya" value="iya"  <?=(@@$data->tahap_dua->logo_ojk_nama=="iya")?"checked":"";?> <?=(empty(@@$data->tahap_dua->logo_ojk_nama))?"checked":"";?>>
                                                            <label class="form-check-label" for="logo_ojk_nama_ya">
                                                                Iya
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="logo_ojk_nama" id="logo_ojk_tidak" value="tidak" <?=(@@$data->tahap_dua->logo_ojk_nama=="tidak")?"checked":"";?>>
                                                            <label class="form-check-label" for="logo_ojk_tidak">
                                                                Tidak
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="file" class="form-control" id="logo_ojk_bukti" name="files[logo_ojk_bukti]">
                                                <?php if (!empty(@$data->tahap_dua->logo_ojk_bukti) AND file_exists("uploads/logo_ojk_bukti/".@$data->tahap_dua->logo_ojk_bukti)) : ?>
                                                <div class=mt-1>
                                                    <a href="<?=base_url("uploads/logo_ojk_bukti/".@$data->tahap_dua->logo_ojk_bukti)?>" target="_blank" class="btn btn-success ">Download</a> <?=@$data->tahap_dua->logo_ojk_bukti?>
                                                </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="lastName1">3. Apakah domain dapat digunakan lebih dari 5 tahun ? Jika iya mohon untuk dapat melampirkan bukti</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" id="domain_usia_nama" name="domain_usia_nama"  value="<?=@@$data->tahap_dua->domain_usia_nama?>">
                                            </div>
                                            <div class="col-md-6">
                                                <input type="file" class="form-control" id="domain_usia_bukti" name="files[domain_usia_bukti]">
                                                <?php if (!empty(@$data->tahap_dua->domain_usia_bukti) AND file_exists("uploads/domain_usia_bukti/".@$data->tahap_dua->domain_usia_bukti)) : ?>
                                                <div class=mt-1>
                                                    <a href="<?=base_url("uploads/domain_usia_bukti/".@$data->tahap_dua->domain_usia_bukti)?>" target="_blank" class="btn btn-success ">Download</a> <?=@$data->tahap_dua->domain_usia_bukti?>
                                                </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="lastName1">4. Apakah ada catatan teknis IT yang tidak mengikuti ketentuan POJK 57/4/2020 pada tampilan depan?</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" id="logo_ojk_nama" name="logo_ojk_nama" value="<?=@@$data->tahap_dua->logo_ojk_nama?>">
                                            </div>
                                            <div class="col-md-6">
                                                <input type="file" class="form-control" id="domain_bukti" name="files[domain_bukti]">
                                                <?php if (!empty(@$data->tahap_dua->domain_bukti) AND file_exists("uploads/domain_bukti/".@$data->tahap_dua->domain_bukti)) : ?>
                                                <div class=mt-1>
                                                    <a href="<?=base_url("uploads/domain_bukti/".@$data->tahap_dua->domain_bukti)?>" target="_blank" class="btn btn-success ">Download</a> <?=@$data->tahap_dua->domain_bukti?>
                                                </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="lastName1">5. Apakah server berada pada indonesia ? Jika iya lampirkan dokumen pendukung</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" id="server_lokasi_nama" name="server_lokasi_nama" value="<?=@@$data->tahap_dua->server_lokasi_nama?>">
                                            </div>
                                            <div class="col-md-6">
                                                <input type="file" class="form-control" id="server_lokasi_bukti" name="files[server_lokasi_bukti]">
                                                <?php if (!empty(@$data->tahap_dua->server_lokasi_bukti) AND file_exists("uploads/server_lokasi_bukti/".@$data->tahap_dua->server_lokasi_bukti)) : ?>
                                                <div class=mt-1>
                                                    <a href="<?=base_url("uploads/server_lokasi_bukti/".@$data->tahap_dua->server_lokasi_bukti)?>" target="_blank" class="btn btn-success ">Download</a> <?=@$data->tahap_dua->server_lokasi_bukti?>
                                                </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="lastName1">6. Apakah Platform sudah mendapatkan sertifikat SSL ? Jika iya mohon lampirkan dokumen pendukung</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" id="platform_ssl_nama" name="platform_ssl_nama" value="<?=@@$data->tahap_dua->platform_ssl_nama?>">
                                            </div>
                                            <div class="col-md-6">
                                                <input type="file" class="form-control" id="platform_ssl_bukti" name="files[platform_ssl_bukti]">
                                                <?php if (!empty(@$data->tahap_dua->platform_ssl_bukti) AND file_exists("uploads/platform_ssl_bukti/".@$data->tahap_dua->platform_ssl_bukti)) : ?>
                                                <div class=mt-1>
                                                    <a href="<?=base_url("uploads/platform_ssl_bukti/".@$data->tahap_dua->platform_ssl_bukti)?>" target="_blank" class="btn btn-success ">Download</a> <?=@$data->tahap_dua->platform_ssl_bukti?>
                                                </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="lastName1">7. Apakah penyelenggara sudah mempunyai SOP pasar sekunder?</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="ssl_detail_nama" id="ssl_detail_nama_ya" value="iya" <?=(@@$data->tahap_dua->ssl_detail_nama=="iya")?"checked":"";?> <?=(empty(@@$data->tahap_dua->ssl_detail_nama))?"checked":"";?>>
                                                            <label class="form-check-label" for="logo_ojk_nama_ya">
                                                                Iya
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="ssl_detail_nama" id="ssl_detail_nama_tidak"  value="tidak" <?=(@@$data->tahap_dua->ssl_detail_nama=="tidak")?"checked":"";?> >
                                                            <label class="form-check-label" for="ssl_detail_nama_tidak">
                                                                Tidak
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="file" class="form-control" id="ssl_detail_bukti" name="files[ssl_detail_bukti]">
                                                <?php if (!empty(@$data->tahap_dua->ssl_detail_bukti) AND file_exists("uploads/ssl_detail_bukti/".@$data->tahap_dua->ssl_detail_bukti)) : ?>
                                                <div class=mt-1>
                                                    <a href="<?=base_url("uploads/ssl_detail_bukti/".@$data->tahap_dua->ssl_detail_bukti)?>" target="_blank" class="btn btn-success ">Download</a> <?=@$data->tahap_dua->ssl_detail_bukti?>
                                                </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="lastName1">8. Apakah Apps (Android/IOS) hanya dapat mendapatkan akses terhadap Camera, Microphone, dan Location ? Jika iya mohon lampirkan dokumen pendukung</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" id="akses_camera_nama" name="akses_camera_nama" value="<?=@@$data->tahap_dua->akses_camera_nama?>">
                                            </div>
                                            <div class="col-md-6">
                                                <input type="file" class="form-control"  id="akses_camera_bukti" name="files[akses_camera_bukti]">
                                                <?php if (!empty(@$data->tahap_dua->akses_camera_bukti) AND file_exists("uploads/akses_camera_bukti/".@$data->tahap_dua->akses_camera_bukti)) : ?>
                                                <div class=mt-1>
                                                    <a href="<?=base_url("uploads/akses_camera_bukti/".@$data->tahap_dua->akses_camera_bukti)?>" target="_blank" class="btn btn-success ">Download</a> <?=@$data->tahap_dua->akses_camera_bukti?>
                                                </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="lastName1">9. Apakah penyelenggara sudah mempunyai SOP prospektus ?</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="technical_apps_ssl_nama" id="technical_apps_ssl_nama_ya" value="iya" <?=(@@$data->tahap_dua->technical_apps_ssl_nama=="iya")?"checked":"";?> <?=(empty(@@$data->tahap_dua->technical_apps_ssl_nama))?"checked":"";?>>
                                                            <label class="form-check-label" for="technical_apps_ssl_nama_ya">
                                                                Iya
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="technical_apps_ssl_nama" id="technical_apps_ssl_nama_tidak"  value="tidak" <?=(@@$data->tahap_dua->technical_apps_ssl_nama=="tidak")?"checked":"";?>>
                                                            <label class="form-check-label" for="technical_apps_ssl_nama_tidak">
                                                                Tidak
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="technical_apps_ssl_nama" id="technical_apps_ssl_nama_na"  value="n/a" <?=(@@$data->tahap_dua->technical_apps_ssl_nama=="n/a")?"checked":"";?> >
                                                            <label class="form-check-label" for="technical_apps_ssl_nama_na">
                                                                N/A
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="file" class="form-control" id="technical_apps_ssl_bukti" name="files[technical_apps_ssl_bukti]">
                                                <?php if (!empty(@$data->tahap_dua->technical_apps_ssl_bukti) AND file_exists("uploads/technical_apps_ssl_bukti/".@$data->tahap_dua->technical_apps_ssl_bukti)) : ?>
                                                <div class=mt-1>
                                                    <a href="<?=base_url("uploads/technical_apps_ssl_bukti/".@$data->tahap_dua->technical_apps_ssl_bukti)?>" target="_blank" class="btn btn-success ">Download</a> <?=@$data->tahap_dua->technical_apps_ssl_bukti?>
                                                </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="lastName1">10. Apakah platform sudah menggunakan layanan masyarakat ? jika iya maka lampirkan SC, SOP yang berlaku</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" id="customer_service_nama" name="customer_service_nama" value="<?=@@$data->tahap_dua->customer_service_nama?>">
                                            </div>
                                            <div class="col-md-6">
                                                <input type="file" class="form-control" id="customer_service_bukti" name="files[customer_service_bukti]">
                                                <?php if (!empty(@$data->tahap_dua->customer_service_bukti) AND file_exists("uploads/customer_service_bukti/".@$data->tahap_dua->customer_service_bukti)) : ?>
                                                <div class=mt-1>
                                                    <a href="<?=base_url("uploads/customer_service_bukti/".@$data->tahap_dua->customer_service_bukti)?>" target="_blank" class="btn btn-success ">Download</a> <?=@$data->tahap_dua->customer_service_bukti?>
                                                </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="lastName1">13. Apakah platform mempunyai SOP keamanan terhadap platform ?</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" id="sop_keamanan_nama" name="sop_keamanan_nama" value="<?=@@$data->tahap_dua->sop_keamanan_nama?>">
                                            </div>
                                            <div class="col-md-6">
                                                <input type="file" class="form-control" id="sop_keamanan_bukti" name="files[sop_keamanan_bukti]">
                                                <?php if (!empty(@$data->tahap_dua->sop_keamanan_bukti) AND file_exists("uploads/sop_keamanan_bukti/".@$data->tahap_dua->sop_keamanan_bukti)) : ?>
                                                <div class=mt-1>
                                                    <a href="<?=base_url("uploads/sop_keamanan_bukti/".@$data->tahap_dua->sop_keamanan_bukti)?>" target="_blank" class="btn btn-success ">Download</a> <?=@$data->tahap_dua->sop_keamanan_bukti?>
                                                </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="lastName1">14. Apakah platform menggunakan sistem validasi menggunakan SMS/Email/PIN confirm ? jika mohon lampirkan dokumen pendukung terkait hal ini dan mohon diperjelas digunakan pada bagian mana. </label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" id="validasi_sms_nama" name="validasi_sms_nama" value="<?=@@$data->tahap_dua->validasi_pin_nama?>">
                                            </div>
                                            <div class="col-md-6">
                                                <input type="file" class="form-control" id="validasi_sms_bukti" name="files[validasi_sms_bukti]">
                                                <?php if (!empty(@$data->tahap_dua->validasi_pin_bukti) AND file_exists("uploads/validasi_sms_bukti/".@$data->tahap_dua->validasi_pin_bukti)) : ?>
                                                <div class=mt-1>
                                                    <a href="<?=base_url("uploads/validasi_pin_bukti/".@$data->tahap_dua->validasi_pin_bukti)?>" target="_blank" class="btn btn-success ">Download</a> <?=@$data->tahap_dua->validasi_pin_bukti?>
                                                </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="lastName1">15. Apakah sudah mempunyai SOP obligasi dan sukuk?.</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" id="validasi_pin_nama" name="validasi_pin_nama" value="<?=@@$data->tahap_dua->validasi_pin_nama?>">
                                            </div>
                                            <div class="col-md-6">
                                                <input type="file" class="form-control" id="validasi_pin_bukti" name="files[validasi_pin_bukti]">
                                                <?php if (!empty(@$data->tahap_dua->validasi_pin_bukti) AND file_exists("uploads/validasi_pin_bukti/".@$data->tahap_dua->validasi_pin_bukti)) : ?>
                                                <div class=mt-1>
                                                    <a href="<?=base_url("uploads/validasi_pin_bukti/".@$data->tahap_dua->validasi_pin_bukti)?>" target="_blank" class="btn btn-success ">Download</a> <?=@$data->tahap_dua->validasi_pin_bukti?>
                                                </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="lastName1">16. Apakah platform menggunakan fitur KYC ( Know your customer ) ? Jika Mohon lampirkan dokumen pendukung dan lampirkan list KYC</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" id="kyc_nama" name="kyc_nama" value="<?=@@$data->tahap_dua->kyc_nama?>">
                                            </div>
                                            <div class="col-md-6">
                                                <input type="file" class="form-control" id="kyc_bukti" name="files[kyc_bukti]">
                                                <?php if (!empty(@$data->tahap_dua->kyc_bukti) AND file_exists("uploads/kyc_bukti/".@$data->tahap_dua->kyc_bukti)) : ?>
                                                <div class=mt-1>
                                                    <a href="<?=base_url("uploads/kyc_bukti/".@$data->tahap_dua->kyc_bukti)?>" target="_blank" class="btn btn-success ">Download</a> <?=@$data->tahap_dua->kyc_bukti?>
                                                </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="lastName1">17. Berapakah SLA (%) dari platform ? (mohon unggah bukti SLA / perjanjian)</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" id="sla_nama" name="sla_nama" value="<?=@@$data->tahap_dua->sla_nama?>">
                                            </div>
                                            <div class="col-md-6">
                                                <input type="file" class="form-control" id="sla_bukti" name="files[sla_bukti]">
                                                <?php if (!empty(@$data->tahap_dua->sla_bukti) AND file_exists("uploads/sla_bukti/".@$data->tahap_dua->sla_bukti)) : ?>
                                                <div class=mt-1>
                                                    <a href="<?=base_url("uploads/sla_bukti/".@$data->tahap_dua->sla_bukti)?>" target="_blank" class="btn btn-success ">Download</a> <?=@$data->tahap_dua->sla_bukti?>
                                                </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="lastName1">18. Apakah platform sudah mempunyai sertifikat DRC ( Disaster Recovery Center ), dimanakah lokasi Data Center dan Disaster Recovery Center tersebut?*</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" id="sertifikat_drc_nama" name="sertifikat_drc_nama" value="<?=@@$data->tahap_dua->sertifikat_drc_nama?>">
                                            </div>
                                            <div class="col-md-6">
                                                <input type="file" class="form-control" id="sertifikat_drc_bukti" name="files[sertifikat_drc_bukti]">
                                                <?php if (!empty(@$data->tahap_dua->sertifikat_drc_bukti) AND file_exists("uploads/sertifikat_drc_bukti/".@$data->tahap_dua->sertifikat_drc_bukti)) : ?>
                                                <div class=mt-1>
                                                    <a href="<?=base_url("uploads/sertifikat_drc_bukti/".@$data->tahap_dua->sertifikat_drc_bukti)?>" target="_blank" class="btn btn-success ">Download</a> <?=@$data->tahap_dua->sertifikat_drc_bukti?>
                                                </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="lastName1">19. Apakah platform sudah terdafatar dalam ISO 27001 ? Jika iya mohon dapat melampirkan sertifikat sebagai dokumen pendukung</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" id="iso_nama" name="iso_nama" value="<?=@@$data->tahap_dua->iso_nama?>">
                                            </div>
                                            <div class="col-md-6">
                                                <input type="file" class="form-control" id="iso_bukti" name="files[iso_bukti]">
                                                <?php if (!empty(@$data->tahap_dua->iso_bukti) AND file_exists("uploads/iso_bukti/".@$data->tahap_dua->iso_bukti)) : ?>
                                                <div class=mt-1>
                                                    <a href="<?=base_url("uploads/iso_bukti/".@$data->tahap_dua->iso_bukti)?>" target="_blank" class="btn btn-success ">Download</a> <?=@$data->tahap_dua->iso_bukti?>
                                                </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="lastName1">20. Apakah platform sudah mendapat validitas dari KOMINFO sebagai PSE ( Penyelenggara System Elektronik ) jika ada tolong lampirkan buktinya</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" id="validasi_kominfo_nama" name="validasi_kominfo_nama" value="<?=@@$data->tahap_dua->validasi_kominfo_nama?>">
                                            </div>
                                            <div class="col-md-6">
                                                <input type="file" class="form-control" id="validasi_kominfo_bukti" name="files[validasi_kominfo_bukti]">
                                                <?php if (!empty(@$data->tahap_dua->validasi_kominfo_bukti) AND file_exists("uploads/validasi_kominfo_bukti/".@$data->tahap_dua->validasi_kominfo_bukti)) : ?>
                                                <div class=mt-1>
                                                    <a href="<?=base_url("uploads/validasi_kominfo_bukti/".@$data->tahap_dua->validasi_kominfo_bukti)?>" target="_blank" class="btn btn-success ">Download</a> <?=@$data->tahap_dua->validasi_kominfo_bukti?>
                                                </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="lastName1">21. Apakah platform sudah melaksanakan test penetrasi dari pihak ke 3 ? jika iya tolong dilampirkan sertifikatnya</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" id="test_penetrasi_nama" name="test_penetrasi_nama" value="<?=@@$data->tahap_dua->test_penetrasi_nama?>">
                                            </div>
                                            <div class="col-md-6">
                                                <input type="file" class="form-control" id="test_penetrasi_bukti" name="files[test_penetrasi_bukti]">
                                                <?php if (!empty(@$data->tahap_dua->test_penetrasi_bukti) AND file_exists("uploads/test_penetrasi_bukti/".@$data->tahap_dua->test_penetrasi_bukti)) : ?>
                                                <div class=mt-1>
                                                    <a href="<?=base_url("uploads/test_penetrasi_bukti/".@$data->tahap_dua->test_penetrasi_bukti)?>" target="_blank" class="btn btn-success ">Download</a> <?=@$data->tahap_dua->test_penetrasi_bukti?>
                                                </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <hr>
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="firstName1">Nama Saya</label>
                                            <input type="text" class="form-control" id="nama_saya" name="nama_saya" value="<?=@@$data->tahap_dua->nama_saya?>" onkeyup="myNama()">
                                        </div>
                                    </div>

                                    <div class="text-left">
                                        Saya, Bapak/Ibu <div class="nama"></div> menyatakan bahwa keterangan yang saya berikan dalam aplikasi ini adalah benar.
                                        <br> Saya memberi kuasa Bapak/Ibu <div class="nama"></div> untuk memberikan informasi tambahan yang diperlukan.
                                        Saya memahami bahwa pernyataan palsu atau informasi palsu yang diberikan, <br/> pelanggaran serius yang dapat mengakibatkan tidak lulusnya tahap 2 it review ini. 
                                    </div>
                                </div>

                                
                                <div class="col-md-12 text-center">
                                    <div class="form-group">
                                        <label for="firstName1">TTD Digital Perusahaan</label>
                                        <input type="file" class="form-control" id="ttd_perusahaan" name="files[ttd_perusahaan]">
                                                <?php if (!empty(@$data->tahap_dua->ttd_perusahaan) AND file_exists("uploads/ttd_perusahaan/".@$data->tahap_dua->ttd_perusahaan)) : ?>
                                                <div class=mt-1>
                                                    <a href="<?=base_url("uploads/ttd_perusahaan/".@$data->tahap_dua->ttd_perusahaan)?>" target="_blank" class="btn btn-success ">Download</a> <?=@$data->tahap_dua->ttd_perusahaan?>
                                                </div>
                                                <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <?php if($data->statuse->grup == "8" AND !empty(@$data->tahap_dua->id)): ?>
            <div class="card-footer text-center">
                <div class="form-group row ">
                    <div class="col-md-12">
                        <a href="<?=site_url("po_peserta/status/27/".@$data->tahap_dua->id.'/tahap2');//Verifikasi Tahap 2 Ditolak?>" class="btn btn-outline-danger mr-1" onclick="return confirm('Apakah kamu yakin ditolak?')">Tolak</a>
                        <a href="<?=site_url("po_peserta/status/26/".@$data->tahap_dua->id.'/tahap2');//Verifikasi Tahap 2 Diterima?>" class="btn btn-success  mr-1" onclick="return confirm('Apakah kamu yakin diterima?')">Terima</a>
                    </div>
                </div>
            </div>
            <?php endif; ?>
        </div>
        
    </div>
</section>

<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript">
    $("#form-tahap2 :input").prop("disabled", true);
</script>
