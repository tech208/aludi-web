
<!-- account setting page start -->
<section id="page-account-settings">
    <div class="row">
        <?php
            $this->load->view('web/po_peserta/index_status');
        ?>
        <!-- left menu section -->
        <div class="col-md-3 mb-2 mb-md-0">
            <div class="bug-list-sidebar-content">
                <!-- Predefined Views -->
                <div class="card">
                    <div class="card-head">
                        <div class="media p-1">
                            <div class="media-body media-middle">
                                <h3 class="media-heading"><?=@$data->personals->name?></h3>
                            </div>
                        </div>
                    </div>

                    <!-- contacts view -->
                    <div class="card-body border-top-blue-grey border-top-lighten-5">
                        <div class="list-group">
                            <ul class="nav nav-pills flex-column mt-md-0 mt-1">
                                <?php
                                    if(MasKaryawan()->user->role_id!=3):
                                ?>
                                <li class="nav-item">
                                    <a class="nav-link d-flex active" id="account-pill-general" data-toggle="pill" href="#account-vertical-pendaftaran" aria-expanded="true">
                                        <i class="ft-globe mr-50"></i>
                                        Pendaftaran
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link d-flex" id="account-pill-password" data-toggle="pill" href="#account-vertical-tahap1" aria-expanded="false">
                                        <i class="ft-upload mr-50"></i>
                                        Tahap 1
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link d-flex" id="account-pill-info" data-toggle="pill" href="#account-vertical-pembayaran" aria-expanded="false">
                                    <i class="ft-paperclip mr-50"></i>
                                        Pembayaran
                                    </a>
                                </li>
                                <?php
                                    endif;
                                ?>

                                <li class="nav-item">
                                    <a class="nav-link d-flex" id="account-pill-social" data-toggle="pill" href="#account-vertical-tahap2" aria-expanded="false">
                                        <i class="ft-upload mr-50"></i>
                                        Tahap 2
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link d-flex" id="account-pill-connections" data-toggle="pill" href="#account-vertical-visit" aria-expanded="false">
                                        <i class="ft-user-check mr-50"></i>
                                        Visit
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link d-flex" id="account-pill-notifications" data-toggle="pill" href="#account-vertical-rekomendasikan" aria-expanded="false">
                                        <i class="ft-compass mr-50"></i>
                                        Rekomendasi & Berkas
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>


                </div>
                <!--/ Predefined Views -->
            </div>


        </div>
        <!-- right content section -->
        <div class="col-md-9">
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="account-vertical-pendaftaran" aria-labelledby="account-pill-general" aria-expanded="true">
                                <?php
                                    $this->load->view('web/po_peserta/index_pendaftaran');
                                ?>
                            </div>
                            <div class="tab-pane fade " id="account-vertical-tahap1" role="tabpanel" aria-labelledby="account-pill-password" aria-expanded="false">
                                <?php
                                   $this->load->view('web/po_peserta/index_tahap1');
                                ?>
                            </div>
                            <div class="tab-pane fade" id="account-vertical-pembayaran" role="tabpanel" aria-labelledby="account-pill-info" aria-expanded="false">
                                <?php
                                   $this->load->view('web/po_peserta/index_pembayaran');
                                ?>
                            </div>
                            <div class="tab-pane fade " id="account-vertical-tahap2" role="tabpanel" aria-labelledby="account-pill-social" aria-expanded="false">
                                <?php
                                   $this->load->view('web/po_peserta/index_tahap2');
                                ?>
                            </div>
                            <div class="tab-pane fade" id="account-vertical-visit" role="tabpanel" aria-labelledby="account-pill-connections" aria-expanded="false">
                                <?php
                                    $this->load->view('web/po_peserta/index_visit');
                                ?>
                            </div>
                            <div class="tab-pane fade" id="account-vertical-rekomendasikan" role="tabpanel" aria-labelledby="account-pill-notifications" aria-expanded="false">
                                <?php
                                   $this->load->view('web/po_peserta/index_rekomendasi');
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- account setting page end -->