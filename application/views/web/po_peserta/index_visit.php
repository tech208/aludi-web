<h1>Data List Visit</h1>
<div class="row " id="tambah" >
    <div class="col-12">
        <div class="card p-1">
            <form method="POST" action="<?= site_url('po_peserta/saveVisit/') ?>"  class="form form-horizontal row-separator">
                <input type="hidden" name="id_tahap" value="<?= @$data->id ?>" id="id" class="form-control">
                <div class="form-group">
                    <label for="exampleInputEmail1">Tujuan Visit</label>
                    <input type="text" class="form-control" id="nama" name="nama" aria-describedby="emailHelp" placeholder="Masukan Tujuan Visit">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Lokasi</label>
                    <input type="text" class="form-control" id="lokasi" name="lokasi" aria-describedby="emailHelp" placeholder="Masukan Lokasi">
                </div>
                <div class=" row">
                    <div class="col-md-12">
                        <label for="inputEmail3" class="col-form-label">Mulai</label>
                    </div>
                    <div class="col-md-7">
                        <div class="form-group">
                            <input type="text" name="tanggal_mulai" id="tanggal_mulai" class="form-control datepicker" required>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <input type="time" class="form-control " id="tanggal_mulai_jam" name="tanggal_mulai_jam" placeholder="Waktu" value="<?=date("H:i"); ?>" required>
                        </div>
                    </div>
                </div>
                <div class=" row">
                    <div class="col-md-12">
                        <label for="inputEmail3" class="col-form-label">Selesai</label>
                    </div>
                    <div class="col-md-7">
                        <div class="form-group">
                            <input type="text" name="tanggal_selesai" id="tanggal_selesai" class="form-control datepicker" required>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <input type="time" class="form-control " id="tanggal_selesai_jam" name="tanggal_selesai_jam" placeholder="Waktu" value="<?=date("H:i"); ?>" required>
                        </div>
                    </div>
                </div>
                <br>
                <table class="table">
                    <tr>
                        <th></th>
                        <th>Nama</th>
                        <th>Jabatan</th>
                    </tr>
                    <tr>
                        <th>Visitor 1</th>
                        <td><input type="text" class="form-control" id="visitor_nama_1" name="visitor[1][nama]"></td>
                        <td><input type="text" class="form-control" id="visitor_jabatan_1" name="visitor[1][jabatan]"></td>
                    </tr>
                    <tr>
                        <th>Visitor 2</th>
                        <td><input type="text" class="form-control" id="visitor_nama_2" name="visitor[2][nama]"></td>
                        <td><input type="text" class="form-control" id="visitor_jabatan_2" name="visitor[2][jabatan]"></td>
                    </tr>
                    <tr>
                        <th>Visitor 3</th>
                        <td><input type="text" class="form-control" id="visitor_nama_3" name="visitor[3][nama]"></td>
                        <td><input type="text" class="form-control" id="visitor_jabatan_3" name="visitor[3][jabatan]"></td>
                    </tr>
                </table>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <button type="reset" class="btn ">Reset</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card p-1">
            <div class="row">
                <div class="col-sm-12">
                    <a href="#" id="showItem" class="btn btn-success">Tambah</a>
                    <?php
                        if (@$data->statuse->grup == 9 ):
                    ?>
                    <a href="<?=site_url("po_peserta/status/30/".@$data->id.'/rekomendasi');//Menunggu Proses Rekomendasi?>" id="showItema" class="btn alert-primary" onclick="return confirm('Yakin Merekomendasikan?');">Rekomendasi</a>
                    <?php
                        endif;
                    ?>
                </div>
            </div><hr>
            <div class="row">
                <div class="col-sm-12 table-responsive">
                    <table class="table table-striped data_list" width=100%>
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Lokasi</th>
                                <th>Visitor</th>
                                <th>Tanggal Mulai</th>
                                <th>Tanggal Selesai</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (@$data->tahap_visit->count() > 0 ):
                            $no=1; foreach(@$data->tahap_visit as $val): ?>
                                <tr>
                                    <td><?= $no ?></td>
                                    <td><?= @$val->nama ?></td>
                                    <td><?= @$val->lokasi ?></td>
                                    <td>
                                        <ul>
                                        <?php
                                            if (@$val->visitors->count() > 0 ):
                                            foreach(@$val->visitors as $val_visitors): 
                                        ?>
                                            <li><b><?=$val_visitors->nama."</b> Sebagai ".$val_visitors->jabatan?></li>
                                        <?php endforeach;  endif;?>
                                        </ul>
                                    </td>
                                    <td><?= tgl_indo(@$val->tanggal_mulai)."  ".@$val->tanggal_mulai_jam  ?></td>
                                    <td><?= tgl_indo(@$val->tanggal_selesai)."  ".@$val->tanggal_selesai_jam ?></td>
                                    <td>
                                        <a href="<?=site_url('po_peserta/delete_visit/'. @$val->id )?>" class="btn btn-icon btn-pure dark" title="Menghapus data" onclick="return confirm('Yakin Menghapus?');">
                                            <i class="la la-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php $no++; endforeach;  endif;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<h1>Data List Hasil Visit</h1>
<div class="row " id="tambah_hasil" >
    <div class="col-12">
        <div class="card p-1">
            <form method="POST" action="<?= site_url('po_peserta/saveVisitHasil/') ?>"  class="form form-horizontal row-separator" enctype="multipart/form-data">
                <input type="hidden" name="id_tahap" value="<?= @$data->id ?>" id="id" class="form-control">
                <div class="form-group">
                    <label for="exampleInputEmail1">Nama</label>
                    <input type="text" class="form-control" id="nama" name="nama" aria-describedby="emailHelp" placeholder="Masukan Nama">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Lampiran</label>
                    <input class="form-control " type="file" id="lampiran" name="lampiran" aria-describedby="emailHelp" placeholder="Upload File">
                </div>
                <!-- <div class=" row">
                    <div class="col-md-12">
                        <label for="inputEmail3" class="col-form-label">Mulai</label>
                    </div>
                    <div class="col-md-7">
                        <div class="form-group">
                            <input type="text" name="tanggal_mulai" id="tanggal_mulai" class="form-control datepicker" required>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <input type="time" class="form-control " id="tanggal_mulai_jam" name="tanggal_mulai_jam" placeholder="Waktu" value="<?=date("H:i"); ?>" required>
                        </div>
                    </div>
                </div>
                <div class=" row">
                    <div class="col-md-12">
                        <label for="inputEmail3" class="col-form-label">Selesai</label>
                    </div>
                    <div class="col-md-7">
                        <div class="form-group">
                            <input type="text" name="tanggal_selesai" id="tanggal_selesai" class="form-control datepicker" required>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <input type="time" class="form-control " id="tanggal_selesai_jam" name="tanggal_selesai_jam" placeholder="Waktu" value="<?=date("H:i"); ?>" required>
                        </div>
                    </div>
                </div>
                <br>
                <table class="table">
                    <tr>
                        <th></th>
                        <th>Nama</th>
                        <th>Jabatan</th>
                    </tr>
                    <tr>
                        <th>Visitor 1</th>
                        <td><input type="text" class="form-control" id="visitor_nama_1" name="visitor[1][nama]"></td>
                        <td><input type="text" class="form-control" id="visitor_jabatan_1" name="visitor[1][jabatan]"></td>
                    </tr>
                    <tr>
                        <th>Visitor 2</th>
                        <td><input type="text" class="form-control" id="visitor_nama_2" name="visitor[2][nama]"></td>
                        <td><input type="text" class="form-control" id="visitor_jabatan_2" name="visitor[2][jabatan]"></td>
                    </tr>
                    <tr>
                        <th>Visitor 3</th>
                        <td><input type="text" class="form-control" id="visitor_nama_3" name="visitor[3][nama]"></td>
                        <td><input type="text" class="form-control" id="visitor_jabatan_3" name="visitor[3][jabatan]"></td>
                    </tr> 
                </table>-->
                <br>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <button type="reset" class="btn ">Reset</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card p-1">
            <div class="row">
                <div class="col-sm-12">
                    <a href="#" id="showItem_hasil" class="btn btn-success">Tambah</a>
                    <!-- <?php
                        if (@$data->statuse->grup == 9 ):
                    ?>
                    <a href="<?=site_url("po_peserta/status/30/".@$data->id.'/rekomendasi');//Menunggu Proses Rekomendasi?>" id="showItema" class="btn alert-primary" onclick="return confirm('Yakin Merekomendasikan?');">Rekomendasi</a>
                    <?php
                        endif;
                    ?> -->
                </div>
            </div><hr>
            <div class="row">
                <div class="col-sm-12 table-responsive">
                    <table class="table table-striped data_list" width=100%>
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Lampiran</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (@$data_hasil->count() > 0 ):
                            $no=1; foreach(@$data_hasil as $val): ?>
                                <tr>
                                    <td><?= $no ?></td>
                                    <td><?= @$val->nama ?></td>
                                    <td>
                                        <?php if (!empty($val->lampiran) AND file_exists("uploads/visit_hasil/".$val->lampiran)) : ?>
                                                        <a href="<?=base_url("uploads/visit_hasil/".$val->lampiran)?>" target="_blank" class="btn btn-success ">Download</a> 
                                        <?php endif; ?>                                      
                                    </td>
                                    <td>
                                        <a href="<?=site_url('po_peserta/delete_visit/'. @$val->id )?>" class="btn btn-icon btn-pure dark" title="Menghapus data" onclick="return confirm('Yakin Menghapus?');">
                                            <i class="la la-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php $no++; endforeach;  endif;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript">
    var divToHide = $('div#tambah');
    var showBtn = $('a#showItem');

    var divToHide2 = $('div#tambah_hasil');
    var showBtn2 = $('a#showItem_hasil');


    showBtn.click(function () {
        divToHide.show();
    });

    $(document).ready(function () {
        divToHide.hide();
    });

    showBtn2.click(function () {
        divToHide2.show();
    });

    $(document).ready(function () {
        divToHide2.hide();
    });

</script>
