<?php if(!has_permission('UPDATE_PEKERJAAN')) return; ?>
<div class="row">
    <div class="col-12">
        <div class="card p-1">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-3 col-sm-12">
                        <img src="<?= $user->personal->avatar_path() ?>" alt="Photo profile" class="rounded-circle img-border height-150 mx-auto d-block">
                    </div>
                    <div class="col-md-9 col-sm-12 pt-2">
                        <h3><?= $user->personal->name ?></h3>
                        <p><?= $user->personal->jobHistory->jobTitle->job_titles ?></p>
                        <p><?= $user->email ?></p>
                    </div>
                </div>
                <div class="row mt-2 pb-2 border-bottom">
                    <div class="col-12">
                        <ul class="nav nav-pills nav-active-bordered-pill nav-justified">
                            <li class="nav-item">
                                <a href="<?= site_url('user/detail/'. $user->id) ?>" class="nav-link">Basic Information</a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= site_url('user/pekerjaan/'. $user->id) ?>" class="nav-link active">Pekerjaan</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card p-1">
                            <div class="card-body">
                                <h3 class="row-section text-bold mb-1">Histori Pekerjaan :</h3>
                                <form method="POST" action="<?= site_url('user/save_pekerjaan/') ?>"  class="form form-horizontal row-separator">
                                <input type="hidden" name="id" value="<?= @$data['id'] ?>" id="id" class="form-control">
                                <input type="hidden" name="user_id" value="<?= @$user->id ?>" id="user_id" class="form-control">
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="posisi">Posisi</label>
                                                    <select name="posisi" id="posisi" class="form-control select2" required>
                                                        <option value="" selected disabled>Pilih Jabatan</option>
                                                        <?php foreach($job_titles as $job): ?>
                                                            <option value="<?= $job->id ?>" <?= $job->id == @$data['job_title_id'] ? 'selected':'' ?>><?= $job->job_titles ?></option>
                                                        <?php endforeach ?>
                                                    </select>
                                                </div><br>
                                                <div class="form-group">
                                                    <label for="effective_date">Tanggal Efektif</label>
                                                    <input type="text" name="effective_date" value="<?= @$data['effective_date'] ?>" id="effective_date" class="form-control datepicker">
                                                </div><br>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="effective_date">Tanggal Mulai</label>
                                                            <input type="text" name="start_date" value="<?= @$data['start_date'] ?>" id="start_date" class="form-control datepicker">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="effective_date">Tanggal Selesai</label>
                                                            <input type="text" name="end_date" value="<?= @$data['end_date'] ?>" <?= @($data['saat_ini']==1)?"disabled":"" ?> id="end_date" class="form-control datepicker">
                                                            <div class="form-group form-check">
                                                                <input type="checkbox" class="form-check-input" name="saat_ini" <?= @($data['saat_ini']==1)?"checked":"" ?> value="1" id="saatIni">
                                                                <label class="form-check-label" for="saatIni">Saat Ini <?= $data['saat_ini'] ?></label>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="period">Period</label>
                                                    <select name="period" id="period" class="form-control select2" required>
                                                        <option value="" selected disabled>Pilih Period</option>
                                                        <?php foreach(\App\Constants\UserManagement::PERIOD_LIST as $i => $period): ?>
                                                            <option value="<?= $i ?>" <?= $i == @$data['employment_period'] ? 'selected':'' ?>><?= $period ?></option>
                                                        <?php endforeach ?>
                                                    </select>
                                                </div><br>
                                                <div class="form-group">
                                                    <label for="status">Employment Status</label>
                                                    <select name="status" id="status" class="form-control select2" required>
                                                        <option value="" selected disabled>Jenis Karyawan</option>
                                                        <?php foreach(\App\Constants\UserManagement::TYPE_LIST as $type): ?>
                                                            <option value="<?= $type ?>" <?= $type == @$data['job_status'] ? 'selected':'' ?>><?= $type ?></option>
                                                        <?php endforeach ?>
                                                    </select>
                                                </div><br>
                                                <div class="form-group">
                                                    <label for="hak_akses">Hak Akses</label>
                                                    <select name="hak_akses[]" multiple id="hak_akses" class="form-control select2">
                                                        <?php foreach($accesses as $access): ?>
                                                            <option value="<?= $access['id'] ?>" <?= @$access['selected'] ?>><?= $access['access'] ?></option>
                                                        <?php endforeach ?>
                                                    </select>
                                                </div><br>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="form-group ">
                                            <button type="submit" class="btn btn-primary pull-right ml-2">Simpan</button>
                                            <a href="<?= site_url('user/') ?>" class="btn btn-default pull-right mr-1">BATAL</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .input-group-append {
        display: none;
    }
</style>


<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript">
$(function() {
    $('#saatIni').change(function(){
        if ($('#saatIni').is(':checked') == true){
            $("#end_date").prop("disabled", true);
        } else {
            $("#end_date").removeAttr('disabled');
        }
    });
});
</script>
