<section id="number-tabs">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Pembayaran Iuran</h4>
                </div>
                <form method="POST" enctype="multipart/form-data" action="<?= site_url('po_tahap_pendaftaran/savePembayaran') ?>" id="form-pembayaran" class="form form-horizontal" >
                    <input type="hidden" id="id" name="id" value="<?=@$data->tahap_pembayaran->id;?>">
                    <input type="hidden" id="id_personal" name="id_personal" value="<?=empty(@$data->tahap_pembayaran->id_personal)?MasKaryawan()->id:@$data->tahap_pembayaran->id_personal;?>" >
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="proposalTitle1">Nama Pengirim</label>
                                        <input type="text" class="form-control" id="nama" name="nama" value="<?=@$data->tahap_pembayaran->nama;?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="proposalTitle1">Bukti Transfer</label>
                                        <input type="file" class="form-control" id="bukti" name="files[bukti]">
                                        <?php if (!empty(@$data->tahap_pembayaran->bukti) AND file_exists("uploads/bukti/".@$data->tahap_pembayaran->bukti)) : ?>
                                            <div class="row no-gutters mt-1">
                                                <div class="col-4">
                                                    <a href="<?=base_url("uploads/bukti/".@$data->tahap_pembayaran->bukti)?>" >
                                                        <img class="img-fluid" src="<?=base_url("uploads/bukti/".@$data->tahap_pembayaran->bukti)?>" alt="">
                                                    </a>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <?php if($data->statuse->grup == "7" AND !empty(@$data->tahap_pembayaran->id)): ?>
                <div class="card-footer text-center">
                    <div class="form-group row ">
                        <div class="col-md-12">
                            <a href="<?=site_url("po_peserta/status/23/".@$data->tahap_pembayaran->id.'/pembayaran');//Verifikasi Pembayaran Ditolak?>" class="btn btn-outline-danger mr-1" onclick="return confirm('Apakah kamu yakin ditolak?')">Tolak</a>
                            <a href="<?=site_url("po_peserta/status/22/".@$data->tahap_pembayaran->id.'/pembayaran');//Verifikasi Pembayaran Diterima?>" class="btn btn-success  mr-1" onclick="return confirm('Apakah kamu yakin diterima?')">Terima</a>
                        </div>
                    </div>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript">
    $("#form-pembayaran :input").prop("disabled", true);
</script>