<section id="number-tabs">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <form method="POST" enctype="multipart/form-data" action="<?= site_url('po_tahap_pendaftaran/saveTahap1') ?>" id="form-pendaftaran" class="form form-horizontal" >
                    <input type="hidden" id="id" name="id" value="<?=@$data->id;?>">
                    <input type="hidden" id="id_personal" name="id_personal" value="<?=empty(@$data->id_personal)?MasKaryawan()->id:@$data->id_personal;?>" >
                    
                    <div class="card-header">
                        <h4 class="card-title">Data Pendaftaran</h4>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="firstName1">Nama Lengkap</label>
                                        <input type="text" class="form-control" id="nama_platform" name="nama_platform" value="<?=@$data->personals->name?>" required>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="lastName1">Nama Email</label>
                                        <input type="text" class="form-control" id="nama_perusahaan" name="nama_perusahaan" value="<?=@$data->personals->user->email?>" required>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="date1">Nomor Hp</label>
                                        <input type="text" class="form-control" id="paid_up_capital" name="paid_up_capital" value="<?=@$data->personals->phone?>">
                                    </div>
                                </div>

                                <!-- <a href="#" class="btn btn-primary">Reset Password</a> -->
                            </div>
                            
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>



<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript">
    $("#form-pendaftaran :input").prop("disabled", true);
</script>
