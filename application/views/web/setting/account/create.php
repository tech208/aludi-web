<?php if(!has_permission('CREATE_ACCOUNT')) return; ?>

<link rel="stylesheet" type="text/css" href="<?= base_url('app-assets/vendors/css/forms/selects/select2.min.css') ?>">
<div class="row">
    <div class="col-12">
        <div class="card p-1">
            <ul class="nav nav-pills nav-active-bordered-pill nav-justified">
                <li class="nav-item">
                    <a href="<?= site_url('setting/role') ?>" class="nav-link">Roles</a>
                </li>
                <li class="nav-item">
                    <a href="<?= site_url('setting/account') ?>" class="nav-link active">Accounts</a>
                </li>
            </ul>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card p-1">
            <div class="card-body">
                <form method="POST" action="<?= site_url('setting/insert_account') ?>" class="form form-horizontal row-separator">
                    <div class="form-body">
                        <h3 class="row-section">Tambah Akun</h3>
                        <div class="form-group row border-bottom-0">
                            <label for="candidat" class="col-md-2 label-control text-left">Nama</label>
                            <div class="col-md-10">
                            <input type="text" name="candidat" id="candidat" class="form-control" required>
                                <!-- <select name="candidat" id="candidat" class="select2" required>
                                    <?php foreach($candidats as $c): ?>
                                        <option value="<?= $c->id ?>"><?= $c->name ?></option>
                                    <?php endforeach ?>
                                </select> -->
                            </div>
                        </div>

                        <div class="form-group row border-bottom-0">
                            <label for="email" class="col-md-2 label-control text-left">Email</label>
                            <div class="col-md-10">
                                <input type="email" name="email" id="email" class="form-control" required>
                            </div>
                        </div>

                        <div class="form-group row border-bottom-0">
                            <label for="password" class="col-md-2 label-control text-left">Password</label>
                            <div class="col-md-10">
                                <input type="password" name="password" id="password" class="form-control" required>
                            </div>
                        </div>

                        <div class="form-group row border-bottom-0">
                            <label for="role" class="col-md-2 label-control text-left">Role</label>
                            <div class="col-md-10">
                                <select name="role" id="role" class="select2" required>
                                    <?php foreach($roles as $role): ?>
                                        <option value="<?= $role->id ?>"><?= $role->name ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row border-bottom-0">
                            <?= csrf_field() ?>
                            <label for="id_role" class="col-md-2 label-control text-left"></label>
                            <div class="col-md-10">
                                <button type="submit" class="btn btn-primary pull-right">SIMPAN</button>
                                <a href="<?= site_url('setting/account') ?>" class="btn btn-default pull-right mr-1">BATAL</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="<?= base_url('app-assets/vendors/js/forms/select/select2.full.min.js') ?>"></script>
<script>
    $('.select2').select2({
        width: '100%',
    });
</script>
