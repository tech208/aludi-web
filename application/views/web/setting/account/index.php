<link rel="stylesheet" type="text/css" href="<?= base_url('app-assets/vendors/css/forms/selects/select2.min.css') ?>">
<link rel="stylesheet" type="text/css" href="<?= base_url('app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css') ?>">

<div class="row">
    <div class="col-12">
        <div class="card p-1">
            <ul class="nav nav-pills nav-active-bordered-pill nav-justified">
                <li class="nav-item">
                    <a href="<?= site_url('setting/role') ?>" class="nav-link">Roles</a>
                </li>
                <li class="nav-item">
                    <a href="<?= site_url('setting/account') ?>" class="nav-link active">Accounts</a>
                </li>
            </ul>
        </div>
    </div>
</div>

<?php if(has_permission('CREATE_ACCOUNT')): ?>
<div class="row">
    <div class="col-12">
        <div class="card p-1">
            <div class="row">
                <div class="col-sm-12 col-md-9">
                    <h3 style="padding-top: 3px;" class="m-0">Role</h3>
                </div>

                <div class="col-sm-12 col-md-3">
                    <a href="<?= site_url('setting/add_account') ?>" class="btn btn-outline-primary btn-block">
                        Add Account
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endif ?>

<?php if(!has_permission('READ_ACCOUNT')) return; ?>

<div class="row">
    <div class="col-12">
        <div class="card p-1">
            <div class="row">
                <div class="col-sm-12">
                    <table id="tbl_inventaris" class="table table-striped">
                        <thead>
                            <tr>
                                <th>Email</th>
                                <th>Role</th>
                                <th>Last Update</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($accounts as $account): ?>
                                <tr>
                                    <td><?= $account->email ?></td>
                                    <td><?= $account->role ? $account->role->name : '-' ?></td>
                                    <td><?= $account->updated_at ?></td>
                                    <td>
                                        <?php if(has_permission('UPDATE_ACCOUNT')): ?>
                                            <a href="<?= site_url('setting/edit_account/'. $account->id) ?>" class="btn btn-icon btn-pure dark">
                                                <i class="material-icons">edit</i>
                                            </a>
                                        <?php endif ?>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?= base_url('app-assets/vendors/js/forms/select/select2.full.min.js') ?>"></script>
<script src="<?= base_url('app-assets/vendors/js/tables/jquery.dataTables.min.js') ?>"></script>
<script src="<?= base_url('app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js') ?>"></script>
<script>
    $('.select2').select2();
    $('#tbl_inventaris').dataTable();
</script>