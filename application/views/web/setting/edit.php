<?php if(!has_permission('UPDATE_ROLE')) return; ?>

<?php $permissions = explode(',', $role->permissions) ?>
<div class="row">
    <div class="col-12">
        <div class="card p-1">
            <ul class="nav nav-pills nav-active-bordered-pill nav-justified">
                <li class="nav-item">
                    <a href="<?= site_url('setting/role') ?>" class="nav-link active">Roles</a>
                </li>
                <li class="nav-item">
                    <a href="<?= site_url('setting/account') ?>" class="nav-link">Accounts</a>
                </li>
            </ul>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card p-1">
            <div class="card-body">
                <form method="POST" action="<?= site_url('setting/update_role/'. $role->id) ?>" class="form form-horizontal row-separator">
                    <div class="form-body">
                        <h3 class="row-section">Edit Role</h3>
                        <div class="form-group row border-bottom-0">
                            <label for="name" class="col-md-2 label-control text-left">Role</label>
                            <div class="col-md-10">
                                <input type="text" value="<?= $role->name ?>" name="name" id="name" class="form-control">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12">
                                <h4 class="mx-0 p-0 my-2">Permissions</h4>
                            </div>
                        </div>

                        <?php foreach($menus as $m): ?>
                            <div class="row border-bottom mt-1">
                                <div class="col-12 col-md-2">
                                    <div class="custom-control col-md-3 col-sm-12 mb-1 custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" value="<?= $m->id ?>" name="access[]" data-type="menu" id="permission-<?= $m->id ?>" <?= in_array($m->id, $permissions) ? 'checked':'' ?>>
                                        <label class="custom-control-label" style="font-size: 11px" for="permission-<?= $m->id ?>">
                                            <?= $m->title ?>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-12 col-md-10">
                                    <div class="row">
                                        <?php foreach($m->permissions as $perm): ?>
                                            <div class="custom-control col-md-3 col-sm-12 mb-1 custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" value="<?= $perm->permission ?>" name="permissions[]" data-type="role_perm" data-related="permission-<?= $m->id ?>" id="role_perm-<?= $perm->id ?>" <?= has_permission($perm->permission) ? 'checked':'' ?>>
                                                <label class="custom-control-label" style="font-size: 11px" for="role_perm-<?= $perm->id ?>">
                                                    <?= $perm->permission ?>
                                                </label>
                                            </div>
                                        <?php endforeach ?>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach ?>

                        <div class="form-group row border-bottom-0">
                            <?= csrf_field() ?>
                            <label for="id_role" class="col-md-2 label-control text-left"></label>
                            <div class="col-md-10">
                                <button type="submit" class="btn btn-primary pull-right">SIMPAN</button>
                                <a href="<?= site_url('setting/role') ?>" class="btn btn-default pull-right mr-1">BATAL</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).on('change', '[data-type=role_perm]', function() {
        var related = $(this).data('related');
        if($(this).is(':checked')) {
            if(!$('#'+related).is(':checked')) {
                $('#'+related).prop('checked', true);
            }
        }
    });

    $(document).on('change', '[data-type=menu]', function() {
        var relates = $('[data-related='+ $(this).attr('id') +']');

        for(var i=0; i < relates.length; i++) {

            if(!$(this).is(':checked')) {
                if($(relates[i]).is(':checked')) {
                    $(relates[i]).prop('checked', false)
                }
            }
        }
    })
</script>