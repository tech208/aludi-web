<?php// if(!has_permission('CREATE_ACCOUNT') OR !has_permission('UPDATE_ARTIKEL')) return; ?>

<div class="row">
    <div class="col-12">
        <div class="card p-1">
            <div class="card-body">
                <form method="POST" enctype="multipart/form-data" action="<?= site_url('po_berita/save') ?>" class="form form-horizontal" >
                    <input type="hidden" autocomplete="off" name="id" id="id" value="<?=@$data->id?>" class="form-control " >
                    <div class="form-body">
                        <!-- <h3 class="row-section">Pengajuan Perizinan</h3> -->
                        <!--<div class="form-group row ">
                            <label for="id_kategori" class="col-md-2 label-control text-left">Kategori *</label>
                            <div class="col-md-10">
                                <select name="id_kategori" id="id_kategori" class="form-control square select2" required>
                                    <option value="" selected disabled>Pilih..</option>
                                    <?php foreach($refKategori as $l): ?>
                                        <option value="<?= $l->id ?>" <?=(@$data->id_kategori==$l->id)?"selected":" ";?> ><?= $l->name ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>-->
						<input type="hidden" name="grup" value="Headlines">
						<input type="hidden" name="id_kategori" value="2">
                        <div class="form-group row ">
                            <label for="tanggal_mulai" class="col-md-2 label-control text-left">Tanggal</label>
                            <div class="col-md-3 col-sm-12">
                                <input type="text" autocomplete="off" name="tanggal" id="tanggal" value="<?=@$data->tanggal?>" class="form-control datepicker" required>
                            </div>
                        </div>
                        <div class="form-group row ">
                            <label for="Judul" class="col-md-2 label-control text-left">Judul*</label>
                            <div class="col-md-3 col-sm-12">
                                <input type="text" autocomplete="off" name="name" id="name" value="<?=@$data->name?>" class="form-control " >
                            </div>
                        </div>
                        <div class="form-group row ">
                            <label for="alasan" class="col-md-2 label-control text-left">Keterangan*</label>
                            <div class="col-md-10">
                                <textarea name="deskripsi" id="deskripsi" class="form-control summernote" rows="4" ><?=@$data->deskripsi?></textarea>
                            </div>
                        </div>
                        <div class="form-group row ">
                            <label for="id_kategori" class="col-md-2 label-control text-left">Status *</label>
                            <div class="col-md-10">
                                <select name="status" id="status" class="form-control square select2" required>
                                    <option value="" selected disabled>Pilih..</option>
                                    <?php foreach($refStatus as $l): ?>
                                        <option value="<?= $l->id ?>" <?=(@$data->status==$l->id)?"selected":" ";?>><?= $l->name ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                        <!--<div class="form-group row ">
                            <label for="id_kategori" class="col-md-2 label-control text-left">Grup</label>
                            <div class="col-md-10">
                                <select name="grup" id="grup" class="form-control square select2" >
                                    <option value="" selected disabled>Pilih..</option>
                                    <option value="Terbaru" <?=(@$data->grup=="Terbaru")?"selected":" ";?>>Terbaru</option>
                                    <option value="Headlines" <?=(@$data->grup=="Headlines")?"selected":" ";?>>Headlines</option>
                                    <option value="Penting"  <?=(@$data->grup=="Penting")?"selected":" ";?>>Penting</option>
                                </select>
                            </div>
                        </div>-->
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="is_karyawan" <?=(@$data->is_karyawan==1)?"checked":" ";?> name="is_karyawan" value="1">
                            <label class="form-check-label" for="exampleCheck1">Khusus Karyawan</label>
                        </div>
                        <div class="form-group row ">
                            <?= csrf_field(); ?>
                            <label for="id_role" class="col-md-2 label-control text-left"></label>
                            <div class="col-md-10">
                                <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                                <a href="<?= site_url('po_artikel/') ?>" class="btn btn-default pull-right mr-1">Batal</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<style>
    /* .input-group-append {
        display: none;
    } */
</style>

<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript">

$(function() {

});

</script>

