<div class="card">
    <div class="card-content collapse show">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12">
                    <form enctype="multipart/form-data" method="POST" action="<?= site_url('po_banner/save') ?>" class="form form-horizontal" >
                        <input type="hidden" class="form-control" id="id" name="id" value="<?=@$data->id;?>">
                        <div class="form-group">
                            <label>Title</label>
                            <input type="text" class="form-control" id="nama" name="nama" placeholder="Masukan Title"   value="<?=@$data->nama;?>">
                        </div>
                        <div class="form-group">
                            <label>Url</label>
                            <input type="text" class="form-control" id="url" name="url" placeholder="Masukan Url"   value="<?=@$data->url;?>">
                        </div>
                        <div class="form-group">
                            <label for="proposalTitle1">Lampiran</label>
                            <input type="file" class="form-control" id="lampiran" name="files[lampiran]">
                            <?php if (!empty(@$data->lampiran) AND file_exists("uploads/banner_lampiran/".@$data->lampiran)) :?>
                            <div class=mt-1>
                                <a href="<?=base_url("uploads/banner_lampiran/".@$data->lampiran)?>" target="_blank" class="btn btn-success ">Download</a> <?=@$data->lampiran?>
                            </div>
                            <?php endif; ?>
                        </div>
						<div class="form-group">
                                <label>Jenis (*)</label>
                                    <select name="jenis" id="jenis" class="form-control square select2" required>
                                        <option value="">--pilih--</option>
                                        <?php foreach($ref_jenis as $index => $value): ?>
                                            <option value="<?= $index ?>" 
                                            <?=(@$data->status==$index)?"selected":" ";?> >
                                                <?= $value ?>
                                            </option>
                                        <?php endforeach ?>
                                    </select>                                    

                        </div>
                        <div class="form-group">
                                <label>Status</label>
                                    <select name="status" id="status" class="form-control square select2">
                                        <option value="">--pilih--</option>
                                        <?php foreach($ref_status as $l): ?>
                                            <option value="<?= $l->id ?>" 
                                            <?=(@$data->status==$l->id)?"selected":" ";?> >
                                                <?= $l->name ?>
                                            </option>
                                        <?php endforeach ?>
                                    </select>                                    

                        </div>
                        <hr>
                        <div class="row px-1">
                            <button type="submit" class="btn btn-primary col-2 ">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>