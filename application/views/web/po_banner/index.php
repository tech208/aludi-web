
<div class="row">
    <div class="col-12">
        <div class="card p-1">
        <div class="row">
                 <div class="col-sm-12">
                    <a href="<?=site_url("po_banner/form")?>" class="btn btn-success">CREATE</a>
                </div> 
            </div><hr>        
            <div class="row">
                <div class="col-sm-12">
                    <table id="tbl_inventaris" class="table table-striped data_list">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Deskripsi</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no=1; foreach($data as $val): ?>
                                <tr>
                                    <td><?= $no ?></td>
                                    <td><?= $val->nama ?></td>
                                    <td><?= @$val->url ?></td>
                                    <td><?= @$val->statuse->name ?></td>
                                    <td>
                                        <!-- <a href="<?= site_url('po_profile/form/'. $val->id.'/detail') ?>" class="btn btn-icon btn-pure dark" title="Detail Data" >
                                            <i class="la la-file"></i>
                                        </a> -->
                                        <a href="<?=site_url('po_banner/delete/'. $val->id )?>" class="btn btn-ico  n btn-pure dark" title="Menghapus data" onclick="return confirm('Yakin Menghapus?');">
                                            <i class="la la-trash"></i>

                                        <a href="<?= site_url('po_banner/form/'. $val->id) ?>" class="btn btn-icon btn-pure dark" title="Rubah Data" >
                                            <i class="la la-pencil"></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php $no++; endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?= base_url('app-assets/vendors/js/forms/select/select2.full.min.js') ?>"></script>
<script src="<?= base_url('app-assets/vendors/js/tables/jquery.dataTables.min.js') ?>"></script>
<script src="<?= base_url('app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js') ?>"></script>
<script>
    $('.select2').select2();
    $('#tbl_inventaris').dataTable();
</script>