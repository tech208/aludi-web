<?php $colors = ['success', 'warning', 'info', 'danger','success', 'warning', 'info', 'danger']; ?>

<section >
    <div class="row d-flex justify-content-center">
        <!-- Menunggu Verifikasi Tahap 1 -->
        <div class="col-md-2 col-12">
            <div class="card pull-up">
                <div class="card-content">
                    <div class="card-body">
                        <div class="media d-flex">
                            <div class="media-body text-center">
                                <h3 class="<?= $colors[1] ?> font-weight-bolder" style="font-size: 2.2rem"><?=$data->menunggu_verifikasi_tahap1_total?></h3>
                                <h6>Menunggu Verifikasi Tahap 1</h6>
                            </div>
                            <div>
                                <i class="icon-user-follow <?= $colors[1] ?> font-large-2 float-right"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end Menunggu Verifikasi Tahap 1 -->

        <!-- Menunggu Verifikasi Pembayaran -->
        <div class="col-md-2 col-12">
            <div class="card pull-up">
                <div class="card-content">
                    <div class="card-body">
                        <div class="media d-flex">
                            <div class="media-body text-center">
                                <h3 class="<?= $colors[2] ?> font-weight-bolder" style="font-size: 2.2rem"><?=$data->menunggu_verifikasi_pembayaran_total?></h3>
                                <h6>Menunggu Verifikasi Pembayaran</h6>
                            </div>
                            <div>
                                <i class="icon-user-follow <?= $colors[2] ?> font-large-2 float-right"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end Menunggu Verifikasi Pembayaran -->
        
        <!-- Menunggu Menunggu Verifikasi Tahap 2 -->
        <div class="col-md-2 col-12">
            <div class="card pull-up">
                <div class="card-content">
                    <div class="card-body">
                        <div class="media d-flex">
                            <div class="media-body text-center">
                                <h3 class="<?= $colors[3] ?> font-weight-bolder" style="font-size: 2.2rem"><?=$data->menunggu_verifikasi_tahap2_total?></h3>
                                <h6>Menunggu Menunggu Verifikasi Tahap 2</h6>
                            </div>
                            <div>
                                <i class="icon-user-follow <?= $colors[3] ?> font-large-2 float-right"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end Menunggu Menunggu Verifikasi Tahap 2 -->

        
        <!-- Menunggu Diskusi Site Visit-->
        <div class="col-md-2 col-12">
            <div class="card pull-up">
                <div class="card-content">
                    <div class="card-body">
                        <div class="media d-flex">
                            <div class="media-body text-center">
                                <h3 class="<?= $colors[4] ?> font-weight-bolder" style="font-size: 2.2rem"><?=$data->menunggu_verifikasi_visit_total?></h3>
                                <h6>Menunggu Diskusi Site Visit</h6>
                            </div>
                            <div>
                                <i class="icon-user-follow <?= $colors[4] ?> font-large-2 float-right"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end Menunggu Diskusi Site Visit -->

        <!-- Menunggu Proses Rekomendasi-->
        <div class="col-md-2 col-12">
            <div class="card pull-up">
                <div class="card-content">
                    <div class="card-body">
                        <div class="media d-flex">
                            <div class="media-body text-center">
                                <h3 class="<?= $colors[5] ?> font-weight-bolder" style="font-size: 2.2rem"><?=$data->menunggu_verifikasi_rekomendasi_total?></h3>
                                <h6>Menunggu Proses Rekomendasi</h6>
                            </div>
                            <div>
                                <i class="icon-user-follow <?= $colors[5] ?> font-large-2 float-right"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end Menunggu Diskusi Site Visit -->

    </div>

</section>


<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                <a href="<?=site_url("dashboard/detail?mode=jumlah_member")?>" title="Detail Jumlah Member"><h4 class="card-title">Jumlah Member</h4></a>
            </div>
            <div class="card-content collapse show">
                <div class="card-body">
                    <a href="<?=site_url("dashboard/detail?mode=jumlah_member")?>" title="Detail Jumlah Member"><h1><?=@ $data->total_member_total?><small> Member</small></h1></a>
                    <div id="piechart"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                <a href="<?=site_url("dashboard/detail?mode=status_member")?>" title="Detail Status Member"><h4 class="card-title">Status Member</h4></a>
            </div>
            <div class="card-content collapse show">
                <div class="card-body">
                    <div id="piechart_status"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                <a href="<?=site_url("dashboard/detail?mode=jumlah_member_perkembangan_tahunan")?>" title="Detail Jumlah Perkembangan Member Tahunan"><h4 class="card-title">Jumlah Member</h4></a>
            </div>
            <div class="card-content collapse show">
                <div class="card-body">
                    <div id="chartContainer" style="height: 300px; width: 100%;"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                <a href="<?=site_url("dashboard/detail?mode=jumlah_member_perkembangan_bulanan")?>" title="Detail Jumlah Perkembangan Member"><h4 class="card-title">Status Member</h4></a>
            </div>
            <div class="card-content collapse show">
                <div class="card-body">
                    <div id="chartContainerMember" style="height: 300px; width: 100%;"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php

$dataPoints = $data->perkembangan;
?>
<script type="text/javascript">
  window.onload = function () {
    
    var chart = new CanvasJS.Chart("chartContainer", {
        animationEnabled: true,
        theme: "light2",
        title:{
            text: "Perkembangan Member [tahunan]"
        },
        data: [{
            type: "column",
            yValueFormatString: "#,##0.## tonnes",
            dataPoints: <?php echo json_encode($data->perkembangan, JSON_NUMERIC_CHECK); ?>
        }]
    });
    chart.render();

    var chart_member = new CanvasJS.Chart("chartContainerMember",
    {

      title:{
      text: "Perkembangan Member "
      },
       data: [
      {
        type: "line",
        dataPoints: <?php echo json_encode($data->perkembangan_bulanan, JSON_NUMERIC_CHECK); ?>
      }
      ]
    });

    chart_member.render();
  }
  </script>
 <script type="text/javascript" src="https://canvasjs.com/assets/script/canvasjs.min.js"></script></head>
 


<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script type="text/javascript">
    // Load google charts
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);

    // Draw the chart and set the chart values
    function drawChart() {

    var data = google.visualization.arrayToDataTable([
        ['Jenis Perusahaan', 'Jumlah'],
        ['Konvensional', <?=@$data->total_konvensional_total; ?>],
        ['Syariah',<?=@$data->total_syariah_total; ?>],
        ['Hybrid',<?=@$data->total_hybrid_total; ?>],
        // ['Konvensional', 10],
        // ['Syariah',1],
    ]);

    // Optional; add a title and set the width and height of the chart
    var options = {
          title: "Jumlah Member",
          width: '100%',
          height: '100%'
        };
    // var options = {'title':'Status Member', 'width: '100%', 'height':500};
    
    // Display the chart inside the <div> element with id="piechart"
    var chart = new google.visualization.PieChart(document.getElementById('piechart'));
    chart.draw(data, options);
    
    var data_status = google.visualization.arrayToDataTable([
        ['Kategori', 'Jumlah'],
        ['Tahap 1 ', <?=@$data->tahap_1; ?>],
        ['Menunggu pembayaran', <?=@$data->menunggu_pembayaran; ?>],
        ['Tahap 2', <?=@$data->tahap_2; ?>],
        ['Site Visit', <?=@$data->site_visit; ?>],
        ['Rekomendasi OJK', <?=@$data->rekomendasi_ojk; ?>],
    ]);

    // Optional; add a title and set the width and height of the chart
    var options_status = {
          title: "Status Member",
          width: '100%',
          height: '100%'
        };
    
    var chart_status = new google.visualization.PieChart(document.getElementById('piechart_status'));
    chart_status.draw(data_status, options_status);
}
</script>

