<div class="row">
    <div class="col-12">
        <div class="card p-1">
            <div class="row">
                <div class="col-sm-12">
                    <h3 >Total Syariah : <b><?=$data['total_syariah_total'];?></b></h3>
                </div>
            </div><hr>
            <div class="row">
                <div class="col-sm-12">
                    <table id="tbl_inventaris" class="table table-striped data_list">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Nama PT</th>
                                <th>Tanggal Pendaftaran</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no=1; foreach($data['total_syariah'] as $val): ?>
                                <tr>
                                    <td><?= $no ?></td>
                                    <td><?= @$val->personals->name ?></td>
                                    <td><?= @$val->nama_perusahaan ?></td>
                                    <td><?= MasTanggal(@$val->personals->pendaftarans->created_at,1); ?></td>
                                    <td><?= $val->utamas->statuse->name ?></td>
                                    <td>
                                        <a href="<?= site_url('po_peserta/detail/'. $val->id_tahap) ?>" class="btn btn-icon btn-pure dark" title="Detail data" >
                                            <i class="la la-file"></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php $no++; endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="card p-1">
            <div class="row">
                <div class="col-sm-12">
                    <h3 >Total Konvensional : <b><?=$data['total_konvensional_total'];?></b></h3>
                </div>
            </div><hr>
            <div class="row">
                <div class="col-sm-12">
                    <table id="tbl_inventaris" class="table table-striped data_list">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Nama PT</th>
                                <th>Tanggal Pendaftaran</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no=1; foreach($data['total_konvensional'] as $val): ?>
                                <tr>
                                    <td><?= $no ?></td>
                                    <td><?= @$val->personals->name ?></td>
                                    <td><?= @$val->nama_perusahaan ?></td>
                                    <td><?= MasTanggal(@$val->personals->pendaftarans->created_at,1); ?></td>
                                    <td><?= $val->utamas->statuse->name ?></td>
                                    <td>
                                        <a href="<?= site_url('po_peserta/detail/'. $val->id_tahap) ?>" class="btn btn-icon btn-pure dark" title="Detail data" >
                                            <i class="la la-file"></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php $no++; endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="card p-1">
            <div class="row">
                <div class="col-sm-12">
                    <h3 >Total Hybrid : <b><?=$data['total_hybrid_total'];?></b></h3>
                </div>
            </div><hr>
            <div class="row">
                <div class="col-sm-12">
                    <table id="tbl_inventaris" class="table table-striped data_list">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Nama PT</th>
                                <th>Tanggal Pendaftaran</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no=1; foreach($data['total_hybrid'] as $val): ?>
                                <tr>
                                    <td><?= $no ?></td>
                                    <td><?= @$val->personals->name ?></td>
                                    <td><?= @$val->nama_perusahaan ?></td>
                                    <td><?= MasTanggal(@$val->personals->pendaftarans->created_at,1); ?></td>
                                    <td><?= $val->utamas->statuse->name ?></td>
                                    <td>
                                        <a href="<?= site_url('po_peserta/detail/'. $val->id_tahap) ?>" class="btn btn-icon btn-pure dark" title="Detail data" >
                                            <i class="la la-file"></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php $no++; endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?= base_url('app-assets/vendors/js/forms/select/select2.full.min.js') ?>"></script>
<script src="<?= base_url('app-assets/vendors/js/tables/jquery.dataTables.min.js') ?>"></script>
<script src="<?= base_url('app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js') ?>"></script>
<script>
    $('.select2').select2();
    $('#tbl_inventaris').dataTable();
</script>