<div class="row">
    <div class="col-12">
        <div class="card p-1">
            <div class="row">
                <div class="col-sm-12">

                    <a href="<?=site_url("po_calon_penerbit/form")?>" class="btn btn-secondary" >Tambah Calon Penerbit</a>
                    <!-- <a href="<?//=site_url("po_calon_penerbit/printxlx")?>" class="btn btn-success" ><i class="la la-download"></i> Laporan excel</a> -->
                </div>
            </div><hr>
            <div class="row">
                <div class="col-sm-12">
                    <table id="tbl_inventaris" class="table table-striped data_list">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Nama PT</th>
                                <th>Nama Brand</th>
                                <th>Lokasi</th>
                                <th>Total Pendanaan</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no=1; foreach($data as $val): ?>
                                <tr>
                                    <td><?= $no ?></td>
                                    <td><?= @$val->owner ?></td>
                                    <td><?= @$val->nama_perusahaan ?></td>
                                    <td><?= @$val->nama_brand ?></td>
                                    <td><?= @$val->lokasi ?></td>
                                    <td><?= MasUang(@$val->total_pendanaan,0) ?></td>
                                    <td><?= @$val->statuse->name ?></td>
                                    <td>
                                        <a href="<?= site_url('po_calon_penerbit/form/'. $val->id.'/detail') ?>" class="btn btn-icon btn-pure dark" title="Detail data" >
                                            <i class="la la-file"></i>
                                        </a>
                                        <a href="<?= site_url('po_calon_penerbit/form/'. $val->id) ?>" class="btn btn-icon btn-pure dark" title="Update data" >
                                            <i class="la la-pencil"></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php $no++; endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?= base_url('app-assets/vendors/js/forms/select/select2.full.min.js') ?>"></script>
<script src="<?= base_url('app-assets/vendors/js/tables/jquery.dataTables.min.js') ?>"></script>
<script src="<?= base_url('app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js') ?>"></script>
<script>
    $('.select2').select2();
    $('#tbl_inventaris').dataTable();
</script>