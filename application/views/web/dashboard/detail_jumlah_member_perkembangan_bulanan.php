<div class="row">
    <?php 
    foreach ($data['perkembangan_bulanan'] as $key => $value):
        foreach ($value as $key_bulanan => $value_bulanan):
    ?>
            <div class="col-12">
                <div class="card p-1">
                    <div class="row">
                        <div class="col-sm-12">
                            <h3 >Pada Tahun <?=$key?> Di Bulan <?=MasBulan($key_bulanan)?>: <b><?=@$data['perkembangan_bulanan_group'][$key][$key_bulanan];?></b></h3>
                        </div>
                    </div><hr>
                    <div class="row">
                        <div class="col-sm-12">
                            <table id="tbl_inventaris" class="table table-striped data_list">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Nama PT</th>
                                        <th>Tanggal Pendaftaran</th>
                                        <th>Status</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no=1; foreach(@$value_bulanan as $val): ?>
                                        <tr>
                                            <td><?= $no ?></td>
                                            <td><?= @$val->personals->name ?></td>
                                            <td><?= @$val->tahap_satu->nama_perusahaan ?></td>
                                            <td><?= MasTanggal(@$val->personals->pendaftarans->created_at,1); ?></td>
                                            <td><?= $val->statuse->name ?></td>
                                            <td>
                                                <a href="<?= site_url('po_peserta/detail/'. $val->id) ?>" class="btn btn-icon btn-pure dark" title="Detail data" >
                                                    <i class="la la-file"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php $no++; endforeach ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
    <?php 
        endforeach;
    endforeach;
    ?>
    

</div>

<script src="<?= base_url('app-assets/vendors/js/forms/select/select2.full.min.js') ?>"></script>
<script src="<?= base_url('app-assets/vendors/js/tables/jquery.dataTables.min.js') ?>"></script>
<script src="<?= base_url('app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js') ?>"></script>
<script>
    $('.select2').select2();
    $('#tbl_inventaris').dataTable();
</script>