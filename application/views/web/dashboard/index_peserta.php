<?php $colors = ['success', 'warning', 'info', 'danger','success', 'warning', 'info', 'danger']; ?>

<section >
    <a href="<?=site_url("po_tahap_pendaftaran")?>">
        <div class="col-md-12 col-12">
            <div class="card pull-up">
                <div class="card-content">
                    <div class="card-body">
                        <div class="media d-flex">
                            <div class="media-body text-center">
                                <h3 class="font-weight-bolder" style="font-size: 2.2rem">Saat Ini</h3>
                                <div class="<?=$data->statuse->css?>"><?=$data->statuse->name?></div>
                            </div>
                            <div>
                                <i class="icon-user-follow <?= $colors[3] ?> font-large-2 float-right"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </a>
</section>

<?php
    if($data->status==31  ) ://status selesai
?>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Berkas Bulanan</h4>
                </div>
                <div class="card-content collapse show ">
                    <div class="card-body ">
                        <form method="POST" enctype="multipart/form-data"  action="<?= site_url('dashboard/save_berkas/') ?>"  >
                            <input type="hidden" name="id_rekomendasi" value="<?= @$data->tahap_rekomendasi->id ?>" id="id" class="form-control">
                            <div class="form-group">
                                <label for="formGroupExampleInput">Nama*</label>
                                <input type="text" class="form-control" id="nama" name="nama" placeholder="Masukan Nama File" required>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="formGroupExampleInput">Tanggal*</label>
                                        <input type="text" class="form-control datepicker" id="tanggal" name="tanggal" required>
                                    </div>
                                </div> 
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="formGroupExampleInput2">File*</label>
                                        <input type="file" class="form-control" id="rekomendasi_log" name="files[rekomendasi_log]"  placeholder="upload File" required>
                                    </div>
                                </div> 
                            </div>
                            <div class="form-group">
                                <label for="formGroupExampleInput2">Deskripsi*</label>
                                <textarea class="form-control" id="deskripsi" name="deskripsi" placeholder="Deskripsi" required></textarea>
                            </div>
                            <br>
                            <div class="form-group ">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </form>
                    </div>
                    <div class="card-body">
                        <h3>Data List</h3>
                        

                        
                        <div class="row">
                            <div class="col-12">
                                <div class="card p-1">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <table id="tbl_inventaris" class="table table-striped data_list">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama</th>
                                                        <th>Tanggal</th>
                                                        <th>Dekripsi</th>
                                                        <th>File</th>
                                                        <!-- <th>Aksi</th> -->
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php if (!empty(@$data->tahap_rekomendasi->details))  : ?> 
                                                    <?php $no=1; foreach(@$data->tahap_rekomendasi->details as $val): ?>
                                                        <tr>
                                                            <td><?= $no ?></td>
                                                            <td><?= $val->nama ?></td>
                                                            <td><?= MasTanggal(@$val->tanggal); ?></td>
                                                            <td><?= $val->deskripsi ?></td>
                                                            <td><a href="<?=base_url("uploads/rekomendasi_log/".$val->nama_file)?>" ><?= $val->nama_file ?></a></td>
                                                            <!-- <td>
                                                                <a href="<?= site_url('po_peserta/detail/'. $val->id) ?>" class="btn btn-icon btn-pure dark" title="Mengirimkan data" >
                                                                    <i class="la la-file"></i>
                                                                </a>
                                                            </td> -->
                                                        </tr>
                                                    <?php $no++; endforeach ?>
                                                    <?php endif ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
    endif;
?>




