<?php// if(!has_permission('CREATE_ACCOUNT') OR !has_permission('UPDATE_ARTIKEL')) return; ?>

<div class="row">
    <div class="col-12">
        <div class="card p-1">
            <div class="card-body">
                <form method="POST" enctype="multipart/form-data" action="<?= site_url('po_ujian/save') ?>" class="form form-horizontal" >
                    <input type="hidden" autocomplete="off" name="id" id="id" value="<?=@$data->id?>" class="form-control " >
                    <div class="form-body">
                        <!-- <h3 class="row-section">Pengajuan Perizinan</h3> -->
                        <div class="form-group row ">
                            <label for="tanggal_mulai" class="col-md-2 label-control text-left">Name*</label>
                            <div class="col-md-3 col-sm-12">
                                <input type="text" autocomplete="off" name="name" id="name" value="<?=@$data->name?>" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group row ">
                            <label for="Judul" class="col-md-2 label-control text-left">Url*</label>
                            <div class="col-md-8 col-sm-12">
                                <input type="text" autocomplete="off" name="url" id="url" value="<?=@$data->url?>" class="form-control " >
                            </div>
                        </div>
                        <div class="form-group row ">
                            <label for="alasan" class="col-md-2 label-control text-left">Keterangan*</label>
                            <div class="col-md-10">
                                <textarea name="deskripsi" id="deskripsi" class="form-control tinymce" rows="4" ><?=@$data->deskripsi?></textarea>
                            </div>
                        </div>
                        <div class="form-group row ">
                            <label for="id_kategori" class="col-md-2 label-control text-left">Status *</label>
                            <div class="col-md-10">
                                <select name="status" id="status" class="form-control square select2" required>
                                    <option value="" selected disabled>Pilih..</option>
                                    <?php foreach($refStatus as $l): ?>
                                        <option value="<?= $l->id ?>" <?=(@$data->status==$l->id)?"selected":" ";?>><?= $l->name ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row ">
                            <?= csrf_field(); ?>
                            <label for="id_role" class="col-md-2 label-control text-left"></label>
                            <div class="col-md-10">
                                <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                                <a href="<?= site_url('po_ujian/') ?>" class="btn btn-default pull-right mr-1">Batal</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<style>
    /* .input-group-append {
        display: none;
    } */
</style>

<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript">

$(function() {

});

</script>

