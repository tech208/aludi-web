<?php
    $personal = personal();
?>

<div class="row">
    <div class="col-12">
        <div class="card p-1">
            <div class="row">
                <div class="col-sm-12 col-md-3">
                    <a href="<?=site_url('/dokumentasiApi/')?>" class="btn  btn-block">
                        Dokumentasi Rest API
                    </a>
                </div>
                <div class="col-sm-12 col-md-3">
                    <a href="<?=site_url('/dokumentasiApi/referensi')?>" class="btn btn-outline-primary btn-block">
                        Referensi Data Rest API
                    </a>
                </div>
            </div>
        </div>
    </div>


    <div class="col-12">
        <div class="card p-1">
            <div class="card-header">
                <a class="" data-toggle="collapse" href="#negara" role="button" aria-expanded="false" aria-controls="negara">
                    <h3>Negara</h3>
                </a>
            </div>
            <div class="card-body collapse" id="negara">
            
                <table class="table table-striped data_list dataTable no-footer dtr-inline collapsed" width="100%">
                    <thead>
                        <tr>
                            <th  width="10px">No</th>
                            <th >Kode</th>
                            <th >Negara</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $no = 1; 
                            foreach ($data['negara'] as $key => $value):
                        ?>
                        <tr>
                            <th ><?=$no;?></th>
                            <td ><?=$value['kode'];?></td>
                            <td ><?=$value['nama'];?></td>
                        </tr>
                        <?php
                            $no++;
                            endforeach;
                        ?>
                    </tbody>
                </table>

            </div>
        </div>
    </div>
    
    <div class="col-12">
        <div class="card p-1">
            <div class="card-header">
                <a class="" data-toggle="collapse" href="#provinsi" role="button" aria-expanded="false" aria-controls="provinsi">
                    <h3>Provinsi</h3>
                </a>
            </div>
            <div class="card-body collapse" id="provinsi">
            
                <table class="table table-striped data_list dataTable no-footer dtr-inline collapsed" width="100%">
                    <thead>
                        <tr>
                            <th  width="10px">No</th>
                            <th >Kode</th>
                            <th >Negara</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $no = 1; 
                            foreach ($data['provinsi'] as $key_provinsi => $value_provinsi):
                        ?>
                        <tr>
                            <th ><?=$no;?></th>
                            <td ><?=$value_provinsi['kode'];?></td>
                            <td ><?=$value_provinsi['nama'];?></td>
                        </tr>
                        <?php
                            $no++;
                            endforeach;
                        ?>
                    </tbody>
                </table>

            </div>
        </div>
    </div>

    <div class="col-12">
        <div class="card p-1">
            <div class="card-header">
                <a class="" data-toggle="collapse" href="#kabupaten" role="button" aria-expanded="false" aria-controls="kabupaten">
                    <h3>Kabupaten/Kota</h3>
                </a>
            </div>
            <div class="card-body collapse" id="kabupaten">
            
                <table class="table table-striped data_list dataTable no-footer dtr-inline collapsed" width="100%">
                    <thead>
                        <tr>
                            <th  width="10px">No</th>
                            <th >Kode</th>
                            <th >Provinsi</th>
                            <th >Negara</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $no = 1; 
                            foreach ($data['kabupaten'] as $key_kabupaten => $value_kabupaten):
                        ?>
                        <tr>
                            <th ><?=$no;?></th>
                            <td ><?=$value_kabupaten['id'];?></td>
                            <td ><?=$value_kabupaten->provinsis->nama;?></td>
                            <td ><?=$value_kabupaten['nama'];?></td>
                        </tr>
                        <?php
                            $no++;
                            endforeach;
                        ?>
                    </tbody>
                </table>

            </div>
        </div>
    </div>

    <div class="col-12">
        <div class="card p-1">
            <div class="card-header">
                <a class="" data-toggle="collapse" href="#jenis_penerbit" role="button" aria-expanded="false" aria-controls="jenis_penerbit">
                    <h3>Jenis Penerbit</h3>
                </a>
            </div>
            <div class="card-body collapse" id="jenis_penerbit">
            
                <table class="table table-striped data_list dataTable no-footer dtr-inline collapsed" width="100%">
                    <thead>
                        <tr>
                            <th  width="10px">No</th>
                            <th >Kode</th>
                            <th >Jenis</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $no = 1; 
                            foreach ($data['jenis_penerbit'] as $key_jenis_penerbit => $value_jenis_penerbit):
                        ?>
                        <tr>
                            <th ><?=$no;?></th>
                            <td ><?=$value_jenis_penerbit['id'];?></td>
                            <td ><?=$value_jenis_penerbit['nama'];?></td>
                        </tr>
                        <?php
                            $no++;
                            endforeach;
                        ?>
                    </tbody>
                </table>

            </div>
        </div>
    </div>

    
    <div class="col-12">
        <div class="card p-1">
            <div class="card-header">
                <a class="" data-toggle="collapse" href="#status" role="button" aria-expanded="false" aria-controls="status">
                    <h3>Status</h3>
                </a>
            </div>
            <div class="card-body collapse" id="status">
            
                <table class="table table-striped data_list dataTable no-footer dtr-inline collapsed" width="100%">
                    <thead>
                        <tr>
                            <th  width="10px">No</th>
                            <th >Kode</th>
                            <th >Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $no = 1; 
                            foreach ($data['status'] as $key_status => $value_status):
                        ?>
                        <tr>
                            <th ><?=$no;?></th>
                            <td ><?=$value_status['id'];?></td>
                            <td ><?=$value_status['name'];?></td>
                        </tr>
                        <?php
                            $no++;
                            endforeach;
                        ?>
                    </tbody>
                </table>

            </div>
        </div>
    </div>

</div>
<style>
    .input-group-append {
        display: none;
    }
    .input-group {
        margin-top: unset;
    }
</style>

