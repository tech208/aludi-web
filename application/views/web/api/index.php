<?php
    $personal = personal();
?>

<div class="row">
    <div class="col-12">
        <div class="card p-1">
            <div class="row">
                <div class="col-sm-12 col-md-3">
                    <a href="<?=site_url('/dokumentasiApi/')?>" class="btn btn-outline-primary btn-block">
                        Dokumentasi Rest API
                    </a>
                </div>
                <div class="col-sm-12 col-md-3">
                    <a href="<?=site_url('/dokumentasiApi/referensi')?>" class="btn  btn-block">
                        Referensi Data Rest API
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="col-12">
        <div class="card p-1">
            <div class="card-body">
                <form method="POST" action="<?= site_url('dokumentasiApi/update_token/') ?>" class="form form-horizontal row-separator">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class=" form-group">
                                    <span><h3><label>Token Production <!--<a href=""><i class="ft-copy"></i></a>--></label></h3><small>Kadaluarsa Pada <code><?=MasTanggal(MasKaryawan()->user->token_expired)?> 23:59</code></small></span>
                                    <hr>
                                    <input type="text" value="<?= MasKaryawan()->user->token ?>" name="no_karyawan" id="no_karyawan" class="form-control" disabled>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="form-group ">
                            <button type="submit" class="btn btn-primary pull-right ml-2">Update Token</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
	
	<div class="col-12">
        <div class="card p-1">
			<div class="card-header">
                <a class="" data-toggle="collapse" href="#token_test" role="button" aria-expanded="false" aria-controls="token_test">
                    <h3>Token Dummy</h3>
                </a>
            </div>
            <div class="card-body collapse" id="token_test">
                <form method="POST" action="<?= site_url('dokumentasiApi/update_token_test/') ?>" class="form form-horizontal row-separator">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class=" form-group">
                                    <span><small>Kadaluarsa Pada <code><?=MasTanggal(MasKaryawan()->user->token_expired_test)?> 23:59</code></small></span>
                                    <hr>
                                    <input type="text" value="<?= MasKaryawan()->user->token_test ?>" name="no_karyawan" id="no_karyawan" class="form-control" disabled>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="form-group ">
                            <button type="submit" class="btn btn-primary pull-right ml-2">Update Token</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
	
	<div class="col-12">
        <div class="card p-1">
			<div class="card-header">
                <a class="" data-toggle="collapse" href="#secret_key" role="button" aria-expanded="false" aria-controls="token_test">
                    <h3>Secret Key</h3>
                </a>
            </div>
            <div class="card-body collapse" id="secret_key">
                <form method="POST" action="<?= site_url('dokumentasiApi/update_secret_key/') ?>" class="form form-horizontal row-separator">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class=" form-group">
                                    
                                    <hr>
                                    <input type="text" value="<?= MasKaryawan()->user->secret_key ?>" name="no_karyawan" id="no_karyawan" class="form-control" disabled>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="form-group ">
                            <button type="submit" class="btn btn-primary pull-right ml-2">Change Secret Key</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="col-12">
        <div class="card p-1">
            <div class="card-header">
                <a class="" data-toggle="collapse" href="#auth" role="button" aria-expanded="false" aria-controls="auth">
                    <h3>Auth</h3>
                </a>
            </div>
            <div class="card-body collapse" id="auth">
            
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th  width="10px">No</th>
                            <th >Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th >1</th>
                            <td>
                                <a class="" title="selected" data-toggle="collapse" href="#dashboardSub" role="button" aria-expanded="false" aria-controls="dashboardSub">
                                    <h4>Refresh Token</h4>
                                </a>
                                <table class="table table-bordered collapse" id="dashboardSub">
                                    <tr>
                                        <td colspan="2">
                                            <code>Digunakan untuk memperbarui token yang sudah kadaluarsa.</code>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="30px">url prod</td>
                                        <td>
                                            <a ><i class="ft-copy"></i></a>
                                            <code><?=site_url('/api/v1/auth_api/refresh_token')?></code>
                                        </td>
                                    </tr>
									<tr>
                                        <td width="30px">url dummy</td>
                                        <td>
                                            <a ><i class="ft-copy"></i></a>
                                            <code><?=site_url('/api/test/auth_api/refresh_token')?></code>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="30px">method</td>
                                        <td>
                                            POST
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>params</td>
                                        <td>
                                            <ul>
                                                <li>
                                                    <label>Body</label>
                                                    <ul>
                                                        <li>email*</li>
                                                        <li>password*</li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
									<tr>
                                        <td>response code</td>
                                        <td>
                                            
                                               
                                                    
                                                    <ul>
                                                        <li>200 : Success</li>
                                                        <li>400 : User / Password / Secret Key Not Match</li>
                                                    </ul>
                                               
                                           
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>ex response</td>
                                        <td>
                                        <pre class="EnlighterJSRAW" data-enlighter-language="json" data-enlighter-theme="atomic" data-enlighter-highlight="" data-enlighter-linenumbers="false" data-enlighter-lineoffset="" data-enlighter-title="" data-enlighter-group="">
                                        [{"data":{token : xxxx}},
                                        "pesan":{"1":"Berhasil Memperbarui Token"},
                                        "respon":{
                                            "kode":"200",
                                            "nama":"Ok",
                                            "kode_grup":"success"}
                                        }]
                                    </pre>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>

            </div>
        </div>
    </div>

    <div class="col-12">
        <div class="card p-1">
            <div class="card-header">
                <a class="" data-toggle="collapse" href="#penyelenggara" role="button" aria-expanded="false" aria-controls="penyelenggara">
                    <h3>Penyelenggara</h3>
                </a>
            </div>
            <div class="card-body collapse" id="penyelenggara">
            
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th  width="10px">No</th>
                            <th >Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th >1</th>
                            <td>
                                <a class="" title="selected" data-toggle="collapse" href="#penyelenggaraSelect" role="button" aria-expanded="false" aria-controls="penyelenggaraSelect">
                                    <h4>Pulling Data</h4>
                                </a>
                                <table class="table table-bordered collapse" id="penyelenggaraSelect">
                                    <tr>
                                        <td colspan="2">
                                            <code>Digunakan untuk para penyelenggara melakukan melakukan pulling data yang nanti akan dilaporkan ke ojk</code>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="30px">url prod</td>
                                        <td>
                                            <a ><i class="ft-copy"></i></a>
                                            <code><?=base_url('api/v1/penyelenggara/pulling')?></code>
                                        </td>
                                    </tr>
									<tr>
                                        <td width="30px">url dummy</td>
                                        <td>
                                            <a ><i class="ft-copy"></i></a>
                                            <code><?=base_url('api/test/penyelenggara/pulling')?></code>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="30px">method</td>
                                        <td>
                                            POST
                                        </td>
                                    </tr>
									<tr>
                                        <td width="30px">Note</td>
                                        <td>
                                            <ul>
                                                <li>`kode_penerbit` diambil diambil dari API Penerbit->View</li>
                                                <li>`id_provinsi` diambil dari <?= site_url()?>/dokumentasiApi/referensi (Provinsi)</li>
                                                <li>`id_industri` diambil dari <?= site_url()?>/dokumentasiApi/referensi (Jenis Penerbit)</li>
                                                <li>`tanggal_listed` menggunakan format YYYY-mm-dd (Ex: 2022-06-27)</li>
                                                <li>`tanggal_fully_funded` menggunakan format YYYY-mm-dd (Ex: 2022-06-27)</li>
											</ul>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>params</td>
                                        <td>
                                            <ul>
                                                <!--<li>
                                                    <label>Header</label>
                                                    <ul>
                                                        <li>Token</li>
                                                    </ul>
                                                </li>-->
                                                <li>
                                                    <label>Body</label>
                                                    <ul>
                                                        <li>kode_penerbit*</li>
                                                        <li>id_provinsi*</li>
                                                        <li>id_industri*</li>
                                                        <li>kode_saham*</li>
														<li>jumlah_user</li>
                                                        <li>jumlah_pemodal*</li>
                                                        <li>saham_ditawarkan*</li>
                                                        <li>saham_dihimpun*</li>
                                                        <li>jumlah_dividen</li>
                                                        <li>saham_syariah_ditawarkan</li>
                                                        <li>saham_syariah_dihimpun</li>
														<li>jumlah_dividen_syariah</li>
                                                        <li>sukuk_ditawarkan</li>
														<li>sukuk_dihimpun</li>
														<li>obligasi_ditawarkan</li>
                                                        <li>obligasi_dihimpun</li>
                                                        <li>pembagian_bunga</li>
                                                        <li>tenor</li>
                                                        <li>tanggal_listed</li>
                                                        <li>tanggal_fully_funded</li>

                                                    </ul>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
									<tr>
                                        <td>response code</td>
                                        <td>
                                            
                                               
                                                    
                                                    <ul>
                                                        <li>201 : Insert Success</li>
                                                        <li>400 : Requirement Not Fulfilled</li>
                                                        <li>401 : Token Invalid</li>
                                                    </ul>
                                               
                                           
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>response</td>
                                        <td>
                                        <pre class="EnlighterJSRAW" data-enlighter-language="json" data-enlighter-theme="atomic" data-enlighter-highlight="" data-enlighter-linenumbers="false" data-enlighter-lineoffset="" data-enlighter-title="" data-enlighter-group="">
                                        [{"data":{...}},
                                        "pesan":{"1":"Berhasil Input Laporan Penerbit"},
                                        "respon":{
                                            "kode":"201",
                                            "nama":"Created",
                                            "kode_grup":"success"}
                                        }]
                                    </pre>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <!--<tr>
                            <th >2</th>
                            <td>
                                <a class="" title="created" data-toggle="collapse" href="#penyelenggaraCreated" role="button" aria-expanded="false" aria-controls="penyelenggaraCreated">
                                    <h4>view</h4>
                                </a>
                                <table class="table table-bordered collapse" id="penyelenggaraCreated">
                                    <tr>
                                        <td colspan="2">
                                            <code>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ipsum, temporibus quidem perferendis quos numquam vel! Debitis harum ratione molestias doloribus quod facere id sapiente consequatur, dicta iure adipisci molestiae culpa.</code>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="30px">url prod</td>
                                        <td>
                                            <a ><i class="ft-copy"></i></a>
                                            <code><?=site_url('/api/v1/penyelenggara/pulling_view')?></code>
                                        </td>
                                    </tr>
									<tr>
                                        <td width="30px">url dummy</td>
                                        <td>
                                            <a ><i class="ft-copy"></i></a>
                                            <code><?=site_url('/api/test/penyelenggara/pulling_view')?></code>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="30px">method</td>
                                        <td>
                                            POST
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>params</td>
                                        <td>
                                            <ul>
                                                <li>
                                                    <label>Header</label>
                                                    <ul>
                                                        <li>Token</li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <label>Body</label>
                                                    <ul>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>response</td>
                                        <td>
                                        <pre class="EnlighterJSRAW" data-enlighter-language="json" data-enlighter-theme="atomic" data-enlighter-highlight="" data-enlighter-linenumbers="false" data-enlighter-lineoffset="" data-enlighter-title="" data-enlighter-group="">
                                        [{"data":{data: array(...) }},
                                        "pesan":{"1":"Data Ditemukan"},
                                        "respon":{
                                            "kode":"200",
                                            "nama":"Ok",
                                            "kode_grup":"success"}
                                        }]
                                    </pre>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>-->
						<!-- 
                        <tr>
                            <th >3</th>
                            <td>
                                <a class="" title="created" data-toggle="collapse" href="#penyelenggaraCreated" role="button" aria-expanded="false" aria-controls="penyelenggaraCreated">
                                    <h4>View All (OJK)</h4>
                                </a>
                                <table class="table table-bordered collapse" id="penyelenggaraCreated">
                                    <tr>
                                        <td colspan="2">
                                            <code>Digunakan oleh akun ojk untuk mengambil data.</code>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="30px">url prod</td>
                                        <td>
                                            <a ><i class="ft-copy"></i></a>
                                            <code><?=base_url()?></code>
                                        </td>
                                    </tr>
									<tr>
                                        <td width="30px">url dummy</td>
                                        <td>
                                            <a ><i class="ft-copy"></i></a>
                                            <code><?=base_url()?></code>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="30px">method</td>
                                        <td>
                                            POST
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>params</td>
                                        <td>
                                            <ul>
                                                <li>
                                                    <label>Header</label>
                                                    <ul>
                                                        <li>Token</li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <label>Body</label>
                                                    <ul>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>response</td>
                                        <td>
                                        <pre class="EnlighterJSRAW" data-enlighter-language="json" data-enlighter-theme="atomic" data-enlighter-highlight="" data-enlighter-linenumbers="false" data-enlighter-lineoffset="" data-enlighter-title="" data-enlighter-group="">
                                        [{"data":{data: array(...) }},
                                        "pesan":{"1":"Data Ditemukan"},
                                        "respon":{
                                            "kode":"200",
                                            "nama":"Ok",
                                            "kode_grup":"success"}
                                        }]
                                    </pre>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
						-->
                    </tbody>
                </table>

            </div>
        </div>
    </div>
    
    <div class="col-12">
        <div class="card p-1">
            <div class="card-header">
                <a class="" data-toggle="collapse" href="#dashboard" role="button" aria-expanded="false" aria-controls="dashboard">
                    <h3>Penerbit</h3>
                </a>
            </div>
            <div class="card-body collapse" id="dashboard">
            
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th  width="10px">No</th>
                            <th >Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th >1</th>
                            <td>
                                <a class="" title="selected" data-toggle="collapse" href="#createdSub" role="button" aria-expanded="false" aria-controls="createdSub">
                                    <h4>Created</h4>
                                </a>
                                <table class="table table-bordered collapse" id="createdSub">
                                    <tr>
                                        <td colspan="2">
                                            <code>Membuat Penerbit</code>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="30px">url prod</td>
                                        <td>
                                            <a ><i class="ft-copy"></i></a>
                                            <code><?=site_url('/api/v1/penerbit/created')?></code>
                                        </td>
                                    </tr>
									<tr>
                                        <td width="30px">url dummy</td>
                                        <td>
                                            <a ><i class="ft-copy"></i></a>
                                            <code><?=site_url('/api/test/penerbit/created')?></code>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="30px">method</td>
                                        <td>
                                            POST
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>params</td>
                                        <td>
                                            <ul>
                                                <!--<li>
                                                    <label>Header</label>
                                                    <ul>
                                                        <li>Token</li>
                                                    </ul>
                                                </li>-->
                                                <li>
                                                    <label>Body</label>
                                                    <ul>
                                                        <li>owner*</li>
                                                        <li>nomor_telepon*</li>
                                                        <li>email*</li>
                                                        <li>nama_perusahaan*</li>
														<li>nomor_telepon_perusahaan</li>
                                                        <li>email_perusahaan</li>
                                                        <li>nama_brand*</li>
                                                        <li>bidang_usaha*</li>
                                                        <li>total_pendanaan*</li>
                                                        <li>deskripsi*</li>
                                                        <li>status*</li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
									<tr>
                                        <td>response code</td>
                                        <td>
                                            
                                               
                                                    
                                                    <ul>
                                                        <li>201 : Insert Success</li>
                                                        <li>400 : Requirement Not Fulfilled</li>
                                                        <li>401 : Token Invalid</li>
                                                        <li>411 : Data Sudah Terdaftar Di System</li>
														<li>412 : Data Sudah Pernah Diinput</li>
                                                    </ul>
                                               
                                           
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>ex response</td>
                                        <td>
                                        <pre class="EnlighterJSRAW" data-enlighter-language="json" data-enlighter-theme="atomic" data-enlighter-highlight="" data-enlighter-linenumbers="false" data-enlighter-lineoffset="" data-enlighter-title="" data-enlighter-group="">
                                        [{"data":{...}},
                                        "pesan":{"1":"Berhasil Menambahkan Data Penerbit"},
                                        "respon":{
                                            "kode":"201",
                                            "nama":"Created",
                                            "kode_grup":"success"}
                                        }]
                                    </pre>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <!--<tr>
                            <th >2</th>
                            <td>
                                <a class="" title="selected" data-toggle="collapse" href="#dashboardSub" role="button" aria-expanded="false" aria-controls="dashboardSub">
                                    <h4>Update Status</h4>
                                </a>
                                <table class="table table-bordered collapse" id="dashboardSub">
                                    <tr>
                                        <td colspan="2">
                                            <code>Memperbarui Status</code>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="30px">url prod</td>
                                        <td>
                                            <a ><i class="ft-copy"></i></a>
                                            <code><?=site_url('/api/v1/penerbit/updated_status')?></code>
                                        </td>
                                    </tr>
									<tr>
                                        <td width="30px">url dummy</td>
                                        <td>
                                            <a ><i class="ft-copy"></i></a>
                                            <code><?=site_url('/api/test/penerbit/updated_status')?></code>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="30px">method</td>
                                        <td>
                                            POST
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>params</td>
                                        <td>
                                            <ul>
                                                <li>
                                                    <label>Header</label>
                                                    <ul>
                                                        <li>Token</li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <label>Body</label>
                                                    <ul>
                                                        <li>kode*</li>
                                                        <li>status*</li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>response</td>
                                        <td>
                                        <pre class="EnlighterJSRAW" data-enlighter-language="json" data-enlighter-theme="atomic" data-enlighter-highlight="" data-enlighter-linenumbers="false" data-enlighter-lineoffset="" data-enlighter-title="" data-enlighter-group="">
                                        [{"data":{}},
                                        "pesan":{"1":"Data Ditemukan"},
                                        "respon":{
                                            "kode":"200",
                                            "nama":"Ok",
                                            "kode_grup":"success"}
                                        }]
                                    </pre>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>-->
						<tr>
                            <th >2</th>
                            <td>
                                <a class="" title="created" data-toggle="collapse" href="#penerbitList" role="button" aria-expanded="false" aria-controls="penerbitList">
                                    <h4>View</h4>
                                </a>
                                <table class="table table-bordered collapse" id="penerbitList">
                                    <tr>
                                        <td colspan="2">
                                            <code>Melihat Data Penerbit</code>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="30px">url prod</td>
                                        <td>
                                            <a ><i class="ft-copy"></i></a>
                                            <code><?=site_url('/api/v1/penerbit/list_view')?></code>
                                        </td>
                                    </tr>
									<tr>
                                        <td width="30px">url dummy</td>
                                        <td>
                                            <a ><i class="ft-copy"></i></a>
                                            <code><?=site_url('/api/test/penerbit/list_view')?></code>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="30px">method</td>
                                        <td>
                                            POST
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>params</td>
                                        <td>
                                            <ul>
                                                <!--<li>
                                                    <label>Header</label>
                                                    <ul>
                                                        <li>Token</li>
                                                    </ul>
                                                </li>-->
                                                <li>
                                                    <label>Body</label>
                                                    <ul>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
									<tr>
                                        <td>response code</td>
                                        <td>
                                            
                                               
                                                    
                                                    <ul>
                                                        <li>200 : Success</li>
                                                        <li>400 : Requirement Not Fulfilled</li>
                                                        <li>401 : Token Invalid</li>
                                                    </ul>
                                               
                                           
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>ex response</td>
                                        <td>
                                        <pre class="EnlighterJSRAW" data-enlighter-language="json" data-enlighter-theme="atomic" data-enlighter-highlight="" data-enlighter-linenumbers="false" data-enlighter-lineoffset="" data-enlighter-title="" data-enlighter-group="">
                                        [{"data": {...} },
                                        "pesan":{"1":"Data Ditemukan"},
                                        "respon":{
                                            "kode":"200",
                                            "nama":"Ok",
                                            "kode_grup":"success"}
                                        }]
                                    </pre>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
						<tr>
                            <th >3</th>
                            <td>
                                <a class="" title="created" data-toggle="collapse" href="#penerbitUpdated" role="button" aria-expanded="false" aria-controls="penerbitUpdated">
                                    <h4>Update</h4>
                                </a>
                                <table class="table table-bordered collapse" id="penerbitUpdated">
                                    <tr>
                                        <td colspan="2">
                                            <code>Memperbarui Data Penerbit</code>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="30px">url prod</td>
                                        <td>
                                            <a ><i class="ft-copy"></i></a>
                                            <code><?=site_url('/api/v1/penerbit/updated')?></code>
                                        </td>
                                    </tr>
									<tr>
                                        <td width="30px">url dummy</td>
                                        <td>
                                            <a ><i class="ft-copy"></i></a>
                                            <code><?=site_url('/api/test/penerbit/updated')?></code>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="30px">method</td>
                                        <td>
                                            POST
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>params</td>
                                        <td>
                                            <ul>
                                                <!--<li>
                                                    <label>Header</label>
                                                    <ul>
                                                        <li>Token</li>
                                                    </ul>
                                                </li>-->
                                                <li>
                                                    <label>Body</label>
                                                    <ul>
                                                        <li>kode*</li>
                                                        <li>owner</li>
                                                        <li>nomor_telepon</li>
                                                        <li>email</li>
                                                        <li>nama_perusahaan</li>
                                                        <li>nama_brand</li>
                                                        <li>bidang_usaha</li>
                                                        <li>total_pendanaan</li>
                                                        <li>deskripsi</li>
                                                        <li>status</li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
									<tr>
                                        <td>response code</td>
                                        <td>
                                            
                                               
                                                    
                                                    <ul>
                                                        <li>202 : Success</li>
                                                        <li>400 : Requirement Not Fulfilled</li>
                                                        <li>401 : Token Invalid</li>
                                                    </ul>
                                               
                                           
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>ex response</td>
                                        <td>
                                        <pre class="EnlighterJSRAW" data-enlighter-language="json" data-enlighter-theme="atomic" data-enlighter-highlight="" data-enlighter-linenumbers="false" data-enlighter-lineoffset="" data-enlighter-title="" data-enlighter-group="">
                                        [{"data":{...}},
                                        "pesan":{"1":"Berhasil Memperbarui Data Penerbit"},
                                        "respon":{
                                            "kode":"202",
                                            "nama":"Updated",
                                            "kode_grup":"success"}
                                        }]
                                    </pre>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
						<tr>
                            <th >4</th>
                            <td>
                                <a class="" title="created" data-toggle="collapse" href="#penerbitDelete" role="button" aria-expanded="false" aria-controls="penerbitDelete">
                                    <h4>Delete</h4>
                                </a>
                                <table class="table table-bordered collapse" id="penerbitDelete">
                                    <tr>
                                        <td colspan="2">
                                            <code>Hapus Data Penerbit</code>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="30px">url prod</td>
                                        <td>
                                            <a ><i class="ft-copy"></i></a>
                                            <code><?=site_url('/api/v1/penerbit/deleted')?></code>
                                        </td>
                                    </tr>
									<tr>
                                        <td width="30px">url dummy</td>
                                        <td>
                                            <a ><i class="ft-copy"></i></a>
                                            <code><?=site_url('/api/test/penerbit/deleted')?></code>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="30px">method</td>
                                        <td>
                                            POST
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>params</td>
                                        <td>
                                            <ul>
                                                <!--<li>
                                                    <label>Header</label>
                                                    <ul>
                                                        <li>Token</li>
                                                    </ul>
                                                </li>-->
                                                <li>
                                                    <label>Body</label>
                                                    <ul>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
									<tr>
                                        <td>response code</td>
                                        <td>
                                            
                                               
                                                    
                                                    <ul>
                                                        <li>203 : Success</li>
                                                        <li>400 : Requirement Not Fulfilled</li>
                                                        <li>401 : Token Invalid</li>
                                                    </ul>
                                               
                                           
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>ex response</td>
                                        <td>
                                        <pre class="EnlighterJSRAW" data-enlighter-language="json" data-enlighter-theme="atomic" data-enlighter-highlight="" data-enlighter-linenumbers="false" data-enlighter-lineoffset="" data-enlighter-title="" data-enlighter-group="">
                                        [{"data":{data: array(...) }},
                                        "pesan":{"1":"Data Pernerbit Berhasil Dihapus"},
                                        "respon":{
                                            "kode":"203",
                                            "nama":"Deleted",
                                            "kode_grup":"success"}
                                        }]
                                    </pre>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>

            </div>
        </div>
    </div>

</div>
<style>
    .input-group-append {
        display: none;
    }
    .input-group {
        margin-top: unset;
    }
</style>

