
<div class="row">
    <div class="col-12">
        <div class="card p-1">
            <div class="row">
                <div class="col-sm-12 col-md-7">
                    <form action="#" class="form-inline">
                    </form>
                </div>

                <div class="col-12 col-md-1">
                    <?php if(has_permission('IMPORT_USER')): ?>
                        <!-- <button data-action="import" class="btn btn-outline-teal btn-block" title="Import user data from excel" data-toggle="modal" data-target="#importModal">
                            <i class="material-icons">cloud_upload</i>&nbsp;
                        </button> -->
                    <?php endif; ?>
                </div>

                <div class="col-12 col-md-1">
                    <?php if(has_permission('EXPORT_USER')): ?>
                        <a href="<?= site_url('user/export_data'); ?>" target="_blank" class="btn btn-outline-teal btn-block" title="Download user data to excel">
                            <i class="la la-download"></i>&nbsp;
                    </a>
                    <?php endif; ?>
                </div>

                <div class="col-sm-12 col-md-3">
                    <?php if(has_permission('CREATE_USER')): ?>
                        <a href="<?= site_url('user/create'); ?>" class="btn btn-outline-primary btn-block">
                            Tambah Karyawan
                        </a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if(!has_permission('READ_USER')) return ?>

<div class="row">
    <div class="col-12">
        <div class="card p-1">
            <div class="row">
                <div class="col-sm-12 ">
                    <table class="table table-striped data_list">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>ID Karyawan</th>
                                <th>Nama</th>
                                <th>Jabatan</th>
                                <th>Tgl Bergabung</th>
                                <th>No HP</th>
                                <th>Jenis</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if($data->count()>0): ?>
                            <?php $no=1; 
                                foreach($data as $val): ?>
                                <tr>
                                    <td><?= $no ?></td>
                                    <td><?= $val->uuid ?></td>
                                    <td><?= $val->personal->name ?></td>
                                    <td><?= $val->personal->jobHistory->jobTitle->job_titles ?></td>
                                    <td><?= (!empty($val->personal->join_date)) ? hari_tgl_indo($val->personal->join_date) : "~" ;  ?></td>
                                    <td><?= (!empty($val->personal->phone)) ? $val->personal->phone : "~" ;  ?></td>
                                    <td><?= $val->personal->jobHistory->job_status ?></td>
                                    <td><?= ($val->personal->status==1)? "Aktif":"Tidak Aktif"; ?></td>
                                    <td>
                                        <a href="<?=site_url('user/update_detail/'. $val->id )?>" class="btn btn-icon btn-pure dark" title="Merubah data" >
                                            <i class="la la-pencil"></i>
                                        </a>
                                    </td>
                                </tr>
                                <?php $no++; endforeach ?>
                                <?php else: ?>
                                <tr>
                                    <td colspan="999">--Data Tidak Ada--</td>
                                </tr>
                                <?php endif ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="importModal" tabindex="-1" role="dialog" aria-labelledby="importModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="<?= site_url('user/import_user_from_excel') ?>" method="post" enctype="multipart/form-data">
                <div class="modal-header">
                    <h5 class="modal-title" id="importModalLabel">Import User From Excel</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="file_user" class="form-label">File Excel</label>
                        <input type="file" name="file_user" id="file_user" accept=".xlsx,.xls">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary text-white" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary text-white">Import</button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- 
<script >
        $(document).ready(function() {
            
            $('#tbl_karyawan').DataTable({
                responsive: true
            });    
        });
    </script> -->