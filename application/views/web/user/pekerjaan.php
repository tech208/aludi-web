<div class="row">
    <div class="col-12">
        <div class="card p-1">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-3 col-sm-12">
                        <img src="<?= $user->personal->avatar_path() ?>" alt="Photo profile" class="rounded-circle img-border height-150 mx-auto d-block">
                    </div>
                    <div class="col-md-9 col-sm-12 pt-2">
                        <h3><?= $user->personal->name ?></h3>
                        <p><?= $user->personal->jobHistory->jobTitle->job_titles ?></p>
                        <p><?= $user->email ?></p>
                    </div>
                </div>
                <div class="row mt-2 pb-2 border-bottom">
                    <div class="col-12">
                        <ul class="nav nav-pills nav-active-bordered-pill nav-justified">
                            <li class="nav-item">
                                <a href="<?= site_url('user/detail/'. $user->id) ?>" class="nav-link">Basic Information</a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= site_url('user/pekerjaan/'. $user->id) ?>" class="nav-link active">Pekerjaan</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card p-1">
                            <div class="row">

                                <div class="col-sm-12 col-md-3">
                                    <?php if(has_permission('UPDATE_PEKERJAAN')): ?>
                                        <a href="<?= site_url('user/form_pekerjaan/'. $user->id); ?>" class="btn btn-outline-primary btn-block">
                                            Tambah Pekerjaan
                                        </a>
                                    <?php endif; ?>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12 pt-2 table-responsive">
                        <table id="tbl_karyawan" class="table table-striped data_list">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tanggal Efektif</th>
                                    <th>Tanggal Mulai</th>
                                    <th>Tanggal Selesai</th>
                                    <th>Jenis Karyawan</th>
                                    <th>Periode</th>
                                    <th>Posisi</th>
                                    <th>Hak Akses</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no=1; ?>
                                <?php foreach($data as $val): ?>
                                <tr>
                                    <td><?= $no ?></td>
                                    <td><?= ($val->effective_date)?tgl_indo($val->effective_date):'<i>(Belum Disetting)</i>'; ?></td>
                                    <td><?= ($val->start_date)?tgl_indo($val->start_date):'<i>(Belum Disetting)</i>'; ?></td>
                                    <td>
                                        <?php if($val->saat_ini==0){
                                                if(!empty($val->end_date)){
                                                    echo tgl_indo($val->end_date);
                                                }
                                                else{ 
                                                    echo'<i>(Belum Disetting)</i>'; 
                                                }
                                            }
                                            else{
                                                echo '<i>(Sampai Saat Ini)</i>';
                                            }
                                         ?>
                                    </td>
                                    <td><?= $val->job_status ?></td>
                                    <td><?= $val->employment_period ? \App\Constants\UserManagement::PERIOD_LIST[$val->employment_period] : '-' ?></td>
                                    <td><?= $val->jobTitle->job_titles ?></td>
                                    <td>
                                        <?php foreach($val->akses as $ac): ?>
                                            <li><?= $ac->access ?></li>
                                        <?php endforeach ?>
                                    </td>
                                    <td>
                                        <a href="<?=site_url('user/form_pekerjaan/'. $user->id.'/'.$val->id )?>" class="btn btn-icon btn-pure dark" title="Merubah data" >
                                            <i class="la la-pencil"></i>
                                        </a>
                                        
                                        <a href="<?=site_url('user/delete_pekerjaan/'. $val->id )?>" class="btn btn-icon btn-pure dark" title="Menghapus data" onclick="return confirm('Yakin Menghapus?');">
                                            <i class="la la-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>