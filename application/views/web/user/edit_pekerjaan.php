
<?php if(!has_permission('UPDATE_PEKERJAAN')) return; ?>
<link rel="stylesheet" type="text/css" href="<?= base_url('app-assets/vendors/css/forms/selects/select2.min.css') ?>">
<link rel="stylesheet" type="text/css" href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css">
<div class="row">
    <div class="col-12">
        <div class="card p-1">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-3 col-sm-12">
                        <img src="<?= $user->personal->avatar_path() ?>" alt="Photo profile" class="rounded-circle img-border height-150 mx-auto d-block">
                    </div>
                    <div class="col-md-9 col-sm-12 pt-2">
                        <h3><?= $user->personal->name ?></h3>
                        <p><?= $user->personal->jobHistory->jobTitle->job_titles ?></p>
                        <p><?= $user->email ?></p>
                    </div>
                </div>
                <div class="row mt-2 pb-2 border-bottom">
                    <div class="col-12">
                        <ul class="nav nav-pills nav-active-bordered-pill nav-justified">
                            <li class="nav-item">
                                <a href="<?= site_url('user/detail/'. $user->id) ?>" class="nav-link">Basic Information</a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= site_url('user/pekerjaan/'. $user->id) ?>" class="nav-link active">Pekerjaan</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <form method="POST" action="<?= site_url('user/update_pekerjaan/'. $user->id) ?>" class="form">
                    <div class="form-body pt-1">
                        <h3 class="row-section text-bold mb-1">Add Employment Status</h3>
                        <div class="row">
                            <div class="col-md-4 col-sm-12">
                                <div class="form-group">
                                    <label for="posisi">Posisi</label>
                                    <select name="posisi" id="posisi" class="form-control select2" required>
                                        <option value="" selected disabled>Pilih Jabatan</option>
                                        <?php foreach($job_titles as $job): ?>
                                            <option value="<?= $job->id ?>" <?= $job->id == $user->personal->jobHistory->job_title_id ? 'selected':'' ?>><?= $job->job_titles ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-12">
                                <div class="form-group">
                                    <label for="effective_date">Effective Date</label>
                                    <input type="text" name="effective_date" value="<?= $user->personal->jobHistory->effective_date ?>" id="effective_date" class="form-control datepicker">
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-12">
                                <div class="form-group">
                                    <label for="period">Period</label>
                                    <select name="period" id="period" class="form-control select2" required>
                                        <option value="" selected disabled>Pilih Period</option>
                                        <?php foreach(\App\Constants\UserManagement::PERIOD_LIST as $i => $period): ?>
                                            <option value="<?= $i ?>" <?= $i == $user->personal->jobHistory->employment_period ? 'selected':'' ?>><?= $period ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-12">
                                <div class="form-group">
                                    <label for="status">Employment Status</label>
                                    <select name="status" id="status" class="form-control select2" required>
                                        <option value="" selected disabled>Jenis Karyawan</option>
                                        <?php foreach(\App\Constants\UserManagement::TYPE_LIST as $type): ?>
                                            <option value="<?= $type ?>" <?= $type == $user->personal->jobHistory->job_status ? 'selected':'' ?>><?= $type ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-12">
                                <div class="form-group">
                                    <label for="basic_salary">Basic Salary</label>
                                    <input type="number" name="basic_salary" value="<?= $user->personal->jobHistory->basic_salary ?>" id="basic_salary" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="allowance">Allowance</label>
                                    <select name="allowance[]" multiple id="allowance" class="form-control select2">
                                        <?php foreach($allowances as $allowance): ?>
                                            <option value="<?= $allowance->id ?>"><?= $allowance->allowance ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="benefit">Benefit</label>
                                    <select name="benefit[]" multiple id="benefit" class="form-control select2">
                                        <?php foreach($benefits as $benefit): ?>
                                            <option value="<?= $benefit->id ?>"><?= $benefit->benefit ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="hak_akses">Hak Akses</label>
                                    <select name="hak_akses[]" multiple id="hak_akses" class="form-control select2">
                                        <?php foreach($accesses as $access): ?>
                                            <option value="<?= $access->id ?>"><?= $access->access ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary pull-right">SIMPAN</button>
                                    <a href="<?= site_url('user/pekerjaan/'. $user->id) ?>" class="btn btn-default pull-right mr-1">BATAL</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<style>
    .input-group-append {
        display: none;
    }
</style>
<script src="<?= base_url('app-assets/vendors/js/forms/select/select2.full.min.js') ?>"></script>
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js"></script>
<script>
$('.select2').select2();
$('.datepicker').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
    zIndexOffset: 1500,
    uiLibrary: 'bootstrap4',
    icons: {
        rightIcon: false
    },
    modal: true, header: true, footer: true
});
</script>
