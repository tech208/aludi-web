<?php
require_once(APPPATH.'constants/UserManagement.php');
?>
<link rel="stylesheet" type="text/css" href="<?= base_url('app-assets/vendors/css/forms/selects/select2.min.css') ?>">
<link rel="stylesheet" type="text/css" href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css">
<div class="row">
    <div class="col-12">
        <div class="card p-1">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-3 col-sm-12">
                        <img src="<?= $user->personal->avatar_path() ?>" alt="Photo profile" class="rounded-circle img-border height-150 mx-auto d-block">
                    </div>
                    <div class="col-md-9 col-sm-12 pt-2">
                        <h3><?= $user->personal->name ?></h3>
                        <p><?= $user->personal->jobHistory->jobTitle->job_titles ?></p>
                        <p><?= $user->email ?></p>
                    </div>
                </div>
                <div class="row mt-2 pb-2 border-bottom">
                    <div class="col-12">
                        <ul class="nav nav-pills nav-active-bordered-pill nav-justified">
                            <li class="nav-item">
                                <a href="<?= site_url('user/detail/'. $user->id) ?>" class="nav-link active">Basic Information</a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= site_url('user/pekerjaan/'. $user->id) ?>" class="nav-link">Pekerjaan</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row pt-2">
                    <div class="col-12">
                        <?php if(has_permission('UPDATE_USER')): ?>
                            <form method="POST" action="<?= site_url('user/update_detail/'. $user->id) ?>" class="form">
                        <?php endif ?>
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label for="id_karyawan">ID Karyawan</label>
                                            <input type="text" name="id_karyawan" value="<?= $user->uuid ?>" id="id_karyawan" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label for="status">Status</label>
                                            <select name="status" id="status" class="form-control select2">
                                                <option value="" selected disabled>Pilih Status</option>
                                                <?php foreach(\App\Constants\UserManagement::STATUS_LIST as $i => $status): ?>
                                                    <option value="<?= $i ?>" <?= $i == $user->personal->status ? 'selected':'' ?>><?= $status ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label for="name">Nama Lengkap</label>
                                            <input type="text" name="name" value="<?= $user->personal->name ?>" id="name" class="form-control" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label for="no_ktp">No KTP</label>
                                            <input type="text" name="no_ktp" value="<?= $user->personal->idcard_number; ?>" id="no_ktp" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label for="npwp">NPWP</label>
                                            <input type="text" name="npwp" value="<?= $user->personal->npwp_number; ?>" id="npwp" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 col-sm-12">
                                        <div class="form-group">
                                            <label for="tempat_lahir">Tempat Lahir</label>
                                            <input type="text" name="tempat_lahir" value="<?= $user->personal->place_of_birth; ?>" id="tempat_lahir" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-12">
                                        <div class="form-group">
                                            <label for="tgl_lahir">Tanggal Lahir</label>
                                            <input type="text" name="tgl_lahir" value="<?= $user->personal->date_of_birth; ?>" id="tgl_lahir" class="form-control datepicker" required>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-12">
                                        <div class="form-group">
                                            <label for="religion">Religion</label>
                                            <select name="religion" id="religion" class="form-control select2">
                                                <option value="" selected disabled>Pilih Religion</option>
                                                <?php foreach(\App\Constants\UserManagement::RELIGION_LIST as $religion): ?>
                                                    <option value="<?= $religion ?>" <?= $religion == $user->personal->religion ? 'selected':'' ?>><?= $religion ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label for="gender">Gender</label>
                                            <select name="gender" id="gender" class="form-control select2">
                                                <option value="" disabled selected></option>
                                                <option value="Male" <?= $user->personal->gender == "Male" ? 'selected':'' ?>>Male</option>
                                                <option value="Female" <?= $user->personal->gender == "Female" ? 'selected':'' ?>>Female</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label for="marital_status">Marital Status</label>
                                            <select name="marital_status" id="marital_status" class="form-control select2">
                                                <option value="" selected disabled>Pilih Martial Status</option>
                                                <?php foreach(\App\Constants\UserManagement::MARTIAL_LIST as $martial): ?>
                                                    <option value="<?= $martial ?>" <?= $martial == $user->personal->marital_status ? 'selected':'' ?>><?= $martial ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label for="address">Address</label>
                                            <textarea name="address" id="address" class="form-control" required> <?= $user->personal->address; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label for="tgl_bergabung">Join Date</label>
                                            <input type="text" name="tgl_bergabung" value="<?= $user->personal->join_date; ?>" id="tgl_bergabung" class="form-control datepicker" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label for="id_role">Role</label>
                                            <select name="id_role" id="id_role" class="form-control select2" required>
                                                <option value="" selected disabled>Pilih Role</option>
                                                <?php foreach($roles as $role): ?>
                                                    <option value="<?= $role->id ?>" <?= $role->id == $user->role_id ? 'selected':'' ?>><?= $role->name ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <?php if(has_permission('UPDATE_USER')): ?>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-primary pull-right">SIMPAN</button>
                                                <a href="<?= site_url('user') ?>" class="btn btn-default pull-right mr-1">BATAL</a>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif ?>
                            </div>

                        <?php if(has_permission('UPDATE_USER')): ?>
                            </form>
                        <?php endif ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .input-group-append {
        display: none;
    }
</style>
<script src="<?= base_url('app-assets/vendors/js/forms/select/select2.full.min.js') ?>"></script>
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js"></script>
<script>
$('.select2').select2({
    width: '100%'
});
$('#tgl_lahir').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
    zIndexOffset: 1500,
    uiLibrary: 'bootstrap4',
    icons: {
        rightIcon: ''
    },
    modal: true, header: true, footer: true
});

$('#tgl_bergabung').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
    zIndexOffset: 1500,
    uiLibrary: 'bootstrap4',
    icons: {
        rightIcon: ''
    },
    modal: true, header: true, footer: true
});
</script>
