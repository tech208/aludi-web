<div class="card">
    <div class="card-content collapse show">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12">
                    <form enctype="multipart/form-data" method="POST" action="<?= site_url('po_aduan/save') ?>" class="form form-horizontal" >
                        <input type="hidden" class="form-control" id="id" name="id" value="<?=@$data->id;?>">
                        <div class="form-group">
                            <label>Nama</label>
                            <input type="text" class="form-control" id="nama" name="nama" placeholder="Masukan Nama Lengkap" disabled required value="<?=@$data->nama;?>">
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>No Kontak</label>
                                    <input type="text" class="form-control" id="no_kontak" name="no_kontak"  placeholder="Masukan Nomor Kontak" disabled required value="<?=@$data->no_kontak;?>">
                                </div>
                                <div class="col-md-6">
                                    <label>Email</label>
                                    <input type="email" class="form-control" id="email" name="email"  placeholder="Masukan Email" disabled required value="<?=@$data->email;?>">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Nama Platform</label>
                            <input type="text" class="form-control" id="id_nama_platform" name="id_nama_platform"  placeholder="Masukan Nama Platform" disabled required value="<?=@$data->id_nama_platform;?>">
                        </div>
                        <div class="form-group">
                            <label>Kronologis</label>
                            <textarea class="form-control" id="kronologis" name="kronologis"  placeholder="Masukan Bentuk Aduan" disabled required style="height: 150px;" ><?=@$data->kronologis;?></textarea>
                        </div>
                        <div class="form-group">
                            <a href="<?=base_url("uploads/pengaduan_lampiran/".@$data->lampiran)?>" target="_blank" class="btn btn-success ">Download</a> <?=@$data->lampiran?>
                        </div>
                        <div class="form-group">
                            <label for="location1">Status</label>
                            <select class="custom-select form-control" id="status" name="status" >
                                <?php foreach ($ref_status as $key => $value): ?>
                                <option value="<?=$value->id;?>" <?=($value->id==$data->status) ? "selected" : "" ;?>><?=$value->name;?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <hr>
                        <div class="row px-1">
                            <a href="<?=site_url("/po_aduan")?>" class="btn  col-2 ">Kembali</a>
                            <button type="submit" class="btn btn-primary col-2 ">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>