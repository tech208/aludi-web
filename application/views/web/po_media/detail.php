<script type="text/javascript" src="https://platform-api.sharethis.com/js/sharethis.js#property=5f2ccdb52684e00014234564&product=inline-share-buttons" async="async"></script>

<div class="card">
    <div class="row">
        <div class="col-md-4">
            <a href="#"><img src="<?=base_url("uploads/artikel/".$data->lampiran)?>" alt="" class=" card-img-top img-fluid"></a>
        </div>
        <div class="col-md-6">
            <div class="news-feed-overlay"><span class="badge badge-sm badge-primary float-left news-feed-badge news-feed-badge-trip position-absolute"><?=$data->kategoris->name?></span>
            </div>
            <div class="card-body">
                <h2 class="card-title font-medium-1">
                    <?=$data->name?>
                    <div class="font-small-1 text-muted"><?=tgl_indo($data->tanggal)?></div>
                </h2>
                <?=$data->deskripsi?>
            </div>
        </div>
    </div>
</div>