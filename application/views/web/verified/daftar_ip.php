<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title>Verified IP - PT. SANTARA DAYA INSPIRATAMA</title>
    <link rel="apple-touch-icon" href="<?= base_url() ?>app-assets/images/logo/logo.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?= base_url() ?>app-assets/images/logo/logo.png">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i%7CQuicksand:300,400,500,700" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="<?= base_url('/')?>app-assets/vendors/css/vendors.min.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="<?= base_url('/')?>app-assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url('/')?>app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url('/')?>app-assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url('/')?>app-assets/css/components.css">
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="<?= base_url('/')?>app-assets/css/core/menu/menu-types/vertical-compact-menu.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url('/')?>app-assets/css/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url('/')?>app-assets/fonts/mobiriseicons/24px/mobirise/style.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url('/')?>app-assets/css/pages/under-maintenance.css">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="../../../assets/css/style.css">
    <!-- END: Custom CSS-->

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-compact-menu 1-column  bg-maintenance-image blank-page" data-open="click" data-menu="vertical-compact-menu" data-col="1-column">
    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <section class="flexbox-container">
                    <div class="col-12 d-flex align-items-center justify-content-center">
                        <div class="col-md-4 col-10 box-shadow-2 p-0">
                            <div class="card border-grey border-lighten-3 px-1 py-1 box-shadow-3 m-0">
                                <div class="card-body">
                                    <span class="card-title text-center">
                                        <img src="<?= base_url() ?>app-assets/images/logo/santara logo.png" class="img-fluid mx-auto d-block pt-2" width="250" alt="branding logo" >
                                    </span>
                                </div>
                                <div class="card-body text-center">
                                    <?php
                                        if($this->session->flashdata('error_verified')):
                                    ?>
                                        <div class="alert alert-danger">
                                            <?= $this->session->flashdata('error_verified') ?>
                                        </div>
                                    <?php
                                        endif;
                                    ?>
                                    <h3><b><?=getIp()?></b><br> (Belum Terverifikasi)</h3>
                                    <form class="form-horizontal" action="<?= site_url('verified/saveIp')?>" method="POST" >
                                        <fieldset class="form-group position-relative has-icon-left">
                                            <input type="email" class="form-control" id="user-email" name="email" placeholder="Your Email Address" required>
                                            <div class="form-control-position">
                                                <i class="la la-envelope"></i>
                                            </div>
                                        </fieldset>
                                        <button type="submit" class="btn btn-outline-info btn-lg btn-block"><i class="ft-unlock"></i>Kirim</button>
                                    </form>

                                </div>
                                <hr>
                                <p class="socialIcon card-text text-center pt-2 pb-2">
                                    <a href="http://facebook.com/santaracoid" target="_blank" class="btn btn-social-icon mr-1 mb-1 btn-outline-facebook"><span class="la la-facebook"></span></a>
                                    <a href="https://www.youtube.com/c/SantaraOfficial" target="_blank" class="btn btn-social-icon mr-1 mb-1 btn-outline-twitter"><span class="la la-youtube"></span></a>
                                    <a href="http://linkedin.com/company/santara-marketplace" target="_blank" class="btn btn-social-icon mr-1 mb-1 btn-outline-linkedin"><span class="la la-linkedin font-medium-4"></span></a>
                                    <a href="http://instagram.com/santaracoid" target="_blank" class="btn btn-social-icon mr-1 mb-1 btn-outline-instagram"><span class="la la-instagram font-medium-4"></span></a>
                                </p>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <!-- END: Content-->


    <!-- BEGIN: Vendor JS-->
    <script src="<?= base_url('/')?>app-assets/vendors/js/vendors.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="<?= base_url('/')?>app-assets/js/core/app-menu.js"></script>
    <script src="<?= base_url('/')?>app-assets/js/core/app.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <!-- END: Page JS-->

</body>
<!-- END: Body-->

</html>


                                
