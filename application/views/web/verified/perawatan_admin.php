
    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <section class="flexbox-container">
                    <div class="col-12 d-flex align-items-center justify-content-center">
                        <div class="col-md-4 col-10 box-shadow-2 p-0">
                            <div class="card border-grey border-lighten-3 px-1 py-1 box-shadow-3 m-0">
                                <div class="card-body">
                                    <span class="card-title text-center">
                                        <img src="<?= base_url() ?>app-assets/images/logo/santara logo.png" class="img-fluid mx-auto d-block pt-2" width="250" alt="branding logo" >
                                    </span>
                                </div>
                                <div class="card-body text-center">
                                    <h3>Sistem Sedang Perawatan</h3>
                                    <p>Kami mohon maaf atas ketidaknyamanan ini.
                                        <br> Silakan kunjungi lagi nanti.</p>
                                    <div class="mt-2"><i class="la la-cog spinner font-large-2"></i></div>
                                </div>
                                <hr>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <!-- END: Content-->


    <!-- BEGIN: Vendor JS-->
    <script src="<?= base_url('/')?>app-assets/vendors/js/vendors.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="<?= base_url('/')?>app-assets/js/core/app-menu.js"></script>
    <script src="<?= base_url('/')?>app-assets/js/core/app.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <!-- END: Page JS-->
