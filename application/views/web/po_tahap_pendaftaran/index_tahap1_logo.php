<section id="number-tabs">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <form method="POST" enctype="multipart/form-data" action="<?= site_url('po_tahap_pendaftaran/saveTahap1_logo') ?>" id="form-tahap1" class="form form-horizontal" >
                    <input type="hidden" id="id" name="id" value="<?=@$data->id;?>">
                    <input type="hidden" id="id_personal" name="id_personal" value="<?=empty(@$data->id_personal)?MasKaryawan()->id:@$data->id_personal;?>" >
                    
                    <div class="card-header">
                        <h4 class="card-title">Logo</h4>
                    </div>
                    <div class="card-content collapse show">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="file" class="form-control" id="anggota" name="files[anggota]" required>
                                        <?php if (!empty(@$data->logo_perusahaan) AND file_exists("uploads/anggota/".@$data->logo_perusahaan)) :?>
                                        <div class=mt-1>
                                            <a href="<?=base_url("uploads/anggota/".$data->logo_perusahaan)?>" target="_blank" class="btn btn-success ">Download</a> <?=$data->logo_perusahaan?>
                                        </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    
                    <div class="card-footer">
                        <div class="form-group row ">
                            <?= csrf_field(); ?>
                            <div class="col-md-12">
                                <a href="<?=site_url('po_tahap_pendaftaran/tahap1')?>" class="btn pull-right mr-1">Kembali</a>
                                <button type="submit" class="btn btn-primary pull-right mr-1">Simpan</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>


<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
