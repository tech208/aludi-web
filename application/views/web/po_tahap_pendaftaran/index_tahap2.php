<?php
    $this->load->view('web/po_tahap_pendaftaran/index_step');
?>

<style>
a {
  text-decoration: none;
  display: inline-block;
  padding: 8px 16px;
}

a:hover {
  background-color: #ddd;
  color: black;
}

.previous {
  background-color: #7c7c7c;
  color: white;
}

.next {
  background-color: #5bc0de;
  color: white;
}
</style>
<div class="card">
    <div class="card-body ">
        <div class="row">
            <div class="col-md-8">
                <span class="<?=@$data->statuse->css;?>"><?=@$data->statuse->name;?></span>
                <p class="text-secondary"><?=(!empty(@$data->updated_at))?MasTanggal(@$data->updated_at,1):MasTanggal(@$data->created_at,1);?></p>
            </div>
            <div class="col-md-4 pull-right">
                <?php if (@$data->utamas->statuse->grup > 8 OR @$data->status==26):?>
                <a href="<?=site_url('po_tahap_pendaftaran/visit')?>" class="next pull-right">Next &raquo;</a>
                <?php endif; ?>
                <a href="<?=site_url('po_tahap_pendaftaran/pembayaran')?>"  class="previous pull-right">&laquo; Previous</a>
            </div>
        </div>
    </div>
</div>



<section class="article-clean">
    <div class="container" style="width: auto;height: auto;">
        <div class="row">
            <div class="col-lg-11 col-xl-11 offset-lg-1 offset-xl-1">
            <form method="POST" enctype="multipart/form-data" action="<?= site_url('po_tahap_pendaftaran/saveTahap2') ?>" id="form-tahap2" class="form form-horizontal" >
                <div class="card shadow mb-3">
                    <div class="card-body" style="width: auto;height: auto;">
                        <input type="hidden" id="id" name="id" value="<?=@$data->id;?>">
                        <input type="hidden" id="id_personal" name="id_personal" value="<?=empty(@$data->id_personal)?MasKaryawan()->id:@$data->id_personal;?>" >
                        <h1 class="text-center d-xl-flex justify-content-xl-center align-items-xl-center" style="font-family: Nunito, sans-serif;font-size: 24px;margin-top: 21px;text-align: center;color: rgb(28,131,148);"><strong>Formulir Tahap 2</strong></h1>
                        <h1 class="text-center d-xl-flex justify-content-xl-center align-items-xl-center" style="font-family: Nunito, sans-serif;font-size: 24px;text-align: center;color: rgb(28,131,148);"><strong>Instruksi</strong></h1>
                        <h1 class="d-xl-flex justify-content-xl-center align-items-xl-center" style="font-family: Nunito, sans-serif;font-size: 20px;margin-top: 0px;">Akan diisi oleh perwakilan platform</h1>
                        <div class="form-row" style="margin-top: 23px;margin-bottom: 50px;">
                            <div class="col">
                                <div class="form-group">
                                    <label style="font-family: Nunito, sans-serif;font-size: 18px;">Nama Platform*</label>
                                    <input class="form-control border" type="text" id="nama_platform" name="nama_platform" value="<?=@$data->nama_platform?>" required style="font-family: Nunito, sans-serif;border-color: rgb(227,230,240);border-top-color: rgb(110,;border-right-color: 112,;border-bottom-color: 126);border-left-color: 112,;margin-top: 48px;">
                                </div>
                            </div>
                                <div class="col">
                                    <label style="font-family: Nunito, sans-serif;font-size: 18px;">Tanggal*</label>
                                    <input class="border rounded form-control" type="date" id="tanggal" name="tanggal" value="<?=@$data->tanggal?>" style="margin-top: 48px;border-color: rgb(227,230,240);font-family: Nunito, sans-serif;">
                                </div>
                        </div>

                        <h1 class="d-xl-flex" style="font-family: Nunito, sans-serif;font-size: 18px;margin-top: 21px;">1. Mohon tuliskan nama Domain (Domain wajib menggunakan “.ID”)*</h1>
                        <div class="form-row">
                            <div class="col">
                                <input class="border rounded form-control d-xl-flex justify-content-xl-center align-items-xl-center" id="domain_nama" name="domain_nama" value="<?=@$data->domain_nama?>" type="text" required style="margin-top: 10px;margin-bottom: 20px;font-family: Nunito, sans-serif;">
                            </div>
                            <div class="col" style="border-color: rgb(227,230,240);">
                                <div class="file-drop-area" style="width: 375px;border-color: rgb(227,230,240);">
                                    <input class="form-control " type="file"  id="domain_bukti" name="files[domain_bukti]" style="border-style: none;border-color: rgb(227,230,240); " required>
                                    <h6>• Ukuran file maksimal 10 MB</h6>
                                    <h6>• Pastikan file dalam bentuk JPG/JPEG/PNG/PDF</h6>
                                    <?php if (!empty($data->domain_bukti) AND file_exists("uploads/domain_bukti/".$data->domain_bukti)) : ?>
                                    <div class=mt-1>
                                        <a href="<?=base_url("uploads/domain_bukti/".$data->domain_bukti)?>" target="_blank" class="btn btn-success ">Lihat</a> <?=$data->domain_bukti?>

                                    </div>
                                    <?php endif; ?>                                    
                                </div>
                            </div>
                        </div>

                        <h1 class="d-xl-flex" style="font-family: Nunito, sans-serif;font-size: 18px;margin-top: 21px;">2. Apakah sudah mempunyai SOP APU-PPT ?*</h1>
                        <div class="form-row">
                            <div class="col" style="margin-bottom: 40px;">
                                <div class="form-row" style="margin-top: 0px;">
                                    <div class="col">
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="logo_ojk_nama" id="logo_ojk_nama_ya" value="iya"  <?=(@$data->logo_ojk_nama=="iya")?"checked":"";?> <?=(empty(@$data->logo_ojk_nama))?"checked":"";?> style="width: 20px;height: 20px;">
                                            <label class="form-check-label" for="formCheck-1" style="margin-left: 22px;font-family: Nunito, sans-serif;color: rgb(89,91,94);font-size: 18px;">Ya</label>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-check"
                                            ><input class="form-check-input" type="radio" name="logo_ojk_nama" id="logo_ojk_tidak" value="tidak" <?=(@$data->logo_ojk_nama=="tidak")?"checked":"";?> style="width: 20px;height: 20px;">
                                            <label class="form-check-label" for="formCheck-1" style="margin-left: 22px;font-family: Nunito, sans-serif;color: rgb(89,91,94);font-size: 18px;">Tidak</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col" style="border-color: rgb(227,230,240);">
                                <div class="file-drop-area" style="width: 375px;border-color: rgb(227,230,240);">
                                    <input class="form-control " type="file" id="logo_ojk_bukti" name="files[logo_ojk_bukti]" style="border-style: none;border-color: rgb(227,230,240);" required>
                                    <h6>• Ukuran file maksimal 10 MB</h6>
                                    <h6>• Pastikan file dalam bentuk JPG/JPEG/PNG/PDF</h6>                                       
                                    <?php if (!empty($data->logo_ojk_bukti) AND file_exists("uploads/logo_ojk_bukti/".$data->logo_ojk_bukti)) : ?>
                                        <div class=mt-1>
                                            <a href="<?=base_url("uploads/logo_ojk_bukti/".$data->logo_ojk_bukti)?>" target="_blank" class="btn btn-success ">Lihat</a> <?=$data->logo_ojk_bukti?>
                                        </div>
                                    <?php endif; ?>                                    
                                </div>
                            </div>
                        </div>

                        <h1 class="d-xl-flex" style="font-family: Nunito, sans-serif;font-size: 18px;margin-top: 21px;">3. Apakah domain dapat digunakan lebih dari 5 tahun ? Jika iya mohon untuk dapat melampirkan bukti*<br></h1>
                        <div class="form-row">
                            <div class="col"><input class="border rounded form-control d-xl-flex justify-content-xl-center align-items-xl-center" type="text" id="domain_usia_nama" name="domain_usia_nama"  value="<?=@$data->domain_usia_nama?>" style="margin-top: 10px;margin-bottom: 20px;font-family: Nunito, sans-serif;"></div>
                            <div class="col" style="border-color: rgb(227,230,240);">
                                <div class="file-drop-area" style="width: 375px;border-color: rgb(227,230,240);">
                                    <input class="form-control " type="file" id="domain_usia_bukti" name="files[domain_usia_bukti]" style="border-style: none;border-color: rgb(227,230,240);" required>
                                    <h6>• Ukuran file maksimal 10 MB</h6>
                                    <h6>• Pastikan file dalam bentuk JPG/JPEG/PNG/PDF</h6>                                    
                                    <?php if (!empty($data->domain_usia_bukti) AND file_exists("uploads/domain_usia_bukti/".$data->domain_usia_bukti)) : ?>
                                    <div class=mt-1>
                                        <a href="<?=base_url("uploads/domain_usia_bukti/".$data->domain_usia_bukti)?>" target="_blank" class="btn btn-success ">Lihat</a> <?=$data->domain_usia_bukti?>
                                    </div>
                                    <?php endif; ?>                                    
                                </div>
                            </div>
                        </div>
                        
                        <h1 class="d-flex d-xl-flex justify-content-start" style="font-family: Nunito, sans-serif;font-size: 18px;margin-top: 21px;">4. Apakah ada catatan teknis IT yang tidak mengikuti ketentuan POJK 57/4/2020 pada tampilan depan?*<br></h1>
                        <div class="form-row">
                            <div class="col">
                                <input class="border rounded form-control d-xl-flex justify-content-xl-center align-items-xl-center" type="text" id="logo_ojk_nama" name="logo_ojk_nama" value="<?=@$data->logo_ojk_nama?>" style="margin-top: 10px;margin-bottom: 20px;font-family: Nunito, sans-serif;" required>
                            </div>
                            <div class="col" style="border-color: rgb(227,230,240);">
                                <div class="file-drop-area" style="width: 375px;border-color: rgb(227,230,240);">
                                    <input class="form-control " type="file" id="domain_bukti" name="files[domain_bukti]" style="border-style: none;border-color: rgb(227,230,240);" required>
                                    <h6>• Ukuran file maksimal 10 MB</h6>
                                                    <h6>• Pastikan file dalam bentuk JPG/JPEG/PNG/PDF</h6>
                                    <?php if (!empty($data->domain_bukti) AND file_exists("uploads/domain_bukti/".$data->domain_bukti)) : ?>
                                                <div class=mt-1>
                                                    <a href="<?=base_url("uploads/domain_bukti/".$data->domain_bukti)?>" target="_blank" class="btn btn-success ">Lihat</a> <?=$data->domain_bukti?>
                                                </div>
                                                <?php endif; ?>                                    
                                </div>
                            </div>
                        </div>
                        
                        <h1 class="d-flex d-xl-flex justify-content-start" style="font-family: Nunito, sans-serif;font-size: 18px;margin-top: 21px;text-align: left;">5. Apakah server berada pada indonesia ? Jika iya lampirkan dokumen pendukung*<br></h1>
                        <div class="form-row">
                            <div class="col">
                                <input class="border rounded form-control d-xl-flex justify-content-xl-center align-items-xl-center" type="text" id="server_lokasi_nama" name="server_lokasi_nama" value="<?=@$data->server_lokasi_nama?>" style="margin-top: 10px;margin-bottom: 20px;font-family: Nunito, sans-serif;" required>
                            </div>
                            <div class="col" style="border-color: rgb(227,230,240);">
                                <div class="file-drop-area" style="width: 375px;border-color: rgb(227,230,240);">
                                    <input class="form-control " type="file"  id="server_lokasi_bukti" name="files[server_lokasi_bukti]" style="border-style: none;border-color: rgb(227,230,240);" required>
                                    <h6>• Ukuran file maksimal 10 MB</h6>
                                    <h6>• Pastikan file dalam bentuk JPG/JPEG/PNG/PDF</h6>
                                    <?php if (!empty($data->server_lokasi_bukti) AND file_exists("uploads/server_lokasi_bukti/".$data->server_lokasi_bukti)) : ?>
                                    <div class=mt-1>
                                        <a href="<?=base_url("uploads/server_lokasi_bukti/".$data->server_lokasi_bukti)?>" target="_blank" class="btn btn-success ">Lihat</a> <?=$data->server_lokasi_bukti?>
                                    </div>
                                    <?php endif; ?>                                    
                                </div>
                            </div>
                        </div>

                        <h1 class="d-flex d-xl-flex justify-content-start" style="font-family: Nunito, sans-serif;font-size: 18px;margin-top: 21px;">6. Apakah Platform sudah mendapatkan sertifikat SSL ? Jika iya mohon lampirkan dokumen pendukung*<br></h1>
                        <div class="form-row">
                            <div class="col">
                                <input class="border rounded form-control d-xl-flex justify-content-xl-center align-items-xl-center" type="text" id="platform_ssl_nama" name="platform_ssl_nama" value="<?=@$data->platform_ssl_nama?>" style="margin-top: 10px;margin-bottom: 20px;font-family: Nunito, sans-serif;" required>
                            </div>
                            <div class="col" style="border-color: rgb(227,230,240);">
                                <div class="file-drop-area" style="width: 375px;border-color: rgb(227,230,240);">
                                    <input class="form-control " type="file" id="platform_ssl_bukti" name="files[platform_ssl_bukti]" style="border-style: none;border-color: rgb(227,230,240);" required>
                                    <h6>• Ukuran file maksimal 10 MB</h6>
                                    <h6>• Pastikan file dalam bentuk JPG/JPEG/PNG/PDF</h6>
                                    <?php if (!empty($data->platform_ssl_bukti) AND file_exists("uploads/platform_ssl_bukti/".$data->platform_ssl_bukti)) : ?>
                                    <div class=mt-1>
                                        <a href="<?=base_url("uploads/platform_ssl_bukti/".$data->platform_ssl_bukti)?>" target="_blank" class="btn btn-success ">Lihat</a> <?=$data->platform_ssl_bukti?>
                                    </div>
                                    <?php endif; ?>                                    
                                </div>
                            </div>
                        </div>
                        
                        <h1 class="d-flex d-xl-flex justify-content-start" style="font-family: Nunito, sans-serif;font-size: 18px;margin-top: 21px;">7. Apakah penyelenggara sudah mempunyai SOP pasar sekunder ?*</h1>
                        <div class="form-row">
                            <div class="col" style="margin-bottom: 40px;">
                                <div class="form-row" style="margin-top: 0px;">
                                    <div class="col">
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="ssl_detail_nama" id="ssl_detail_nama_ya" value="iya" <?=(@$data->ssl_detail_nama=="iya")?"checked":"";?> <?=(empty(@$data->ssl_detail_nama))?"checked":"";?> style="width: 20px;height: 20px;">
                                            <label class="form-check-label" for="formCheck-1" style="margin-left: 22px;font-family: Nunito, sans-serif;color: rgb(89,91,94);font-size: 18px;">Ya</label>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio"  name="ssl_detail_nama" id="ssl_detail_nama_tidak"  value="tidak" <?=(@$data->ssl_detail_nama=="tidak")?"checked":"";?> style="width: 20px;height: 20px;">
                                            <label class="form-check-label" for="formCheck-1" style="margin-left: 22px;font-family: Nunito, sans-serif;color: rgb(89,91,94);font-size: 18px;">Tidak</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col" style="border-color: rgb(227,230,240);">
                                <div class="file-drop-area" style="width: 375px;border-color: rgb(227,230,240);">
                                    <input class="form-control " type="file" id="ssl_detail_bukti" name="files[ssl_detail_bukti]" style="border-style: none;border-color: rgb(227,230,240);" required>
                                    <h6>• Ukuran file maksimal 10 MB</h6>
                                    <h6>• Pastikan file dalam bentuk JPG/JPEG/PNG/PDF</h6>
                                    <?php if (!empty($data->ssl_detail_bukti) AND file_exists("uploads/ssl_detail_bukti/".$data->ssl_detail_bukti)) : ?>
                                    <div class=mt-1>
                                        <a href="<?=base_url("uploads/ssl_detail_bukti/".$data->ssl_detail_bukti)?>" target="_blank" class="btn btn-success ">Lihat</a> <?=$data->ssl_detail_bukti?>
                                    </div>
                                    <?php endif; ?>                                    
                                </div>
                            </div>
                        </div>
                        
                        <h1 class="d-flex d-xl-flex justify-content-start" style="font-family: Nunito, sans-serif;font-size: 18px;margin-top: 21px;">8. Apakah Apps (Android/IOS) hanya dapat mendapatkan akses terhadap Camera, Microphone, dan Location ? Jika iya mohon lampirkan dokumen pendukung*</h1>
                        <div class="form-row">
                            <div class="col">
                                <input class="border rounded form-control d-xl-flex justify-content-xl-center align-items-xl-center" type="text" id="akses_camera_nama" name="akses_camera_nama" value="<?=@$data->akses_camera_nama?>" style="margin-top: 10px;margin-bottom: 20px;font-family: Nunito, sans-serif;">
                            </div>
                            <div class="col" style="border-color: rgb(227,230,240);">
                                <div class="file-drop-area" style="width: 375px;border-color: rgb(227,230,240);">
                                    <input class="form-control " type="file"  id="akses_camera_bukti" name="files[akses_camera_bukti]" style="border-style: none;border-color: rgb(227,230,240);" required>
                                    <h6>• Ukuran file maksimal 10 MB</h6>
                                    <h6>• Pastikan file dalam bentuk JPG/JPEG/PNG/PDF</h6>
                                    <?php if (!empty($data->akses_camera_bukti) AND file_exists("uploads/akses_camera_bukti/".$data->akses_camera_bukti)) : ?>
                                    <div class=mt-1>
                                        <a href="<?=base_url("uploads/akses_camera_bukti/".$data->akses_camera_bukti)?>" target="_blank" class="btn btn-success ">Lihat</a> <?=$data->akses_camera_bukti?>
                                    </div>
                                    <?php endif; ?>                                    
                                </div>
                            </div>
                        </div>

                        <h1 class="d-flex d-xl-flex justify-content-start" style="font-family: Nunito, sans-serif;font-size: 18px;margin-top: 21px;">9. Apakah penyelenggara sudah mempunyai SOP Prospektus ?*</h1>
                        <div class="form-row">
                            <div class="col" style="margin-bottom: 40px;">
                                <div class="form-row" style="margin-top: 0px;">
                                    <div class="col">
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="technical_apps_ssl_nama" id="technical_apps_ssl_nama_ya" value="iya" <?=(@$data->technical_apps_ssl_nama=="iya")?"checked":"";?> <?=(empty(@$data->technical_apps_ssl_nama))?"checked":"";?> style="width: 20px;height: 20px;">
                                            <label class="form-check-label" for="formCheck-1" style="margin-left: 22px;font-family: Nunito, sans-serif;color: rgb(89,91,94);font-size: 18px;">Ya</label>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="technical_apps_ssl_nama" id="technical_apps_ssl_nama_tidak"  value="tidak" <?=(@$data->technical_apps_ssl_nama=="tidak")?"checked":"";?> style="width: 20px;height: 20px;">
                                            <label class="form-check-label" for="formCheck-1" style="margin-left: 22px;font-family: Nunito, sans-serif;color: rgb(89,91,94);font-size: 18px;">Tidak</label>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="technical_apps_ssl_nama" id="technical_apps_ssl_nama_na"  value="n/a" <?=(@$data->technical_apps_ssl_nama=="n/a")?"checked":"";?> style="width: 20px;height: 20px;">
                                            <label class="form-check-label" for="formCheck-1" style="margin-left: 22px;font-family: Nunito, sans-serif;color: rgb(89,91,94);font-size: 18px;">N/a</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col" style="border-color: rgb(227,230,240);">
                                <div class="file-drop-area" style="width: 375px;border-color: rgb(227,230,240);">
                                    <input class="form-control " type="file" id="technical_apps_ssl_bukti" name="files[technical_apps_ssl_bukti]" style="border-style: none;border-color: rgb(227,230,240);" required>
                                    <h6>• Ukuran file maksimal 10 MB</h6>
                                    <h6>• Pastikan file dalam bentuk JPG/JPEG/PNG/PDF</h6>
                                    <?php if (!empty($data->technical_apps_ssl_bukti) AND file_exists("uploads/technical_apps_ssl_bukti/".$data->technical_apps_ssl_bukti)) : ?>
                                    <div class=mt-1>
                                        <a href="<?=base_url("uploads/technical_apps_ssl_bukti/".$data->technical_apps_ssl_bukti)?>" target="_blank" class="btn btn-success ">Lihat</a> <?=$data->technical_apps_ssl_bukti?>
                                    </div>
                                    <?php endif; ?>                                    
                                </div>
                            </div>
                        </div>

                        <h1 class="d-flex d-xl-flex justify-content-start" style="font-family: Nunito, sans-serif;font-size: 18px;margin-top: 21px;">10. Apakah platform sudah menggunakan layanan masyarakat ? jika iya maka lampirkan SC, SOP yang berlaku*</h1>
                        <div class="form-row">
                            <div class="col">
                                <input class="border rounded form-control d-xl-flex justify-content-xl-center align-items-xl-center" type="text" id="customer_service_nama" name="customer_service_nama" value="<?=@$data->customer_service_nama?>" style="margin-top: 10px;margin-bottom: 20px;font-family: Nunito, sans-serif;" required>
                            </div>
                            <div class="col" style="border-color: rgb(227,230,240);">
                                <div class="file-drop-area" style="width: 375px;border-color: rgb(227,230,240);">
                                    <input class="form-control " type="file" id="customer_service_bukti" name="files[customer_service_bukti]" style="border-style: none;border-color: rgb(227,230,240);" required>
                                    <h6>• Ukuran file maksimal 10 MB</h6>
                                    <h6>• Pastikan file dalam bentuk JPG/JPEG/PNG/PDF</h6>
                                    <?php if (!empty($data->customer_service_bukti) AND file_exists("uploads/customer_service_bukti/".$data->customer_service_bukti)) : ?>
                                    <div class=mt-1>
                                        <a href="<?=base_url("uploads/customer_service_bukti/".$data->customer_service_bukti)?>" target="_blank" class="btn btn-success ">Lihat</a> <?=$data->customer_service_bukti?>
                                    </div>
                                    <?php endif; ?>                                    
                                </div>
                            </div>
                        </div>

                        <h1 class="d-xl-flex" style="font-family: Nunito, sans-serif;font-size: 18px;margin-top: 21px;">11. Apakah platform mempunyai SOP keamanan terhadap platform ?*</h1>
                        <div class="form-row">
                            <div class="col">
                                <input class="border rounded form-control d-xl-flex justify-content-xl-center align-items-xl-center" type="text" id="sop_keamanan_nama" name="sop_keamanan_nama" value="<?=@$data->sop_keamanan_nama?>" style="margin-top: 10px;margin-bottom: 20px;font-family: Nunito, sans-serif;" required>
                            </div>
                            <div class="col" style="border-color: rgb(227,230,240);">
                                <div class="file-drop-area" style="width: 375px;border-color: rgb(227,230,240);">
                                    <input class="form-control " type="file" id="sop_keamanan_bukti" name="files[sop_keamanan_bukti]" style="border-style: none;border-color: rgb(227,230,240);" required>
                                    <h6>• Ukuran file maksimal 10 MB</h6>
                                    <h6>• Pastikan file dalam bentuk JPG/JPEG/PNG/PDF</h6>
                                    <?php if (!empty($data->sop_keamanan_bukti) AND file_exists("uploads/sop_keamanan_bukti/".$data->sop_keamanan_bukti)) : ?>
                                    <div class=mt-1>
                                        <a href="<?=base_url("uploads/sop_keamanan_bukti/".$data->sop_keamanan_bukti)?>" target="_blank" class="btn btn-success ">Lihat</a> <?=$data->sop_keamanan_bukti?>
                                    </div>
                                    <?php endif; ?>                                    
                                </div>
                            </div>
                        </div>

                        <h1 class="d-flex d-xl-flex justify-content-start" style="font-family: Nunito, sans-serif;font-size: 18px;margin-top: 21px;">12. Apakah platform menggunakan sistem validasi menggunakan SMS/Email/PIN confirm ? jika mohon lampirkan dokumen pendukung terkait hal ini dan mohon diperjelas digunakan pada bagian mana ?*</h1>
                        <div class="form-row">
                            <div class="col">
                                <input class="border rounded form-control d-xl-flex justify-content-xl-center align-items-xl-center" type="text" id="validasi_sms_nama" name="validasi_sms_nama" value="<?=@$data->validasi_pin_nama?>" style="margin-top: 10px;margin-bottom: 20px;font-family: Nunito, sans-serif;" required>
                            </div>
                            <div class="col" style="border-color: rgb(227,230,240);">
                                <div class="file-drop-area" style="width: 375px;border-color: rgb(227,230,240);">
                                    <input class="form-control " type="file" id="validasi_sms_bukti" name="files[validasi_sms_bukti]" style="border-style: none;border-color: rgb(227,230,240);" required>
                                    <h6>• Ukuran file maksimal 10 MB</h6>
                                    <h6>• Pastikan file dalam bentuk JPG/JPEG/PNG/PDF</h6>                                    
                                    <?php if (!empty($data->validasi_pin_bukti) AND file_exists("uploads/validasi_sms_bukti/".$data->validasi_pin_bukti)) : ?>
                                    <div class=mt-1>
                                        <a href="<?=base_url("uploads/validasi_pin_bukti/".$data->validasi_pin_bukti)?>" target="_blank" class="btn btn-success ">Lihat</a> <?=$data->validasi_pin_bukti?>
                                    </div>
                                    <?php endif; ?>                                    
                                </div>
                            </div>
                        </div>

                        <h1 class="d-flex d-xl-flex justify-content-start" style="font-family: Nunito, sans-serif;font-size: 18px;margin-top: 21px;">13. Apakah penyelenggara sudah mempunyai SOP obligasi dan sukuk? *</h1>
                        <div class="form-row">
                            <div class="col">
                                <input class="border rounded form-control d-xl-flex justify-content-xl-center align-items-xl-center" type="text" id="validasi_pin_nama" name="validasi_pin_nama" value="<?=@$data->validasi_pin_nama?>" style="margin-top: 10px;margin-bottom: 20px;font-family: Nunito, sans-serif;">
                            </div>
                            <div class="col" style="border-color: rgb(227,230,240);">
                                <div class="file-drop-area" style="width: 375px;border-color: rgb(227,230,240);">
                                    <input class="form-control " type="file" id="validasi_pin_bukti" name="files[validasi_pin_bukti]" style="border-style: none;border-color: rgb(227,230,240);" required>
                                    <h6>• Ukuran file maksimal 10 MB</h6>
                                    <h6>• Pastikan file dalam bentuk JPG/JPEG/PNG/PDF</h6>                                    
                                    <?php if (!empty($data->validasi_pin_bukti) AND file_exists("uploads/validasi_pin_bukti/".$data->validasi_pin_bukti)) : ?>
                                    <div class=mt-1>
                                        <a href="<?=base_url("uploads/validasi_pin_bukti/".$data->validasi_pin_bukti)?>" target="_blank" class="btn btn-success ">Lihat</a> <?=$data->validasi_pin_bukti?>
                                    </div>
                                    <?php endif; ?>                                
                                </div>
                            </div>
                        </div>

                        <h1 class="d-flex d-xl-flex justify-content-start" style="font-family: Nunito, sans-serif;font-size: 18px;margin-top: 21px;">14. Apakah platform menggunakan sistem validasi menggunakan email confirm&nbsp;? jika mohon lampirkan dokumen pendukung terkait hal ini dan&nbsp; &nbsp; mohon diperjelas digunakan pada bagian mana.*</h1>
                        <div class="form-row">
                            <div class="col">
                                <input class="border rounded form-control d-xl-flex justify-content-xl-center align-items-xl-center" type="text" id="" name="" value="" style="margin-top: 10px;margin-bottom: 20px;font-family: Nunito, sans-serif;" required>
                            </div>
                            <div class="col" style="border-color: rgb(227,230,240);">
                                <div class="file-drop-area" style="width: 375px;border-color: rgb(227,230,240);">
                                
                                    <input class="form-control " type="file" id="" name="" style="border-style: none;border-color: rgb(227,230,240);" required>
                                    <h6>• Ukuran file maksimal 10 MB</h6>
                                    <h6>• Pastikan file dalam bentuk JPG/JPEG/PNG/PDF</h6>
                                </div>
                            </div>
                        </div>

                        <h1 class="d-flex d-xl-flex justify-content-start" style="font-family: Nunito, sans-serif;font-size: 18px;margin-top: 21px;">15. Apakah platform menggunakan fitur KYC ( Know your customer ) ? Jika Mohon lampirkan dokumen pendukung dan lampirkan list KYC*</h1>
                        <div class="form-row">
                            <div class="col">
                                <input class="border rounded form-control d-xl-flex justify-content-xl-center align-items-xl-center" type="text" id="kyc_nama" name="kyc_nama" value="<?=@$data->kyc_nama?>" style="margin-top: 10px;margin-bottom: 20px;font-family: Nunito, sans-serif;" required>
                            </div>
                            <div class="col" style="border-color: rgb(227,230,240);">
                                <div class="file-drop-area" style="width: 375px;border-color: rgb(227,230,240);">
                                    <input class="form-control " type="file"  id="kyc_bukti" name="files[kyc_bukti]" style="border-style: none;border-color: rgb(227,230,240);" required>
                                    <h6>• Ukuran file maksimal 10 MB</h6>
                                    <h6>• Pastikan file dalam bentuk JPG/JPEG/PNG/PDF</h6>                                    
                                    <?php if (!empty($data->kyc_bukti) AND file_exists("uploads/kyc_bukti/".$data->kyc_bukti)) : ?>
                                    <div class=mt-1>
                                        <a href="<?=base_url("uploads/kyc_bukti/".$data->kyc_bukti)?>" target="_blank" class="btn btn-success ">Lihat</a> <?=$data->kyc_bukti?>
                                    </div>
                                    <?php endif; ?>                                    
                                </div>
                            </div>
                        </div>

                        <h1 class="d-flex d-xl-flex justify-content-start" style="font-family: Nunito, sans-serif;font-size: 18px;margin-top: 21px;">16. Berapakah SLA (%) dari platform ? (mohon unggah bukti SLA / perjanjian)*</h1>
                        <div class="form-row">
                            <div class="col">
                                <input class="border rounded form-control d-xl-flex justify-content-xl-center align-items-xl-center" type="text" id="sla_nama" name="sla_nama" value="<?=@$data->sla_nama?>" style="margin-top: 10px;margin-bottom: 20px;font-family: Nunito, sans-serif;" required>
                            </div>
                            <div class="col" style="border-color: rgb(227,230,240);">
                                <div class="file-drop-area" style="width: 375px;border-color: rgb(227,230,240);">
                                    <input class="form-control " type="file" id="sla_bukti" name="files[sla_bukti]" style="border-style: none;border-color: rgb(227,230,240);" required>
                                    <h6>• Ukuran file maksimal 10 MB</h6>
                                    <h6>• Pastikan file dalam bentuk JPG/JPEG/PNG/PDF</h6>                                    
                                    <?php if (!empty($data->sla_bukti) AND file_exists("uploads/sla_bukti/".$data->sla_bukti)) : ?>
                                    <div class=mt-1>
                                        <a href="<?=base_url("uploads/sla_bukti/".$data->sla_bukti)?>" target="_blank" class="btn btn-success ">Lihat</a> <?=$data->sla_bukti?>
                                    </div>
                                    <?php endif; ?>                                    
                                </div>
                            </div>
                        </div>

                        <h1 class="d-flex d-xl-flex justify-content-start" style="font-family: Nunito, sans-serif;font-size: 18px;margin-top: 21px;">17. Apakah platform sudah mempunyai sertifikat DRC ( Disaster Recovery Center ), dimanakah lokasi Data Center dan Disaster Recovery Center tersebut?*
</h1>
                        <div class="form-row">
                            <div class="col">
                                <input class="border rounded form-control d-xl-flex justify-content-xl-center align-items-xl-center" type="text" id="sertifikat_drc_nama" name="sertifikat_drc_nama" value="<?=@$data->sertifikat_drc_nama?>" style="margin-top: 10px;margin-bottom: 20px;font-family: Nunito, sans-serif;" required>
                            </div>
                            <div class="col" style="border-color: rgb(227,230,240);">
                                <div class="file-drop-area" id="sertifikat_drc_bukti" name="files[sertifikat_drc_bukti]" style="width: 375px;border-color: rgb(227,230,240);">
                                    <input class="form-control " type="file" style="border-style: none;border-color: rgb(227,230,240);" required>
                                    <h6>• Ukuran file maksimal 10 MB</h6>
                                    <h6>• Pastikan file dalam bentuk JPG/JPEG/PNG/PDF</h6>                                    
                                    <?php if (!empty($data->sertifikat_drc_bukti) AND file_exists("uploads/sertifikat_drc_bukti/".$data->sertifikat_drc_bukti)) : ?>
                                    <div class=mt-1>
                                        <a href="<?=base_url("uploads/sertifikat_drc_bukti/".$data->sertifikat_drc_bukti)?>" target="_blank" class="btn btn-success ">Lihat</a> <?=$data->sertifikat_drc_bukti?>
                                    </div>
                                    <?php endif; ?>                                    
                                </div>
                            </div>
                        </div>

                        <h1 class="d-flex d-xl-flex justify-content-start" style="font-family: Nunito, sans-serif;font-size: 18px;margin-top: 21px;">18. Apakah platform sudah terdafatar dalam ISO 27001 ? Jika iya mohon dapat melampirkan sertifikat sebagai dokumen pendukung*</h1>
                        <div class="form-row">
                            <div class="col">
                                <input class="border rounded form-control d-xl-flex justify-content-xl-center align-items-xl-center" type="text" id="iso_nama" name="iso_nama" value="<?=@$data->iso_nama?>" style="margin-top: 10px;margin-bottom: 20px;font-family: Nunito, sans-serif;" required>
                            </div>
                            <div class="col" style="border-color: rgb(227,230,240);">
                                <div class="file-drop-area" style="width: 375px;border-color: rgb(227,230,240);">
                                    <input class="form-control " type="file"  id="iso_bukti" name="files[iso_bukti]" style="border-style: none;border-color: rgb(227,230,240);" required>
                                    <h6>• Ukuran file maksimal 10 MB</h6>
                                    <h6>• Pastikan file dalam bentuk JPG/JPEG/PNG/PDF</h6>                                    
                                    <?php if (!empty($data->iso_bukti) AND file_exists("uploads/iso_bukti/".$data->iso_bukti)) : ?>
                                    <div class=mt-1>
                                        <a href="<?=base_url("uploads/iso_bukti/".$data->iso_bukti)?>" target="_blank" class="btn btn-success ">Lihat</a> <?=$data->iso_bukti?>
                                    </div>
                                    <?php endif; ?>                                    
                                </div>
                            </div>
                        </div>

                        <h1 class="d-flex d-xl-flex justify-content-start" style="font-family: Nunito, sans-serif;font-size: 18px;margin-top: 21px;">19. Apakah platform sudah mendapat validitas dari KOMINFO sebagai PSE (Penyelenggara System Elektronik ) jika ada tolong lampirkan buktinya*</h1>
                        <div class="form-row">
                            <div class="col">
                                <input class="border rounded form-control d-xl-flex justify-content-xl-center align-items-xl-center" type="text" id="validasi_kominfo_nama" name="validasi_kominfo_nama" value="<?=@$data->validasi_kominfo_nama?>" style="margin-top: 10px;margin-bottom: 20px;font-family: Nunito, sans-serif;" required>
                            </div>
                            <div class="col" style="border-color: rgb(227,230,240);">
                                <div class="file-drop-area" style="width: 375px;border-color: rgb(227,230,240);">
                                    <input class="form-control " type="file" id="validasi_kominfo_bukti" name="files[validasi_kominfo_bukti]" style="border-style: none;border-color: rgb(227,230,240);" required>
                                    <h6>• Ukuran file maksimal 10 MB</h6>
                                    <h6>• Pastikan file dalam bentuk JPG/JPEG/PNG/PDF</h6>                                    
                                    <?php if (!empty($data->validasi_kominfo_bukti) AND file_exists("uploads/validasi_kominfo_bukti/".$data->validasi_kominfo_bukti)) : ?>
                                    <div class=mt-1>
                                        <a href="<?=base_url("uploads/validasi_kominfo_bukti/".$data->validasi_kominfo_bukti)?>" target="_blank" class="btn btn-success ">Lihat</a> <?=$data->validasi_kominfo_bukti?>
                                    </div>
                                    <?php endif; ?>                                    
                                </div>
                            </div>
                        </div>

                        <h1 class="d-flex d-xl-flex justify-content-start" style="font-family: Nunito, sans-serif;font-size: 18px;margin-top: 21px;">20. Apakah platform sudah melaksanakan test penetrasi dari pihak ke 3 ? jika iya tolong dilampirkan sertifikatnya*</h1>
                        <div class="form-row">
                            <div class="col">
                                <input class="border rounded form-control d-xl-flex justify-content-xl-center align-items-xl-center" type="text" id="test_penetrasi_nama" name="test_penetrasi_nama" value="<?=@$data->test_penetrasi_nama?>" style="margin-top: 10px;margin-bottom: 20px;font-family: Nunito, sans-serif;" required>
                            </div>
                            <div class="col" style="border-color: rgb(227,230,240);">
                                <div class="file-drop-area" style="width: 375px;border-color: rgb(227,230,240);">
                                    <input class="form-control " type="file" id="test_penetrasi_bukti" name="files[test_penetrasi_bukti]" style="border-style: none;border-color: rgb(227,230,240);" required>
                                    <h6>• Ukuran file maksimal 10 MB</h6>
                                    <h6>• Pastikan file dalam bentuk JPG/JPEG/PNG/PDF</h6>                                    
                                    <?php if (!empty($data->test_penetrasi_bukti) AND file_exists("uploads/test_penetrasi_bukti/".$data->test_penetrasi_bukti)) : ?>
                                    <div class=mt-1>
                                        <a href="<?=base_url("uploads/test_penetrasi_bukti/".$data->test_penetrasi_bukti)?>" target="_blank" class="btn btn-success ">Lihat</a> <?=$data->test_penetrasi_bukti?>
                                    </div>
                                    <?php endif; ?>                                    
                                </div>
                            </div>
                        </div>

                        <h1 class="text-center d-xl-flex justify-content-xl-center align-items-xl-center" style="font-family: Nunito, sans-serif;font-size: 24px;margin-top: 50px;text-align: center;color: rgb(28,131,148);"><strong>Instruksi</strong></h1>
                        <div class="row">
                            <div class="col-xl-3">
                                <h1 class="text-justify d-xl-flex" style="font-family: Nunito, sans-serif;font-size: 18px;margin-top: 22px;width: 150px;">Saya, Bapak/Ibu</h1>
                            </div>
                            <div class="col">
                                <input class="d-xl-flex justify-content-xl-center align-items-xl-center" type="text" id="nama_saya" name="nama_saya" value="<?=@$data->nama_saya?>" onkeyup="myNama()" style="margin-top: 18px;margin-bottom: 0px;font-family: Nunito, sans-serif;border-style: none;border-color: rgb(255,255,255);padding-top: 0px;margin-left: 0px;width: 200px;font-size: 18px;color: rgb(28,131,148);font-style: normal;height: 30px;" placeholder="Tuliskan Nama Anda ....">
                            </div>
                        </div>
                        <h1 class="text-justify d-xl-flex" style="font-family: Nunito, sans-serif;font-size: 18px;margin-top: 9px;width: auto;">menyatakan bahwa keterangan yang saya berikan dalam aplikasi ini adalah benar .</h1>
                        <div class="row">
                            <div class="col-xl-5">
                                <h1 class="text-justify d-xl-flex" style="font-family: Nunito, sans-serif;font-size: 18px;margin-top: 22px;width: auto;">Saya memberi kuasa Bapak/Ibu</h1>
                            </div>
                            <div class="col">
                                <input class="d-xl-flex justify-content-xl-center align-items-xl-center" type="text" id="nama_saya" name="nama_saya" value="<?=@$data->nama_saya?>" onkeyup="myNama()" style="margin-top: 18px;margin-bottom: 0px;font-family: Nunito, sans-serif;border-style: none;border-color: rgb(255,255,255);padding-top: 0px;margin-left: 0px;width: 200px;font-size: 18px;color: rgb(28,131,148);font-style: normal;height: 30px;" placeholder="Tuliskan Nama Anda ....">
                            </div>
                        </div>
                        <h1 class="text-justify d-xl-flex" style="font-family: Nunito, sans-serif;font-size: 18px;margin-top: 4px;width: auto;">untuk memberikan informasi tambahan yang diperlukan. Saya memahami bahwa pernyataan palsu atau informasi palsu yang diberikan, merupakan pelanggaran serius yang dapat mengakibatkan tidak lulusnya tahap 2 it review ini. </h1>
                        <h1 class="text-center d-xl-flex justify-content-xl-center align-items-xl-center" style="font-family: Nunito, sans-serif;font-size: 24px;margin-top: 50px;text-align: center;color: rgb(28,131,148);margin-bottom: 23px;"><strong>TTD Digital Perusahaan</strong></h1>
                    
                        <input class="form-control " type="file" id="ttd_perusahaan" name="files[ttd_perusahaan]" style="border-style: none;border-color: rgb(227,230,240);">
                        <?php if (!empty($data->ttd_perusahaan) AND file_exists("uploads/ttd_perusahaan/".$data->ttd_perusahaan)) : ?>
                        <div class=mt-1>
                            <a href="<?=base_url("uploads/ttd_perusahaan/".$data->ttd_perusahaan)?>" target="_blank" class="btn btn-success ">Lihat</a> <?=$data->ttd_perusahaan?>
                        </div>
                        <?php endif; ?>  
                        <p>
                            nb * : wajib diisi!
                        </p>                      
                    </div>
                    
                    <?php if($mode!="detail"): ?>
                    <div class="card-footer">
                        <div clas s="form-group row ">
                            <?= csrf_field(); ?>
                            <div class="col-md-12">
                                <button type="reset" class="btn pull-right mr-1">Reset</button>
                                <button type="submit" class="btn btn-primary pull-right mr-1">Simpan</button>
                            </div>
                        </div>
                    </div>
                    <?php endif ?>
                </div>
                </form>
            </div>
        </div>
    </div>
</section>
<!-- 
<section id="number-tabs">
    <div class="row">
        <div class="col-12">
            <form method="POST" enctype="multipart/form-data" action="<?= site_url('po_tahap_pendaftaran/saveTahap2') ?>" id="form-tahap2" class="form form-horizontal" >
                <div class="card">
                    <input type="hidden" id="id" name="id" value="<?=@$data->id;?>">
                    <input type="hidden" id="id_personal" name="id_personal" value="<?=empty(@$data->id_personal)?MasKaryawan()->id:@$data->id_personal;?>" >

                    <div class="card-header">
                        <h4 class="card-title">Formulir Tahap 2</h4>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-header">
                            <h4 class="card-title text-center">Instruksi: Akan diisi oleh perwakilan platform</h4>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="firstName1">Nama Platform</label>
                                        <input type="text" class="form-control" id="nama_platform" name="nama_platform" value="<?=@$data->nama_platform?>" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="firstName1">Tanggal</label>
                                        <input type="date" class="form-control" id="tanggal" name="tanggal" value="<?=@$data->tanggal?>">
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="lastName1">1. Mohon tuliskan nama Domain (Domain wajib menggunakan “.ID”)</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" id="domain_nama" name="domain_nama" value="<?=@$data->domain_nama?>">
                                            </div>
                                            <div class="col-md-6">
                                                <input type="file" class="form-control" id="domain_bukti" name="files[domain_bukti]" >
                                                <?php if (!empty($data->domain_bukti) AND file_exists("uploads/domain_bukti/".$data->domain_bukti)) : ?>
                                                <div class=mt-1>
                                                    <a href="<?=base_url("uploads/domain_bukti/".$data->domain_bukti)?>" target="_blank" class="btn btn-success ">Lihat</a> <?=$data->domain_bukti?>
                                                </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="lastName1">2. Apakah Platform sudah menggunakan logo OJK atau instansi terkait ?</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="logo_ojk_nama" id="logo_ojk_nama_ya" value="iya"  <?=(@$data->logo_ojk_nama=="iya")?"checked":"";?> <?=(empty(@$data->logo_ojk_nama))?"checked":"";?>>
                                                            <label class="form-check-label" for="logo_ojk_nama_ya">
                                                                Iya
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="logo_ojk_nama" id="logo_ojk_tidak" value="tidak" <?=(@$data->logo_ojk_nama=="tidak")?"checked":"";?>>
                                                            <label class="form-check-label" for="logo_ojk_tidak">
                                                                Tidak
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="file" class="form-control" id="logo_ojk_bukti" name="files[logo_ojk_bukti]">
                                                <?php if (!empty($data->logo_ojk_bukti) AND file_exists("uploads/logo_ojk_bukti/".$data->logo_ojk_bukti)) : ?>
                                                <div class=mt-1>
                                                    <a href="<?=base_url("uploads/logo_ojk_bukti/".$data->logo_ojk_bukti)?>" target="_blank" class="btn btn-success ">Lihat</a> <?=$data->logo_ojk_bukti?>
                                                </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="lastName1">3. Apakah domain dapat digunakan lebih dari 5 tahun ? Jika iya mohon untuk dapat melampirkan bukti</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" id="domain_usia_nama" name="domain_usia_nama"  value="<?=@$data->domain_usia_nama?>">
                                            </div>
                                            <div class="col-md-6">
                                                <input type="file" class="form-control" id="domain_usia_bukti" name="files[domain_usia_bukti]">
                                                <?php if (!empty($data->domain_usia_bukti) AND file_exists("uploads/domain_usia_bukti/".$data->domain_usia_bukti)) : ?>
                                                <div class=mt-1>
                                                    <a href="<?=base_url("uploads/domain_usia_bukti/".$data->domain_usia_bukti)?>" target="_blank" class="btn btn-success ">Lihat</a> <?=$data->domain_usia_bukti?>
                                                </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="lastName1">4. Apakah ada catatan teknis IT yang tidak mengikuti ketentuan POJK 37/4/2018 pada tampilan depan?</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" id="logo_ojk_nama" name="logo_ojk_nama" value="<?=@$data->logo_ojk_nama?>">
                                            </div>
                                            <div class="col-md-6">
                                                <input type="file" class="form-control" id="domain_bukti" name="files[domain_bukti]">
                                                <?php if (!empty($data->domain_bukti) AND file_exists("uploads/domain_bukti/".$data->domain_bukti)) : ?>
                                                <div class=mt-1>
                                                    <a href="<?=base_url("uploads/domain_bukti/".$data->domain_bukti)?>" target="_blank" class="btn btn-success ">Lihat</a> <?=$data->domain_bukti?>
                                                </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="lastName1">5. Apakah server berada pada indonesia ? Jika iya lampirkan dokumen pendukung</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" id="server_lokasi_nama" name="server_lokasi_nama" value="<?=@$data->server_lokasi_nama?>">
                                            </div>
                                            <div class="col-md-6">
                                                <input type="file" class="form-control" id="server_lokasi_bukti" name="files[server_lokasi_bukti]">
                                                <?php if (!empty($data->server_lokasi_bukti) AND file_exists("uploads/server_lokasi_bukti/".$data->server_lokasi_bukti)) : ?>
                                                <div class=mt-1>
                                                    <a href="<?=base_url("uploads/server_lokasi_bukti/".$data->server_lokasi_bukti)?>" target="_blank" class="btn btn-success ">Lihat</a> <?=$data->server_lokasi_bukti?>
                                                </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="lastName1">6. Apakah Platform sudah mendapatkan sertifikat SSL ? Jika iya mohon lampirkan dokumen pendukung</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" id="platform_ssl_nama" name="platform_ssl_nama" value="<?=@$data->platform_ssl_nama?>">
                                            </div>
                                            <div class="col-md-6">
                                                <input type="file" class="form-control" id="platform_ssl_bukti" name="files[platform_ssl_bukti]">
                                                <?php if (!empty($data->platform_ssl_bukti) AND file_exists("uploads/platform_ssl_bukti/".$data->platform_ssl_bukti)) : ?>
                                                <div class=mt-1>
                                                    <a href="<?=base_url("uploads/platform_ssl_bukti/".$data->platform_ssl_bukti)?>" target="_blank" class="btn btn-success ">Lihat</a> <?=$data->platform_ssl_bukti?>
                                                </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="lastName1">7. Turunan detail SSL akan dilampirkan pada fase 2 pengembangan back end website</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="ssl_detail_nama" id="ssl_detail_nama_ya" value="iya" <?=(@$data->ssl_detail_nama=="iya")?"checked":"";?> <?=(empty(@$data->ssl_detail_nama))?"checked":"";?>>
                                                            <label class="form-check-label" for="logo_ojk_nama_ya">
                                                                Iya
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="ssl_detail_nama" id="ssl_detail_nama_tidak"  value="tidak" <?=(@$data->ssl_detail_nama=="tidak")?"checked":"";?> >
                                                            <label class="form-check-label" for="ssl_detail_nama_tidak">
                                                                Tidak
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="file" class="form-control" id="ssl_detail_bukti" name="files[ssl_detail_bukti]">
                                                <?php if (!empty($data->ssl_detail_bukti) AND file_exists("uploads/ssl_detail_bukti/".$data->ssl_detail_bukti)) : ?>
                                                <div class=mt-1>
                                                    <a href="<?=base_url("uploads/ssl_detail_bukti/".$data->ssl_detail_bukti)?>" target="_blank" class="btn btn-success ">Lihat</a> <?=$data->ssl_detail_bukti?>
                                                </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="lastName1">8. Apakah Apps (Android/IOS) hanya dapat mendapatkan akses terhadap Camera, Microphone, dan Location ? Jika iya mohon lampirkan dokumen pendukung</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" id="akses_camera_nama" name="akses_camera_nama" value="<?=@$data->akses_camera_nama?>">
                                            </div>
                                            <div class="col-md-6">
                                                <input type="file" class="form-control"  id="akses_camera_bukti" name="files[akses_camera_bukti]">
                                                <?php if (!empty($data->akses_camera_bukti) AND file_exists("uploads/akses_camera_bukti/".$data->akses_camera_bukti)) : ?>
                                                <div class=mt-1>
                                                    <a href="<?=base_url("uploads/akses_camera_bukti/".$data->akses_camera_bukti)?>" target="_blank" class="btn btn-success ">Lihat</a> <?=$data->akses_camera_bukti?>
                                                </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="lastName1">9. Turunan detail technical Apps SSL akan dilampirkan pada fase 2 pengembangan back end website</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="technical_apps_ssl_nama" id="technical_apps_ssl_nama_ya" value="iya" <?=(@$data->technical_apps_ssl_nama=="iya")?"checked":"";?> <?=(empty(@$data->technical_apps_ssl_nama))?"checked":"";?>>
                                                            <label class="form-check-label" for="technical_apps_ssl_nama_ya">
                                                                Iya
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="technical_apps_ssl_nama" id="technical_apps_ssl_nama_tidak"  value="tidak" <?=(@$data->technical_apps_ssl_nama=="tidak")?"checked":"";?>>
                                                            <label class="form-check-label" for="technical_apps_ssl_nama_tidak">
                                                                Tidak
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="technical_apps_ssl_nama" id="technical_apps_ssl_nama_na"  value="n/a" <?=(@$data->technical_apps_ssl_nama=="n/a")?"checked":"";?> >
                                                            <label class="form-check-label" for="technical_apps_ssl_nama_na">
                                                                N/A
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="file" class="form-control" id="technical_apps_ssl_bukti" name="files[technical_apps_ssl_bukti]">
                                                <?php if (!empty($data->technical_apps_ssl_bukti) AND file_exists("uploads/technical_apps_ssl_bukti/".$data->technical_apps_ssl_bukti)) : ?>
                                                <div class=mt-1>
                                                    <a href="<?=base_url("uploads/technical_apps_ssl_bukti/".$data->technical_apps_ssl_bukti)?>" target="_blank" class="btn btn-success ">Lihat</a> <?=$data->technical_apps_ssl_bukti?>
                                                </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="lastName1">10. Apakah platform sudah menggunakan layanan customer service ? jika iya maka lampirkan sc, sop yang berlaku</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" id="customer_service_nama" name="customer_service_nama" value="<?=@$data->customer_service_nama?>">
                                            </div>
                                            <div class="col-md-6">
                                                <input type="file" class="form-control" id="customer_service_bukti" name="files[customer_service_bukti]">
                                                <?php if (!empty($data->customer_service_bukti) AND file_exists("uploads/customer_service_bukti/".$data->customer_service_bukti)) : ?>
                                                <div class=mt-1>
                                                    <a href="<?=base_url("uploads/customer_service_bukti/".$data->customer_service_bukti)?>" target="_blank" class="btn btn-success ">Lihat</a> <?=$data->customer_service_bukti?>
                                                </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="lastName1">13. Apakah platform mempunyai SOP keamanan terhadap platform ?</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" id="sop_keamanan_nama" name="sop_keamanan_nama" value="<?=@$data->sop_keamanan_nama?>">
                                            </div>
                                            <div class="col-md-6">
                                                <input type="file" class="form-control" id="sop_keamanan_bukti" name="files[sop_keamanan_bukti]">
                                                <?php if (!empty($data->sop_keamanan_bukti) AND file_exists("uploads/sop_keamanan_bukti/".$data->sop_keamanan_bukti)) : ?>
                                                <div class=mt-1>
                                                    <a href="<?=base_url("uploads/sop_keamanan_bukti/".$data->sop_keamanan_bukti)?>" target="_blank" class="btn btn-success ">Lihat</a> <?=$data->sop_keamanan_bukti?>
                                                </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="lastName1">14. Apakah platform menggunakan sistem validasi menggunakan SMS ? jika mohon lampirkan dokumen pendukung terkait hal ini dan mohon diperjelas digunakan pada bagian mana.</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" id="validasi_sms_nama" name="validasi_sms_nama" value="<?=@$data->validasi_pin_nama?>">
                                            </div>
                                            <div class="col-md-6">
                                                <input type="file" class="form-control" id="validasi_sms_bukti" name="files[validasi_sms_bukti]">
                                                <?php if (!empty($data->validasi_pin_bukti) AND file_exists("uploads/validasi_sms_bukti/".$data->validasi_pin_bukti)) : ?>
                                                <div class=mt-1>
                                                    <a href="<?=base_url("uploads/validasi_pin_bukti/".$data->validasi_pin_bukti)?>" target="_blank" class="btn btn-success ">Lihat</a> <?=$data->validasi_pin_bukti?>
                                                </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="lastName1">15. Apakah platform menggunakan sistem validasi menggunakan PIN ? jika mohon lampirkan dokumen pendukung terkait hal ini dan mohon diperjelas digunakan pada bagian mana.</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" id="validasi_pin_nama" name="validasi_pin_nama" value="<?=@$data->validasi_pin_nama?>">
                                            </div>
                                            <div class="col-md-6">
                                                <input type="file" class="form-control" id="validasi_pin_bukti" name="files[validasi_pin_bukti]">
                                                <?php if (!empty($data->validasi_pin_bukti) AND file_exists("uploads/validasi_pin_bukti/".$data->validasi_pin_bukti)) : ?>
                                                <div class=mt-1>
                                                    <a href="<?=base_url("uploads/validasi_pin_bukti/".$data->validasi_pin_bukti)?>" target="_blank" class="btn btn-success ">Lihat</a> <?=$data->validasi_pin_bukti?>
                                                </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="lastName1">16. Apakah platform menggunakan fitur KYC ( Know your customer ) ? Jika Mohon lampirkan dokumen pendukung dan lampirkan list KYC</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" id="kyc_nama" name="kyc_nama" value="<?=@$data->kyc_nama?>">
                                            </div>
                                            <div class="col-md-6">
                                                <input type="file" class="form-control" id="kyc_bukti" name="files[kyc_bukti]">
                                                <?php if (!empty($data->kyc_bukti) AND file_exists("uploads/kyc_bukti/".$data->kyc_bukti)) : ?>
                                                <div class=mt-1>
                                                    <a href="<?=base_url("uploads/kyc_bukti/".$data->kyc_bukti)?>" target="_blank" class="btn btn-success ">Lihat</a> <?=$data->kyc_bukti?>
                                                </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="lastName1">17. Berapakah SLA (%) dari platform ? (mohon unggah bukti SLA / perjanjian)</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" id="sla_nama" name="sla_nama" value="<?=@$data->sla_nama?>">
                                            </div>
                                            <div class="col-md-6">
                                                <input type="file" class="form-control" id="sla_bukti" name="files[sla_bukti]">
                                                <?php if (!empty($data->sla_bukti) AND file_exists("uploads/sla_bukti/".$data->sla_bukti)) : ?>
                                                <div class=mt-1>
                                                    <a href="<?=base_url("uploads/sla_bukti/".$data->sla_bukti)?>" target="_blank" class="btn btn-success ">Lihat</a> <?=$data->sla_bukti?>
                                                </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="lastName1">18. Apakah platform sudah mempunyai sertifikat DRC ( Disaster Recovery Center ) ?</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" id="sertifikat_drc_nama" name="sertifikat_drc_nama" value="<?=@$data->sertifikat_drc_nama?>">
                                            </div>
                                            <div class="col-md-6">
                                                <input type="file" class="form-control" id="sertifikat_drc_bukti" name="files[sertifikat_drc_bukti]">
                                                <?php if (!empty($data->sertifikat_drc_bukti) AND file_exists("uploads/sertifikat_drc_bukti/".$data->sertifikat_drc_bukti)) : ?>
                                                <div class=mt-1>
                                                    <a href="<?=base_url("uploads/sertifikat_drc_bukti/".$data->sertifikat_drc_bukti)?>" target="_blank" class="btn btn-success ">Lihat</a> <?=$data->sertifikat_drc_bukti?>
                                                </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="lastName1">19. Apakah platform sudah terdafatar dalam ISO 27001 ? Jika iya mohon dapat melampirkan sertifikat sebagai dokumen pendukung</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" id="iso_nama" name="iso_nama" value="<?=@$data->iso_nama?>">
                                            </div>
                                            <div class="col-md-6">
                                                <input type="file" class="form-control" id="iso_bukti" name="files[iso_bukti]">
                                                <?php if (!empty($data->iso_bukti) AND file_exists("uploads/iso_bukti/".$data->iso_bukti)) : ?>
                                                <div class=mt-1>
                                                    <a href="<?=base_url("uploads/iso_bukti/".$data->iso_bukti)?>" target="_blank" class="btn btn-success ">Lihat</a> <?=$data->iso_bukti?>
                                                </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="lastName1">20. Apakah platform sudah mendapat validitas dari KOMINFO sebagai PSE ( Penyelenggara System Elektronik ) jika ada tolong lampirkan buktinya</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" id="validasi_kominfo_nama" name="validasi_kominfo_nama" value="<?=@$data->validasi_kominfo_nama?>">
                                            </div>
                                            <div class="col-md-6">
                                                <input type="file" class="form-control" id="validasi_kominfo_bukti" name="files[validasi_kominfo_bukti]">
                                                <?php if (!empty($data->validasi_kominfo_bukti) AND file_exists("uploads/validasi_kominfo_bukti/".$data->validasi_kominfo_bukti)) : ?>
                                                <div class=mt-1>
                                                    <a href="<?=base_url("uploads/validasi_kominfo_bukti/".$data->validasi_kominfo_bukti)?>" target="_blank" class="btn btn-success ">Lihat</a> <?=$data->validasi_kominfo_bukti?>
                                                </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="lastName1">21. Apakah platform sudah melaksanakan test penetrasi dari pihak ke 3 ? jika iya tolong dilampirkan sertifikatnya</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" id="test_penetrasi_nama" name="test_penetrasi_nama" value="<?=@$data->test_penetrasi_nama?>">
                                            </div>
                                            <div class="col-md-6">
                                                <input type="file" class="form-control" id="test_penetrasi_bukti" name="files[test_penetrasi_bukti]">
                                                <?php if (!empty($data->test_penetrasi_bukti) AND file_exists("uploads/test_penetrasi_bukti/".$data->test_penetrasi_bukti)) : ?>
                                                <div class=mt-1>
                                                    <a href="<?=base_url("uploads/test_penetrasi_bukti/".$data->test_penetrasi_bukti)?>" target="_blank" class="btn btn-success ">Lihat</a> <?=$data->test_penetrasi_bukti?>
                                                </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <hr>
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="firstName1">Nama Saya</label>
                                            <input type="text" class="form-control" id="nama_saya" name="nama_saya" value="<?=@$data->nama_saya?>" onkeyup="myNama()">
                                        </div>
                                    </div>

                                    <div class="text-left">
                                        Saya, Bapak/Ibu <div class="nama"></div> menyatakan bahwa keterangan yang saya berikan dalam aplikasi ini adalah benar.
                                        <br> Saya memberi kuasa Bapak/Ibu <div class="nama"></div> untuk memberikan informasi tambahan yang diperlukan.
                                        Saya memahami bahwa pernyataan palsu atau informasi palsu yang diberikan, <br/> pelanggaran serius yang dapat mengakibatkan tidak lulusnya tahap 2 it review ini. 
                                    </div>
                                </div>

                                
                                <div class="col-md-12 text-center">
                                    <div class="form-group">
                                        <label for="firstName1">TTD Digital Perusahaan</label>
                                        <input type="file" class="form-control" id="ttd_perusahaan" name="files[ttd_perusahaan]">
                                                <?php if (!empty($data->ttd_perusahaan) AND file_exists("uploads/ttd_perusahaan/".$data->ttd_perusahaan)) : ?>
                                                <div class=mt-1>
                                                    <a href="<?=base_url("uploads/ttd_perusahaan/".$data->ttd_perusahaan)?>" target="_blank" class="btn btn-success ">Lihat</a> <?=$data->ttd_perusahaan?>
                                                </div>
                                                <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php if($mode!="detail"): ?>
                    <div class="card-footer">
                        <div clas s="form-group row ">
                            <?= csrf_field(); ?>
                            <div class="col-md-12">
                                <button type="reset" class="btn pull-right mr-1">Reset</button>
                                <button type="submit" class="btn btn-primary pull-right mr-1">Simpan</button>
                            </div>
                        </div>
                    </div>
                    <?php endif ?>
                </div>
            </form>
        </div>
    </div>
</section>
 -->
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>


<script>
    function myNama() {
        var x =    $("#nama_saya").val()
        $("div.nama").html( "<b>"+x+"</b>" );
    }
</script>


<?php if($mode=="detail" OR @$data->status == 25 OR @$data->status == 26): ?>
<script type="text/javascript">
    $("#form-tahap2 :input").prop("disabled", true);
</script>
<?php endif ?>
