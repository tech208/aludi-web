<?php
    $this->load->view('web/po_tahap_pendaftaran/index_step');
?>


<style>
a {
  text-decoration: none;
  display: inline-block;
  padding: 8px 16px;
}

a:hover {
  background-color: #ddd;
  color: black;
}

.previous {
  background-color: #7c7c7c;
  color: white;
}

.next {
  background-color: #5bc0de;
  color: white;
}
</style>
<div class="card">
    <div class="card-body ">
        <div class="row">
            <div class="col-md-8">
                <span class="<?=@$data->statuse->css;?>"><?=@$data->statuse->name;?></span>
                <p class="text-secondary"><?=(!empty(@$data->updated_at))?MasTanggal(@$data->updated_at,1):MasTanggal(@$data->created_at,1);?></p>
            </div>
            <div class="col-md-4 pull-right">
                <?php if (@$data->utamas->statuse->grup > 7 OR @$data->status==22):?>
                <a href="<?=site_url('po_tahap_pendaftaran/tahap2')?>" class="next pull-right">Next &raquo;</a>
                <?php endif; ?>
                <a href="<?=site_url('po_tahap_pendaftaran/tahap1')?>"  class="previous pull-right">&laquo; Previous</a>
            </div>
        </div>
    </div>
</div>


<section>
    <div id="multple-step-form-n" class="container overflow-hidden" style="margin-top: 0px;margin-bottom: 10px;padding-bottom: 300px;padding-top: 57px;background: #ffffff;">
        <section class="article-clean">
            <div class="container">
                <div class="row">
                    <div class="col-lg-10 col-xl-8 offset-lg-1 offset-xl-2">
                        <div class="card shadow mb-3">
                            <div class="card-header py-3" style="background: rgb(255,255,255);">
                                <div class="row">
                                    <div class="col-xl-8">
                                        <h1 class="d-xl-flex" style="font-family: Nunito, sans-serif;font-size: 16px;margin-top: 21px;"><strong>Transfer Bank (Manual Transfer)</strong></h1>
                                    </div>
                                    <div class="col"><img src="<?= base_url() ?>app-assets/assets/img/logo-BNI-46-1.png" style="width: 100px;"></div>
                                </div>
                            </div>
                            <div class="card-body" style="width: auto;">
                                <form>
                                    <h1 class="d-xl-flex" style="font-family: Nunito, sans-serif;font-size: 18px;margin-top: 21px;"><strong>Nomor Rekening</strong></h1>
                                    <h1 class="d-xl-flex" style="font-family: Nunito, sans-serif;font-size: 22px;margin-top: 18px;width: 200px;">0123037895&nbsp;&nbsp;
                                        <!-- <i class="material-icons">content_copy</i> -->
                                    </h1>
                                    <h1 class="d-xl-flex" style="font-family: Nunito, sans-serif;font-size: 16px;margin-top: 5px;">PERK LAYANAN TEKNOLOGI GKCU Gajah Mada</h1>
                                </form>
                                <form>
                                    <h1 class="d-xl-flex" style="font-family: Nunito, sans-serif;font-size: 18px;margin-top: 45px;"><strong>Nominal yang Harus Dibayarkan</strong></h1>
                                    <h1 class="d-xl-flex" style="font-family: Nunito, sans-serif;font-size: 22px;margin-top: 18px;width: 200px;color: #009EC8;">Rp 20.000.000&nbsp;&nbsp;
                                    <!-- <i class="material-icons">content_copy</i> -->
                                    </h1>
                                </form>
                            </div>
                        </div>
                        <form method="POST" enctype="multipart/form-data" action="<?= site_url('po_tahap_pendaftaran/savePembayaran') ?>" id="form-pembayaran" class="form form-horizontal" >
                            <input type="hidden" id="id" name="id" value="<?=@$data->id;?>">
                            <input type="hidden" id="id_personal" name="id_personal" value="<?=empty(@$data->id_personal)?MasKaryawan()->id:@$data->id_personal;?>" >
                            <div class="card shadow mb-3" style="margin-top: 30px;">
                                <div class="card-header py-3" style="background: rgb(255,255,255);">
                                    <div class="row">
                                        <div class="col-xl-8">
                                            <h1 class="d-xl-flex" style="font-family: Nunito, sans-serif;font-size: 16px;margin-top: 21px;"><strong>Unggah Bukti Transfer</strong></h1>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body" style="width: auto;">
                                    <!-- <input type="file" name="Unggah Bukti"> -->
                                    <div class="form-group">
                                        <label for="proposalTitle1">Nama Pengirim</label>
                                        <input type="text" class="form-control" id="nama" name="nama" value="<?=@$data->nama;?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="proposalTitle1">Bukti Transfer</label>
                                        <input type="file" class="form-control" id="bukti" name="files[bukti]">
                                        <?php if (!empty($data->bukti) AND file_exists("uploads/bukti/".$data->bukti)) : ?>
                                            <div class="row no-gutters mt-1">
                                                <div class="col-12 text-center">
                                                    <a href="<?=base_url("uploads/bukti/".$data->bukti)?>" >
                                                        <img class="img-fluid" src="<?=base_url("uploads/bukti/".$data->bukti)?>" alt="">
                                                    </a>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                    <?php if($mode!="detail"): ?>
                                    <div class="card-footer">
                                        <div class="form-group row ">
                                            <?= csrf_field(); ?>
                                            
                                            <?php if (@$data->status!=22):?>
                                            <div class="col-md-12">
                                                <button type="reset" class="btn pull-right mr-1">Reset</button>
                                                <button type="submit" class="btn btn-primary pull-right mr-1">Simpan</button>
                                            </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <?php endif ?>
                                </div>
                            </div>
                        </form>
                        <p>
                            nb * : wajib diisi!
                        </p>
                        <div class="row" style="margin-top: 33px;">
                        
                        <?php if (@$data->status==22):?>
                        <div class="form-group text-center">
                            <a href="<?=site_url("po_tahap_pendaftaran/pembayaran_bukti_invoice")?>" target="_blank" class="btn btn-outline-danger">Unduh Invoice</a>
                            <a href="<?=site_url("po_tahap_pendaftaran/pembayaran_bukti_pelunasan")?>" target="_blank" class="btn btn-outline-primary">Unduh Bukti Pelunasan</a>
                            <a href="<?=site_url("po_tahap_pendaftaran/pembayaran_bukti_sertifikat")?>" target="_blank" class="btn btn-outline-success">Unduh Sertifikat</a>
                        </div>
                        <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</section>



<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>


<?php if($mode=="detail" OR @$data->status == 21 OR @$data->status == 22): ?>
<script type="text/javascript">
    $("#form-pembayaran :input").prop("disabled", true);
</script>
<?php endif ?>