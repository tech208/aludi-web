<?php
    $this->load->view('web/po_tahap_pendaftaran/index_step');
?>


<style>
a {
  text-decoration: none;
  display: inline-block;
  padding: 8px 16px;
}

a:hover {
  background-color: #ddd;
  color: black;
}

.previous {
  background-color: #7c7c7c;
  color: white;
}

.next {
  background-color: #5bc0de;
  color: white;
}
</style>
<div class="card">
    <div class="card-body ">
        <div class="row">
            <div class="col-md-8">
                <span class="<?=@$data->statuse->css;?>"><?=@$data->statuse->name;?></span>
                <p class="text-secondary"><?=(!empty(@$data->updated_at))?MasTanggal(@$data->updated_at,1):MasTanggal(@$data->created_at,1);?></p>
            </div>
            <div class="col-md-4 pull-right">
                <?php if (@$data->statuse->grup > 9 OR @$data->status==30):?>
                <a href="<?=site_url('po_tahap_pendaftaran/rekomendasi')?>" class="next pull-right">Next &raquo;</a>
                <?php endif; ?>
                <a href="<?=site_url('po_tahap_pendaftaran/tahap2')?>"  class="previous pull-right">&laquo; Previous</a>
            </div>
        </div>
    </div>
</div>

<div class="row">
    
    <div class="col-md-6">
        <section id="number-tabs">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Site Visit</h4>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-header">
                                <h4 class="card-title text-center">Langkah Pelaksanaan Site Visit</h4>
                            </div>
                            <div class="card-body">
                                <p class="text-muted">
                                    1. Diskusi penentuan tanggal site visit
                                        <br>* Diskusi dapat dilakukan melalui chat / email pihak ALUDI yang tercantum dibawah
                                </p>
                                <p class="text-muted">
                                    2. ALUDI membuat jadwal dan rundown site visit sesuai dengan hasil diskusi dengan calon penyelenggara SCF dan mengunggah jadwal dan rundown tersebut pada website
                                </p>
                                <p class="text-muted">
                                    3. Pelaksanaan Site Visit
                                </p>
                                <p class="text-muted">
                                    4. Aludi mengunggah hasil site visit dalam platform
                                        <br>* Jika penyelenggara dinyatakan tidak lolos site visit maka penyelenggara dapat mengajukan site visit kembali saat seluruh hasil ketidaksesuaian (temuan) telah diselesaikan, diskusi pengajuan dapat dilakukan melalui chat / email pihak ALUDI yang tercantum dibawah
                                </p>
                                <div class="title">
                                    <b>Kontak Admin</b>
                                    <br>
                                    Wa/Call : <?=MasProfile("wa")->deskripsi?><br>
                                    Email : <?=MasProfile("email")->deskripsi?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>  
        <section id="number-tabs">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title text-center">Hasil Visit</h4>
                        </div>
                        <div class="card-content collapse show">
                            <div class="card-body">
                                <div class="card-text">
                                <?php
                                    $no=1; foreach(@$data->visits_hasil as $val): ?>
                                    <a href="<?=base_url('uploads/visit_hasil/'.$val->lampiran)?>" target="_blank" class="alert bg-success alert-icon-left mb-2" role="alert">
                                        <span class="alert-icon"><i class="la la-download"></i></span>
                                        Download
                                    </a>

                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>  
    </div>
    <div class="col-md-6">
        <section id="uji_kompetensi">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Ujian Kompetensi</h4>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body">
                        <div class="card-text">
                            <ul>
                                <li>Ujian akan berlangsung selama 60 Menit</li>
                                <li>Ujian terdiri dari 30 soal pilihan ganda</li>
                                <li>Minimal passing grade 60</li>
                                <li>Jika gagal, Anda dapat mengulang ujian kembali dalam 30 hari kedepan</li>
                            </ul>
                        </div>
                        <table class="table " >
                            <thead>
                                <tr>
                                    <td style="width: 5%;">No</td>
                                    <td style="width: 50%;">Ujian</td>
                                    <td> &nbsp; </td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $no=1; foreach(@$data->ujian as $val_ujian): 
                                ?>
                                <tr>
                                    <td><?=$no?></td>
                                    <td><?=$val_ujian->name?></td>
                                    <td>
                                        <a href="<?=$val_ujian->url?>" target="_blank" class="btn btn-primary">Mulai Ujian</a>
                                    </td>
                                </tr>
                                <?php $no++; endforeach; ?>
                            </tbody>
                        </table>
                        <?php
                            // $this->load->view('web/po_tahap_pendaftaran/index_visit_status');
                        ?>
                    </div>
                </div>
            </div>
        </section>
        <section id="agenda_visit">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Agenda Visit</h4>
                </div>
                <div class="card-content collapse show">
                    <?php
                        $this->load->view('web/po_tahap_pendaftaran/index_visit_status');
                    ?>
                </div>
            </div>
        </section>
    </div>
</div>