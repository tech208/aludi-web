<?php
    $this->load->view('web/po_tahap_pendaftaran/index_step');
?>



<style>
a {
  text-decoration: none;
  display: inline-block;
  padding: 8px 16px;
}

a:hover {
  background-color: #ddd;
  color: black;
}

.previous {
  background-color: #7c7c7c;
  color: white;
}

.next {
  background-color: #5bc0de;
  color: white;
}
</style>
<div class="card">
    <div class="card-body ">
        <div class="row">
            <div class="col-md-8">
                <span class="<?=@$data->statuse->css;?>"><?=@$data->statuse->name;?></span>
                <p class="text-secondary"><?=(!empty(@$data->updated_at))?MasTanggal(@$data->updated_at,1):MasTanggal(@$data->created_at,1);?></p>
            </div>
            <div class="col-md-4 pull-right">
                <a href="<?=site_url('po_tahap_pendaftaran/visit')?>"  class="previous pull-right">&laquo; Previous</a>
            </div>
        </div>
    </div>
</div>


<div class="row d-flex">
    <div class="col-md-2">
    </div>
    <div class="col-md-8">

        <div class="card">
            <div class="card-header text-center " style="background-color: #009EC8;">
                <h2 >ALUDI</h2>
                <h5 >Asosiasi Layanan Urun Dana Indonesia</h5>
            </div>
            <div class="card-content collapse show">
                <div class="card-body">

                        <div class="row text-center">
                            <div class="col-md-12 ">
                                <img src="<?=base_url('app-assets/images/aludi/verif.png')?>" >
                                <h2>Surat Rekomendasi OJK</h2>
                                <h5>Surat rekomendasi ini dibuat sesuai dengan ketentuan POJK nomor 57 tahun 2020</h5>
                                <a href="<?=site_url('po_tahap_pendaftaran/rekomendasiSuratOjk')?>" target="_blank" class="btn btn-primary">Unduh Surat Rekomendasi</a>
                            </div>
                        </div>
                        
                </div>
            </div>
        </div>
    </div>
</div>
