<?php if (@$data->visits->count() > 0 ): ?>
<div class="row">
    <div class="col-md-12">
        <!-- <div class="page-header">
            <h1>Agenda Visit</h1>
        </div> -->
        <ul class="timeline">
            <?php
            $no=1; foreach(@$data->visits as $val): ?>

                <li class="timeline-item">
                    <div class="timeline-badge"><i class="glyphicon glyphicon-on"></i></div>
                    <div class="timeline-panel">
                        <div class="timeline-heading">
                            <h4 class="timeline-title"><?= @$val->nama ?></h4>
                            <p><small class="text-muted">
                                <i class="glyphicon glyphicon-time"></i> 
                                <?= tgl_indo(@$val->tanggal_mulai)."  ".@$val->tanggal_mulai_jam  ?> S/d <?= tgl_indo(@$val->tanggal_selesai)."  ".@$val->tanggal_selesai_jam ?></small></p>
                        </div>
                        <div class="timeline-body">
                            <p>
                                Berlokasi : <?= @$val->lokasi ?>
                                <br>
                                Visitor :<br>

                                <ul>
                                    <?php
                                        if (@$val->visitors->count() > 0 ):
                                        foreach(@$val->visitors as $val_visitors): 
                                    ?>
                                        <li><b><?=$val_visitors->nama."</b> Sebagai ".$val_visitors->jabatan?></li>
                                    <?php endforeach;  endif;?>
                                </ul>
                            </p>
                        </div>
                    </div>
                </li>
                    
            <?php endforeach; ?>

            <li class="timeline-item">
            </li>
        </ul>
    </div>
</div>
    
<?php else: ?>
<div class="row">
    <div class="col-md-12">
        <div class="page-header">
            <h1>Belum ada Agenda Visit</h1>
        </div>
    </div>
</div>
<?php endif; ?>