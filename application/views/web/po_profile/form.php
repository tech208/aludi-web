<div class="card">
    <div class="card-content collapse show">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12">
                    <form enctype="multipart/form-data" method="POST" action="<?= site_url('po_profile/save') ?>" class="form form-horizontal" >
                        <input type="hidden" class="form-control" id="id" name="id" value="<?=@$data->id;?>">
                        <div class="form-group">
                            <label>Nama</label>
                            <input type="text" class="form-control" id="nama" name="nama" placeholder="Masukan Nama Lengkap" readonly  value="<?=@$data->nama;?>">
                        </div>
                        <div class="form-group">
                            <label for="proposalTitle1">Lampiran</label>
                            <input type="file" class="form-control" id="lampiran" name="files[lampiran]">
                            <?php if (!empty(@$data->lampiran) AND file_exists("uploads/profil_lampiran/".@$data->lampiran)) :?>
                            <div class=mt-1>
                                <a href="<?=base_url("uploads/profil_lampiran/".@$data->lampiran)?>" target="_blank" class="btn btn-success ">Download</a> <?=@$data->lampiran?>
                            </div>
                            <?php endif; ?>
                        </div>
                        <div class="form-group">
                            <label>Deskripsi</label>
                            <textarea class="form-control tinymce" id="deskripsi" name="deskripsi"  placeholder="Masukan Deskripsi"  required style="height: 200px;" ><?=@$data->deskripsi;?></textarea>
                        </div>
                        <hr>
                        <div class="row px-1">
                            <button type="submit" class="btn btn-primary col-2 ">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>