<link rel="stylesheet" type="text/css" href="<?= base_url('app-assets/vendors/css/forms/selects/select2.min.css') ?>">
<link rel="stylesheet" type="text/css" href="<?= base_url('app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css') ?>">


<?php// if(has_permission('CREATE_PORTAL')): ?>
<div class="row">
    <div class="col-12">
        <div class="card p-1">
            <div class="row">
                <!-- <div class="col-sm-12 col-md-9">
                    <h3 style="padding-top: 3px;" class="m-0">Role</h3>
                </div> -->
                <div class="col-sm-12 col-md-3">
                    <a href="<?= site_url('po_artikel/form') ?>" class="btn btn-outline-primary btn-block">
                        Tambah
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<?php// endif ?>

<?php //if(!has_permission('READ_ARTIKEL')) return; ?>

<div class="row">
    <div class="col-12">
        <div class="card p-1">
            <div class="row">
                <div class="col-sm-12">
                    <table id="tbl_inventaris" class="table table-striped data_list">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Judul</th>
                                <th>Kategori</th>
                                <th>Deskripsi</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no=1; foreach($data as $val): ?>
                                <tr>
                                    <td><?= $no ?></td>
                                    <td><?= $val->name ?></td>
                                    <td><?= $val->kategoris->name ?></td>
                                    <td><?= $val->deskripsi ?></td>
                                    <td>
                                        <a href="<?= site_url('po_artikel/detail/'. $val->id) ?>" class="btn btn-icon btn-pure dark" title="Detail Data">
                                            <i class="la la-file"></i>
                                        </a>
                                        <?php// if(has_permission('UPDATE_ARTIKEL')): ?>
                                            <a href="<?= site_url('po_artikel/form/'. $val->id) ?>" class="btn btn-icon btn-pure dark" title="Merubah Data">
                                                <i class="la la-pencil"></i>
                                            </a>
                                        <?php// endif ?>
                                        <?php// if(has_permission('DELETE_ARTIKEL')): ?>
                                            <a href="<?= site_url('po_artikel/delete/'. $val->id) ?>" class="btn btn-icon btn-pure dark" title="Menghapus data" onclick="return confirm('Yakin Menghapus?');">
                                                <i class="la la-trash"></i>
                                            </a>
                                        <?php// endif ?>
                                    </td>
                                </tr>
                            <?php $no++; endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?= base_url('app-assets/vendors/js/forms/select/select2.full.min.js') ?>"></script>
<script src="<?= base_url('app-assets/vendors/js/tables/jquery.dataTables.min.js') ?>"></script>
<script src="<?= base_url('app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js') ?>"></script>
<script>
    $('.select2').select2();
    $('#tbl_inventaris').dataTable();
</script>