<div class="row">
    <div class="col-12">
        <div class="card p-1">
            <div class="card-body">
                <form method="POST" enctype="multipart/form-data" action="<?= site_url('po_calon_penerbit/save') ?>" id="form-calon-penerbit" class="form form-horizontal" >
                    <input type="hidden" autocomplete="off" name="id" id="id" value="<?=@$data->id?>" class="form-control " >
                    <div class="form-body">
                        <div class="form-group row ">
                            <label for="tanggal_mulai" class="col-md-2 label-control text-left">Owner*</label>
                            <div class="col-md-3 col-sm-12">
                                <?php if($mode!="detail"):  ?>
                                    <input type="text" autocomplete="off" name="owner" id="owner" value="<?=@$data->owner?>" class="form-control " required>
                                <?php else:  ?>
                                    <?=@$data->owner?>
                                <?php endif;  ?>
                            </div>
                        </div>
                        <div class="form-group row ">
                            <label for="tanggal_mulai" class="col-md-2 label-control text-left">Nomor Telepon*</label>
                            <div class="col-md-3 col-sm-12">
                                <?php if($mode!="detail"):  ?>
                                    <input type="text" autocomplete="off" name="nomor_telepon" id="nomor_telepon" value="<?=@$data->nomor_telepon?>" class="form-control " required>
                                <?php else:  ?>
                                    <?=@$data->nomor_telepon?>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="form-group row ">
                            <label for="tanggal_mulai" class="col-md-2 label-control text-left">Email*</label>
                            <div class="col-md-3 col-sm-12">
                                <?php if($mode!="detail"):  ?>
                                    <input type="email" autocomplete="off" name="email" id="email" value="<?=@$data->email?>" class="form-control " required>
                                <?php else:  ?>
                                    <?=@$data->email?>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="form-group row ">
                            <label for="tanggal_mulai" class="col-md-2 label-control text-left">Nama Perusahaan*</label>
                            <div class="col-md-3 col-sm-12">
                                <?php if($mode!="detail"):  ?>
                                    <input type="text" autocomplete="off" name="nama_perusahaan" id="nama_perusahaan" value="<?=@$data->nama_perusahaan?>" class="form-control " required>
                                <?php else:  ?>
                                    <?=@$data->nama_perusahaan?>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="form-group row ">
                            <label for="tanggal_mulai" class="col-md-2 label-control text-left">Nama Brand*</label>
                            <div class="col-md-3 col-sm-12">
                                <?php if($mode!="detail"):  ?>
                                    <input type="text" autocomplete="off" name="nama_brand" id="nama_brand" value="<?=@$data->nama_brand?>" class="form-control " required>
                                <?php else:  ?>
                                    <?=@$data->nama_brand?>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="form-group row ">
                            <label for="tanggal_mulai" class="col-md-2 label-control text-left">Bidang Usaha*</label>
                            <div class="col-md-3 col-sm-12">
                                <?php if($mode!="detail"):  ?>
                                    <input type="text" autocomplete="off" name="bidang_usaha" id="bidang_usaha" value="<?=@$data->bidang_usaha?>" class="form-control " required>
                                <?php else:  ?>
                                    <?=@$data->bidang_usaha?>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="form-group row ">
                            <label for="tanggal_mulai" class="col-md-2 label-control text-left">Total Pendanaan*</label>
                            <div class="col-md-3 col-sm-12">
                                <?php if($mode!="detail"):  ?>
                                    <input type="text" autocomplete="off" name="total_pendanaan" id="total_pendanaan" value="<?=@$data->total_pendanaan?>" class="form-control " required>
                                <?php else:  ?>
                                    <?=@$data->total_pendanaan?>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="form-group row ">
                            <label for="alasan" class="col-md-2 label-control text-left">Lokasi*</label>
                            <div class="col-md-10">
                                <?php if($mode!="detail"):  ?>
                                    <textarea name="lokasi" id="lokasi" class="form-control tinymce" rows="4" ><?=@$data->lokasi?></textarea>
                                <?php else:  ?>
                                    <?=@$data->lokasi?>
                                <?php endif;  ?>
                            </div>
                        </div>
                        <div class="form-group row ">
                            <label for="alasan" class="col-md-2 label-control text-left">Deskripsi*</label>
                            <div class="col-md-10">
                                <?php if($mode!="detail"):  ?>
                                    <textarea name="deskripsi" id="deskripsi" class="form-control tinymce" rows="4" ><?=@$data->deskripsi?></textarea>
                                <?php else:  ?>
                                    <?=@$data->deskripsi?>
                                <?php endif;  ?>
                            </div>
                        </div>
                        <div class="form-group row ">
                            <label for="lampiran" class="col-md-2 label-control text-left">Foto</label>
                            <?php if($mode!="detail" ): ?>
                            <div class="col-md-3 col-sm-12">
                                <input type="file" autocomplete="off" name="files[foto]" id="foto" class="form-control " >
                            </div>
                            <?php endif; ?>
                            <?php if (!empty(@$data->foto) AND file_exists("uploads/calon_penerbit/".@$data->foto)) :?>
                            <div class=mt>
                                <a href="<?=base_url("uploads/calon_penerbit/".@$data->foto)?>" target="_blank" class="btn btn-success ">Download</a> <?=@$data->foto?>
                            </div>
                            <?php endif; ?>
                        </div>
                        <div class="form-group row ">
                            <label for="lampiran" class="col-md-2 label-control text-left">Lampiran</label>
                            <?php if($mode!="detail" ): ?>
                            <div class="col-md-3 col-sm-12">
                                <input type="file" autocomplete="off" name="files[lampiran]" id="lampiran" class="form-control " >
                            </div>
                            <?php endif; ?>
                            <?php if (!empty(@$data->lampiran) AND file_exists("uploads/calon_penerbit/".@$data->lampiran)) :?>
                            <div class=mt>
                                <a href="<?=base_url("uploads/calon_penerbit/".@$data->lampiran)?>" target="_blank" class="btn btn-success ">Download</a> <?=@$data->lampiran?>
                            </div>
                            <?php endif; ?>
                        </div>
                        
                        <?php if($mode!="detail" OR MasKaryawanApprove() == 1 ): ?>
                            <div class="form-group row ">
                                <label for="id_kategori" class="col-md-2 label-control text-left">Status *</label>
                                <div class="col-md-10">
                                    <select name="status" id="status" class="form-control square select2" required>
                                        <option value="" selected disabled>Pilih..</option>
                                        <?php foreach($refStatus as $l): ?>
                                            <option value="<?= $l->id ?>" <?=(@$data->status==$l->id)?"selected":" ";?>><?= $l->name ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                            </div>
                        <?php else: ?>
                            <?=@$data->statuse->name?>
                        <?php endif; ?>


                        <div class="form-group row ">
                            <?= csrf_field(); ?>
                            <label for="id_role" class="col-md-2 label-control text-left"></label>
                            
                            <div class="col-md-10">
                                <input type="hidden" id="mode" name="mode" value="<?=$mode?>">
                                <?php if($mode!="detail" OR MasKaryawanApprove() == 1 ):  ?>
                                <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                                <?php endif; ?>
                                <a href="<?= site_url('po_calon_penerbit/') ?>" class="btn btn-secondary pull-right mr-1">Kembali</a>
                            </div>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<style>
    /* .input-group-append {
        display: none;
    } */
</style>



<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript">
$(function() {

});
</script>


