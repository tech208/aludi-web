<?php $colors = ['success', 'warning', 'info', 'danger','success', 'warning', 'info', 'danger']; ?>

<div class="row">
    <div class="col-md-1">
        <a href="<?=site_url("/po_peserta_pulling/")?>" type="button" class="btn btn-secondary">Kembali</a>
    </div>
</div>
<br>



<section >
    <div class="row d-flex justify-content-center">
        <!-- Jumlah Penerbit -->
        <div class="col-md-2 col-12">
            <div class="card pull-up">
                <div class="card-content">
                    <div class="card-body">
                        <div class="media d-flex">
                            <div class="media-body text-center">
                                <h3 class="<?= $colors[1] ?> font-weight-bolder" style="font-size: 2.2rem"><?=MasUang($data->jumlah_penerbit,0)?></h3>
                                <h6>Jumlah Penerbit</h6>
                            </div>
                            <div>
                                <i class="icon-user-follow <?= $colors[1] ?> font-large-2 float-right"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end Jumlah Penerbit -->

        <!-- Jumlah Pemodal -->
        <div class="col-md-2 col-12">
            <div class="card pull-up">
                <div class="card-content">
                    <div class="card-body">
                        <div class="media d-flex">
                            <div class="media-body text-center">
                                <h3 class="<?= $colors[2] ?> font-weight-bolder" style="font-size: 2.2rem"><?=MasUang($data->jumlah_investor,0)?></h3>
                                <h6>Jumlah Pemodal</h6>
                            </div>
                            <div>
                                <i class="icon-user-follow <?= $colors[2] ?> font-large-2 float-right"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end Jumlah Pemodal -->
        
        <!-- Jumlah Pendanaan -->
        <div class="col-md-2 col-12">
            <div class="card pull-up">
                <div class="card-content">
                    <div class="card-body">
                        <div class="media d-flex">
                            <div class="media-body text-center">
                                <h3 class="<?= $colors[3] ?> font-weight-bolder" style="font-size: 2.2rem"><?=MasUang($data->jumlah_dana,0)?></h3>
                                <h6>Jumlah Pendanaan</h6>
                            </div>
                            <div>
                                <i class="icon-user-follow <?= $colors[3] ?> font-large-2 float-right"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end Jumlah Pendanaan -->

        
        <!-- Jumlah Dividen-->
        <div class="col-md-2 col-12">
            <div class="card pull-up">
                <div class="card-content">
                    <div class="card-body">
                        <div class="media d-flex">
                            <div class="media-body text-center">
                                <h3 class="<?= $colors[4] ?> font-weight-bolder" style="font-size: 2.2rem"><?=MasUang($data->jumlah_dividen,0)?></h3>
                                <h6>Jumlah Dividen</h6>
                            </div>
                            <div>
                                <i class="icon-user-follow <?= $colors[4] ?> font-large-2 float-right"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end Jumlah Dividen -->

    </div>

</section>


<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Gambaran Jenis Penerbit</h3>
            </div>
            <div class="card-content collapse show">
                <div class="card-body">
                    
                <div class="row">
                    <div class="col-md-8">
                        <!-- <div id="chart_div_penerbit"></div> -->
                        <div id="chart_div_jenis_penerbit" style="height: 300px; width: 100%;"></div>
                    </div>
                    <div class="col-md-4">
                        <h3><?= MasUang($data->jenis_total,0)?> Bisnis Terdaftar</h3>
                        <p><?= MasUang($data->jenis_total,0)?> Jenis Bisnis telah melakukan pendanaan.</p>
                    </div>
                </div>

                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Lokasi Penerbit</h3>
            </div>
            <div class="card-content collapse show">
                <div class="card-body">
                    <h3 class="card-title"><?=MasUang($data->lokasi_penerbit_jumlah,0)?> Kota</h3>
                    <p>Bisnis tersebar di seluruh Indonesia.</p>
                    <div id="chart_div_lokasi_penerbit" style="height: 300px; width: 100%;"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Gambaran Pemodal</h3>
            </div>
            <div class="card-content collapse show">
                <div class="card-body">

                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <h3> <?=MasUang($data->lokasi_pemodal_jumlah,0)?> <small>Pemodal</small> </h3>
                                <p>Dari berbagai domisili. Pendanaan di Security Crowdfunding melibatkan individu dari berbagai kalangan. </p>
                            </div>
                            <div class="col-md-6">
                                <h3><!--, <?=MasUang($data->lokasi_pemodal_total_negara,0)?> <small>Negara</small> --></h3>
                                <p>Tersebar di  <?=MasUang($data->lokasi_pemodal_total_kota,0)?> kota di Indonesia. </p>
                            </div>
                        </div>
                        <br>
                    </div> 
                    <div class="col-md-12">
                        <div id="chart_div_lokasi_pemodal" style="height: 300px; width: 100%;"></div>
                    </div>
                </div>

                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Gambaran Pendanaan</h3>
            </div>
            <div class="card-content collapse show">
                <div class="card-body">

                <div class="row">
                    <div class="col-md-6">
                        <h3> Rp <?=MasUang($data->jumlah_investor_dividen,0)?></h3>
                        <p>Dana Telah Tersalurkan </p>
                    </div>
                    <div class="col-md-6">
                        <h3> <?=MasUang($data->jumlah_investor,0)?> <small>Pemodal</small> </h3>
                        <p>Perusahaan Menerima Dana Untuk Pengembangan Bisnis.</p>
                    </div>
                        
                </div>

                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Gambaran Pembagian/Dividen</h3>
            </div>
            <div class="card-content collapse show">
                <div class="card-body">

                <div class="row">
                    <div class="col-md-6">
                        <h3>Rp <?=MasUang($data->jumlah_dividen,0)?></h3>
                        <p>Total Dividen Dibagikan </p>
                    </div>
                    <div class="col-md-6">
                        <h3> <?=MasUang($data->jumlah_dividen_penerima,0)?> </h3>
                        <p><b>Pemodal Telah Mendapatkan Dividen</b></p>
                        <p>Sebanyak <?=MasUang($data->jumlah_dividen_penerima,0)?> Pemodal telah menerima pembagian dividen dari bisnis yang mereka dukung.</p>
                    </div>
                        
                </div>

                </div>
            </div>
        </div>
    </div>
    
</div>



<script type="text/javascript">
window.onload = function () {

var chart_jenis = new CanvasJS.Chart("chart_div_jenis_penerbit", {
	theme: "light2", // "light2", "dark1", "dark2"
	animationEnabled: false, // change to true		
	title:{
		text: "Jenis Penerbit"
	},
	data: [
	{
		// Change type to "bar", "area", "spline", "pie",etc.
		type: "pie",
		dataPoints: <?php echo json_encode($data->jenis, JSON_NUMERIC_CHECK); ?>
	}
	]
});
chart_jenis.render();

var chart = new CanvasJS.Chart("chart_div_lokasi_penerbit", {
	theme: "light1", // "light2", "dark1", "dark2"
	animationEnabled: false, // change to true		
	title:{
		text: "Lokasi Penerbit"
	},
	data: [
	{
		// Change type to "bar", "area", "spline", "pie",etc.
		type: "bar",
		dataPoints: <?php echo json_encode($data->lokasi_penerbit, JSON_NUMERIC_CHECK); ?>
	}
	]
});
chart.render();

var chart_pemodal = new CanvasJS.Chart("chart_div_lokasi_pemodal", {
	theme: "light1", // "light2", "dark1", "dark2"
	animationEnabled: false, // change to true		
	title:{
		text: "Lokasi Pemodal"
	},
	data: [
	{
		// Change type to "bar", "area", "spline", "pie",etc.
		type: "bar",
		dataPoints: <?php echo json_encode($data->lokasi_pemodal, JSON_NUMERIC_CHECK); ?>
	}
	]
});
chart_pemodal.render();

}
</script>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"> </script>
 


<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">

      // Load the Visualization API and the corechart package.
      google.charts.load('current', {'packages':['corechart']});

      // Set a callback to run when the Google Visualization API is loaded.
      google.charts.setOnLoadCallback(drawChart);

      // Callback that creates and populates a data table,
      // instantiates the pie chart, passes in the data and
      // draws it.
      function drawChart() {

        // Create the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Slices');
        data.addRows([
          ['Mushrooms', 3],
          ['Onions', 1],
          ['Olives', 1],
          ['Zucchini', 1],
          ['Pepperoni', 2]
        ]);

        // Set chart options
        var options_penerbit = {
          title: "Jenis Usaha",
          width: '100%',
          height: '300'
        };

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById('chart_div_penerbit'));
        chart.draw(data, options_penerbit);
      }
    </script>
