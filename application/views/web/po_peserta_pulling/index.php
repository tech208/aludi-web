<?php $colors = ['success', 'warning', 'info', 'danger','success', 'warning', 'info', 'danger']; ?>

<section >
    <div class="row d-flex justify-content-center">
        <!-- Jumlah Penerbit -->
        <div class="col-md-2 col-12">
            <div class="card pull-up">
                <div class="card-content">
                    <div class="card-body">
                        <div class="media d-flex">
                            <div class="media-body text-center">
                                <h3 class="<?= $colors[1] ?> font-weight-bolder" style="font-size: 2.2rem"><?=MasUang($data->jumlah['jumlah_penerbit'],0)?></h3>
                                <h6>Jumlah Penerbit</h6>
                            </div>
                            <div>
                                <i class="icon-user-follow <?= $colors[1] ?> font-large-2 float-right"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end Jumlah Penerbit -->

        <!-- Jumlah Pemodal -->
        <div class="col-md-2 col-12">
            <div class="card pull-up">
                <div class="card-content">
                    <div class="card-body">
                        <div class="media d-flex">
                            <div class="media-body text-center">
                                <h3 class="<?= $colors[2] ?> font-weight-bolder" style="font-size: 2.2rem"><?=MasUang($data->jumlah['jumlah_investor'],0)?></h3>
                                <h6>Jumlah Pemodal</h6>
                            </div>
                            <div>
                                <i class="icon-user-follow <?= $colors[2] ?> font-large-2 float-right"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end Jumlah Pemodal -->
        
        <!-- Jumlah Pendanaan -->
        <div class="col-md-2 col-12">
            <div class="card pull-up">
                <div class="card-content">
                    <div class="card-body">
                        <div class="media d-flex">
                            <div class="media-body text-center">
                                <h3 class="<?= $colors[3] ?> font-weight-bolder" style="font-size: 2.2rem"><?=MasUang($data->jumlah['jumlah_dana'],0)?></h3>
                                <h6>Jumlah Pendanaan</h6>
                            </div>
                            <div>
                                <i class="icon-user-follow <?= $colors[3] ?> font-large-2 float-right"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end Jumlah Pendanaan -->

        
        <!-- Jumlah Dividen-->
        <div class="col-md-2 col-12">
            <div class="card pull-up">
                <div class="card-content">
                    <div class="card-body">
                        <div class="media d-flex">
                            <div class="media-body text-center">
                                <h3 class="<?= $colors[4] ?> font-weight-bolder" style="font-size: 2.2rem"><?=MasUang($data->jumlah['jumlah_dividen'],0)?></h3>
                                <h6>Jumlah Dividen</h6>
                            </div>
                            <div>
                                <i class="icon-user-follow <?= $colors[4] ?> font-large-2 float-right"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end Jumlah Dividen -->

        <!-- Rangkuman-->
        <div class="col-md-2 col-12">
            <a href="<?=site_url("po_peserta_pulling/ringkasan")?>"  title="Download laporan">
                <div class="card overflow-hidden">
                    <div class="card-content">
                        <div class="media align-items-stretch">
                            <div class="bg-info p-1 media-middle">
                                <i class="la la-list font-large-2 text-white"></i>
                            </div>
                            <div class="media-right p-1 media-middle">
                                <h4 class="info">Rangkuman</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <!--end Rangkuman -->

    </div>

</section>

<div class="row">
    <div class="col-12">
        <div class="card p-1">
            <div class="row">
                <div class="col-sm-12">
                    <a href="<?=site_url("po_peserta_pulling/printxlx")?>" class="btn btn-success" >Laporan excel</a>
                </div>
            </div><hr>
            <div class="row">
                <div class="col-sm-12">
                    <table id="tbl_inventaris" class="table table-striped data_list">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama PT</th>
                                <th>Nama Brand</th>
                                <th style="text-align:center;">Jumlah Penerbit</th>
                                <th style="text-align:center;">Jumlah Pendanaan</th>
                                <th style="text-align:center;">Jumlah Dividen</th>
                                <th style="text-align:center;">Jumlah Investor</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no=1; foreach($data as $val): ?>
                                <tr>
                                    <td><?= $no ?></td>
                                    <td><?= @$val->nama_pt ?></td>
                                    <td><?= @$val->nama_brand ?></td>
                                    <td style="text-align:right;"><?= MasUang(@$val->jumlah_penerbit,0) ?></td>
                                    <td style="text-align:right;"><?= MasUang(@$val->jumlah_dana,0) ?></td>
                                    <td style="text-align:right;"><?= MasUang(@$val->jumlah_dividen,0) ?></td>
                                    <td style="text-align:right;"><?= MasUang(@$val->jumlah_investor,0) ?></td>
                                    <td>
                                        <a href="<?= site_url('po_peserta_pulling/detail/'. $val->id) ?>" class="btn btn-icon btn-pure dark" title="Mengirimkan data" >
                                            <i class="la la-file"></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php $no++; endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?= base_url('app-assets/vendors/js/forms/select/select2.full.min.js') ?>"></script>
<script src="<?= base_url('app-assets/vendors/js/tables/jquery.dataTables.min.js') ?>"></script>
<script src="<?= base_url('app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js') ?>"></script>
<script>
    $('.select2').select2();
    $('#tbl_inventaris').dataTable();
</script>