<div class="row">
    <div class="col-12">
        <div class="card p-1">
            <div class="card-body">
                <form method="POST" enctype="multipart/form-data" action="<?= site_url('po_penerbit/save') ?>" id="form-penerbit" class="form form-horizontal" >
                    <input type="hidden" autocomplete="off" name="id" id="id" value="<?=@$data->id?>" class="form-control " >
                    <div class="form-body">
                        <div class="form-group row ">
                            <label for="id_kategori" class="col-md-2 label-control text-left">Data Direksi</label>
                            <div class="col-md-10" id="owner_div">
                                <div class="form-group row ">
									<label for="id_kategori" class="col-md-2 label-control text-left">Nama</label>
									<div class="col-md-4">	 
										<input type="text" autocomplete="off" name="owner" id="owner" value="<?=@$data->owner?>" class="form-control " >
									</div>
									<label for="id_kategori" class="col-md-2 label-control text-left">Jabatan</label>
									<div class="col-md-4">	
										<input type="text" autocomplete="off" name="owner" id="owner" value="<?=@$data->owner?>" class="form-control " >
									</div>
								</div>
								<div class="form-group row ">
									<label for="id_kategori" class="col-md-2 label-control text-left">Nomor KTP</label>
									<div class="col-md-4">	 
										<input type="text" autocomplete="off" name="owner" id="owner" value="<?=@$data->owner?>" class="form-control " >
									</div>
									<label for="id_kategori" class="col-md-2 label-control text-left">Nomor Telepon</label>
									<div class="col-md-4">	
										<input type="text" autocomplete="off" name="owner" id="owner" value="<?=@$data->owner?>" class="form-control " >
									</div>
								</div>
								<div class="form-group row ">
									<label for="id_kategori" class="col-md-2 label-control text-left">Email</label>
									<div class="col-md-4">	 
										<input type="text" autocomplete="off" name="owner" id="owner" value="<?=@$data->owner?>" class="form-control " >
									</div>
									<div class="col-md-6">	
										
										<a href="#" class="btn btn-danger pull-right mr-1">-</a>
										<a href="#" onclick="add_div_owner()" class="btn btn-success pull-right mr-1">+</a>
									</div>
								</div>
								<hr>
                            </div>
                        </div>
						<div class="form-group row ">
                            <label for="id_kategori" class="col-md-2 label-control text-left">Calon Penerbit</label>
                            <div class="col-md-10">
                                <select name="status" id="status" class="form-control square select2" >
                                    <option value="" selected disabled>Pilih..</option>
                                    <?php foreach($refCalonPenerbit as $l): ?>
                                        <option value="<?= $l->id ?>" data-id="<?= $l->id ?>" <?=(@$data->status==$l->id)?"selected":" ";?>><?= $l->nama_perusahaan ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>

                        <?php 
                            if(!empty(@$data->id)):
                        ?>
                        <div class="form-group row ">
                            <label for="tanggal_mulai" class="col-md-2 label-control text-left">Kode</label>
                            <div class="col-md-3 col-sm-12">
                                <input type="text" autocomplete="off" name="owner" id="owner" value="<?=@$data->kode?>" class="form-control " readonly>
                            </div>
                        </div>
                        <?php
                            endif;
                        ?>

                        <div class="form-group row ">
                            <label for="tanggal_mulai" class="col-md-2 label-control text-left">Owner</label>
                            <div class="col-md-3 col-sm-12">
                                <input type="text" autocomplete="off" name="owner" id="owner" value="<?=@$data->owner?>" class="form-control " >
                            </div>
                        </div>
                        <div class="form-group row ">
                            <label for="tanggal_mulai" class="col-md-2 label-control text-left">Email*</label>
                            <div class="col-md-3 col-sm-12">
                                <input type="email" autocomplete="off" name="email" id="email" value="<?=@$data->email?>" class="form-control " required>
                            </div>
                        </div>
                        <div class="form-group row ">
                            <label for="tanggal_mulai" class="col-md-2 label-control text-left">Nama Perusahaan*</label>
                            <div class="col-md-3 col-sm-12">
                                <input type="text" autocomplete="off" name="nama_perusahaan" id="nama_perusahaan" value="<?=@$data->nama_perusahaan?>" class="form-control " required>
                            </div>
                        </div>
                        <div class="form-group row ">
                            <label for="tanggal_mulai" class="col-md-2 label-control text-left">Nama Brand*</label>
                            <div class="col-md-3 col-sm-12">
                                <input type="text" autocomplete="off" name="nama_brand" id="nama_brand" value="<?=@$data->nama_brand?>" class="form-control " required>
                            </div>
                        </div>
                        <div class="form-group row ">
                            <label for="tanggal_mulai" class="col-md-2 label-control text-left">Bidang Usaha*</label>
                            <div class="col-md-3 col-sm-12">
                                <input type="text" autocomplete="off" name="bidang_usaha" id="bidang_usaha" value="<?=@$data->bidang_usaha?>" class="form-control " required>
                            </div>
                        </div>
                        <div class="form-group row ">
                            <label for="tanggal_mulai" class="col-md-2 label-control text-left">Total Pendanaan</label>
                            <div class="col-md-3 col-sm-12">
                                <input type="text" autocomplete="off" name="total_pendanaan" id="total_pendanaan" value="<?=@$data->total_pendanaan?>" class="form-control " required>
                            </div>
                        </div>
                        <div class="form-group row ">
                            <label for="alasan" class="col-md-2 label-control text-left">deskripsi*</label>
                            <div class="col-md-10">
                                <textarea name="deskripsi" id="deskripsi" class="form-control " rows="4" ><?=@$data->deskripsi?></textarea>
                            </div>
                        </div>
                        <!-- <div class="form-group row ">
                            <label for="lampiran" class="col-md-2 label-control text-left">Foto</label>
                            <div class="col-md-3 col-sm-12">
                                <input type="file" autocomplete="off" name="foto" id="foto" class="form-control " >
                            </div>
                            <?php if (!empty(@$data->foto) AND file_exists("uploads/po_calon_penerbit/".@$data->foto)) :?>
                            <div class=mt>
                                <a href="<?=base_url("uploads/po_calon_penerbit/".@$data->foto)?>" target="_blank" class="btn btn-success ">Download</a> <?=@$data->foto?>
                            </div>
                            <?php endif; ?>
                        </div>
                        <div class="form-group row ">
                            <label for="lampiran" class="col-md-2 label-control text-left">Lampiran</label>
                            <div class="col-md-3 col-sm-12">
                                <input type="file" autocomplete="off" name="lampiran" id="lampiran" class="form-control " >
                            </div>
                            <?php if (!empty(@$data->lampiran) AND file_exists("uploads/po_calon_penerbit/".@$data->lampiran)) :?>
                            <div class=mt>
                                <a href="<?=base_url("uploads/po_calon_penerbit/".@$data->lampiran)?>" target="_blank" class="btn btn-success ">Download</a> <?=@$data->lampiran?>
                            </div>
                            <?php endif; ?>
                        </div> -->
                        <div class="form-group row ">
                            <label for="id_kategori" class="col-md-2 label-control text-left">Status *</label>
                            <div class="col-md-10">
                                <select name="status" id="status" class="form-control square select2" required>
                                    <option value="" selected disabled>Pilih..</option>
                                    <?php foreach($refStatus as $l): ?>
                                        <option value="<?= $l->id ?>" <?=(@$data->status==$l->id)?"selected":" ";?>><?= $l->name ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row ">
                            <?= csrf_field(); ?>
                            <label for="id_role" class="col-md-2 label-control text-left"></label>
                            <div class="col-md-10">
                                <?php if($mode!="detail" ): ?>
                                <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                                <?php endif ?>
                                <a href="<?= site_url('po_penerbit/') ?>" class="btn btn-secondary pull-right mr-1">Kembali</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<style>
    /* .input-group-append {
        display: none;
    } */
</style>

<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>


<?php if($mode=="detail" ): ?>
<script type="text/javascript">
    $("#form-penerbit :input").prop("disabled", true);
</script>
<?php endif ?>

<script type="text/javascript">
var i = 0;
function add_div_owner(){
	alert('asd');
}
$(function() {

});

</script>

