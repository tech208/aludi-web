<?php if(!has_permission('CREATE_USER')) return; ?>
<link rel="stylesheet" type="text/css" href="<?= base_url('app-assets/vendors/css/forms/selects/select2.min.css') ?>">
<link rel="stylesheet" type="text/css" href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css">
<div class="row">
    <div class="col-12">
        <div class="card p-1">
            <div class="card-body">
                <form method="POST" action="<?= site_url('user/insert') ?>" class="form form-horizontal row-separator">
                    <div class="form-body">
                        <h3 class="row-section">Tambah Karyawan</h3>
                        <div class="form-group row border-bottom-0">
                            <label for="id_karyawan" class="col-md-2 label-control text-left">ID Karyawan</label>
                            <div class="col-md-10">
                                <input type="text" name="id_karyawan" id="id_karyawan" value="<?= flashdata('id_karyawan') ?>" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group row border-bottom-0">
                            <label for="name" class="col-md-2 label-control text-left">Nama Lengkap</label>
                            <div class="col-md-10">
                                <input type="text" name="name" id="name" value="<?= flashdata('name') ?>" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group row border-bottom-0">
                            <label for="id_jabatan" class="col-md-2 label-control text-left">Jabatan</label>
                            <div class="col-md-10">
                                <select name="id_jabatan" id="id_jabatan" class="form-control select2" required>
                                    <option value="" selected disabled>Pilih Jabatan</option>
                                    <?php foreach($job_titles as $job): ?>
                                        <option value="<?= $job->id ?>" <?= $job->id == flashdata('id_jabatan') ? 'selected':'' ?>><?= $job->job_titles ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row border-bottom-0">
                            <label for="id_jenis_karyawan" class="col-md-2 label-control text-left">Jenis Karyawan</label>
                            <div class="col-md-10">
                                <select name="id_jenis_karyawan" id="id_jenis_karyawan" class="form-control select2" required>
                                    <option value="" selected disabled>Jenis Karyawan</option>
                                    <?php foreach(\App\Constants\UserManagement::TYPE_LIST as $type): ?>
                                        <option value="<?= $type ?>" <?= $type == flashdata('id_jenis_karyawan') ? 'selected':'' ?>><?= $type ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row border-bottom-0">
                            <label for="email" class="col-md-2 label-control text-left">Email Perusahaan</label>
                            <div class="col-md-10">
                                <input type="email" name="email" id="email" value="<?= flashdata('email') ?>" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group row border-bottom-0">
                            <label for="tgl_bergabung" class="col-md-2 label-control text-left">Tanggal Bergabung</label>
                            <div class="col-md-10">
                                <input type="text" name="tgl_bergabung" id="tgl_bergabung" value="<?= flashdata('tgl_bergabung') ?>" class="form-control datepicker" autocomplete="off" required>
                            </div>
                        </div>
                        <div class="form-group row border-bottom-0">
                            <label for="id_role" class="col-md-2 label-control text-left">Role</label>
                            <div class="col-md-10">
                                <select name="id_role" id="id_role" class="form-control select2" required>
                                    <option value="" selected disabled>Pilih Role</option>
                                    <?php foreach($roles as $role): ?>
                                        <option value="<?= $role->id ?>" <?= $role->id == flashdata('id_role') ? 'selected':'' ?>><?= $role->name ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row border-bottom-0">
                            <?= csrf_field(); ?>
                            <label for="id_role" class="col-md-2 label-control text-left"></label>
                            <div class="col-md-10">
                                <button type="submit" class="btn btn-primary pull-right">SIMPAN</button>
                                <a href="<?= site_url('user') ?>" class="btn btn-default pull-right mr-1">BATAL</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<style>
    .input-group-append {
        display: none;
    }
</style>
<script src="<?= base_url('app-assets/vendors/js/forms/select/select2.full.min.js') ?>"></script>
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js"></script>
<script>
$('.select2').select2();
$('.datepicker').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
    zIndexOffset: 1500,
    uiLibrary: 'bootstrap4',
    icons: {
        rightIcon: false
    },
    modal: true, header: true, footer: true
});
</script>
