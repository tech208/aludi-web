<div class="card">
    <div class="card-content collapse show">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12">
                    <form enctype="multipart/form-data" method="POST" action="<?= site_url('m_language/save') ?>" class="form form-horizontal" >
                        <input type="hidden" class="form-control" id="id" name="id" value="<?=@$data['language_id'];?>">
                        <div class="form-group">
                            <label>Kode</label>
                            <input type="text" class="form-control" id="nama" name="data[language_code]" placeholder="Masukan Nama Lengkap" value="<?=@$data['language_code'];?>">
                        </div>
						<div class="form-group">
                            <label>Deskripsi</label>
							<textarea class="form-control" id="deskripsi" name="data[language_description]"  placeholder="Masukan Deskripsi"  required style="height: 80px;" ><?=@$data['language_description'];?></textarea>
                        </div>
                        
                        <div class="form-group">
                            <label>Bahas Indonesia</label>
                            <textarea class="form-control summernote" id="deskripsi" name="data[language_ind]"  placeholder="Masukan Bahasa Indonesia"  required style="height: 200px;" ><?=@$data['language_ind'];?></textarea>
                        </div>
						<div class="form-group">
                            <label>Bahasa Inggris</label>
                            <textarea class="form-control summernote" id="deskripsi" name="data[language_eng]"  placeholder="Masukan Bahasa Inggris"  required style="height: 200px;" ><?=@$data['language_eng'];?></textarea>
                        </div>
                        <hr>
                        <div class="row px-1">
                            <button type="submit" class="btn btn-primary col-2 ">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>