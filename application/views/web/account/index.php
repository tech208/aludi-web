<div class="row">
    <div class="col-12">
        <div class="card p-1">
            <ul class="nav nav-pills nav-active-bordered-pill nav-justified">
                <li class="nav-item">
                    <a href="<?= site_url('account') ?>" class="nav-link active">Akun</a>
                </li>
                <li class="nav-item">
                    <a href="<?= site_url('account/changepassword') ?>" class="nav-link">Change Password</a>
                </li>
            </ul>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card p-1">
            <div class="card-body">
                <form enctype="multipart/form-data" method="POST" action="<?= site_url('account/save_account') ?>" class="form form-horizontal row-separator">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-3 col-sm-12">
                                <img id="avatar_thumb" src="<?= avatar_path() ?>" alt="Photo profile" class="rounded-circle img-border height-150 mx-auto d-block">
                            </div>
                            <div class="col-md-9 col-sm-12 pt-2">
                                <label for="photo" class="col-md-2 label-control text-left">Photo</label>
                                <div class="col-md-10">
                                    <input type="file" accept=".jpg,.jpeg,.png,.gif" name="photo" id="photo" class="form-control input-sm">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row border-bottom-0">
                            <label for="akun" class="col-md-2 label-control text-left">Akun</label>
                            <div class="col-md-10">
                                <input type="email" value="<?= authUser()->email ?>" name="akun" id="akun" class="form-control" disabled>
                            </div>
                        </div>
                        <div class="form-group row border-bottom-0">
                            <label for="email_pribadi" class="col-md-2 label-control text-left">Email Pribadi</label>
                            <div class="col-md-10">
                                <input type="email" value="<?= personal()->personal_email ?>" name="email_pribadi" id="email_pribadi" class="form-control">
                            </div>
                        </div>

                        <div class="form-group row border-bottom-0">
                            <label for="id_role" class="col-md-2 label-control text-left"></label>
                            <div class="col-md-10">
                                <button type="submit" class="btn btn-primary pull-right ml-2">Simpan</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
$('#photo').change(function(event) {
    if (event.target.files && event.target.files[0]) {
        const reader = new FileReader();
        reader.onload = e => {
            $('#avatar_thumb').attr('src', e.target.result);
        };

        reader.readAsDataURL(event.target.files[0]);
    }
});
</script>