<div class="row">
    <div class="col-12">
        <div class="card p-1">
            <ul class="nav nav-pills nav-active-bordered-pill nav-justified">
                <li class="nav-item">
                    <a href="<?= site_url('account/berkas') ?>" class="nav-link active">Data Berkas</a>
                </li>
                <?php if(has_permission('ADD_BERKAS')): ?>
                <li class="nav-item">
                    <a href="<?= site_url('account/add_berkas') ?>" class="nav-link ">Form</a>
                </li>
                <?php endif ?>
                <?php if(has_permission('RIWAYAT_BERKAS')): ?>
                <li class="nav-item">
                    <a href="<?= site_url('account/riwayat_berkas') ?>" class="nav-link">Riwayat</a>
                </li>
                <?php endif ?>
            </ul>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-12">
        <div class="card p-1">
            <div class="row">
                <div class="col-sm-12 ">
                    <table id="tbl_detail_training" class="table table-striped data_list">
                        <thead>
                            <tr>
                                <th>Nama Berkas</th>
                                <th>Jenis Berkas</th>
                                <th>Tanggal Upload</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach(personal()->personalFiles()->orderBy('id', 'desc')->get() as $file): ?>
                                <tr>
                                    <td><?= $file->file_name ?></td>
                                    <td><?= $file->file_type ?></td>
                                    <td><?= date('d F Y', strtotime($file->created_at)) ?></td>
                                    <td>
                                        <a href="<?=site_url('account/delete_berkas/'. $file->id )?>" class="btn btn-icon btn-pure dark" title="Menghapus data" onclick="return confirm('Yakin Menghapus?');">
                                            <i class="la la-trash"></i>
                                        </a>
                                        <a href="<?= base_url($file->file) ?>" target="_blank" class="btn btn-icon btn-pure dark" title="Detail Data">
                                            <i class="la la-file"></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>