<div class="row">
    <div class="col-12">
        <div class="card p-1">
            <ul class="nav nav-pills nav-active-bordered-pill nav-justified">
                <li class="nav-item">
                    <a href="<?= site_url('account') ?>" class="nav-link">Akun</a>
                </li>
                <li class="nav-item">
                    <a href="<?= site_url('account/changepassword') ?>" class="nav-link active">Change Password</a>
                </li>
            </ul>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card p-1">
            <div class="card-body">
                <form method="POST" class="form form-horizontal row-separator">
                    <div class="form-body">
                        <div class="form-group row border-bottom-0">
                            <div class="col-md-6 col-sm-12">
                            <label for="current_password" class="col-12 label-control text-left">Current Password</label>
                                <input type="password" name="current_password" id="current_password" class="form-control">
                            </div>
                        </div>
                        <div class="form-group border-bottom-0">
                            <div class="row">
                                <div class="col-md-6 col-sm-12">
                                    <label for="new_password" class="col-12 label-control text-left">New Password</label>
                                    <input type="password" name="new_password" id="new_password" class="form-control">
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <label for="confirm_new_password" class="col-12 label-control text-left">New Password</label>
                                    <input type="password" name="confirm_new_password" id="confirm_new_password" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row border-bottom-0">
                            <?= csrf_field(); ?>
                            <label for="id_role" class="col-md-2 label-control text-left"></label>
                            <div class="col-md-10">
                                <button type="submit" class="btn btn-primary pull-right ml-2">Simpan</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>