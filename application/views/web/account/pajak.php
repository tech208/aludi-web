<div class="row">
    <div class="col-12">
        <div class="card p-1">
            <div class="card-body">
                
                <div class="row">
                    <div class="col-12">
                        <table class="table">
                            <tr>
                                <td>NPWP</td>
                                <td><?= personal()->npwp_number ?></td>
                            </tr>
                            <tr>
                                <td>NIK</td>
                                <td><?= personal()->idcard_number ?></td>
                            </tr>
                            <tr>
                                <td>Alamat</td>
                                <td><?= personal()->address ?></td>
                            </tr>
                            <tr>
                                <td>Status/ Jumlah tanggungan keluarga untuk PTKP</td>
                                <td>-</td>
                            </tr>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>