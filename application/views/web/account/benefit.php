<div class="row">
    <div class="col-12">
        <div class="card p-1">
            <div class="card-body">
                
                <div class="row">
                    <div class="col-12">
                        <table class="table">
                            <tr>
                                <td>
                                    Benefit
                                </td>
                                <td>
                                    <?php foreach($benefits as $b): ?>
                                        - <?= $b ?> <br>
                                    <?php endforeach ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>