<div class="row">
    <div class="col-12">
        <div class="card p-1">
            <ul class="nav nav-pills nav-active-bordered-pill nav-justified">
                <li class="nav-item">
                    <a href="<?= site_url('hr_pengajuan/') ?>" class="nav-link active">Riwayat</a>
                </li>
                <?php if(has_permission('CREATE_PENGAJUAN_PJ')): ?>
                <li class="nav-item">
                    <a href="<?= site_url('hr_pengajuan/form') ?>" class="nav-link">Ajukan</a>
                </li>
                <?php endif ?>
                <?php if(has_permission('APPROVEMENT_PENGAJUAN_PJ')): ?>
                <li class="nav-item">
                    <a href="<?= site_url('hr_pengajuan/approval') ?>" class="nav-link">Approval</a>
                </li>
                <?php endif ?>
            </ul>
        </div>
    </div>
</div>

<?php if(!has_permission('READ_PENGAJUAN_PJ')) return; ?>
<div class="row">
    <div class="col-12">
        <div class="card p-1">
            <div class="row">
                <div class="col-sm-12">
                    <table id="tbl_inventaris" class="table table-striped data_list">
                        <thead>
                            <tr>
                                <th>Tanggal Pengajuan</th>
                                <th>Tujuan</th>
                                <th>Ditujukan</th>
                                <th>Jumlah</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no=1; foreach($data as $val): ?>
                                <tr>
                                    <td><?= date('d F Y', strtotime($val->tanggal)) ?></td>
                                    <td><?= $val->name ?></td>
                                    <td><?= $val->ditujukans->name ?></td>
                                    <td>Rp <?= number_format($val->total) ?></td>
                                    <td>
                                        <button class="btn <?=($val->status==21 OR $val->status==22 OR $val->status==25)?"btn-success":"btn-danger";?> btn-sm"><?= $val->statuse->name ?></button>
                                    </td>
                                    <td>
                                        <?php if($val->status=="18" OR $val->status=="19" ): ?>
                                        <a href="<?=site_url('hr_pengajuan/delete/'. $val->id )?>" class="btn btn-icon btn-pure dark" title="Menghapus data" onclick="return confirm('Yakin Menghapus?');">
                                            <i class="la la-trash"></i>
                                        </a>
                                        <?php endif ?>
                                        <a href="<?=site_url('hr_pengajuan/detail/'. $val->id )?>" class="btn btn-icon btn-pure dark" title="Detail Data">
                                            <i class="la la-file"></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php $no++; endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>