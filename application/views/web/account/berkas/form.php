<div class="row">
    <div class="col-12">
        <div class="card p-1">
            <ul class="nav nav-pills nav-active-bordered-pill nav-justified">
                <li class="nav-item">
                    <a href="<?= site_url('account/berkas') ?>" class="nav-link ">Data Berkas</a>
                </li>
                <?php if(has_permission('ADD_BERKAS')): ?>
                <li class="nav-item">
                    <a href="<?= site_url('account/add_berkas') ?>" class="nav-link active">Form</a>
                </li>
                <?php endif ?>
                <?php if(has_permission('RIWAYAT_BERKAS')): ?>
                <li class="nav-item">
                    <a href="<?= site_url('account/riwayat_berkas') ?>" class="nav-link">Riwayat</a>
                </li>
                <?php endif ?>
            </ul>
        </div>
    </div>
</div>

<?php if(!has_permission('ADD_BERKAS')) return; ?>
<div class="row">
    <div class="col-12">
        <div class="card p-1">
            <div class="card-body">
                <form enctype="multipart/form-data" method="POST" class="form form-horizontal row-separator">
                    <div class="form-body">
                        <h3 class="row-section">Tambah Berkas</h3>
                        <div class="form-group row border-bottom-0">
                            <label for="nama_berkas" class="col-md-2 label-control text-left">Nama Berkas</label>
                            <div class="col-md-10">
                                <input type="text" name="nama_berkas" id="nama_berkas" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group row border-bottom-0">
                            <label for="tipe" class="col-md-2 label-control text-left">Tipe Berkas</label>
                            <div class="col-md-10">
                                <select name="tipe" id="tipe" class="form-control square" required>
                                    <?php foreach(\App\Constants\PersonalData::DOCUMENT_LIST as $type): ?>
                                        <option value="<?= $type ?>"><?= $type ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row border-bottom-0">
                            <label for="name" class="col-md-2 label-control text-left">File</label>
                            <div class="col-md-10">
                                <input type="file" accept=".jpg,.pdf,.png"  name="file" id="file" required>
                            </div>
                        </div>
                        <div class="form-group row border-bottom-0">
                            <label for="id_role" class="col-md-2 label-control text-left"></label>
                            <div class="col-md-10">
                                <button type="submit" class="btn btn-primary pull-right">SIMPAN</button>
                                <a href="<?= site_url('account/berkas') ?>" class="btn btn-default pull-right mr-1">BATAL</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>