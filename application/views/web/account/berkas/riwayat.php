<?php if(!has_permission('MANAGER_CLAIM_APPROVEMENT')) return ?>
<div class="row">
    <div class="col-12">
        <div class="card p-1">
            <ul class="nav nav-pills nav-active-bordered-pill nav-justified">
            <li class="nav-item">
                    <a href="<?= site_url('account/berkas') ?>" class="nav-link ">Data Berkas</a>
                </li>
                <?php if(has_permission('ADD_BERKAS')): ?>
                <li class="nav-item">
                    <a href="<?= site_url('account/add_berkas') ?>" class="nav-link active">Form</a>
                </li>
                <?php endif ?>
                <?php if(has_permission('RIWAYAT_BERKAS')): ?>
                <li class="nav-item">
                    <a href="<?= site_url('account/riwayat_berkas') ?>" class="nav-link">Riwayat</a>
                </li>
                <?php endif ?>
            </ul>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card p-1">
            <div class="row">
                <div class="col-sm-12 ">
                    <table id="tbl_detail_training" class="table table-striped data_list">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>No Karyawan</th>
                                <th>Nama Karyawan</th>
                                <th>File</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no=1; foreach($data as $value): ?>
                                <tr class="<?= ($value->personalFiles()->count()==0)?'text-danger':'' ?>" >
                                    <td><?= @$no ?></td>
                                    <td><?= @$value->user->uuid ?></td>
                                    <td><?= $value->name ?></td>
                                    <td>
                                        <ul>
                                            <?php foreach($value->personalFiles()->orderBy('id', 'desc')->get() as $value_files): ?>
                                                <li>
                                                    <a href="<?= base_url($value_files->file) ?>" target="_blank" class="btn btn-icon btn-pure dark" title="Detail Data">
                                                        <?=$value_files->file_name?> (<?=$value_files->file_type?>)
                                                    </a>
                                                </li> 
                                            <?php endforeach ?>
                                        </ul>
                                    </td>
                                </tr>
                            <?php $no++; endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
