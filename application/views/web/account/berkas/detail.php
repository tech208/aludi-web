<link rel="stylesheet" type="text/css" href="<?= base_url('app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css') ?>">
<div class="row">
    <div class="col-12">
        <div class="card p-1">
            <ul class="nav nav-pills nav-active-bordered-pill nav-justified">
                <li class="nav-item">
                    <a href="<?= site_url('hr_pengajuan/') ?>" class="nav-link ">Riwayat</a>
                </li>
                <?php if(has_permission('CREATE_PENGAJUAN_PJ')): ?>
                <li class="nav-item">
                    <a href="<?= site_url('hr_pengajuan/form') ?>" class="nav-link">Ajukan</a>
                </li>
                <?php endif ?>
                <?php if(has_permission('APPROVEMENT_PENGAJUAN_PJ')): ?>
                <li class="nav-item">
                    <a href="<?= site_url('hr_pengajuan/approval') ?>" class="nav-link active">Approval</a>
                </li>
                <?php endif ?>
            </ul>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card p-1">
            <div class="row">
                <div class="col-12">
                    <h3 style="padding-top: 3px;" class="m-0">Pengajuan Claim</h3>
                </div>
            </div>

            <div class="row mt-2">
                <div class="col-12">
                    <table class="table">
                        <tr>
                            <td>Tanggal Pengajuan</td>
                            <td>:</td>
                            <td><?= tgl_indo($data->tanggal) ?></td>
                        </tr>
                        <tr>
                            <td>Tanggal Berangkat</td>
                            <td>:</td>
                            <td><?= tgl_indo($data->tanggal_berangkat) ?></td>
                        </tr>
                        <tr>
                            <td>Tanggal Kembali</td>
                            <td>:</td>
                            <td><?= tgl_indo($data->tanggal_kembali) ?></td>
                        </tr>
                        <tr>
                            <td>Status</td>
                            <td>:</td>
                            <td>
                                <button class="btn <?=($data->status==21 OR $data->status==22 OR $data->status==25)?"btn-success":"btn-danger";?> btn-sm">
                                    <?= $data->statuse->name ?>
                                </button>
                            </td>
                        </tr>
                        <tr>
                            <td>Nama Leader</td>
                            <td>:</td>
                            <td><?= $data->personals->name ?></td>
                        </tr>
                        <tr>
                            <td>Ditujukan Karyawan</td>
                            <td>:</td>
                            <td><?= $data->ditujukans->name ?></td>
                        </tr>
                        <tr>
                            <td>Jabatan Karyawan</td>
                            <td>:</td>
                            <td><?= $data->ditujukans->jobHistory->jobTitle->job_titles ?></td>
                        </tr>
                        <tr>
                            <td>Lokasi Tujuan</td>
                            <td>:</td>
                            <td><?= $data->name ?></td>
                        </tr>
                        <tr>
                            <td>Lokasi Keberangkatan</td>
                            <td>:</td>
                            <td><?= $data->keberangkatan ?></td>
                        </tr>
                        <tr>
                            <td>Jenis Pengajuan</td>
                            <td>:</td>
                            <td><?= $data->types->name ?></td>
                        </tr>
                        <tr>
                            <td>Keterangan</td>
                            <td>:</td>
                            <td><?= $data->note ?></td>
                        </tr>
                        <tr>
                            <td>Lampiran</td>
                            <td>:</td>
                            <td>
                                <a href="<?= base_url("/uploads/pengajuan/".$data->lampiran."") ?>" download="<?= date('d_F_Y')."-".$data->lampiran; ?>" target="_blank">
                                    <?=$data->lampiran?>
                                </a>
                            </td>
                        </tr>

                        
                        <tr>
                            <td>Jumlah Pengajuan</td>
                            <td>:</td>
                            <td>Rp <?= MasUang($data->total) ?></td>
                        </tr>
                        <?php if($data->details->count() > 0): ?>
                        <tr>
                            <td>Settlement Biaya </td>
                            <td>:</td>
                            <td>
                                <table class="table table-bordered" id="table_settlement">
                                    <thead>
                                        <tr>
                                            <th>Biaya</th>
                                            <th>Nominal</th>
                                        </tr>
                                    </thead>
                                    <tbody id="data_settlement">
                                        
                                        <?php 
                                            $total_realisasi    = 0 ;
                                            $no=1; foreach($data->details as $value): 

                                            $total_realisasi += (int) $value->nominal_realisasi;
                                        ?>
                                        <tr>
                                            <td><?=$value->typeClaimDetails->name?></td>
                                            <td align="right">
                                                Rp. <?= MasUang($value->nominal_realisasi) ?>
                                            </td>
                                        </tr>
                                        <?php $no++; endforeach ?>
                                        <tr align="right">
                                            <th>Total</th>
                                            <th>
                                                Rp. <?= MasUang($total_realisasi) ?>
                                            </th>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <?php endif ?>

                    </table>
                </div>
            </div>
            
            <div class="form-group row border-bottom-0 <?=($data->status!="24")?'hidden':'';?>">
                <label for="keterangan" class="col-md-2 label-control text-left">Alasan Di Tolak</label>
                <div class="col-md-10">
                    <textarea class="form-control" rows="3" disabled><?=$data->reason?></textarea>
                </div>
            </div>

            <?php if($data->status=="18" AND $data->id_approve==personal()->id ): ?> <!-- Diproses GA,Diterima Manager dan role human resource-->
            <div class="row mt-2">
                <div class="col-12">
                    <a href="<?= site_url('hr_pengajuan/save_status/'. $data->id.'/'.$data->status.'/ga_terima') ?>" class="btn btn-success pull-right ml-2" onclick="return confirm('Yakin Mengkonfirmasi?');">Di Setuju</a>
                    <a href="<?= site_url('hr_pengajuan/form_notice/'. $data->id)?>" class="btn btn-danger pull-right" >Di Tolak</a>
                </div>
            </div>
            <?php endif ?>
            <?php if($data->status=="22" AND personal()->id==71 ): ?> <!-- menunggku konfirmasi pembayaran-->
            <div class="row mt-2">
                <div class="col-12">
                    <a href="<?=site_url('hr_pengajuan/save_konfirmasi/'. $data->id )?>" class="btn btn-success pull-right ml-2" title="Konfirmasi Pembayaran" onclick="return confirm('Yakin Mengkonfirmasi?');">Konfirmasi Pembayaran</a>
                </div>
            </div>
            <?php endif ?>
        </div>
    </div>
</div>