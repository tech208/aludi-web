<?php

    require_once(APPPATH.'constants/UserManagement.php');
    $personal = personal();
?>
<link rel="stylesheet" type="text/css" href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css">
<div class="row">
    <div class="col-12">
        <div class="card p-1">
            <div class="card-body">
                <form method="POST" class="form form-horizontal row-separator">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class=" form-group">
                                    <label>No Karyawan</label>
                                    <input type="text" value="<?= authUser()->uuid ?>" name="no_karyawan" id="no_karyawan" class="form-control" disabled>
                                </div><br>
                                <div class=" form-group">
                                    <label>Nama Lengkap</label>
                                    <input type="text" value="<?= $personal->name ?>" name="nama_lengkap" id="nama_lengkap" class="form-control">
                                </div><br>
                                <div class=" form-group">
                                    <label>NPWP</label>
                                    <input type="text" value="<?= $personal->npwp_number ?>" name="npwp" id="npwp" class="form-control">
                                </div><br>
                                <div class=" form-group">
                                    <label>Tempat Lahir</label>
                                    <input type="text" value="<?= $personal->place_of_birth ?>" name="tempat_lahir" id="tempat_lahir" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row form-group">
                                    <label>Status</label>
                                    <select name="status" id="status" class="form-control" disabled>
                                        <?php foreach(\App\Constants\UserManagement::STATUS_LIST as $i => $l): ?>
                                            <option <?= $personal->status == $i ? 'selected':'' ?> value="<?= $i ?>"><?= $l ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div><br>
                                <div class="row form-group">
                                    <label>NIK</label>
                                    <input type="text" value="<?= $personal->idcard_number ?>" name="nik" id="nik" class="form-control">
                                </div><br>
                                <div class="row form-group">
                                    <label>Jenis Kelamin</label>
                                    <select name="jenis_kelamin" id="jenis_kelamin" class="form-control square">
                                        <option value="" selected disabled>Pilih Jenis Kelamin</option>
                                        <option value="Male" <?= $personal->gender == 'Male' ? 'selected':'' ?>>Laki-laki</option>
                                        <option value="Female" <?= $personal->gender == 'Female' ? 'selected':'' ?>>Perempuan</option>
                                    </select>
                                </div><br>
                                <div class="row form-group">
                                    <label>Tanggal Lahir</label>
                                    <input type="text" value="<?= $personal->date_of_birth ?>" name="tanggal_lahir" id="tanggal_lahir" class="form-control datepicker">
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group row">
                                    <label for="alamat" class="col-12 label-control text-left">Alamat</label>
                                    <textarea name="alamat" id="alamat" rows="4" class="form-control"><?= $personal->address ?></textarea>
                                </div><br>
                            </div>
                            <div class="col-md-6">
                                <div class="row form-group">
                                    <label>No HP</label>
                                    <input type="tel" value="<?= $personal->phone ?>" name="no_hp" id="no_hp" class="form-control">
                                </div><br>
                                <div class="row form-group">
                                    <label>Bank Account Number</label>
                                    <input type="text" value="<?= $personal->bank ? $personal->bank->account : '' ?>" name="bank_account" id="bank_account" class="form-control" required>
                                </div><br>
                            </div>
                            <div class="col-md-6">
                                <div class="row form-group">
                                    <label>Bank Name</label>
                                    <input type="text" value="<?= $personal->bank ? $personal->bank->bank : '' ?>" name="bank_name" id="bank_name" class="form-control" required>
                                </div>
                            </div><br>
                        </div>
                        <br>
                        <div class="form-group ">
                            <?php if(has_permission('UPDATE_PROFILE')): ?>
                                <button type="submit" class="btn btn-primary pull-right ml-2">Simpan</button>
                            <?php endif ?>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<style>
    .input-group-append {
        display: none;
    }
    .input-group {
        margin-top: unset;
    }
</style>

