<div class="row">
    <div class="col-12">
        <div class="card p-1">
            <div class="card-body">
                
                <div class="row">
                    <div class="col-12">
                        <table class="table">
                            <?php foreach($allowances as $a): ?>
                                <tr>
                                    <td><?= $a->allowance ?></td>
                                    <td>Rp <?= number_format($a->total) ?></td>
                                </tr>
                            <?php endforeach ?>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>