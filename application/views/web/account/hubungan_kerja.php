<?php $personal = personal(); ?>
<div class="row">
    <div class="col-12">
        <div class="card p-1">
            <div class="card-body">
                
                <div class="row">
                    <div class="col-12">
                        <table class="table">
                            <tr>
                                <td>Status Karyawan</td>
                                <td><?= $data->job_status ?></td>
                            </tr>
                            <tr>
                                <td>Tanggal Bergabung</td>
                                <td><?= $personal->join_date ? date('d F Y', strtotime($personal->join_date)) : ' ~ ' ?></td>
                            </tr>
                            <tr>
                                <td>Jabatan</td>
                                <td><?=  $data->jobTitle->job_titles ?></td>
                            </tr>
                            <tr>
                                <td>Divisi</td>
                                <td><?=  $data->jobTitle->division ?  $data->jobTitle->division->division : '-' ?></td>
                            </tr>
                            <tr>
                                <td>Departemen</td>
                                <td><?=  $data->jobTitle->department ?  $data->jobTitle->department->departement : '-' ?></td>
                            </tr>
                        </table>
                        <h2>Histori Pekerjaan</h2>
                        <table id="tbl_karyawan" class="table table-striped data_list">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tanggal Efektif</th>
                                    <th>Tanggal Mulai</th>
                                    <th>Tanggal Selesai</th>
                                    <th>Jenis Karyawan</th>
                                    <!-- <th>Periode</th> -->
                                    <th>Posisi</th>
                                    <!-- <th>Hak Akses</th> -->
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no=1; ?>
                                <?php foreach($data_histori as $val): ?>
                                <tr>
                                    <td><?= $no ?></td>
                                    <td><?= ($val->effective_date)?tgl_indo($val->effective_date):'<i>(Belum Disetting)</i>'; ?></td>
                                    <td><?= ($val->start_date)?tgl_indo($val->start_date):'<i>(Belum Disetting)</i>'; ?></td>
                                    <td>
                                        <?php if($val->saat_ini==0){
                                                if(!empty($val->end_date)){
                                                    echo tgl_indo($val->end_date);
                                                }
                                                else{ 
                                                    echo'<i>(Belum Disetting)</i>'; 
                                                }
                                            }
                                            else{
                                                echo '<i>(Sampai Saat Ini)</i>';
                                            }
                                         ?>
                                    </td>
                                    <td><?= $val->job_status ?></td>
                                    <!-- <td><?//= $val->employment_period ? \App\Constants\UserManagement::PERIOD_LIST[$val->employment_period] : '-' ?></td> -->
                                    <td><?= $val->jobTitle->job_titles ?></td>
                                    <!-- <td>
                                        <?php //foreach($val->akses as $ac): ?>
                                            <li><?//= $ac->access ?></li>
                                        <?php //endforeach ?>
                                    </td> -->
                                </tr>
                                <?php $no++; endforeach ?>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row " style="text-align:left;">
                    <?php if(!empty($atasan)): ?>
                    <!-- atasan -->
                    <div class="col-md-12 " >
                        <div class="container">
                            <div class="row justify-content-md-center" >
                                <div class="card border-black border-lighten-2">
                                    <div class="text-center">
                                        <div class="card-body">
                                            <!-- avatar -->
                                            <?php
                                            if(empty($atasan->gender)): ?>
                                                <?php if($atasan->gender=="Male"): ?>
                                                <img src="<?=base_url()?>app-assets/images/portrait/small/avatar-s-1.png" style="width: 100px !important;height: 100px !important; " class="rounded-circle  " alt="Card image">
                                                <?php elseif($atasan->gender=="Female"): ?>
                                                <img src="<?=base_url()?>app-assets/images/portrait/small/avatar-s-1-f.png" style="width: 100px !important;height: 100px !important; " class="rounded-circle  " alt="Card image">
                                                <?php endif ?>
                                            <?php else: ?>
                                                <?php if(file_exists($atasan->photo)): ?>
                                                    <img src="<?=base_url()?><?=$atasan->photo?>" class="rounded-circle " style="width: 100px !important;height: 100px !important; " alt="Card image">
                                                <?php else: ?>
                                                    <?php if($atasan->gender=="Male"): ?>
                                                    <img src="<?=base_url()?>app-assets/images/portrait/small/avatar-s-1.png" style="width: 100px !important;height: 100px !important; " class="rounded-circle  " alt="Card image">
                                                    <?php elseif($atasan->gender=="Female"): ?>
                                                    <img src="<?=base_url()?>app-assets/images/portrait/small/avatar-s-1-f.png" style="width: 100px !important;height: 100px !important; " class="rounded-circle  " alt="Card image">
                                                    <?php endif ?>
                                                <?php endif ?>
                                            <?php endif ?>
                                            <!-- avatar -->
                                        </div>
                                        <div class="card-body">
                                            <h4 class="card-title"><?=$atasan->name?></h4>
                                            <h6 class="card-subtitle text-muted"><?=$atasan->jobHistory->jobTitle->job_titles?></h6>
                                        </div>
                                        <i class="la la-angle-double-down " style="font-size: 50px;"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end atasan -->
                    <?php endif ?>

                    <!-- data diri -->
                    <div class="col-md-12 " style="padding: 5px;">
                        <div class="container">
                            <div class="row justify-content-md-center" >

                                <div class="col-md-6">
                                    <!-- biodata nya -->
                                    <div class="card border-black border-lighten-2" style="margin: 5px;">
                                        <div class="text-center">
                                            <div class="card-body">
                                                <!-- avatar -->
                                                <?php
                                                if(empty($data->personal->gender)): ?>
                                                    <?php if($data->personal->gender=="Male"): ?>
                                                    <img src="<?=base_url()?>app-assets/images/portrait/small/avatar-s-1.png" style="width: 100px !important;height: 100px !important; " class="rounded-circle  " alt="Card image">
                                                    <?php elseif($data->personal->gender=="Female"): ?>
                                                    <img src="<?=base_url()?>app-assets/images/portrait/small/avatar-s-1-f.png" style="width: 100px !important;height: 100px !important; " class="rounded-circle  " alt="Card image">
                                                    <?php endif ?>
                                                <?php else: ?>
                                                    <?php if(file_exists($data->personal->photo)): ?>
                                                        <img src="<?=base_url()?><?=$data->personal->photo?>" style="width: 100px !important;height: 100px !important; " class="rounded-circle  " alt="Card image">
                                                    <?php else: ?>
                                                        <?php if($data->personal->gender=="Male"): ?>
                                                        <img src="<?=base_url()?>app-assets/images/portrait/small/avatar-s-1.png" style="width: 100px !important;height: 100px !important; " class="rounded-circle  " alt="Card image">
                                                        <?php elseif($data->personal->gender=="Female"): ?>
                                                        <img src="<?=base_url()?>app-assets/images/portrait/small/avatar-s-1-f.png" style="width: 100px !important;height: 100px !important; " class="rounded-circle  " alt="Card image">
                                                        <?php endif ?>
                                                    <?php endif ?>
                                                <?php endif ?>
                                                <!-- avatar -->
                                            </div>
                                            <div class="card-body">
                                                <h4 class="card-title"><?= $personal->name?></h4>
                                                <h6 class="card-subtitle text-muted"><?=  $data->jobTitle->job_titles ?></h6>
                                            </div>
                                            <?php if(!empty($turunan)): ?>
                                            <i class="la la-angle-double-down " style="font-size: 50px;"></i>
                                            <?php endif ?>
                                        </div>
                                    </div>
                                    <!--end  biodata nya -->
                                </div>

                                <?php if(!empty($team)): ?>
                                <div class="col-md-6">
                                    <!-- team -->
                                    <div class="card border-black border-lighten-2" >
                                        <div class="text-center">
                                            <div class="card-body">
                                                <?php foreach($team as $key_team => $val_team): ?>
                                                    <div class="card border-black border-lighten-2" >
                                                        <div class="row ">
                                                            <div class="col-md-4">
                                                                <div class="card-body">
                                                                    <!-- avatar -->
                                                                    <?php if(empty($val_team->photo)): ?>
                                                                        <?php if($val_team->gender=="Male"): ?>
                                                                        <img src="<?=base_url()?>app-assets/images/portrait/small/avatar-s-1.png" style="width: 100px !important;height: 100px !important; " class="rounded-circle " alt="Card image">
                                                                        <?php elseif($val_team->gender=="Female"): ?>
                                                                        <img src="<?=base_url()?>app-assets/images/portrait/small/avatar-s-1-f.png" style="width: 100px !important;height: 100px !important; " class="rounded-circle " alt="Card image">
                                                                        <?php endif ?>
                                                                    <?php else: ?>
                                                                        <?php if(file_exists($val_team->photo)): ?>
                                                                            <img src="<?=base_url()?><?=$val_team->photo?>" style="width: 100px !important;height: 100px !important; " class="rounded-circle  " alt="Card image">
                                                                        <?php else: ?>
                                                                            <?php if($val_team->gender=="Male"): ?>
                                                                            <img src="<?=base_url()?>app-assets/images/portrait/small/avatar-s-1.png" style="width: 100px !important;height: 100px !important; " class="rounded-circle  " alt="Card image">
                                                                            <?php elseif($val_team->gender=="Female"): ?>
                                                                            <img src="<?=base_url()?>app-assets/images/portrait/small/avatar-s-1-f.png" style="width: 100px !important;height: 100px !important; " class="rounded-circle  " alt="Card image">
                                                                            <?php endif ?>
                                                                        <?php endif ?>
                                                                    <?php endif ?>
                                                                    <!-- avatar -->
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="card-body ">    
                                                                    <h4><?=$val_team->name?></h4>
                                                                    <p><?=$val_team->jobHistory->jobTitle->job_titles?></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php endforeach ?>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end team -->   
                                </div>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                    <!-- end data diri -->

                    <?php if($turunan->count()>0): ?>
                    <!-- turunan -->
                    <div class="col-md-12 border border-info" >
                        <div class="container text-center">
                            <div class="row justify-content-md-center" >
                                <h3>Anggota Team</h3>
                            </div>
                        </div>
                        <div class="container row">

                                <!-- data turunan -->
                                <?php foreach($turunan as $key_turunan => $val_turunan): ?>
                                <div class="card border-black col-md-2 " style="margin-left: 2px; margin-right: 2px;">
                                    <div class="text-center">
                                        <div class="card-body">
                                            <!-- avatar -->
                                            <?php
                                            if(empty($val_turunan->gender)): ?>
                                                <?php if($val_turunan->gender=="Male"): ?>
                                                <img src="<?=base_url()?>app-assets/images/portrait/small/avatar-s-1.png" class="rounded-circle  height-100" alt="Card image">
                                                <?php elseif($val_turunan->gender=="Female"): ?>
                                                <img src="<?=base_url()?>app-assets/images/portrait/small/avatar-s-1-f.png" class="rounded-circle  height-100" alt="Card image">
                                                <?php endif ?>
                                            <?php else: ?>
                                                <?php if(file_exists($val_turunan->photo)): ?>
                                                    <img src="<?=base_url()?><?=$val_turunan->photo?>" class="rounded-circle  " width="100px" alt="Card image">
                                                <?php else: ?>
                                                    <?php if($val_turunan->gender=="Male"): ?>
                                                    <img src="<?=base_url()?>app-assets/images/portrait/small/avatar-s-1.png" class="rounded-circle  height-100" alt="Card image">
                                                    <?php elseif($val_turunan->gender=="Female"): ?>
                                                    <img src="<?=base_url()?>app-assets/images/portrait/small/avatar-s-1-f.png" class="rounded-circle  height-100" alt="Card image">
                                                    <?php endif ?>
                                                <?php endif ?>
                                            <?php endif ?>
                                            <!-- avatar -->

                                            <h4 class="card-title"><?=$val_turunan->name?></h4>
                                            <h6 class="card-subtitle text-muted"><?=$val_turunan->jobHistory->jobTitle->job_titles?></h6>
                                        </div>
                                    </div>
                                </div>
                                <?php endforeach ?>
                                <!-- end data turunan -->

                        </div>
                    </div>
                    <!--end turunan -->
                    <?php endif ?>


                </div>
            </div>
        </div>
    </div>
</div>