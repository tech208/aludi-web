<!-- BEGIN: Main Menu-->
<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
        <div class="main-menu-content">
            <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <?php foreach($side_menus as $menu): ?>
                <?php if(hasMenuAccess($menu->id)): ?>
                    <?php if($menu->child()->count() > 0): ?>
                        <li class=" nav-item">
                            <a href="#" title="<?= $menu->title ?>">
                                <i class="<?= (!empty($menu->icon)) ?  $menu->icon : "la ft-smartphone" ; ?>"></i>
                                <span class="menu-title" data-i18n="<?= $menu->title ?>"><?= $menu->title ?></span>
                            </a>
                            <ul class="menu-content">
                            <?php 
                            $karyawan = MasKaryawan(personal()->id);
                            foreach($menu->child as $child): 
                            ?>
                            <?php
                                $has_access = $child->access_permission ? has_permission($child->access_permission) : true;
                                if($has_access):

                            ?>
                            <li>
                                <a class="menu-item" href="<?= str_replace('%20', '', site_url($child->route)) ?>" title="<?= $menu->title ?>">
                                    <i class="<?= (!empty($child->icon)) ?  $child->icon : "la ft-smartphone" ; ?>"></i>
                                    <span  data-i18n="<?= $child->title ?>"><?= $child->title ?></span>
                                </a>
                            </li>

                            <?php
                                    //endif;
                                endif;
                            ?>
                            <?php endforeach ?>
                            </ul>
                        </li>
                    <?php else: ?>
                        <li class=" nav-item">
                            <a href="<?= site_url($menu->route) ?>" title="<?= $menu->title ?>">
                                <i class="<?= (!empty($menu->icon)) ?  $menu->icon : "la ft-smartphone" ; ?>"></i>
                                <span class="menu-title" data-i18n="<?= $menu->title ?>"><?= $menu->title ?></span>
                            </a>
                        </li>

                    <?php endif ?>
                <?php endif ?>
            <?php endforeach ?>
            </ul>
        </div>
    </div>
    <!-- END: Main Menu-->