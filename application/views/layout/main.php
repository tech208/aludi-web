<?php 
ini_set('date.timezone', 'Asia/Jakarta');
?>
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
	<title>ALUDI - Asosiasi Layanan Urun Dana Indonesia</title>
    <link rel="apple-touch-icon" href="<?= base_url() ?>app-assets/images/aludi/logo.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?= base_url() ?>app-assets/images/aludi/logo.png">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i%7CQuicksand:300,400,500,700" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/vendors/css/ui/prism.min.css">
    <link href="<?= base_url() ?>app-assets/summernote/dist/summernote.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/vendors/css/forms/selects/select2.min.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/css/components.css">
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/css/core/menu/menu-types/vertical-compact-menu.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/css/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/fonts/mobiriseicons/24px/mobirise/style.css">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/style.css">
    <!-- END: Custom CSS-->

    <style>
        select.form-control {
            background-image: none !important;
        }
    </style>

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-compact-menu 2-columns   fixed-navbar" data-open="click" data-menu="vertical-compact-menu" data-col="2-columns">

    <!-- BEGIN: Header-->
    <nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow fixed-top navbar-light navbar-shadow navbar-brand-center">
        <div class="navbar-wrapper">
            <div class="navbar-header">
                <ul class="nav navbar-nav flex-row">
                    <li class="nav-item mobile-menu d-md-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu font-large-1"></i></a></li>
                    <li class="nav-item">
                        <a class="navbar-brand" href="<?=site_url('/dashboard')?>">
                            <img class="brand-logo" alt="Aludi" style="width: 100px; " src="<?= base_url() ?>app-assets/images/logo/logo_aludi.png">
                            <!-- <h3 class="brand-text">Aludi</h3> -->
                        </a>
                    </li>
                    <li class="nav-item d-md-none"><a class="nav-link open-navbar-container" data-toggle="collapse" data-target="#navbar-mobile"><i class="la la-ellipsis-v"></i></a></li>
                </ul>
            </div>
            <div class="navbar-container content">
                <div class="collapse navbar-collapse" id="navbar-mobile">
                    <ul class="nav navbar-nav mr-auto float-left">
                        <li class="nav-item d-none d-md-block"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu"></i></a></li>
                        <li class="nav-item d-none d-lg-block"><a class="nav-link nav-link-expand" href="#"><i class="ficon ft-maximize"></i></a></li>
                    </ul>
                    <ul class="nav navbar-nav float-right">
                        <li class="dropdown dropdown-user nav-item">
                            <a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
                                <span class="mr-1 user-name text-bold-700">
                                    <?= authUser()->personal ? authUser()->personal->name : authUser()->email ?>
                                </span>
                                <span class="avatar avatar-online">
                                    <img src="<?= avatar_path() ?>" alt="avatar" style="width: 36px !important;height: 36px !important; "><i></i>
                                </span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="<?= site_url('account') ?>"><i class="ft-user"></i> Account</a>
                                <a class="dropdown-item" href="<?= site_url('dokumentasiApi') ?>"><i class="ft-server"></i> Rest API</a>
                                
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="<?= site_url('auth/logout') ?>"><i class="ft-power"></i> Logout</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <!-- END: Header-->


    <!-- BEGIN: Main Menu-->
    <?php $this->load->view('layout/sidebar'); ?>

    <!-- END: Main Menu-->
    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    <h3 class="content-header-title "><?= isset($page_title) ? $page_title : ' Aludi' ?></h3>
                    <div class="row breadcrumbs-top">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <?php
                                    if(isset($breadcrumb)){
                                        foreach ($breadcrumb as $key => $value) {
                                            if($value['aktif']!=1){
                                                echo '<li class="breadcrumb-item"><a href="'.$value['url'].'">'.$value['name'].'</a></li>';
                                            }else{
                                                echo '<li class="breadcrumb-item active">'.$value['name'].'</li>';
                                            }
                                        }
                                    }
                                ?>
                            </ol>
                        </div>
                    </div>
                </div>
                <div class="content-header-right text-md-right col-md-6 col-12">
                    <div class="btn-group">
                        <!-- <button class="btn btn-round btn-info mb-1" type="button"><i class="icon-cog3"></i> Settings</button> -->
                    </div>
                </div>
            </div>

            <div class="content-overlay"></div>
            <div class="content-body">
                <!-- Global alert -->
                <?php if(flashdata('success_notice')): ?>
                    <div class="alert round bg-success alert-icon-left alert-dismissible mb-2" role="alert">
                        <span class="alert-icon"><i class="la la-thumbs-o-up"></i></span>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <strong>Data Berhasil Disimpan!</strong>
                    </div>
                <?php endif ?>
                <?php if(flashdata('info_notice')): ?>
                    <div class="alert round bg-warning alert-icon-left alert-dismissible mb-2" role="alert">
                        <span class="alert-icon"><i class="la la-thumbs-o-up"></i></span>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <strong><?php echo flashdata('info_notice'); ?></strong>
                    </div>
                <?php endif ?>

                <?php $this->load->view($view) ?>
            </div>
        </div>
    </div>
    <!-- END: Content-->

    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>

    
    <!-- BEGIN: Footer-->
    <footer class="footer footer-static footer-light navbar-border navbar-shadow">
        <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2">
            <span class="float-md-center d-block d-md-inline-block">Copyright &copy; <?=date('Y')?> 
                <a class="text-bold-800 grey darken-2" href="#" target="_blank">Aludi </a>
            </span>
        </p>
    </footer>
    <!-- END: Footer-->


    <style>
        .card-2 {
            box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
        }

        @media screen and (max-width: 767px) {
            div.dataTables_wrapper div.dataTables_length {
                text-align: left;
            }

            div.dataTables_wrapper div.dataTables_filter {
                text-align: right;
            }
        }
    </style>

    <div aria-live="polite" class="col-sm-12 col-md-3 py-3" aria-atomic="true" style="position: fixed; min-height: 200px;right:20px;top:20px;z-index:9;display: <?= flashdata('error') ? 'block':'none' ?>">
        <div style="text-overflow: ellipsis;" class="card-2 toast <?= flashdata('error') ? 'd-block':'' ?>" id="global-toast">
            <div class="toast-body p-2 bg-warning text-white font-weight-bold">
                <?= flashdata('error') ?>
            </div>
        </div>
    </div>


    <!-- Global alert -->
    <?php if(flashdata('success')): ?>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
        <script>
            Swal.fire({
                title: 'Berhasil',
                text: '<?= flashdata('success') ?>',
                icon: 'success',
                confirmButtonText: 'Ok'
            });
        </script>
    <?php endif ?>

    <?php if(flashdata('error')): ?>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
        <script>
            Swal.fire({
                title: 'Gagal',
                text: '<?= flashdata('error') ?>',
                icon: 'error',
                confirmButtonText: 'Ok'
            });
        </script>
    <?php endif ?>


    <!-- BEGIN: Vendor JS-->
    <script src="<?= base_url() ?>app-assets/vendors/js/vendors.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="<?= base_url() ?>app-assets/vendors/js/ui/prism.min.js"></script>

    <script src="<?= base_url() ?>app-assets/vendors/js/forms/select/select2.full.min.js"></script>
    <script >
        $(document).ready(function() {
            $('.select2').select2();
        });
    </script>
    
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="<?= base_url() ?>app-assets/js/core/app-menu.js"></script>
    <script src="<?= base_url() ?>app-assets/js/core/app.js"></script>
    <!-- END: Theme JS-->

    
    <!-- BEGIN: Page JS-->
    <!-- END: Page JS-->

    
    <script src="<?= base_url() ?>app-assets/vendors/js/editors/tinymce/tinymce.min.js"></script>
    <script src="<?= base_url() ?>app-assets/js/scripts/editors/editor-tinymce.js"></script>

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/vendors/css/tables/datatable/datatables.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/vendors/css/tables/extensions/responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/vendors/css/tables/extensions/colReorder.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/vendors/css/tables/extensions/buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/vendors/css/tables/extensions/fixedHeader.dataTables.min.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="<?= base_url() ?>app-assets/summernote/dist/summernote.js"></script>
    <script src="<?= base_url() ?>app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
    <script src="<?= base_url() ?>app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js"></script>
    <script src="<?= base_url() ?>app-assets/vendors/js/tables/buttons.colVis.min.js"></script>
    <script src="<?= base_url() ?>app-assets/vendors/js/tables/datatable/dataTables.colReorder.min.js"></script>
    <script src="<?= base_url() ?>app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js"></script>
    <script src="<?= base_url() ?>app-assets/vendors/js/tables/datatable/buttons.bootstrap4.min.js"></script>
    <script src="<?= base_url() ?>app-assets/vendors/js/tables/datatable/dataTables.fixedHeader.min.js"></script>
    <!-- END: Page Vendor JS-->

	<script src="<?= base_url() ?>app-assets/summernote/dist/widgets/summernote.js"></script>
    <script >
            $(document).ready(function() {
                
            <?php if(@$url != ""){?>   
           
			$('.data_list').DataTable({
			"pagingType": "full_numbers",scrollY:        "300px",
			
				"processing": true,
				"serverSide": true,
				"ajax": "<?= site_url() ?>/<?= $url ?>"
				});  
			<?php }else{?>
			 $('.data_list').DataTable({
                responsive: true
            });    
			<?php }?>
        });
    </script>


        
    <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script> -->
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

    
    <script type="text/javascript">
        var rupiah = document.getElementById('rupiah');
        rupiah.addEventListener('keyup', function(e){
            // tambahkan 'Rp.' pada saat form di ketik
            // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
            rupiah.value = formatRupiah(this.value, 'Rp. ');
        });
    
        /* Fungsi formatRupiah */
        function formatRupiah(angka, prefix){
            var number_string = angka.replace(/[^,\d]/g, '').toString(),
            split   		= number_string.split(','),
            sisa     		= split[0].length % 3,
            rupiah     		= split[0].substr(0, sisa),
            ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
    
            // tambahkan titik jika yang di input sudah menjadi angka ribuan
            if(ribuan){
                separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }
    
            rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
            return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
        }
    </script>

    <script type="text/javascript">
        $(function() {
            $('.datepicker').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                locale: {
                    format: 'YYYY-MM-DD'
                    },
                autoclose: true,
                // zIndexOffset: 1500,
                uiLibrary: 'bootstrap4',
                icons: {
                    rightIcon: false
                },
                modal: true, header: true, footer: true,
            });

            $('.datepickertime').daterangepicker({
                singleDatePicker: true,
                timePicker : true,
                timePicker24Hour : true,
                timePickerIncrement : 1,
                timePickerSeconds : true,
                locale : {
                    format : 'HH:mm'
                }
            });
            
            $('.datepickerlahir').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                locale: {
                    format: 'YYYY-MM-DD'
                    },
                autoclose: true,
                // zIndexOffset: 1500,
                uiLibrary: 'bootstrap4',
                icons: {
                    rightIcon: false
                },
                modal: true, header: true, footer: true,
            });
        });
    </script>

</body>
<!-- END: Body-->

</html>