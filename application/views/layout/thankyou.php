<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <meta name="description" content="Modern admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities with bitcoin dashboard.">
        <meta name="keywords" content="admin template, modern admin template, dashboard template, flat admin template, responsive admin template, web app, crypto dashboard, bitcoin dashboard">
        <meta name="author" content="ervanprastyanto@gmail.com">
        <title>Aludi</title>
        <!-- <link rel="apple-touch-icon" href="<?= base_url() ?>app-assets/images/logo/Logo.png">
        <link rel="shortcut icon" type="image/x-icon" href="<?= base_url() ?>app-assets/images/logo/Logo.png"> -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i%7CQuicksand:300,400,500,700" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>app-assets/fonts/material-icons/material-icons.css">
    </head>
    <body>
        <style>
            body {
                font-family: "Open Sans", -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif;
            }
            .card-2 {
                box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
            }
            .box {
                display: block;
                margin: 15% auto;
                width: 450px;
                min-height: 220px;
                text-align:center;
                padding: 10px;
            }
        </style>
        <div class="card-2 box">
            <h1 style="font-size: 25px;">
                <i class="material-icons">check</i>
            </h1>
            <p style="font-size: 35px">Thank You !</p>
        </div>
    </body>
</html>