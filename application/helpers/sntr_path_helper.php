<?php

if(!function_exists('asset_path')) {
    function asset_path(string $resource_path = null) {
        return $resource_path ? base_url().'assets/'.$resource_path : base_url().'assets';
    }
}


// Function to get the client IP address
function getIp() {
	$ipaddress = '';
	if (isset($_SERVER['HTTP_CLIENT_IP'])){
		$ipaddress = $_SERVER['HTTP_CLIENT_IP'];
	}
	else if(isset($_SERVER['HTTP_X_FORWARDED_FOR'])){
		$ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
	}
	else if(isset($_SERVER['HTTP_X_FORWARDED'])){
		$ipaddress = $_SERVER['HTTP_X_FORWARDED'];
	}
	else if(isset($_SERVER['HTTP_FORWARDED_FOR'])){
		$ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
	}
	else if(isset($_SERVER['HTTP_FORWARDED'])){
		$ipaddress = $_SERVER['HTTP_FORWARDED'];
	}
	else if(isset($_SERVER['REMOTE_ADDR'])){
		$ipaddress = $_SERVER['REMOTE_ADDR'];
	}
	else{
		$ipaddress = 'UNKNOWN';
	}
		
	return $ipaddress;
}

	
function cekSetting($name){
	$ci=& get_instance();
	
	$ci->db->where('name',$name);
	$ci->db->from('core_setting');
	$query	= $ci->db->get();
	$data	= $query->row();

	return $data->value;
}

function cekIp($ip){
	$ci=& get_instance();
	
	$ci->db->where('ip',$ip );
	$ci->db->where('status','2' );
	$ci->db->from('core_ip');
	$query = $ci->db->count_all_results();

	return $query;
}

function getToken($length){
    $token = "";
    $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
    $codeAlphabet.= "0123456789";
    $max = strlen($codeAlphabet);

    for ($i=0; $i < $length; $i++) {
        $token .= $codeAlphabet[random_int(0, $max-1)];
    }

    return $token;
}

	
function logs($method='',$route='',$table_id='',$table_name='',$table_aksi='',$id_user='`',$deskripsi=''){
	$ci =& get_instance();
	
	$params_logs = array(
		"method"    	=> $method,
		"route"     	=> $route,
		"table_id"     	=> $table_id,
		"table_name"    => $table_name,
		"table_aksi"    => $table_aksi,
		"user_id"		=> $id_user,
		"deskripsi"		=> $deskripsi,
	);

	$ci->db->insert('activity_logs', $params_logs);
}

function MasDebug(){
    $ci=& get_instance();
    echo '<pre>';
    print_r($ci->db->last_query());
    echo '</pre>';
}

function MasDebugPree($data,$die=NULL){
    echo '<pre>';
    print_r($data);
    echo '</pre>';

    if(!empty($die)){
        die();
    }
}

function MasRefPerformancesGeneral() {
	$CI = get_instance();
	$CI->load->model('MHrRefPerformance');

	$data = MHrRefPerformance::
	where('id_personal', null)
	->where('id_leader', null)
	->where('id_periode', null)
	->orderBy('id', 'desc')
	->get();

	return $data;
}

function MasRatingPerformance($total){
    $ci=& get_instance();
	$query_rating = $ci->db->query("SELECT
		a.*
	FROM hr_ref_rating_performance a
	WHERE $total BETWEEN a.persen_awal AND a.persen_akhir
	LIMIT 1
	");
	$rating = $query_rating->row();
	return $rating;
}

function MasCekPerformanceDetail($id_performance,$id_ref_performance,$id_ref_performance_detail){
    $ci=& get_instance();
	$query = $ci->db->query("SELECT
		a.*
	FROM hr_performance_detail a
	WHERE a.id_performance = '$id_performance' AND a.id_ref_performance='$id_ref_performance' AND a.id_ref_performance_detail='$id_ref_performance_detail'
	LIMIT 1
	");
	$data = $query->row();

	$result = (!empty($data)) ? 1 : 0 ;
	
	return $result;
}

function getMac() {
    $ci=& get_instance();

	$ci->load->library('user_agent');
    return $ci->agent; 

	// $_IP_SERVER = $_SERVER['SERVER_ADDR'];
	// $_IP_ADDRESS = $_SERVER['REMOTE_ADDR']; 
	// if($_IP_ADDRESS == $_IP_SERVER)
	// {
	// 	ob_start();
	// 	system('ipconfig /all');
	// 	$_PERINTAH  = ob_get_contents();
	// 	ob_clean();
	// 	$_PECAH = strpos($_PERINTAH, "Physical");
	// 	$_HASIL = substr($_PERINTAH,($_PECAH+36),17);
	// }
	// else {
	// 	$_PERINTAH = "arp -a $_IP_ADDRESS";
	// 	ob_start();
	// 	system($_PERINTAH);
	// 	$_HASIL = ob_get_contents();
	// 	ob_clean();
	// 	$_PECAH = strstr($_HASIL, $_IP_ADDRESS);
	// 	$_PECAH_STRING = explode($_IP_ADDRESS, str_replace(" ", "", $_PECAH));
	// 	$_HASIL = substr($_PECAH_STRING[1], 0, 17);
	// }

	// return $_HASIL;
}

function hari_tgl_indo($date){
    $timestamp  = strtotime($date);
    $day        = date('D', $timestamp);
    $hari       = hari($day);
    $tanggal    = tgl_indo($date);
 
	return $hari. ', ' .$tanggal;
}

function MasTanggal($tanggal,$date_time='0'){
	if ($tanggal=="" OR empty($tanggal)) {
		return " ";
	}

	$jam		= '';
	if ($date_time=="1") {
		$jam		= date('H:i', strtotime($tanggal));
		$tanggal	= date('Y-m-d', strtotime($tanggal));

	}


	$bulan = array (
		1 =>   'Januari',
		'Februari',
		'Maret',
		'April',
		'Mei',
		'Juni',
		'Juli',
		'Agustus',
		'September',
		'Oktober',
		'November',
		'Desember'
	);
	$pecahkan = explode('-', $tanggal);
	
 
	return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0].' '.$jam;
}

function MasBulan($bulan){
	$data = array (
		1 =>   'Januari',
		'Februari',
		'Maret',
		'April',
		'Mei',
		'Juni',
		'Juli',
		'Agustus',
		'September',
		'Oktober',
		'November',
		'Desember'
	);

	return $data[ (int)$bulan ];
}

function tgl_indo($tanggal){
	if ($tanggal=="" OR empty($tanggal)) {
		return "(belum di setting)";
	}

	$bulan = array (
		1 =>   'Januari',
		'Februari',
		'Maret',
		'April',
		'Mei',
		'Juni',
		'Juli',
		'Agustus',
		'September',
		'Oktober',
		'November',
		'Desember'
	);
	$pecahkan = explode('-', $tanggal);
	
	// variabel pecahkan 0 = tanggal
	// variabel pecahkan 1 = bulan
	// variabel pecahkan 2 = tahun
 
	return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
}


function bulan_indo($bln){
	$bulan = array (
		1 =>   'Januari',
		'Februari',
		'Maret',
		'April',
		'Mei',
		'Juni',
		'Juli',
		'Agustus',
		'September',
		'Oktober',
		'November',
		'Desember'
	);
 
	return $bulan[ (int)$bln ] ;
}
 
function hari($hari){
	
	switch($hari){
		case 'Sun':
			$hari_ini = "Minggu";
		break;
 
		case 'Mon':			
			$hari_ini = "Senin";
		break;
 
		case 'Tue':
			$hari_ini = "Selasa";
		break;
 
		case 'Wed':
			$hari_ini = "Rabu";
		break;
 
		case 'Thu':
			$hari_ini = "Kamis";
		break;
 
		case 'Fri':
			$hari_ini = "Jumat";
		break;
 
		case 'Sat':
			$hari_ini = "Sabtu";
		break;
		
		default:
			$hari_ini = "Tidak di ketahui";		
		break;
	}
 
	return $hari_ini;
 
}

function masHari($hari){
	$hari_array = array ( 1 =>    'Senin',
			'Selasa',
			'Rabu',
			'Kamis',
			'Jumat',
			'Sabtu',
			'Minggu'
		);

	// Misal hari ini adalah sabtu
	if (empty($hari)) {
		$hari = date('N'); // Hasil 6
	}
	
	return $hari_array[$hari]; // Sama seperti echo $hari[6], hasil: Sabtu
}


function MasUangAwal($amount=0,$rp=0){
	if ($rp==1) {
		$amount = str_replace('Rp. ', '', $amount); 
	}
	
	$data 	= explode("," , $amount);
	$result = str_replace('.', '', $data[0]); 
	return $result;
}

function MasUang($amount=0,$belakang=2){
	$hasil_rupiah = number_format($amount,$belakang,',','.');
	if (empty($belakang)) {
		$hasil_rupiah = number_format($amount,0,'','.');
	}
	return $hasil_rupiah;
}

function MasConvertTerbilang($nilai=NULL){
    if($nilai<0) {
        $hasil = "minus ". trim(penyebut($nilai));
    } else {
        $hasil = trim(penyebut($nilai));
    }     		
    return $hasil.' rupiah';
}

function penyebut($nilai) {
    $nilai = abs($nilai);
    $huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
    $temp = "";
    if ($nilai < 12) {
        $temp = " ". $huruf[$nilai];
    } else if ($nilai <20) {
        $temp = penyebut($nilai - 10). " belas";
    } else if ($nilai < 100) {
        $temp = penyebut($nilai/10)." puluh". penyebut($nilai % 10);
    } else if ($nilai < 200) {
        $temp = " seratus" . penyebut($nilai - 100);
    } else if ($nilai < 1000) {
        $temp = penyebut($nilai/100) . " ratus" . penyebut($nilai % 100);
    } else if ($nilai < 2000) {
        $temp = " seribu" . penyebut($nilai - 1000);
    } else if ($nilai < 1000000) {
        $temp = penyebut($nilai/1000) . " ribu" . penyebut($nilai % 1000);
    } else if ($nilai < 1000000000) {
        $temp = penyebut($nilai/1000000) . " juta" . penyebut($nilai % 1000000);
    } else if ($nilai < 1000000000000) {
        $temp = penyebut($nilai/1000000000) . " milyar" . penyebut(fmod($nilai,1000000000));
    } else if ($nilai < 1000000000000000) {
        $temp = penyebut($nilai/1000000000000) . " trilyun" . penyebut(fmod($nilai,1000000000000));
    }     
    return $temp;
}


function MasKaryawan($id_personal=0) {
	if ($id_personal==0) {
		$id_personal=personal()->id;
	}
	$CI 	= get_instance();
	$CI->load->model('MPersonal');
	$result  = MPersonal::where('id',$id_personal)->first();

	return $result;
}

function MasKaryawanApprove($id_personal=0) {
	if ($id_personal==0) {
		$id_personal=personal()->id;
	}
	$CI		= get_instance();
	$CI->load->model('MPoCalonPenerbitApprove');
	$data	= MPoCalonPenerbitApprove::where('id_personal',$id_personal)->first();

	if(!empty($data->id)){
		$result = 1;
	}else{
		$result = 0;
	}

	return $result;
}

function MasKaryawanUser($id_user=0,$array=0) {
	$CI 	= get_instance();
	$CI->load->model('MPersonal');

	if ($array=="1") {
		$result  = MPersonal::where('user_id',$id_user)->first()->toArray();
	}else {
		$result  = MPersonal::where('user_id',$id_user)->first();
	}

	return $result;
}

function MasPeriode() {
	$CI 	= get_instance();
	$CI->load->model('MHrRefPeriode');
	$result  = MHrRefPeriode::where('status',2)->first();

	return $result;
}

function MasPeriodePerformance() {
	$CI 	= get_instance();
	$CI->load->model('MHrRefPeriodePerformance');
	$result  = MHrRefPeriodePerformance::where('status',2)->first();

	return $result;
}

function MasTanggalRange($start, $end, $format = 'Y-m-d') { 
    $array = array(); 
      
    // of period 1 day 
    $interval = new DateInterval('P1D'); 
  
    $realEnd = new DateTime($end); 
    $realEnd->add($interval); 
  
    $period = new DatePeriod(new DateTime($start), $interval, $realEnd); 
  
    // Use loop to store date into array 
    foreach($period as $date) {                  
        $array[] = $date->format($format);  
    } 
  
    return $array; 
} 

function MasPeriodePerformanceNext() {
	$CI 	= get_instance();
	$CI->load->model('MHrRefPeriodePerformance');
	$result  = MHrRefPeriodePerformance::orderBy('id', 'desc')->first();

	return $result;
}

function MasLamaKaryawan($id) {
	$CI 	= get_instance();
	$CI->load->model('MPersonal');
	$data_diri  = MPersonal::where('id',$id)->first()->toArray();

	$awal_masuk = date_create($data_diri['join_date']);
	$akhir      = date_create(date("Y-m-d")); // waktu sekarang
	$diff       = date_diff($awal_masuk,$akhir );

	return $diff;
}

function MasTutupTahun() {
	$awal_masuk = date_create(date("Y-m-d")); // waktu sekarang
	$akhir      = date_create(date("Y")."-12-31"); // waktu akhir tahun
	$diff       = date_diff( $awal_masuk, $akhir );

	return $diff;
}

function sisa_claim($type_id,$id_personal=0) {
	if ($id_personal==0) {
		$id_personal=personal()->id;
	}

	$CI 	= get_instance();
	$CI->load->model('MHrClaim');
	$CI->load->model('MHrRefTypeClaim');
	$CI->load->model('MPersonal');

	// $type_id = 1;//Medical Benefit Reimbursement
	
	$lama_karyawan		= MasLamaKaryawan($id_personal);
	$karyawan_status	= MasKaryawan($id_personal)->jobHistory->job_status;
	
	$data = MHrClaim::where('id_personal', $id_personal)
			->where('id_type_claim',$type_id)
			->whereIn('status',array(22,25)) //status Diterima GA && dikonfirmasi
			->whereBetween('tanggal', [
				date('Y-m-d', strtotime(date('Y') .'-01-01')),
				date('Y-m-d', strtotime(date('Y') .'-12-31'))
			])
			->orderBy('id', 'desc')
			->first();

			
	$saldo = (!empty($data->saldo)) ? $data->saldo : 0 ;

	$type_claim = MHrRefTypeClaim::where('id', $type_id)->first();
	if (empty($data) AND $type_claim->total > 0) {
		$saldo = $type_claim->total;
	}

	// if($saldo==0 AND empty($type_claim->total)){
	// 	$saldo = (empty($type_claim->total) || $type_claim->total=="")?0:$type_claim->total;
	// }
	return $saldo;
}

function cekInputClaim($type_id,$id_personal=0) {
	if ($id_personal==0) {
		$id_personal=personal()->id;
	}

	$CI 	= get_instance();
	$CI->load->model('MHrClaim');
	$CI->load->model('MHrRefTypeClaim');
	$CI->load->model('MPersonal');
	
	$data = MHrClaim::where('id_personal', $id_personal)
			->where('id_type_claim',$type_id)
			->whereIn('status',array(22,25)) //status Diterima GA && dikonfirmasi
			->whereBetween('tanggal', [
				date('Y-m-d', strtotime(date('Y') .'-01-01')),
				date('Y-m-d', strtotime(date('Y') .'-12-31'))
			])
			->orderBy('id', 'desc')
			->first();

	return $data;
}

function MasExcelColumn($col_number){
	$xls_columns=array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P',
	'Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ',
	'AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ',
	'BA','BB','BC','BD','BE','BF','BG','BH','BI','BJ',
	'BK','BL','BM','BN','BO','BP','BQ','BR','BS','BT','BU','BV','BW','BX','BY','BZ',
	'CA','CB','CC','CD','CE','CF','CG','CH','CI','CJ',
	'CK','CL','CM','CN','CO','CP','CQ','CR','CS','CT','CU','CV','CW','CX','CY','CZ',
	'DA','DB','DC','DD','DE','DF','DG','DH','DI','DJ',
	'DK','DL','DM','DN','DO','DP','DQ','DR','DS','DT','DU','DV','DW','DX','DY','DZ',
	'EA','EB','EC','ED','EE','EF','EG','EH','EI','EJ',
	'EK','EL','EM','EN','EO','EP','EQ','ER','ES','ET','EU','EV','EW','EX','EY','EZ',
	'FA','FB','FC','FD','FE','FF','FG','FH','FI','FJ',
	'FK','FL','FM','FN','FO','FP','FQ','FR','FS','FT','FU','FV','FW','FX','FY','FZ',
	'GA','GB','GC','GD','GE','GF','GG','GH','GI','GJ',
	'GK','GL','GM','GN','GO','GP','GQ','GR','GS','GT','GU','GV','GW','GX','GY','GZ',
	'HA','HB','HC','HD','HE','HF','HG','HH','HI','HJ',
	'HK','HL','HM','HN','HO','HP','HQ','HR','HS','HT','HU','HV','HW','HX','HY','HZ',
	'IA','IB','IC','ID','IE','IF','IG','IH','II','IJ',
	'IK','IL','IM','IN','IO','IP','IQ','IR','IS','IT','IU','IV','IW','IX','IY','IZ',
	'KA','KB','KC','KD','KE','KF','KG','KH','KI','KJ',
	'KK','KL','KM','KN','KO','KP','KQ','KR','KS','KT','KU','KV','KW','KX','KY','KZ',
	'LA','LB','LC','LD','LE','LF','LG','LH','LI','LJ',
	'LK','LL','LM','LN','LO','LP','LQ','LR','LS','LT','LU','LV','LW','LX','LY','LZ',
	'MA','MB','MC','MD','ME','MF','MG','MH','MI','MJ',
	'MK','ML','MM','MN','MO','MP','MQ','MR','MS','MT','MU','MV','MW','MX','MY','MZ',
	'NA','NB','NC','ND','NE','NF','NG','NH','NI','NJ',
	'NK','NL','NM','NN','NO','NP','NQ','NR','NS','NT','NU','NV','NW','NX','NY','NZ');
	return $xls_columns[$col_number];
} 
	
function MasCheckIn($tanggal='',$id_personal='') {
	if (empty($tanggal)) {
		$tanggal = date("d");
		$tanggal = date("Y-m-").$tanggal;
	}

	if (empty($id_personal)) {
		$id_personal=personal()->id;
	}

	$CI 	= get_instance();
	$CI->load->model('MHrPresensi');
	
	$data = MHrPresensi::where('id_personal', $id_personal)
			->where('is_in',1)
			->where('tanggal',$tanggal)
			->orderBy('id', 'desc')
			->first();


	return $data;
}

function MasCheckOut($tanggal='',$id_personal='') {
	if (empty($tanggal)) {
		$tanggal = date("d");
		$tanggal = date("Y-m-").$tanggal;
	}

	if (empty($id_personal)) {
		$id_personal=personal()->id;
	}

	$CI 	= get_instance();
	$CI->load->model('MHrPresensi');
	
	$data = MHrPresensi::where('id_personal', $id_personal)
			->where('is_in',2)
			->where('tanggal',$tanggal)
			->orderBy('id', 'desc')
			->first();

	return $data;
}

function MasParent($id_personal='',$is_approve=0) {
	$CI 	= get_instance();
	$CI->load->model('MPersonal');
	

	$query = $CI->db->query("SELECT
		a.*
		FROM `personals` a
		WHERE a.`parent_id`='$id_personal'
		ORDER BY `id` ASC
	");
	$raw_data = $query->result_array();

	$data = array();
	foreach ($raw_data as $key => $value) {
		
		$data[] = $value;
		if ($is_approve==1) {
			$children = MasParent($value['id'], $value['is_approve']);
			$data[] = $children;
		}
	}

	foreach ($data as $key => $value) {
		if (empty($value['id'])) {
			unset($data[$key]);
		}
	}
	
	return $data;
}

function MasParentId($id_personal) {
	$CI 	= get_instance();
	$CI->load->model('MPersonal');
	
	$query = $CI->db->query("SELECT
            a.*
            FROM `personals` a
            WHERE a.`status`='1' AND a.`parent_id`='$id_personal'
            ORDER BY `id` ASC
        ");
        $raw_data = $query->result_array();
        $row    = array();
        foreach ($raw_data as $key => $value) {
            $row[] = $value;
            if ($value['is_approve']==1) {
                $cekBawahan = MasParent($value['id'],1);
                if (!empty($cekBawahan)) {
                    foreach ($cekBawahan as $key => $value) {
                        $row[] = $value;
                    }
                }
            }
        }

        $result = array();
        foreach ($row as $key => $value) {
            $result[] = $value['id'];
        }

    return $result;
}


function MasConvertRomawi($number){
    $map = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400, 'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40, 'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);
    $returnValue = '';
    while ($number > 0) {
        foreach ($map as $roman => $int) {
            if($number >= $int) {
                $number -= $int;
                $returnValue .= $roman;
                break;
            }
        }
    }
    return $returnValue;
}


function MasConvertInteger($key){
    $romans = [
        'M'		=> 1000,
        'CM'	=> 900,
        'D'		=> 500,
        'CD'	=> 400,
        'C'		=> 100,
        'XC'	=> 90,
        'L'		=> 50,
        'XL'	=> 40,
        'X'		=> 10,
        'IX'	=> 9,
        'V'		=> 5,
        'IV'	=> 4,
        'I'		=> 1,
    ];

    $roman = $key;
    $result = 0;

    foreach ($romans as $key => $value) {
        while (strpos($roman, $key) === 0) {
            $result += $value;
            $roman = substr($roman, strlen($key));
        }
    }
    echo $result;
}

function MasNomorLogs($kode,$tabel_nama='',$tabel_id=''){
	$ci =& get_instance();
	
	if ($tabel_nama!='') {
		$params["tabel_nama"]	= $tabel_nama;
	}
	if ($tabel_id!='') {
		$params["tabel_id"]		= $tabel_id;
	}

	$params["saat_ini"]		= MasNomor($kode);
	$params["updated_at"]	= date('Y-m-d H:i:s');
	$params["updated_by"]	= authUser()->id;
	$ci->db->update('core_nomor', $params, array('kode' => $kode));
}

function MasNomorKomponen($kode){
	$raw = MCoreRefNomorKomponen::where('kode', $kode)
        ->orderBy('id', 'desc')
        ->first();


	if ($raw->is_url == 1) {
		if ($raw->url == 'tahun') { //tahun
			$result = date('Y');
		}
		elseif ($raw->url == 'romawi_bulan') { //tahun
			$result = MasConvertRomawi(date('m'));
		}
		
		else{
			$result = '';
		}

	}else{
		$result = $raw->is_url; 
	}

	return $result;
}

function MasNomor($kode){
	if(empty($kode)){ return ""; }
	$CI 	= get_instance();
	$CI->load->model('MCoreNomor');
	$CI->load->model('MCoreRefNomorKomponen');

	$raw		= MCoreNomor::where('kode', $kode)
				->orderBy('id', 'desc')
				->first();

	$raw_array = explode($raw->pemisah,$raw->format);
	$raw_array_saat_ini = explode($raw->pemisah,$raw->saat_ini);

	$urutan_key = 0;
	$params = array();
	$cek_variable = 0;
	foreach ($raw_array as $key => $value) {
		$variable = 0;
		if (stripos($value, "{") !== false AND stripos($value, "}") !== false AND $value!="{urutan}") {
			$isi = MasNomorKomponen($value);
			$raw_array[$key] = $isi;
			$variable = 1;
			$cek_variable = 1;
		}
		elseif($value=="{urutan}"){
			$urutan_key = $key;
		}

		
		$params[$key]['variable'] = $variable;
	}

	
	$nomor_raw          = implode($raw->pemisah,$raw_array);
	$nomor_raw_array    = explode($raw->pemisah,$nomor_raw);

	$status = 0;
	foreach ($params as $key => $value) {
		if ($key != $urutan_key AND $value['variable']==1) {
			if ($raw_array_saat_ini[$key] == $nomor_raw_array[$key]) {
				$status += 1;
			}
		}
	}

	$urutan = 1; 
	if($status>1 OR $cek_variable==0) {
		$urutan = $raw_array_saat_ini[$urutan_key]+1; 
		if ($urutan<10) {
			$urutan = '0'.$urutan;
		}
	}
	$nomor_raw_array[$urutan_key] = $urutan;

	$result = implode($raw->pemisah,$nomor_raw_array);
	
	return $result;

}

function MasRandom($length) {
    $key = '';
    $keys = array_merge(range(0, 9), range('a', 'z'));

    for ($i = 0; $i < $length; $i++) {
        $key .= $keys[array_rand($keys)];
    }

    return $key;
}

function MasProfile($kode){
	if(empty($kode)){ return ""; }
	$CI 	= get_instance();
	$CI->load->model('MPoProfil');

	$data		= MPoProfil::where('kode', $kode)
				->orderBy('id', 'desc')
				->first();

	
	return $data;

}

function MasNegara($kode,$lengkap="0"){
	$CI 	= get_instance();
	$CI->load->model('MCoreRefNegara');

	$data		= MCoreRefNegara::where('kode', $kode)
				->orderBy('id', 'desc')
				->first();

	if ($lengkap==1) {
		return $data;
	}
	
	return $data->id;

}

function MasProvinsi($kode,$lengkap="0"){
	$CI 	= get_instance();
	$CI->load->model('MCoreRefProvinsi');

	$data		= MCoreRefProvinsi::where('kode', $kode)
				->orderBy('id', 'desc')
				->first();

	if ($lengkap==1) {
		return $data;
	}
	
	return $data->id;

}

function MasKota($kode,$lengkap="0"){
	$CI 	= get_instance();
	$CI->load->model('MCoreRefKabupaten');

	$data		= MCoreRefKabupaten::where('id', $kode)
				->orderBy('id', 'desc')
				->first();

	if ($lengkap==1) {
		return $data;
	}
	
	return $data->id;
}

function MasJenisPenerbit($kode,$lengkap="0"){
	$CI 	= get_instance();
	$CI->load->model('MCoreRefJenisPenerbit');

	$data		= MCoreRefJenisPenerbit::where('id', $kode)
				->orderBy('id', 'desc')
				->first();

	if ($lengkap==1) {
		return $data;
	}

	if (empty(@$data->id)) {
		$data = "";
	}

	
	return $data->id;
}


function MasXssClean($data){
	/*
	 * XSS filter 
	 *
	 * This was built from numerous sources
	 * (thanks all, sorry I didn't track to credit you)
	 * 
	 * It was tested against *most* exploits here: http://ha.ckers.org/xss.html
	 * WARNING: Some weren't tested!!!
	 * Those include the Actionscript and SSI samples, or any newer than Jan 2011
	 *
	 *
	 * TO-DO: compare to SymphonyCMS filter:
	 * https://github.com/symphonycms/xssfilter/blob/master/extension.driver.php
	 * (Symphony's is probably faster than my hack)
	 */
	// Fix &entity\n;
	$data = str_replace(array('&amp;','&lt;','&gt;'), array('&amp;amp;','&amp;lt;','&amp;gt;'), $data);
	$data = preg_replace('/(&#*\w+)[\x00-\x20]+;/u', '$1;', $data);
	$data = preg_replace('/(&#x*[0-9A-F]+);*/iu', '$1;', $data);
	$data = html_entity_decode($data, ENT_COMPAT, 'UTF-8');

	// Remove any attribute starting with "on" or xmlns
	$data = preg_replace('#(<[^>]+?[\x00-\x20"\'])(?:on|xmlns)[^>]*+>#iu', '$1>', $data);

	// Remove javascript: and vbscript: protocols
	$data = preg_replace('#([a-z]*)[\x00-\x20]*=[\x00-\x20]*([`\'"]*)[\x00-\x20]*j[\x00-\x20]*a[\x00-\x20]*v[\x00-\x20]*a[\x00-\x20]*s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:#iu', '$1=$2nojavascript...', $data);
	$data = preg_replace('#([a-z]*)[\x00-\x20]*=([\'"]*)[\x00-\x20]*v[\x00-\x20]*b[\x00-\x20]*s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:#iu', '$1=$2novbscript...', $data);
	$data = preg_replace('#([a-z]*)[\x00-\x20]*=([\'"]*)[\x00-\x20]*-moz-binding[\x00-\x20]*:#u', '$1=$2nomozbinding...', $data);

	// Only works in IE: <span style="width: expression(alert('Ping!'));"></span>
	$data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?expression[\x00-\x20]*\([^>]*+>#i', '$1>', $data);
	$data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?behaviour[\x00-\x20]*\([^>]*+>#i', '$1>', $data);
	$data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:*[^>]*+>#iu', '$1>', $data);

	// Remove namespaced elements (we do not need them)
	$data = preg_replace('#</*\w+:\w[^>]*+>#i', '', $data);

	do
	{
			// Remove really unwanted tags
			$old_data = $data;
			$data = preg_replace('#</*(?:applet|b(?:ase|gsound|link)|embed|frame(?:set)?|i(?:frame|layer)|l(?:ayer|ink)|meta|object|s(?:cript|tyle)|title|xml)[^>]*+>#i', '', $data);
	}
	while ($old_data !== $data);

	// we are done...
	return $data;
}