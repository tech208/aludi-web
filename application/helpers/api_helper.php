<?php

if(!function_exists('sayhello')) {
    function sayhello(){
        return "Hello Friends";
    }
}


if(!function_exists('MasToken')) {
    function MasToken($longitud){
        if ($longitud < 4) {
            $longitud = 4;
        }
    
        return bin2hex(random_bytes(($longitud - ($longitud % 2)) / 2));
    }
}


if(!function_exists('MasKodeRespon')) {
    function MasKodeRespon($kode='200'){
        // referensi https://www.restapitutorial.com/httpstatuscodes.html#

        if ($kode >=100 AND $kode < 200) { //kode 100
            if ($kode==100) {
                $result = array(
                    "kode"          => "100",
                    "nama"          => "Continue",
                );
            }

            if ($kode==101) {
                $result = array(
                    "kode"          => "101",
                    "nama"          => "Switching_protocols",
                );
            }

            if ($kode==102) {
                $result = array(
                    "kode"          => "102",
                    "nama"          => "Processing",
                );
            }

            
            $result["kode_grup"] =  "informational";
        }
        elseif ($kode >=200 AND $kode < 300) { //kode 200

            if ($kode==200) {
                $result = array(
                    "kode"          => "200",
                    "nama"          => "Ok",
                );
            }

            if ($kode==201) {
                $result = array(
                    "kode"          => "201",
                    "nama"          => "Created",
                );
            }
			
			if ($kode==202) {
                $result = array(
                    "kode"          => "202",
                    "nama"          => "Updated",
                );
            }
			
			if ($kode==203) {
                $result = array(
                    "kode"          => "203",
                    "nama"          => "Deleted",
                );
            }
            
            if ($kode==204) {
                $result = array(
                    "kode"          => "204",
                    "nama"          => "No Content",
                );
            }

            $result["kode_grup"] = "success";
            
        }
        elseif ($kode >=300 AND $kode < 400) { //kode 300
            
            if ($kode==304) {
                $result = array(
                    "kode"          => "304",
                    "nama"          => "Not Modified",
                );
            }

            $result["kode_grup"] = "redirection";
        }
        elseif ($kode >=400 AND $kode < 500) { //kode 400
            
            if ($kode==400) {
                $result = array(
                    "kode"          => "400",
                    "nama"          => "Bad Request",
                );
            }

            if ($kode==401) {
                $result = array(
                    "kode"          => "401",
                    "nama"          => "Token Expired",
                );
            }

            if ($kode==403) {
                $result = array(
                    "kode"          => "403",
                    "nama"          => "Forbidden",
                );
            }

            if ($kode==404) {
                $result = array(
                    "kode"          => "404",
                    "nama"          => "Not Found",
                );
            }

            if ($kode==409) {
                $result = array(
                    "kode"          => "409",
                    "nama"          => "Conflict",
                );
            }
			
			if ($kode==411) {
                $result = array(
                    "kode"          => "411",
                    "nama"          => "Duplicate",
                );
            }
			
			if ($kode==412) {
                $result = array(
                    "kode"          => "412",
                    "nama"          => "Duplicate",
                );
            }
			
            $result["kode_grup"] = "client_error";
        }
        elseif ($kode >=600 AND $kode < 600) { //kode 500
            
            if ($kode==500) {
                $result = array(
                    "kode"          => "500",
                    "nama"          => "Internal Server Error",
                );
            }

            $result["kode_grup"] = "server_error";
        }

        return $result;
    }
}


if(!function_exists('MasTokenInfo')) {
    function MasTokenInfo($token, $authorization=""){
        $authorization = str_replace('Basic ', '', $authorization);
		$authorization = str_replace(':', '', base64_decode($authorization));
		
        if (empty($token)&&empty($authorization)) {
            //respon balik
            $data   = array();
            $pesan  = array(1 => "Token Invalid",);
            $kode   = 401;
            MasRespon($data,$pesan,$kode);
            //end respon balik
        }

        $date = date('Y-m-d');
        
        $CI     = get_instance();
        $CI->load->model('MCoreUser');
        $result = MCoreUser::whereIn('token_test', array($token, $authorization))
        ->where('token_expired', $date)
        ->first();

        
        if (empty($result)) {
            //respon balik
            $data   = array();
            $pesan  = array(1 => "Data Tidak Ditemukan atau Sudah Kadaluarsa!",);
            $kode   = 401;
            MasRespon($data,$pesan,$kode);
            //end respon balik
            exit();
        }
        $result = MasKaryawanUser($result->id,1);
        
        
        return $result;
    }
}

if(!function_exists('MasTokenInfoTest')) {
    function MasTokenInfoTest($token, $authorization=""){
		$authorization = str_replace('Basic ', '', $authorization);
		$authorization = str_replace(':', '', base64_decode($authorization));
		
        if (empty($token)&&empty($authorization)) {
            //respon balik
            $data   = array();
            $pesan  = array(1 => "Token Invalid",);
            $kode   = 401;
            MasRespon($data,$pesan,$kode);
            //end respon balik
        }

        $date = date('Y-m-d');
        $st="(token_test='$token' OR token_test='$authorization')";
        $CI     = get_instance();
        $CI->load->model('MCoreUser');
        $result = MCoreUser::whereIn('token_test', array($token, $authorization))
        ->first();
		
		

        
        if (empty($result)) {
            //respon balik
            $data   = array();
            $pesan  = array(1 => "Data Tidak Ditemukan atau Sudah Kadaluarsa!",);
            $kode   = 401;
            MasRespon($data,$pesan,$kode);
            //end respon balik
            exit();
        }
        $result = MasKaryawanUser($result->id,1);
        
        
        return $result;
    }
}

if(!function_exists('MasRespon')) {
    function MasRespon($data,$pesan,$kode){
        $respon = MasKodeRespon($kode);

        if(!empty(@$pesan[0])){
            $pesan_raw = array();
            foreach ($pesan as $key => $value) {
                $pesan_raw[$key+1] = $value;
            }
            $pesan = $pesan_raw;
        }
    
        $response = array(
            'data'      => $data,
            'pesan'     => $pesan,
            'respon'    => $respon
        );

        header('Content-Type: application/json');
        echo json_encode($response);
        exit();
    }
}

if(!function_exists('MasResponGagal')) {
    function MasResponGagal($pesan){
        if(!empty(@$pesan[0])){
            $pesan_raw = array();
            foreach ($pesan as $key => $value) {
                $pesan_raw[$key+1] = $value;
            }
            $pesan = $pesan_raw;
        }
        //respon balik
        $data   = array();
        $pesan  = $pesan;
        $kode   = 400;
        MasRespon($data,$pesan,$kode);
        //end respon balik
    }
}




?>