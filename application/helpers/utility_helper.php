<?php

if(!function_exists('authUser')) {
    function authUser() {
        $CI = get_instance();
        $CI->load->model('MCoreUser');
        $user = $CI->session->userdata('user');
        return MCoreUser::where('id', $user['id'])->first();
    }
}

if(!function_exists('personal')) {
    function personal() {
        return authUser() ? authUser()->personal : null;
    }
}

if(!function_exists('hasMenuAccess')) {
    function hasMenuAccess($menu) {
        if (!authUser())
            return false;

        $role = authUser()->role;
        $permissions = explode(',', @$role->permissions);

        return in_array($menu, $permissions);
    }
}

if(!function_exists('CekAksesPermisi')) {
    function CekAksesPermisi($menu) {
        if (!authUser())
            return false;

        $role = authUser()->role;
        $permissions = explode(',', $role->permissions);

        return in_array($menu, $permissions);
    }
}

if(!function_exists('avatar_path')) {
    function avatar_path() {
        $path = personal()->avatar_path();
        if(personal()->photo) {
            $path = base_url(personal()->photo);
        }

        return $path;
    }
}

if(!function_exists('ttd_path')) {
    function ttd_path() {
        $path = MasKaryawan()->ttd_path();
        if(personal()->tanda_tangan) {
            $path = base_url('uploads/ttd/'.personal()->tanda_tangan);
        }

        return $path;
    }
}

if(!function_exists('csrf_field')) {
    function csrf_field() {
        $CI = get_instance();
        $name = $CI->security->get_csrf_token_name();
        $value = $CI->security->get_csrf_hash();
        return '<input type="hidden" name="'. $name .'" value="'. $value .'">';
    }
}

if(!function_exists('redirect_with_input')) {
    function redirect_with_input($route, array $input) {
        $CI = get_instance();

        foreach ($input as $key => $value) {
            $CI->session->set_flashdata($key, $value);
        }
        
        redirect($route);
    }
}

if(!function_exists('set_flashdata')) {
    function set_flashdata($key, $value) {
        $CI = get_instance();

        $CI->session->set_flashdata($key, $value);
    }
}

if(!function_exists('flashdata')) {
    function flashdata($key) {
        $CI = get_instance();

        return $CI->session->flashdata($key);
    }
}

if(!function_exists('get_atasan_langsung')) {
    function get_atasan_langsung($personal_id, $strict = false) {
        $CI = get_instance();

        $CI->load->model([
            'MJobTitle',
            'MPersonal',
            'MJobHistory'
        ]);

        $personal = MPersonal::find($personal_id);
        
        
        if (!$personal->jobHistory)
        return null;
        
        $level  = $personal->jobHistory->jobTitle->level;
        $divisi = $personal->jobHistory->jobTitle->division_id;
        
        
        $job_atasan = null;
        $i = 1;
        while($i < $level) {
            $job_atasan = MJobTitle::where([
                'division_id' => $divisi,
                'level' => $level - $i,
            ])->first();

            if($job_atasan) {
                break;
            }

            $i++;
        }

        if (!$job_atasan && $strict == true) {
            $job_atasan = MJobTitle::where([
                'level' => 1,
            ])->first();
        }
        
        if($job_atasan) {
            $history_atasan = MJobHistory::where('job_title_id', $job_atasan->id)
            ->orderBy('id', 'desc')->first();
            
            if (!$history_atasan){ //bila tidak ada yg menangani akan di arahkan k aditya.putri@santara.co.id
                $history_atasan = MJobHistory::where('personal_id', 76)
                ->orderBy('id', 'desc')->first();
            }

            return $history_atasan->personal;
        }

        return null;
    }
}

if(!function_exists('has_permission')) {
    function has_permission($permissions) {
        if(!authUser()->role) {
            return false;
        }

        $rp = authUser()->role->role_permissions->pluck('permission')->toArray();

        return in_array($permissions, $rp);
    }
}

if(!function_exists('has_permission_cek')) {
    function has_permission_cek($permissions,$role_id) {
        $CI 	= get_instance();
        $CI->load->model('MRolePerm');
        
        $data = MRolePerm::where('permission', $permissions)
        ->where('role_id', $role_id)
        ->orderBy('id', 'desc')
        ->first();
        // ->toArray();

        $result = (!empty($data->id)) ? true : false ;
        // MasDebugPree($result,1);
        return $result;
    }
}

if(!function_exists('category_code')) {
    function category_code() {
        $CI = get_instance();
        $CI->db->select_max('code');
        $query = $CI->db->get('asset_categories')->row_array();
        return $query['code'] + 100;
    }
}

if(!function_exists('resource_type_code')) {
    function resource_type_code($category) {
        $CI = get_instance();
        $CI->db->select_max('code');
        $CI->db->where('category_id', $category->id);
        $query = $CI->db->get('asset_types')->row_array();
        return intval($query['code']) == 0 ? $category->code + 10 : $query['code'] + 10;
    }
}

if(!function_exists('resource_classification_code')) {
    function resource_classification_code($type) {
        $CI = get_instance();
        $CI->db->select_max('code');
        $CI->db->where('type_id', $type->id);
        $query = $CI->db->get('asset_classifications')->row_array();
        return intval($query['code']) == 0 ? $type->code + 1 : $query['code'] + 1;
    }
}

if(!function_exists('resource_inventory_code')) {
    function resource_inventory_code($classification) {
        $CI = get_instance();
        $CI->db->select("code");
        $CI->db->order_by('id', 'desc');
        $CI->db->where('classification_id', $classification->id);
        $query = $CI->db->get('asset_inventories')->row_array();
        if(empty($query['code']) || !isset($query['code'])) {
            return $classification->code .'/01';
        }

        $code = explode('/', $query['code']);
        $increment = intval($code[1]) + 1;
        if ($increment < 10) {
            $increment = "0".$increment;
        }

        return $classification->code .'/'. $increment;
    }
}

if(!function_exists('write_request_log')) {
    /**
     * write log
     */
    function write_request_log($log_name, $data, $tag = null) {
        $tag = $tag !== null ? $tag : 'REQUEST';
        $console_file = "tmp/{$log_name}.txt";
        if (!file_exists($console_file)) {
            $file = fopen($console_file, 'w') or die('Cant create new file');
            fclose($file);
        }

        $file = fopen($console_file, 'a') or die('Cant open file '. $console_file);
        $input = "\r\n[{$tag}]-- ". date('Y-m-d H:i:s') ." --"; 
        $input .= "\r\n".$data;
        fwrite($file, $input);
        fclose($file);
    }
}

if ( ! function_exists('get_random_password'))
{
    /**
     * Generate a random password. 
     * 
     * get_random_password() will return a random password with length 6-8 of lowercase letters only.
     *
     * @access    public
     * @param    $chars_min the minimum length of password (optional, default 6)
     * @param    $chars_max the maximum length of password (optional, default 8)
     * @param    $use_upper_case boolean use upper case for letters, means stronger password (optional, default false)
     * @param    $include_numbers boolean include numbers, means stronger password (optional, default false)
     * @param    $include_special_chars include special characters, means stronger password (optional, default false)
     *
     * @return    string containing a random password 
     */    
    function get_random_password($chars_min=6, $chars_max=8, $use_upper_case=false, $include_numbers=false, $include_special_chars=false)
    {
        $length = rand($chars_min, $chars_max);
        $selection = 'aeuoyibcdfghjklmnpqrstvwxz';
        if($include_numbers) {
            $selection .= "1234567890";
        }
        if($include_special_chars) {
            $selection .= "!@\"#$%&[]{}?|";
        }
                                
        $password = "";
        for($i=0; $i<$length; $i++) {
            $current_letter = $use_upper_case ? (rand(0,1) ? strtoupper($selection[(rand() % strlen($selection))]) : $selection[(rand() % strlen($selection))]) : $selection[(rand() % strlen($selection))];            
            $password .=  $current_letter;
        }                
        
      return $password;
    }

    if(!function_exists('encrypt_data')) {
        function encrypt_data($stuff) {
            $CI = get_instance();
            $CI->load->library('encryption');
            $CI->encryption->initialize([
                'driver' => 'openssl'
            ]);

            return $CI->encryption->encrypt($stuff);
        }
    }

    if(!function_exists('decrypt_data')) {
        function decrypt_data($stuff) {
            $CI = get_instance();
            $CI->load->library('encryption');
            $CI->encryption->initialize([
                'driver' => 'openssl'
            ]);

            return $CI->encryption->decrypt($stuff);
        }
    }

}