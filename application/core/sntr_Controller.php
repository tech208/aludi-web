<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class sntr_Controller extends CI_Controller {
    
    public $data = [];

    public function __construct () {
        parent::__construct();
        if (!authUser()) {
            redirect('auth/login?redirect='. site_url(uri_string()));
            return;
        }

        $this->load->model('MActivityLog');
        $log = new MActivityLog;
        $log->user_id = authUser() ? authUser()->id : null;
        $log->method = strtoupper($this->input->method());
        $log->route = uri_string();
        $log->save();

        $this->data['view'] = null;
    }

    public function setContentView ($path_to_view) {
        $this->data['view'] = $path_to_view;
        return $this;
    }

    public function setParams (array $params) {
        foreach ($params as $key => $value) {
            $this->data[$key] = $value;
        }

        return $this;
    }

    public function setParam ($key, $value) {
        $this->data[$key] = $value;
        return $this;
    }

    public function render ($page_title) {
        $this->load->model('MCoreMenu');
        $side_menus = MCoreMenu::orderBy('order', 'asc')->get();

        $this->data['side_menus'] = $side_menus;
        $this->data['page_title'] = $page_title;
        $main = $this->config->item('main_view');
        $this->load->view($main, $this->data);
    } 
}