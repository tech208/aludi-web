<?php defined('BASEPATH') OR exit('No direct script access allowed');

use Dompdf\Dompdf;
use Dompdf\Options;
class Pdf extends Dompdf{

    public $filename;

    public function __construct(){
        parent::__construct();
        $this->filename = "export.pdf";
    }

    protected function ci()
    {
        return get_instance();
    }

    public function load_view($view, $data = array()){
        $options = new Options();
        $options->setChroot(FCPATH); //<-- Set root nya ke /var/www/html/nama-project
        $options->setDefaultFont('courier');

        $this->setOptions($options);

        // $html = $this->ci->load->view($view, $data, true);
        $html = $this->ci()->load->view($view, $data, TRUE);
        $this->load_html($html);
        $this->render();
        $this->stream($this->filename, ['Attachment' => 0]);


        // $this->load_html($html);
        // // Render the PDF

        // $this->render();
        // // Output the generated PDF to Browser
        // $this->stream($this->filename, array("Attachment" => false));
    }

    public function save($path, $view, $data = array()) {
        $html = $this->ci()->load->view($view, $data, TRUE);
        $this->load_html($html);
        // Render the PDF
        $this->render();

        $pdf_gen = $this->output();
        file_put_contents($path, $pdf_gen);
    }
}