<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mailgun {

  protected $CI;
  private static $api_key;
  private static $api_base_url;

  public function __construct() {
    // assign CI super object
    $this->CI =& get_instance();

    // config
    // self::$api_key = "0408d83f1d8622adfaebdaf4d05a16ff-e470a504-2bf79c85";
    // self::$api_base_url = "https://api.mailgun.net/v3/pages.santara.co.id";
    
    self::$api_key = "b51b56572854c381f431974371cab643-c4d287b4-2bb9ce5d";
    self::$api_base_url = "https://api.mailgun.net/v3/aludi.id";
  }

  /**
   * Send mail
   * $mail = array(from, to, subject, text)
   */
  public static function send($mail) {
    ini_set('date.timezone', 'Asia/Jakarta');
    $CI = get_instance();
    $CI->load->model('MEmailLog');

    $mail['from']     = "(no-reply)Aludi team <tech@aludi.id>";
    $mail['subject']  = "(no-reply)".@$mail['subject']." ".date("Y-m-d H:i:s");
    
    
    try{
      $ch = curl_init();
      
      // curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
      curl_setopt($ch, CURLOPT_USERPWD, 'api:' . self::$api_key);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
      curl_setopt($ch, CURLOPT_URL, self::$api_base_url . '/messages');
      curl_setopt($ch, CURLOPT_POSTFIELDS, $mail);
      curl_setopt($ch, CURLOPT_VERBOSE, true);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);  
      $result = curl_exec($ch);
      curl_close($ch);

    }
    catch(Exception $e) {

      trigger_error(sprintf(
          'Curl failed with error #%d: %s',
          $e->getCode(), $e->getMessage()),
          E_USER_ERROR);
  
  }

    if ($result) {
        $CI->db->trans_begin();
        $params = array(
            "to"            => @$mail['to'],
            "subject"       => @$mail['subject'],
            "cc"            => @$mail['cc'],
            "bcc"           => @$mail['bcc'],
            "status"        => 1,
            "created_at"    => date('Y-m-d H:i:s'),
        );
        $CI->db->insert('email_log', $params);
        
        if ($CI->db->trans_status() === FALSE){
            $CI->db->trans_rollback();
        }
        else{
            $CI->db->trans_commit();   
        }
    }

    return $result;
  }

}