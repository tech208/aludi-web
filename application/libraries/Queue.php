<?php

class Queue {

    private $instance = null;
    private $attachment = null;
    private $data = [];

    public function __construct() {
        $this->instance = get_instance();
    }
    
    public function setData($data = []) {
        $this->data = $data;
    }

    public function setAttachment($path) {
        $this->attachment = $path;
    }

    public function dispatch() {
        $this->instance->load->model('MEmailQueue');
        $queue = MEmailQueue::where('failed', 0)->orderBy('id', 'asc')->first();

        if (!$queue) {
            return;
        }

        try {
            $this->data = $queue->data ? json_decode($queue->data, true) : [];
            $this->attachment = $queue->attachment;
            if ($this->sendEmailDirectly($queue->email_to, $queue->subject, $queue->template)) {
                $queue->delete();
            }
        } catch (\Exception $e) {
            $this->instance->load->model('MEmailQueueFail');
            $failed = MEmailQueueFail::where('email_queue_id', $queue->id)->first();
            $fail = $failed ? $failed : new MEmailQueueFail;
            $fail->email_queue_id = $queue->id;
            $fail->tried = $failed ? ($failed->tried + 1) : 0;
            $fail->exception = json_encode($e);
            $fail->save();
        }
    }

    public function sendEmailViaQueue($to, $subject, $template_body) {
        $this->instance->load->model('MEmailQueue');
        $queue = new MEmailQueue;
        $queue->email_to = $to;
        $queue->subject = $subject;
        $queue->template = $template_body;
        $queue->data = json_encode($this->data);
        $queue->attachment = $this->attachment;
        $queue->save();
    }

    public function sendEmailDirectly($to, $subject, $template_body) {
        write_request_log('email_log', json_encode($this->data), $subject.'-'.$to);
        $this->instance->load->config('email', true);
        $mail_config = $this->instance->config->item('email');

        $this->instance->load->library('email');
        $this->instance->email->initialize($mail_config);

        //Clear the email state
        $this->instance->email->clear(true);

        $this->instance->email->from($mail_config['from_email'], $mail_config['from_name']);
        $this->instance->email->to($to);
        $this->instance->email->subject($subject);
        if($to!='aditya.putri@santara.co.id'){
            $this->instance->email->cc('aditya.putri@santara.co.id');
        }
        $this->instance->email->message(
            $this->instance->load->view($template_body, $this->data, true)
        );

        if ($this->attachment) {
            $this->instance->email->attach($this->attachment);
        }

        $result = $this->instance->email->send();
        if ($result=== false) {
            echo "<pre>";
            echo $this->instance->email->print_debugger();
            echo "</pre>";
            die();
        }
        return $result;
    }

    public function sendEmailDirectlyCuti($to, $subject, $template_body) {
        write_request_log('email_log', json_encode($this->data), $subject.'-'.$to);
        $this->instance->load->config('email', true);
        $mail_config = $this->instance->config->item('email');

        $this->instance->load->library('email');
        $this->instance->email->initialize($mail_config);

        //Clear the email state
        $this->instance->email->clear(true);

        $this->instance->email->from($mail_config['from_email'], $mail_config['from_name']);
        $this->instance->email->to($to);
        $this->instance->email->subject($subject);
        if($to!='aditya.putri@santara.co.id'){
            $this->instance->email->cc('aditya.putri@santara.co.id');
        }
        $this->instance->email->message(
            $this->instance->load->view($template_body, $this->data, true)
        );

        if ($this->attachment) {
            $this->instance->email->attach($this->attachment);
        }

        return $this->instance->email->send();
    }
}