<?php
use Illuminate\Database\Eloquent\Model;

class MCoreMenu extends Model {

    protected $table = 'core_menu';

    protected static function boot() {
        parent::boot();

        static::creating(function($model) {
            $model->created_by = authUser() ? authUser()->id : null;
            $model->updated_by = authUser() ? authUser()->id : null;
        });

        static::updating(function($model) {
            $model->updated_by = authUser() ? authUser()->id : null;
        });
    }

    public function child() {
        $CI = get_instance();
        $CI->load->model('MCoreMenuChild');
        return $this->hasMany(MCoreMenuChild::class, 'menu_id');
    }

    public function permissions() {
        $CI = get_instance();
        $CI->load->model('MPermission');
        return $this->hasMany(MPermission::class, 'menu_id');
    }

}