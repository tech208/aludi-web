<?php
use Illuminate\Database\Eloquent\Model;

class MPoPendaftaranTahapVisitHasil extends Model {

    protected $table = 'po_pendaftaran_tahap_visit_hasil';

    protected static function boot() {
        parent::boot();

        static::creating(function($model) {
            $model->created_by = authUser() ? authUser()->id : null;
            $model->updated_by = authUser() ? authUser()->id : null;
        });

        static::updating(function($model) {
            $model->updated_by = authUser() ? authUser()->id : null;
        });
    }

    public function statuse() {
        $CI = get_instance();
        $CI->load->model('MCoreRefStatus');
        return $this->belongsTo(MCoreRefStatus::class, 'status');
    }

    
    public function visitors() {
        $CI = get_instance();
        $CI->load->model('MPoPendaftaranTahapVisitVisitor');
        return $this->hasMany(MPoPendaftaranTahapVisitVisitor::class, 'id_visit');
    }

}