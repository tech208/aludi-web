<?php
use Illuminate\Database\Eloquent\Model;

class MPermission extends Model {

    protected $table = 'permissions';

    protected static function boot() {
        parent::boot();

        static::creating(function($model) {
            $model->created_by = authUser() ? authUser()->id : null;
            $model->updated_by = authUser() ? authUser()->id : null;
        });

        static::updating(function($model) {
            $model->updated_by = authUser() ? authUser()->id : null;
        });
    }

    public function role() {
        $CI = get_instance();
        $CI->load->model('MRolePerm');
        return $this->belongsTo(MRolePerm::class, 'role_id');
    }

    public function menu() {
        $CI = get_instance();
        $CI->load->model('MCoreMenu');
        return $this->belongsTo(MCoreMenu::class, 'menu_id');
    }

}