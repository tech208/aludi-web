<?php
use Illuminate\Database\Eloquent\Model;

class MPoArtikel extends Model {

    protected $table = 'po_artikel';

    protected static function boot() {
        parent::boot();

        static::creating(function($model) {
            $model->created_by = authUser() ? authUser()->id : null;
            $model->updated_by = authUser() ? authUser()->id : null;
        });

        static::updating(function($model) {
            $model->updated_by = authUser() ? authUser()->id : null;
        });
    }

    
    public function files() {
        $CI = get_instance();
        $CI->load->model('MPoArtikelFile');
        return $this->hasMany(MPoArtikelFile::class, 'id_artikel');
    }

    public function kategoris() {
        $CI = get_instance();
        $CI->load->model('MPoRefKategori');
        return $this->belongsTo(MPoRefKategori::class, 'id_kategori');
    }

    public function statuss() {
        $CI = get_instance();
        $CI->load->model('MRefCoreStatus');
        return $this->belongsTo(MRefCoreStatus::class, 'status');
    }

}