<?php
use Illuminate\Database\Eloquent\Model;

class MPoAnggota extends Model {

    protected $table = 'po_anggota';

    protected static function boot() {
        parent::boot();

        static::creating(function($model) {
            $model->created_by = authUser() ? authUser()->id : null;
            $model->updated_by = authUser() ? authUser()->id : null;
        });

        static::updating(function($model) {
            $model->updated_by = authUser() ? authUser()->id : null;
        });
    }

    public function statuse() {
        $CI = get_instance();
        $CI->load->model('MCoreRefStatus');
        return $this->belongsTo(MCoreRefStatus::class, 'status');
    }
    public function personals() {
        $CI = get_instance();
        $CI->load->model('MPoPersonals');
        return $this->belongsTo(MPoPersonals::class, 'personals');
    }


}