<?php
use Illuminate\Database\Eloquent\Model;

class MPoPendaftaranTahapPembayaran extends Model {

    protected $table = 'po_pendaftaran_tahap_pembayaran';

    protected static function boot() {
        parent::boot();

        static::creating(function($model) {
            $model->created_by = authUser() ? authUser()->id : null;
            $model->updated_by = authUser() ? authUser()->id : null;
        });

        static::updating(function($model) {
            $model->updated_by = authUser() ? authUser()->id : null;
        });
    }

    public function utamas() {
        $CI = get_instance();
        $CI->load->model('MPoPendaftaranTahap');
        return $this->belongsTo(MPoPendaftaranTahap::class, 'id_tahap');
    }

    public function statuse() {
        $CI = get_instance();
        $CI->load->model('MCoreRefStatus');
        return $this->belongsTo(MCoreRefStatus::class, 'status');
    }

    
    public function personals() {
        $CI = get_instance();
        $CI->load->model('MPersonal');
        return $this->hasOne(MPersonal::class, 'id_personal');
    }

}