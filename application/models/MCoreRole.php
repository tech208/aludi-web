<?php
use Illuminate\Database\Eloquent\Model;

class MCoreRole extends Model {

    protected $table = 'core_role';

    protected static function boot() {
        parent::boot();

        static::creating(function($model) {
            $model->created_by = authUser() ? authUser()->id : null;
            $model->updated_by = authUser() ? authUser()->id : null;
        });

        static::updating(function($model) {
            $model->updated_by = authUser() ? authUser()->id : null;
        });
    }

    public function role_permissions() {
        $CI = get_instance();
        $CI->load->model('MCoreRolePerm');
        return $this->hasMany(MCoreRolePerm::class, 'role_id');
    }

}