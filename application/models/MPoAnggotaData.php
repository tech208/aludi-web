<?php
use Illuminate\Database\Eloquent\Model;

class MPoAnggotaData extends Model {

    protected $table = 'po_anggota_data';

    protected static function boot() {
        parent::boot();

        static::creating(function($model) {
            $model->created_by = authUser() ? authUser()->id : null;
            $model->updated_by = authUser() ? authUser()->id : null;
        });

        static::updating(function($model) {
            $model->updated_by = authUser() ? authUser()->id : null;
        });
    }

    public function statuse() {
        $CI = get_instance();
        $CI->load->model('MCoreRefStatus');
        return $this->belongsTo(MCoreRefStatus::class, 'status');
    }

    public function logs() {
        $CI = get_instance();
        $CI->load->model('MPoAnggotaDataLog');
        return $this->hasMany(MPoAnggotaDataLog::class, 'id_anggota_data');
    }

    public function jenise() {
        $CI = get_instance();
        $CI->load->model('MPoAnggotaDataJenis');
        return $this->hasMany(MPoAnggotaDataJenis::class, 'id_anggota_data');
    }

    public function lokasi_pemodals() {
        $CI = get_instance();
        $CI->load->model('MPoAnggotaDataLokasiPemodal');
        return $this->hasMany(MPoAnggotaDataLokasiPemodal::class, 'id_anggota_data');
    }

    public function lokasi_penerbits() {
        $CI = get_instance();
        $CI->load->model('MPoAnggotaDataLokasiPenerbit');
        return $this->hasMany(MPoAnggotaDataLokasiPenerbit::class, 'id_anggota_data');
    }


}