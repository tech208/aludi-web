<?php
use Illuminate\Database\Eloquent\Model;

class MActivityLog extends Model {

    protected $table = 'activity_logs';

    protected static function boot() {
        parent::boot();

        static::creating(function($model) {
            $model->created_by = authUser() ? authUser()->id : null;
            $model->updated_by = authUser() ? authUser()->id : null;
        });

        static::updating(function($model) {
            $model->updated_by = authUser() ? authUser()->id : null;
        });
    }

    
    public function users() {
        $CI = get_instance();
        $CI->load->model('MCoreUser');
        return $this->belongsTo(MCoreUser::class, 'user_id');
    }

}