<?php
use Illuminate\Database\Eloquent\Model;

class MPoAnggotaDataLokasiPenerbit extends Model {

    protected $table = 'po_anggota_data_lokasi_penerbit';

    protected static function boot() {
        parent::boot();

        static::creating(function($model) {
            $model->created_by = authUser() ? authUser()->id : null;
            $model->updated_by = authUser() ? authUser()->id : null;
        });

        static::updating(function($model) {
            $model->updated_by = authUser() ? authUser()->id : null;
        });
    }

    
    public function negaras() {
        $CI = get_instance();
        $CI->load->model('MCoreRefNegara');
        return $this->belongsTo(MCoreRefNegara::class, 'id_negara');
    }

    public function provinsis() {
        $CI = get_instance();
        $CI->load->model('MCoreRefProvinsi');
        return $this->belongsTo(MCoreRefProvinsi::class, 'id_provinsi');
    }

    public function statuse() {
        $CI = get_instance();
        $CI->load->model('MCoreRefStatus');
        return $this->belongsTo(MCoreRefStatus::class, 'status');
    }


}