<?php
use Illuminate\Database\Eloquent\Model;

class MEmailLog extends Model {

    protected $table = 'email_log';

    protected static function boot() {
        parent::boot();

        static::creating(function($model) {
            $model->created_at = date('Y-m-d H:i:s');
        });
        
        static::updating(function($model) {
            $model->updated_by = date('Y-m-d H:i:s');
        });
    }

}