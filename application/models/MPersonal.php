<?php
use Illuminate\Database\Eloquent\Model;


class MPersonal extends Model {

    protected $table = 'personals';

    protected static function boot() {
        parent::boot();

        static::creating(function($model) {
            $model->created_by = authUser() ? authUser()->id : null;
            $model->updated_by = authUser() ? authUser()->id : null;
        });

        static::updating(function($model) {
            $model->updated_by = authUser() ? authUser()->id : null;
        });
    }

    public function user() {
        $CI = get_instance();
        $CI->load->model('MCoreUser');
        return $this->belongsTo(MCoreUser::class, 'user_id');
    }


    public function avatar_path() {
        $path = base_url('app-assets/images/portrait/small/avatar-s-1.png');

        if (strtolower($this->gender ?? 'male') == 'female') {
            $path = base_url('app-assets/images/portrait/small/avatar-s-1-f.png');
        }

        if($this->photo) {
            $path = base_url($this->photo);
        }

        return $path;
    }

    public function statuse() {
        $CI = get_instance();
        $CI->load->model('MCoreRefStatus');
        return $this->belongsTo(MCoreRefStatus::class, 'status');
    }

    public function pendaftarans() {
        $CI = get_instance();
        $CI->load->model('MPoPendaftaran');
        return $this->belongsTo(MPoPendaftaran::class, 'id_pendaftaran');
    }
    
}