<?php
use Illuminate\Database\Eloquent\Model;

class MCoreUser extends Model {

    protected $table = 'core_users';

    protected static function boot() {
        parent::boot();

        static::creating(function($model) {
            $model->created_by = authUser() ? authUser()->id : null;
            $model->updated_by = authUser() ? authUser()->id : null;
        });

        static::updating(function($model) {
            $model->updated_by = authUser() ? authUser()->id : null;
        });
    }

    public function personal() {
        $CI = get_instance();
        $CI->load->model('MPersonal');
        return $this->hasOne(MPersonal::class, 'user_id');
    }

    public function role() {
        $CI = get_instance();
        $CI->load->model('MCoreRole');
        return $this->belongsTo(MCoreRole::class, 'role_id');
    }
}