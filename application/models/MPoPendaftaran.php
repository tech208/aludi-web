<?php
use Illuminate\Database\Eloquent\Model;

class MPoPendaftaran extends Model {

    protected $table = 'po_pendaftaran';

    protected static function boot() {
        parent::boot();

        static::creating(function($model) {
            $model->created_by = authUser() ? authUser()->id : null;
            $model->updated_by = authUser() ? authUser()->id : null;
        });

        static::updating(function($model) {
            $model->updated_by = authUser() ? authUser()->id : null;
        });
    }

    public function statuse() {
        $CI = get_instance();
        $CI->load->model('MRefCoreStatus');
        return $this->belongsTo(MRefCoreStatus::class, 'status');
    }

    public function personals() {
        $CI = get_instance();
        $CI->load->model('MPersonal');
        return $this->belongsTo(MPersonal::class, 'id_personal');
    }

}