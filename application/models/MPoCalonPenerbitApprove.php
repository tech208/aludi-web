<?php
use Illuminate\Database\Eloquent\Model;

class MPoCalonPenerbitApprove extends Model {

    protected $table = 'po_calon_penerbit_approve';

    protected static function boot() {
        parent::boot();

        static::creating(function($model) {
            $model->created_by = authUser() ? authUser()->id : null;
            $model->updated_by = authUser() ? authUser()->id : null;
        });

        static::updating(function($model) {
            $model->updated_by = authUser() ? authUser()->id : null;
        });
    }

    

}