<?php
use Illuminate\Database\Eloquent\Model;

class MPoAnggotaDataLog extends Model {

    protected $table = 'po_anggota_data_log';

    protected static function boot() {
        parent::boot();

        static::creating(function($model) {
            $model->created_by = authUser() ? authUser()->id : null;
            $model->updated_by = authUser() ? authUser()->id : null;
        });

        static::updating(function($model) {
            $model->updated_by = authUser() ? authUser()->id : null;
        });
    }

    public function statuse() {
        $CI = get_instance();
        $CI->load->model('MCoreRefStatus');
        return $this->belongsTo(MCoreRefStatus::class, 'status');
    }

    
    public function utamas() {
        $CI = get_instance();
        $CI->load->model('MPoAnggotaData');
        return $this->belongsTo(MPoAnggotaData::class, 'id_anggota_data');
    }


}