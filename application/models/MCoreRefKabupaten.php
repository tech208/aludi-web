<?php
use Illuminate\Database\Eloquent\Model;

class MCoreRefKabupaten extends Model {

    protected $table = 'core_ref_kabupaten';

    protected static function boot() {
        parent::boot();

        static::creating(function($model) {
            $model->created_by = authUser() ? authUser()->id : null;
            $model->updated_by = authUser() ? authUser()->id : null;
        });

        static::updating(function($model) {
            $model->updated_by = authUser() ? authUser()->id : null;
        });
    }

    
    public function provinsis() {
        $CI = get_instance();
        $CI->load->model('MCoreRefProvinsi');
        return $this->belongsTo(MCoreRefProvinsi::class, 'id_provinsi');
    }

}