<?php
use Illuminate\Database\Eloquent\Model;

class MCoreRefJenisPenerbit extends Model {

    protected $table = 'core_ref_jenis_penerbit';

    protected static function boot() {
        parent::boot();

        static::creating(function($model) {
            $model->created_by = authUser() ? authUser()->id : null;
            $model->updated_by = authUser() ? authUser()->id : null;
        });

        static::updating(function($model) {
            $model->updated_by = authUser() ? authUser()->id : null;
        });
    }

}