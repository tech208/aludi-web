<?php
use Illuminate\Database\Eloquent\Model;

class MPoPendaftaranTahapRekomendasi extends Model {

    protected $table = 'po_pendaftaran_tahap_rekomendasi';

    protected static function boot() {
        parent::boot();

        static::creating(function($model) {
            $model->created_by = authUser() ? authUser()->id : null;
            $model->updated_by = authUser() ? authUser()->id : null;
        });

        static::updating(function($model) {
            $model->updated_by = authUser() ? authUser()->id : null;
        });
    }

    public function statuse() {
        $CI = get_instance();
        $CI->load->model('MCoreRefStatus');
        return $this->belongsTo(MCoreRefStatus::class, 'status');
    }

    
    public function personals() {
        $CI = get_instance();
        $CI->load->model('MPersonal');
        return $this->hasOne(MPersonal::class, 'id_personal');
    }


    public function details() {
        $CI = get_instance();
        $CI->load->model('MPoPendaftaranTahapRekomendasiLog');
        return $this->hasMany(MPoPendaftaranTahapRekomendasiLog::class, 'id_rekomendasi')->orderBy('id', 'desc');
    }
    
    public function rekomendasis() {
        $CI = get_instance();
        $CI->load->model('MPoPendaftaranTahapRekomendasiOjkFile');
        // return $this->hasMany(MPoPendaftaranTahapRekomendasiOjkFile::class, 'id_rekomendasi')->orderBy('id', 'desc');
        return $this->hasOne(MPoPendaftaranTahapRekomendasiOjkFile::class, 'id_rekomendasi')->orderBy('id', 'desc');
    }

}