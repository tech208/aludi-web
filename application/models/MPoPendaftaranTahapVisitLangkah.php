<?php
use Illuminate\Database\Eloquent\Model;

class MPoPendaftaranTahapVisitLangkah extends Model {

    protected $table = 'po_pendaftaran_tahap_visit_langkah';

    protected static function boot() {
        parent::boot();

        static::creating(function($model) {
            $model->created_by = authUser() ? authUser()->id : null;
            $model->updated_by = authUser() ? authUser()->id : null;
        });

        static::updating(function($model) {
            $model->updated_by = authUser() ? authUser()->id : null;
        });
    }

    public function statuse() {
        $CI = get_instance();
        $CI->load->model('MCoreRefStatus');
        return $this->belongsTo(MCoreRefStatus::class, 'status');
    }

    
    public function personals() {
        $CI = get_instance();
        $CI->load->model('MPersonal');
        return $this->hasOne(MPersonal::class, 'id_personal');
    }

}