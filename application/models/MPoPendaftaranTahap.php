<?php
use Illuminate\Database\Eloquent\Model;

class MPoPendaftaranTahap extends Model {

    protected $table = 'po_pendaftaran_tahap';

    protected static function boot() {
        parent::boot();

        static::creating(function($model) {
            $model->created_by = authUser() ? authUser()->id : null;
            $model->updated_by = authUser() ? authUser()->id : null;
        });

        static::updating(function($model) {
            $model->updated_by = authUser() ? authUser()->id : null;
        });
    }

    public function statuse() {
        $CI = get_instance();
        $CI->load->model('MCoreRefStatus');
        return $this->belongsTo(MCoreRefStatus::class, 'status');
    }

    
    public function personals() {
        $CI = get_instance();
        $CI->load->model('MPersonal');
        return $this->belongsTo(MPersonal::class, 'id_personal');
    }
    
    public function tahap_satu() {
        $CI = get_instance();
        $CI->load->model('MPoPendaftaranTahapSatu');
        return $this->hasOne(MPoPendaftaranTahapSatu::class, 'id_tahap');
    }

    public function tahap_pembayaran() {
        $CI = get_instance();
        $CI->load->model('MPoPendaftaranTahapPembayaran');
        return $this->hasOne(MPoPendaftaranTahapPembayaran::class, 'id_tahap');
    }

    public function tahap_dua() {
        $CI = get_instance();
        $CI->load->model('MPoPendaftaranTahapDua');
        return $this->hasOne(MPoPendaftaranTahapDua::class, 'id_tahap');
    }

    public function tahap_visit() {
        $CI = get_instance();
        $CI->load->model('MPoPendaftaranTahapVisit');
        return $this->hasMany(MPoPendaftaranTahapVisit::class, 'id_tahap');
    }
    
    public function tahap_rekomendasi() {
        $CI = get_instance();
        $CI->load->model('MPoPendaftaranTahapRekomendasi');
        return $this->hasOne(MPoPendaftaranTahapRekomendasi::class, 'id_tahap');
    }

}