<?php

class po_peserta_pulling extends sntr_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('MPoAnggotaData');

        $this->load->model('MCoreRefStatus');
        $this->load->model('MCoreUser');
        $this->load->model('MCoreRefProvinsi');
        $this->load->model('MCoreRefKabupaten');
        
        ini_set('date.timezone', 'Asia/Jakarta');
    }

    public function index() {
        $data = MPoAnggotaData::orderBy('id', 'desc')->get();
        
        
        $query = $this->db->query("SELECT
            SUM(`jumlah_penerbit`) AS jumlah_penerbit,
            SUM(`jumlah_penerbit_funded`) AS jumlah_penerbit_funded,
            SUM(`jumlah_investor`) AS jumlah_investor,
            SUM(`jumlah_investor_dividen`) AS jumlah_investor_dividen,
            SUM(`jumlah_dana`) AS jumlah_dana,
            SUM(`jumlah_dividen`) AS jumlah_dividen
        FROM `po_anggota_data` AS pad
        WHERE pad.`status`=2
        ");
        $data->jumlah = @$query->result_array()[0];

        $this->setParam('data', $data);
        $this->setContentView('web/po_peserta_pulling/index');
        $this->render('Pulling Penyelenggara');
    }

    public function detail($id) {
        $data = MPoAnggotaData::where("id",@$id)->orderBy('id', 'desc')->first();
        
        // MasDebugPree($data->jenise,1);
        $jenis = array();
        $jenis_total = 0;
        foreach ($data->jenise as $key => $value) {
            $jenis[$key]["label"]   = $value->jenis;
            $jenis[$key]["y"]       = $value->jumlah;
            $jenis_total            += (int) $value->jumlah;
        }
        $data->jenis        = $jenis;
        $data->jenis_total  = $jenis_total;
        
        $lokasi_penerbit = array();
        $lokasi_penerbit_total = 0;
        foreach ($data->lokasi_penerbits as $key => $value) {
            $lokasi_penerbit[$key]["label"]   = @$value->provinsis->nama;
            $lokasi_penerbit[$key]["y"]       = $value->jumlah;
            $lokasi_penerbit_total            += (int) $value->jumlah;
        }
        
        $data->lokasi_penerbit        = $lokasi_penerbit;
        $data->lokasi_penerbit_jumlah  = $data->lokasi_penerbits->count();

        $lokasi_pemodal = array();
        $lokasi_pemodal_total = 0;
        foreach ($data->lokasi_pemodals as $key => $value) {
            $lokasi_pemodal[$key]["label"]   = @$value->provinsis->nama;
            $lokasi_pemodal[$key]["y"]       = $value->jumlah;
            $lokasi_pemodal_total            += (int) $value->jumlah;
        }

        $query_lokasi_pemodal_total_negara = $this->db->query("SELECT
            count(padlp.id) jumlah
        FROM `po_anggota_data_lokasi_pemodal` AS padlp
        WHERE padlp.`id_anggota_data`=$data->id
        Group By padlp.id_negara
        ");
        $lokasi_pemodal_total_negara = @$query_lokasi_pemodal_total_negara->result_array()[0];

        
        $data->lokasi_pemodal               = $lokasi_pemodal;
        $data->lokasi_pemodal_total_kota    = $data->lokasi_penerbits->count();
        $data->lokasi_pemodal_total_negara  = $lokasi_pemodal_total_negara['jumlah'];
        $data->lokasi_pemodal_jumlah        = $lokasi_pemodal_total;

        $this->setParam('data', $data);
        $this->setContentView('web/po_peserta_pulling/detail');
        $this->render('Pulling Detail Penyelenggara');
    }

    public function ringkasan() {
        $data = new stdClass;

        
        $query_raw = $this->db->query("SELECT
            SUM(`jumlah_penerbit`) jumlah_penerbit,
            SUM(`jumlah_penerbit_funded`) jumlah_penerbit_funded,
            SUM(`jumlah_investor`) jumlah_investor,
            SUM(`jumlah_investor_dividen`) jumlah_investor_dividen,
            SUM(`jumlah_dana`) jumlah_dana,
            SUM(`jumlah_dividen`) jumlah_dividen,
            SUM(`jumlah_dividen_penerima`) jumlah_dividen_penerima
        FROM `po_anggota_data` pad
        WHERE pad.`status`=2
        ");
        $data_raw = @$query_raw->result_array()[0];

        
        $data->jumlah_penerbit          = $data_raw['jumlah_penerbit'];
        $data->jumlah_penerbit_funded   = $data_raw['jumlah_penerbit_funded'];
        $data->jumlah_investor          = $data_raw['jumlah_investor'];
        $data->jumlah_investor_dividen  = $data_raw['jumlah_investor_dividen'];
        $data->jumlah_dana              = $data_raw['jumlah_dana'];
        $data->jumlah_dividen           = $data_raw['jumlah_dividen'];
        $data->jumlah_dividen_penerima  = $data_raw['jumlah_dividen_penerima'];
        
        
        $query_jenis_raw = $this->db->query("SELECT
                padj.`id`,
                padj.`id_anggota_data`,
                padj.`jenis`,
                crjp.`nama` jenis_nama,
                SUM(padj.`jumlah`) jumlah
            FROM `po_anggota_data_jenis` padj
            LEFT JOIN `core_ref_jenis_penerbit` crjp ON crjp.`id`=padj.`jenis`
            WHERE padj.`status`=2
            GROUP BY padj.`jenis`      
        ");
        $jenis_raw = @$query_jenis_raw->result_array();
        
        $jenis = array();
        $jenis_total = 0;
        foreach ($jenis_raw as $key => $value) {
            $jenis[$key]["label"]   = $value['jenis_nama'];
            $jenis[$key]["y"]       = $value['jumlah'];
            $jenis_total            += (int) $value['jumlah'];
        }
        $data->jenis        = $jenis;
        $data->jenis_total  = $jenis_total;

        
        $query_lokasi_pemodal_total_negara = $this->db->query("SELECT COUNT(A.id_negara) as jumlah FROM (
SELECT
            padlp.id_negara
        FROM `po_anggota_data_lokasi_pemodal` AS padlp
        WHERE 1=1
        Group By padlp.id_negara) A
        ");
        $lokasi_pemodal_total_negara = @$query_lokasi_pemodal_total_negara->result_array()[0];
        $data->lokasi_pemodal_total_negara  = $lokasi_pemodal_total_negara['jumlah'];
        
        
        
        $query_penerbit_raw = $this->db->query("SELECT * FROM (SELECT
            `id_kota`,
            crk.`nama` nama,
            SUM(`jumlah`) jumlah
        FROM `po_anggota_data_lokasi_penerbit` padlp
        LEFT JOIN `core_ref_kabupaten` crk ON crk.`id`=padlp.`id_kota`
        WHERE padlp.`status`='2'
        GROUP BY padlp.id_kota) A ORDER BY jumlah  DESC       
        ");
        $penerbit_raw = @$query_penerbit_raw->result_array();
        
        
        $lokasi_penerbit = array();
        $lokasi_penerbit_total = 0;
        foreach ($penerbit_raw as $key => $value) {
            $lokasi_penerbit[$key]["label"]   = @$value['nama'];
            $lokasi_penerbit[$key]["y"]       = $value['jumlah'];
            $lokasi_penerbit_total            += (int) $value['jumlah'];
        }
        
        $data->lokasi_penerbit        = $lokasi_penerbit;
        $data->lokasi_penerbit_jumlah  = count($penerbit_raw);
        
        
        $query_pemodal_raw = $this->db->query("SELECT
                padlpe.`id_provinsi`,
                crp.`nama`,
                SUM(`jumlah`) jumlah
            FROM `po_anggota_data_lokasi_pemodal` padlpe
            LEFT JOIN core_ref_provinsi crp ON crp.id = padlpe.`id_provinsi`
            GROUP BY id_provinsi
        ");
        $pemodal_raw = @$query_pemodal_raw->result_array();
        
        $lokasi_pemodal = array();
        $lokasi_pemodal_total = 0;
        foreach ($pemodal_raw as $key => $value) {
            $lokasi_pemodal[$key]["label"]   = @$value['nama'];
            $lokasi_pemodal[$key]["y"]       = $value['jumlah'];
            $lokasi_pemodal_total            += (int) $value['jumlah'];
        }


        $data->lokasi_pemodal               = $lokasi_pemodal;
        $data->lokasi_pemodal_total_kota    = count($pemodal_raw);
        $data->lokasi_pemodal_jumlah        = $lokasi_pemodal_total;



        $this->setParam('data', $data);
        $this->setContentView('web/po_peserta_pulling/ringkasan');
        $this->render('Ringkasan Pulling Penyelenggara');
    }

    
    public function printxlx() {
        $data = MPoAnggotaData::orderBy('id', 'desc')->get();
        
        $result['data']         = $data;
        // return $this->load->view('laporan/peserta_list_pulling',$result);  
        
        ini_set('memory_limit', '256M');
        ini_set('max_execution_time', 0);
        header('Content-Type: application/vnd.ms-excel');
        header("Content-Disposition: attachment;Filename=report.xls");
        header('Cache-Control: max-age=0');
        $result = $this->load->view('laporan/peserta_list_pulling',$result,true);
        echo $result;
        exit;
    }
}