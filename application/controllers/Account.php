<?php

class Account extends sntr_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        
        $get    = $this->input->get();
        $url    = @$get['url'];
        
        if (!empty($url)) {
            set_flashdata('info_notice', 'Tambahkan Tanda tangan, untuk melanjutkan! ');
        }
        
        $this->setParam('url', @$url);
        $this->setContentView('web/account/index');
        $this->render('Manage Akun');
    }

    public function save_account() {
        $post = $this->input->post();

        $personal = personal();
        if(!empty($_FILES['photo']['name'])) {
            $config['upload_path']          = './uploads/';
            $config['allowed_types']        = 'gif|jpg|png|jpeg';
            $config['max_size']             = 5120;
            $config['max_width']            = 4000;
            $config['max_height']           = 4000;

            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('photo')) {
                set_flashdata('error', $this->upload->display_errors());
                return redirect_with_input('account', []);
            }

            $upload_data = $this->upload->data();
            $personal->photo = 'uploads/'. $upload_data['file_name'];
        }

        if(!empty($_FILES['ttd']['name'])) {
            $config['upload_path']          = './uploads/ttd/';
            $config['allowed_types']        = 'gif|jpg|png|jpeg';
            $config['encrypt_name']         = TRUE;
            $config['max_size']             = 5120;
            $config['max_width']            = 4000;
            $config['max_height']           = 4000;

            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('ttd')) {
                set_flashdata('error', $this->upload->display_errors());
                return redirect_with_input('account', []);
            }

            $upload_data = $this->upload->data();
            $personal->tanda_tangan = $upload_data['file_name'];
        }

        $personal->personal_email = $this->input->post('email_pribadi');
        $personal->save();

        if (!empty($post['url'])) {
            // set_flashdata('success_notice', 'Data berhasil disimpan');
            return redirect(site_url($post['url']));
            exit();
        }else {
            set_flashdata('success', 'Data berhasil disimpan');
            return redirect('account');
        }
    }


    public function changepassword() {
        if (strtolower($this->input->method()) == 'post') {
            $user = authUser();
            $current_password = md5($this->input->post('current_password'));
            if ($user->password != $current_password) {
                set_flashdata('error', 'Password lama tidak sesuai');
                return redirect_with_input('account/changepassword', []);
            }

            $new_password = md5($this->input->post('new_password'));
            $confirm_password = md5($this->input->post('confirm_new_password'));

            if($new_password != $confirm_password) {
                set_flashdata('error', 'Password baru tidak terkonfirmasi dengan benar');
                return redirect_with_input('account/changepassword', []);
            }

            $user->password = md5($this->input->post('new_password'));
            $user->save();

            set_flashdata('success', 'Password berhasil diperbarui');
            redirect('account/changepassword');
            return;
        }
        $this->setContentView('web/account/changepassword');
        $this->render('Manage Akun');
    }

    public function profile() {
        if (strtolower($this->input->method()) == 'post' && has_permission('UPDATE_PROFILE')) {
            $data = $this->input->post();
            $personal = personal();
            $personal->name = $data['nama_lengkap'];
            $personal->idcard_number = $data['nik'];
            $personal->npwp_number = $data['npwp'];
            $personal->place_of_birth = $data['tempat_lahir'];
            $personal->date_of_birth = $data['tanggal_lahir'];
            $personal->gender = $data['jenis_kelamin'];
            $personal->address = $data['alamat'];
            $personal->phone = $data['no_hp'];
            $personal->save();

            if(!$personal->bank) {
                $this->load->model('MBank');
                $bank = new MBank;
                $bank->personal_id = $personal->id;
                $bank->bank = $data['bank_name'];
                $bank->account = $data['bank_account'];
                $bank->save();
            } else {
                $bank = $personal->bank;
                $bank->bank = $data['bank_name'];
                $bank->account = $data['bank_account'];
                $bank->save();
            }

            set_flashdata('success', 'Data berhasil disimpan');
            return redirect('account/profile');
        }

        $this->setContentView('web/account/profile');
        $this->render('Personal Data');
    }


}