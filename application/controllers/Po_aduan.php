<?php

class Po_aduan extends sntr_Controller {

    public function __construct() {
        parent::__construct();
        
        $this->load->model('MPoAduan');
        $this->load->model('MCoreRefStatus');
        
        ini_set('date.timezone', 'Asia/Jakarta');
    }

    public function index() {
        $data = MPoAduan::
                // where('status', 2)
                orderBy('id', 'desc')
                ->get();
                
        $status     = "";
        if (@$_POST['status']!="" AND @$_POST['status']!="All") {
            $data = MPoAduan::
                    where('status', $_POST['status'])
                    ->orderBy('id', 'desc')
                    ->get();
            $status     = @$_POST['status'];
        }

        $refStatus  = MCoreRefStatus::
        where('grup', 3)
        ->orderBy('id', 'desc')
        ->get();
        
        
        $this->setParam('status', $status);
        $this->setParam('refStatus', $refStatus);
        $this->setParam('data', $data);
        $this->setContentView('web/po_aduan/index');
        $this->render('Aduan');
    }



    public function form($id,$detail='') {          
        $data = MPoAduan::find($id);
        
        $ref_status = MCoreRefStatus::
                where('grup', 3)
                ->orderBy('id', 'desc')
                ->get();
        
        
        $this->setParam('ref_status', $ref_status);
        $this->setParam('detail', $detail);
        $this->setParam('data', $data);
        $this->setContentView('web/po_aduan/form');

        $det_detail = (!empty($detail)) ?  "Detail ": "" ;
        $this->render($det_detail.'ANDUAN');
    }

    public function save() {        
        $data   = $this->input->post();

        $this->db->trans_begin();

        $id = @$data['id'];
    
        $params= array(
            'status'        => $data['status'],
            'updated_at'    => date('Y-m-d H:i:s'),
            'updated_by'    => authUser()->id
        );

        $this->db->update('po_aduan', $params, array('id' => $id));
        logs("POST","po_aduan/save/",$id,"po_update","update"); //activity_logs => "method,route,table_id,table_name,table_aksi"

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }
        else{
            $this->db->trans_commit();
        }
        
        
        set_flashdata('success', 'Data berhasil disimpan');
        redirect('/po_aduan/form/'.$id);

    }

    public function delete($id) {

    }

    public function printxlx() {
        $data = MPoAduan::get();
        
        $result['data']         = $data;
        // return $this->load->view('laporan/aduan_list',$result);  
        
        ini_set('memory_limit', '256M');
        ini_set('max_execution_time', 0);
        header('Content-Type: application/vnd.ms-excel');
        header("Content-Disposition: attachment;Filename=report.xls");
        header('Cache-Control: max-age=0');
        $result = $this->load->view('laporan/aduan_list',$result,true);
        echo $result;
        exit;
    }

}