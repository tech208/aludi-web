<?php

use Illuminate\Database\Capsule\Manager as DB;
use App\Constants\UserManagement;
use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;

class User extends sntr_Controller {

    public function __construct() {
        parent::__construct();
    }

    
    public function index() {
        $this->load->model('MUser');
        $data = MUser::get();
        
        if($data->count()>0){
            foreach($data as $key=> $val){
                if($val->personal->status==0){
                    unset($data[$key]);
                }
            }
        }
        
        
        $this->setContentView('web/user/index');
        $this->setParam('data', $data);
        $this->render('User Management');
    }

    // public function index() {
    //     if (strtolower($this->input->method()) == 'get') {
    //         $this->load->model('MDepartment');
    //         $this->load->model('MDivision');
    //         $this->setParam('departments', MDepartment::get());
    //         $this->setParam('divisions', MDivision::get());
    //         $this->setContentView('web/user/index');
    //         $this->render('User Management');
    //         return;
    //     }

    //     $columns = array( 
    //         0 => 'users.id', 
    //         1 => 'p.name',
    //         2 => 'jt.job_titles',
    //         3 => 'p.join_date',
    //         4 => 'p.phone',
    //         5 => 'jh.job_status',
    //         6 => 'p.status',
    //     );

    //     $this->load->model('MUser');
    //     $show = $this->input->post('length') ? $this->input->post('length') : 10;
    //     $page = $this->input->post('start');
    //     $order = $columns[$this->input->post('order')[0]['column']];
    //     $dir = $this->input->post('order')[0]['dir'];

    //     $users = MUser::join('personals as p', 'p.user_id', '=', 'users.id')
    //     ->join('job_histories as jh', 'jh.personal_id', '=', 'p.id')
    //     ->join('job_titles as jt', 'jt.id', '=', 'jh.job_title_id')->groupBy('jh.personal_id');

    //     if($this->input->post('department')) {
    //         $users->where('jt.departement_id', $this->input->post('department'));
    //     }

    //     if($this->input->post('division')) {
    //         $users->where('jt.division_id', $this->input->post('division'));
    //     }

    //     $total = $users->get()->count();
    //     if ($total > 0) {
    //         $users->skip($page)->take($show);
    //     }

    //     $users->select('users.id', 'p.name', 'jt.job_titles', 'p.join_date', 'p.phone', 'jh.job_status', 'p.status');
    //     $users = $users->orderBy($order, $dir)->get();

    //     $users = $users->map(function($u) {
    //         if (has_permission('READ_USER')) {
    //             $u->name = '<a href="'. site_url("user/detail/". $u->id) .'">'. $u->name .'</a>';
    //         }
    //         $u->status = \App\Constants\UserManagement::STATUS_LIST[$u->status];

    //         return $u;
    //     });

    //     echo json_encode([
    //         "draw"            => intval($this->input->post('draw')),  
    //         "recordsTotal"    => $total,  
    //         "recordsFiltered" => $total, 
    //         "data"            => $users->toArray()
    //     ]);
    //     exit;
    // }

    public function export_data($department = null, $division = null) {
        if (!has_permission('EXPORT_USER')) {
            return redirect('user');
        }

        $this->load->model('MUser');

        $users = MUser::join('personals as p', 'p.user_id', '=', 'users.id')
        ->join('job_histories as jh', 'jh.personal_id', '=', 'p.id')
        ->join('job_titles as jt', 'jt.id', '=', 'jh.job_title_id')->groupBy('jh.personal_id');

        if($department) {
            $users->where('jt.departement_id', $department);
        }

        if($division) {
            $users->where('jt.division_id', $department);
        }

        $users->select('users.id', 'p.*', 'jt.job_titles', 'jh.effective_date', 'jh.job_status', 'jh.employment_period', 'jh.basic_salary');
        $users = $users->orderBy('p.name', 'asc')->get();

        $spreadsheet = new Spreadsheet();

        // Set document properties
        $spreadsheet->getProperties()->setCreator('PT Santara Daya Inspira')
        ->setLastModifiedBy(personal()->name)
        ->setTitle('Data_Karyawan-'. date('d F Y'))
        ->setSubject('Karyawan Santara')
        ->setDescription('This document contain list of PT Santara Daya Inspira employee.');

        // Add some data
        $spreadsheet->setActiveSheetIndex(0)
        ->setCellValue('A1', 'Nama')
        ->setCellValue('B1', 'Jabatan')
        ->setCellValue('C1', 'ID Card')
        ->setCellValue('D1', 'Gender')
        ->setCellValue('E1', 'Marital Status')
        ->setCellValue('F1', 'Religion')
        ->setCellValue('G1', 'Place Of Birth')
        ->setCellValue('H1', 'Date Of Birth')
        ->setCellValue('I1', 'Phone')
        ->setCellValue('J1', 'Personal Email')
        ->setCellValue('K1', 'Address')
        ->setCellValue('L1', 'NPWP')
        ->setCellValue('M1', 'Join Date')
        ->setCellValue('N1', 'Photo')
        ->setCellValue('O1', 'Hobby')
        ->setCellValue('P1', 'Dream')
        ->setCellValue('Q1', 'Is Employee')
        ->setCellValue('R1', 'Status')
        ->setCellValue('S1', 'Effective Date')
        ->setCellValue('T1', 'Job Status')
        ->setCellValue('U1', 'Employment Period')
        ->setCellValue('V1', 'Basic Salary');

        // Miscellaneous glyphs, UTF-8
        $i=2;
        foreach($users as $p) {
            $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue('A'.$i, $p->name)
            ->setCellValue('B'.$i, $p->job_titles)
            ->setCellValue('C'.$i, " ".$p->idcard_number)
            ->setCellValue('D'.$i, $p->gender)
            ->setCellValue('E'.$i, $p->marital_status)
            ->setCellValue('F'.$i, $p->religion)
            ->setCellValue('G'.$i, $p->place_of_birth)
            ->setCellValue('H'.$i, $p->date_of_birth ? date('d F Y', strtotime($p->date_of_birth)) : '')
            ->setCellValue('I'.$i, $p->phone)
            ->setCellValue('J'.$i, $p->personal_email)
            ->setCellValue('K'.$i, $p->address)
            ->setCellValue('L'.$i, $p->wpwp_number)
            ->setCellValue('M'.$i, $p->join_date ? date('d F Y', strtotime($p->join_date)) : '')
            ->setCellValue('N'.$i, base_url($p->photo))
            ->setCellValue('O'.$i, $p->hobby)
            ->setCellValue('P'.$i, $p->dream)
            ->setCellValue('Q'.$i, $p->is_employee)
            ->setCellValue('R'.$i, UserManagement::STATUS_LIST[$p->status])
            ->setCellValue('S'.$i, $p->effective_date ? date('d F Y', strtotime($p->effective_date)) : '')
            ->setCellValue('T'.$i, $p->job_status)
            ->setCellValue('U'.$i, $p->employment_period)
            ->setCellValue('V'.$i, $p->basic_salary);
            $i++;
        }

        // Rename worksheet
        $spreadsheet->getActiveSheet()->setTitle('Data_Karyawan-'. date('d F Y'));

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);

        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Data_Karyawan-'. date('d F Y') .'.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
    }

    public function create() {
        $this->load->model('MRole');
        $this->load->model('MJobTitle');
        $job_titles = MJobTitle::get();
        $roles = MRole::get();
        $this->setContentView('web/user/create')
        ->setParams(compact('job_titles', 'roles'))
        ->render('User Management');
    }

    public function insert() {
        if(!has_permission('CREATE_USER')) {
            return redirect('user');
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]', [
            'is_unique' => 'Email already used by other'
        ]);
        $this->form_validation->set_rules('id_role', 'Role', 'required');
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('id_karyawan', 'ID Karyawan', 'required');
        $this->form_validation->set_rules('tgl_bergabung', 'Tanggal Bergabung', 'required');
        $this->form_validation->set_rules('id_jabatan', 'Jabatan', 'required');
        $this->form_validation->set_rules('id_jenis_karyawan', 'Jenis Karyawan', 'required');
        if(!$this->form_validation->run()) {
            $this->session->set_flashdata('error', validation_errors());
            redirect_with_input('user/create', $this->input->post());
        }

        try {
            DB::beginTransaction();

            $data = $this->input->post();
            $this->load->model('MUser');
            $this->load->model('MPersonal');
            $this->load->model('MJobHistory');
            $user = new MUser();
            $user->uuid = $data['id_karyawan'];
            $user->email = $data['email'];
            $user->password = md5($data['email']);
            $user->role_id = $data['id_role'];
            $user->save();

            $personal = new MPersonal();
            $personal->name = $data['name'];
            $personal->join_date = $data['tgl_bergabung'];
            $personal->status = UserManagement::STATUS_ACTIVE;
            $user->personal()->save($personal);

            $job_history = new MJobHistory();
            $job_history->job_title_id = $data['id_jabatan'];
            $job_history->job_status = $data['id_jenis_karyawan'];
            $personal->jobHistory()->save($job_history);

            DB::commit();
            set_flashdata('success', 'Data berhasil disimpan');
            redirect('user');
        } catch (Exception $e) {
            DB::rollback();
            $this->session->set_flashdata('error', $e->getMessage());
            redirect_with_input('user/create', $this->input->post());
        }

    }

    public function detail($id) {
        $this->load->model('MUser');
        $this->load->model('MRole');
        $roles = MRole::get();
        $user = MUser::find($id);
        
        if(!$user || !has_permission('READ_USER')) {
            redirect('user');
        }

        $this->setContentView('web/user/detail');
        $this->setParam('user', $user);
        $this->setParam('roles', $roles);
        $this->render('User Management');
    }

    public function update_detail($user_id) {
        if(!has_permission('UPDATE_USER')) {
            return redirect('user');
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('id_role', 'Role', 'required');
        $this->form_validation->set_rules('name', 'Name', 'required');
        // $this->form_validation->set_rules('id_karyawan', 'ID Karyawan', 'required');
        // $this->form_validation->set_rules('tgl_bergabung', 'Tanggal Bergabung', 'required');
        // $this->form_validation->set_rules('no_ktp', 'No KTP', 'required');
        // $this->form_validation->set_rules('status', 'Status Karyawan', 'required');
        // $this->form_validation->set_rules('tempat_lahir', 'Tempat Lahir', 'required');
        // $this->form_validation->set_rules('tgl_lahir', 'TGL Lahir', 'required');
        // $this->form_validation->set_rules('religion', 'Religion', 'required');
        // $this->form_validation->set_rules('gender', 'Gender', 'required');
        // $this->form_validation->set_rules('marital_status', 'Martial Status', 'required');
        // $this->form_validation->set_rules('address', 'Address', 'required');
        if(!$this->form_validation->run()) {
            // $this->session->set_flashdata('error', validation_errors());
            $this->session->set_flashdata('error', "Gagal Simpan");
            redirect('user/detail/'. $user_id);
        }


        $data = $this->input->post();
        $user = MUser::find($user_id);
        $personal = $user->personal;

        $user->uuid = $data['id_karyawan'];
        $user->role_id = $data['id_role'];
        $user->save();

        $personal->name = $data['name'];
        $personal->idcard_number = $data['no_ktp'];
        $personal->join_date = $data['tgl_bergabung'];
        $personal->status = $data['status'];
        $personal->npwp_number = $data['npwp'];
        $personal->place_of_birth = $data['tempat_lahir'];
        $personal->date_of_birth = $data['tgl_lahir'];
        $personal->religion = $data['religion'];
        $personal->gender = $data['gender'];
        $personal->marital_status = $data['marital_status'];
        $personal->address = $data['address'];
        $personal->save();

        set_flashdata('success', 'Data berhasil disimpan');
        redirect('user/detail/'. $user_id);
    }

    public function pekerjaan($id) {
        $this->load->model('MUser');
        $user = MUser::find($id);
        
        if(!$user) {
            redirect('user');
        }

        if(!$user->personal) {
            redirect('user');
        }

        $this->load->model('MAllowance');
        $this->load->model('MBenefit');
        $this->load->model('MAccess');

        $data = $user->personal->jobHistories;
        foreach ($data as $key => $value) {
            $accesses = MAccess::whereIn('id', explode(',', $value->access))->get();
            $data[$key]->akses = $accesses;
        }

        // $allowances = MAllowance::whereIn('id', explode(',', $user->personal->jobHistory->allowance))->get();
        // $benefits = MBenefit::whereIn('id', explode(',', $user->personal->jobHistory->benefit))->get();
        // $accesses = MAccess::whereIn('id', explode(',', $user->personal->jobHistory->access))->get();

        $this->setParam('data', $data);
        // $this->setParam('allowances', $allowances);
        // $this->setParam('benefits', $benefits);
        // $this->setParam('accesses', $accesses);

        $this->setContentView('web/user/pekerjaan');
        $this->setParam('user', $user);
        $this->render('User Management');
    }

    public function edit_pekerjaan($id) {
        $this->load->model('MUser');
        $this->load->model('MAllowance');
        $this->load->model('MBenefit');
        $this->load->model('MAccess');
        $this->load->model('MJobTitle');
        $job_titles = MJobTitle::get();
        $user = MUser::find($id);
        $allowances = MAllowance::get();
        $benefits = MBenefit::get();
        $accesses = MAccess::get();

        if(!$user) {
            redirect('user');
        }

        $this->setParam('user', $user);
        $this->setParam('allowances', $allowances);
        $this->setParam('benefits', $benefits);
        $this->setParam('accesses', $accesses);
        $this->setParam('job_titles', $job_titles);
        $this->setContentView('web/user/edit_pekerjaan');
        $this->render('User Management');
    }

    public function form_pekerjaan($user_id="",$id="") {
        $this->load->model('MUser');
        $this->load->model('MAccess');
        $this->load->model('MJobTitle');
        $this->load->model('MJobHistory');

        $job_titles = MJobTitle::get();
        $user       = MUser::find($user_id);
        $accesses   = MAccess::get()->toArray();
        
        
        if (!empty($id)) {
            $data = MJobHistory::find($id)->toArray();
            if(!$data) {
                redirect('user');
            }

            $access = explode(",",$data['access']);

            foreach ($accesses as $key => $value) {
                if( in_array( $value['id'],$access ) ){
                    $accesses[$key]['selected'] = "selected";
                }else {
                    $accesses[$key]['selected'] = "";
                }
            }
        }
        $data = (!empty($data))?$data:array();

        // MasDebugPree($accesses,1);

        $this->setParam('data', $data);
        $this->setParam('user', $user);
        $this->setParam('accesses', $accesses);
        $this->setParam('job_titles', $job_titles);
        $this->setContentView('web/user/form_pekerjaan');
        $this->render('Form Pekerjaan');
    }

    public function save_pekerjaan() {
        $data       = $this->input->post();
        $user_id    = $data['user_id'];
        $id         = @$data['id'];

        if(!has_permission('UPDATE_PEKERJAAN')) {redirect('user/pekerjaan/'. $user_id);}

        $this->load->library('form_validation');
        $this->form_validation->set_rules('posisi', 'Posisi', 'required');
        $this->form_validation->set_rules('effective_date', 'Tanggal Efektif', 'required');
        $this->form_validation->set_rules('start_date', 'Tanggal Mulai', 'required');
        $this->form_validation->set_rules('period', 'Period', 'required');
        $this->form_validation->set_rules('status', 'Status', 'required');
        if(!$this->form_validation->run()) {
            set_flashdata('error', 'Silahkan di lengkapin');
            // set_flashdata('error', validation_errors());
            redirect('user/form_pekerjaan/'. $user_id.'/'.$id);
        }

        $this->load->model('MUser');
        $this->load->model('MJobHistory');

        $user = MUser::find($user_id);

        if(!$user) {redirect('user');}

        if (!$user->personal) {
            set_flashdata('error', 'Selected account currently not complete the personal data, complete first.');
            redirect('user');
        }

        
        $this->db->trans_begin();
        $params = array(
            "personal_id"   => $user->personal->id,
            "job_title_id"  => $data['posisi'],
            "effective_date"=> $data['effective_date'],
            "start_date"    => $data['start_date'],
            "end_date"      => $data['end_date'],
            "saat_ini"      => ($data['saat_ini']==1)?"1":"0",
            "job_status"    => $data['status'],
            "employment_period" => $data['period'],
            "access"        => implode(",",$data['hak_akses']),
            "status"        => 2,
        );
        
        if($id){
            $params["updated_at"]    = date('Y-m-d H:i:s');
            $params["updated_by"]    = authUser()->id;

            $this->db->update('job_histories', $params,array("id" => $id));
        }else {
            $params["created_at"]    = date('Y-m-d H:i:s');
            $params["created_by"]    = authUser()->id;

            $this->db->insert('job_histories', $params);
            $id = $this->db->insert_id();
        }
        logs("POST","user/save_pekerjaan/",$id,"job_histories",(!empty($data['id']))?"update":"insert"); //activity_logs => "method,route,table_id,table_name,table_aksi"
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }
        else{
            $this->db->trans_commit();
        }

        set_flashdata('success', 'Data berhasil disimpan');
        redirect('user/pekerjaan/'. $user_id);
    }

    public function delete_pekerjaan($id) {
        $this->load->model('MJobHistory');
        $this->load->model('MPersonal');
        $data = MJobHistory::find($id);
        $user = MPersonal::find($data->personal_id);
        if(!$data) {
            return redirect('user');
        }

        $this->db->trans_begin();
        $this->db->where('id', $id);
        $this->db->delete('job_histories');
        logs("POST","user/delete_pekerjaan/",$id,"job_histories","delete"); //activity_logs => "method,route,table_id,table_name,table_aksi"
        
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }
        else{
            $this->db->trans_commit();
        }
        set_flashdata('success', 'Data berhasil disimpan');
        redirect('user/pekerjaan/'. $user->user_id);
    }

    public function update_pekerjaan($user_id) {

        if(!has_permission('UPDATE_PEKERJAAN')) {
            redirect('user/pekerjaan/'. $user_id);
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('posisi', 'Posisi', 'required');
        $this->form_validation->set_rules('effective_date', 'Effective Date', 'required');
        $this->form_validation->set_rules('period', 'Period', 'required');
        $this->form_validation->set_rules('status', 'Status', 'required');
        $this->form_validation->set_rules('basic_salary', 'Basic Salary', 'required');

        if(!$this->form_validation->run()) {
            $this->session->set_flashdata('error', validation_errors());
            redirect('user/edit_pekerjaan/'. $user_id);
        }
        $this->load->model('MUser');
        $user = MUser::find($user_id);

        if(!$user) {
            redirect('user');
        }

        if (!$user->personal) {
            set_flashdata('error', 'Selected account currently not complete the personal data, complete first.');
            redirect('user');
        }

        $this->load->model('MJobHistory');
        $data = $this->input->post();
        $history = new MJobHistory;
        $history->personal_id = $user->personal->id;
        $history->job_title_id = $data['posisi'];
        $history->effective_date = $data['effective_date'];
        $history->employment_period = $data['period'];
        $history->basic_salary = encrypt_data($data['basic_salary']);
        $history->job_status = $data['status'];
        $history->allowance = implode(',', $data['allowance']);
        $history->benefit = implode(',', $data['benefit']);
        $history->access = empty($data['hak_akses']) ? null : implode(',', $data['hak_akses']);
        $history->save();

        set_flashdata('success', 'Data berhasil disimpan');
        redirect('user/pekerjaan/'. $user_id);
    }

    public function import_user_from_excel() {
        $config['upload_path']          = './uploads/';
        $config['allowed_types']        = 'xlsx|xls';
        $config['max_size']             = 5120;
        $config['max_width']            = 4000;
        $config['max_height']           = 4000;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('file_user')) {
            set_flashdata('error', $this->upload->display_errors());
            return redirect_with_input('user', []);
        }

        $upload_data = $this->upload->data();
        $file = 'uploads/'. $upload_data['file_name'];

        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file);
        $data = $spreadsheet->getActiveSheet()->toArray();

        $this->load->model('MUser');
        $this->load->model('MPersonal');
        $this->load->model('MJobHistory');
        $this->load->library('queue');

        foreach($data as $index => $row) {
            if($index == 0) {
                continue;
            }

            if(!isset($row[0]) || empty($row[1])) {
                break;
            }

            $pass = get_random_password(6, 8, true, true, false);
            $user = new MUser;
            $user->uuid = $row[0];
            $user->email = $row[3];
            $user->password = md5($pass);
            $user->role_id = $row[4];
            $user->save();

            $personal = new MPersonal;
            $personal->user_id = $user->id;
            $personal->name = $row[1];
            $personal->status = 1;
            $personal->save();

            $job = new MJobHistory;
            $job->personal_id = $personal->id;
            $job->job_title_id = $row[2];
            $job->save();

            $this->queue->setAttachment('uploads/Penggunaan_HRIS.pdf');

            $this->queue->setData([
                'email' => $row[3],
                'password' => $pass
            ]);

            //Send email to CEO
            $this->queue->sendEmailViaQueue(
                $row[3],
                'Akun HRIS Santara',
                'email/blas_akun'
            );
        }

        redirect('user');
    }
}