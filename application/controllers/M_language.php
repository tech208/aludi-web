<?php

class M_language extends sntr_Controller {

    public function __construct() {
        parent::__construct();
        ini_set('date.timezone', 'Asia/Jakarta');
        
        $this->load->model('MPoProfil');
        
    }

    public function index() {
        $data = $this->db->query("SELECT * FROM m_language")->result_array();
		
        $this->setParam('data', $data);
        $this->setContentView('web/m_language/index');
        $this->render('Bahasa');
    }



    public function form($id) {       
        $data = $this->db->query("SELECT * FROM m_language WHERE language_id = '$id'")->row_array();
        $this->setParam('data', $data);
        $this->setContentView('web/m_language/form');
        $this->render('Bahasa');
    }

    public function save() {
        $data   = $this->input->post();

        $this->db->trans_begin();

        $id = @$data['id'];
        $arrdata = @$data['data'];
		$this->db->update('m_language', $arrdata, array('language_id' => $id));
        

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }
        else{
            $this->db->trans_commit();
        }
        
        
        set_flashdata('success', 'Data berhasil disimpan');
        redirect('/m_language/form/'.$id);

    }

    public function delete($id) {

    }

}