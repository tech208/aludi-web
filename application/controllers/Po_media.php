<?php

class Po_media extends sntr_Controller {

    public function __construct() {
        parent::__construct();
        
        $this->load->model('MCoreRefStatus');
        $this->load->model('MPoMedia');
    }

    public function index() {
        $data = MPoMedia::orderBy('id', 'DESC')->get();

        $this->setParam('data', $data);
        $this->setContentView('web/po_media/index');
        $this->render('Partner');
    }

    public function form($id='',$mode='') {
        if (!empty($id)) {
            $data = MPoMedia::where('id', $id)->orderBy('id', 'asc')->first();
        }
        
        $refStatus      = MCoreRefStatus::
                            where('grup', '1')
                            // whereIn('id', explode(',', '1,3,4'))
                            ->orderBy('name', 'asc')->get();

        $this->setParam('refStatus', $refStatus);
        $this->setParam('mode', $mode);
        $this->setParam('data', @$data);
        $this->setContentView('web/po_media/form');
        $this->render('Form Partner');
    }

    public function save() {        
        $post   = $this->input->post();
        $id     = !empty($post['id'])?$post['id']:null;

        
        $result = true;
        // validasi
        $msg = '';
        // end validasi

        if($result==true){
            $this->db->trans_begin();

            $params = array(
                "name"          => $post['name'],
                "url_site"      => $post['url_site'],
                "status"        => $post['status'],
            );


            if (empty($id)) { //untuk insert
                $params['created_at'] = date('Y-m-d H:i:s');
                $params['created_by'] = authUser()->id;

                $this->db->insert('po_media', $params);
                $id = $this->db->insert_id();
                logs("POST","po_media/save/",$id,"po_media","insert",authUser()->id,"Menambahkan Partner"); //activity_logs => "method,route,table_id,table_name,table_aksi"   
            }else { //untuk update
                $params['updated_at'] = date('Y-m-d H:i:s');
                $params['updated_by'] = authUser()->id;

                $this->db->update('po_media', $params, array('id' => $id));
                logs("POST","po_media/save/",$id,"po_media","update",authUser()->id,"Merubah Partner"); //activity_logs => "method,route,table_id,table_name,table_aksi"
            }

            
            $data_media = MPoMedia::find($id); //data media
            if(!empty($_FILES['files']['name']['logo'])) {//bila ada lampiran po_media
                if (!empty(@$data_media->logo)) {
                    $path = './uploads/po_media/'.@$data_media->logo;
                    unlink($path);
                }

                $config = array();
                $config['upload_path']          = './uploads/po_media/';
                $config['allowed_types']        = 'jpg|jpeg|gif|png';
                $config['overwrite']        	= true;
                //$config['encrypt_name']        	= true;
                $config['max_size']             = 10120;

                // $this->load->library('upload', $config);
                $this->load->library('upload', $config, 'logo'); // Create custom object for cover upload
                $this->logo->initialize($config);

                
                $_FILES['logo']['name']       = $_FILES['files']['name']['logo'];
                $_FILES['logo']['type']       = $_FILES['files']['type']['logo'];
                $_FILES['logo']['tmp_name']   = $_FILES['files']['tmp_name']['logo'];
                $_FILES['logo']['error']      = $_FILES['files']['error']['logo'];
                $_FILES['logo']['size']       = $_FILES['files']['size']['logo'];

                
                if (!$this->logo->do_upload('logo')) {
                    set_flashdata('success', 'Data Tidak Berhasil Disimpan, Pastikan File jpg|jpeg|gif|png Dan ukuran Maximal 10mb');
                    redirect('/po_media/');
                }

                $upload_data        = $this->logo->data();
                $params_logo['logo']  = $upload_data['file_name'];
        
                $this->db->update('po_media', $params_logo, array('id' => $id));
            }

            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
            }
            else{
                $this->db->trans_commit();
            }
            set_flashdata('success', 'Data berhasil disimpan');
            redirect('/po_media/form/'.$id);
            
        }
        else{
            set_flashdata('error', 'Data Tidak berhasil disimpan. '.$msg);
            redirect('/po_media/');
        }
    }

    public function delete($id) {
        $this->load->model('MPoMedia');
        $data = MPoMedia::find($id);
        if(!$data) {
            return redirect('po_media/');
        }

        $this->db->trans_begin();
        $this->db->delete('po_media_file', array('id_Partner' => $id));  
        $this->db->delete('po_media', array('id' => $id)); 
        logs("POST","po_media/delete/",$id,"po_media","delete"); //activity_logs => "method,route,table_id,table_name,table_aksi"
        
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }
        else{
            $this->db->trans_commit();
        }
        set_flashdata('success', 'Data berhasil disimpan');
        redirect('/po_media/');
    }
    
    public function detail($id) {
        $this->load->model('MPoMedia');
        $data = MPoMedia::find($id);
        if(!$data) {
            return redirect('po_media/');
        }
        
        $this->setParam('data', @$data);
        $this->setContentView('web/po_media/detail');
        $this->render('Detail Partner');
    }


}