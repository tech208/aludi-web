<?php
class Penerbit extends CI_Controller {

    public function __construct() {
        parent::__construct();

        
        $this->load->model('MCoreRefStatus');
        $this->load->model('MPoAnggotaData');
        $this->load->model('MPoAnggotaDataJenis');
        $this->load->model('MPoAnggotaDataJenisLog');
        $this->load->model('MPoAnggotaDataLog');
        $this->load->model('MPoAnggotaDataLokasiPemodal');
        $this->load->model('MPoAnggotaDataLokasiPemodalLog');
        $this->load->model('MPoAnggotaDataLokasiPenerbit');
        $this->load->model('MPoAnggotaDataLokasiPenerbitLog');

        $this->load->model('MPoPenerbit');
        $this->load->model('MCoreRefNegara');
        $this->load->model('MCoreRefProvinsi');

        ini_set('date.timezone', 'Asia/Jakarta');
    }

    public function index() {
        $header = $this->input->request_headers();
        
        $info = MasTokenInfo($header['Token']);
        MasDebugPree($info,1);
        $result = array();

        //respon balik
        $data   = $result;
        $pesan  = array(1 => "Berhasil Menambahkan Data",);
        $kode   = 200;
        MasRespon($data,$pesan,$kode);
        //end respon balik
    }

    private function validasi($post,$aksi) {
        $result = true;
        $pesan  = array();
        
        if ($aksi == "updated_status" ) {    
            // xss clean
            $post["kode"]   = MasXssClean($post["kode"]);
            $post["status"] = MasXssClean($post["status"]);
            // end xss clean

            if (empty(@$post["status"]) ) {
                $result = false;
                $pesan[] = "status Tidak Boleh Kosong!"; 
            }else{
                if ( filter_var(@$post["status"], FILTER_VALIDATE_INT) === false ) {
                    $result     = false;
                    $pesan[]    = "status Hanya Boleh Angka!"; 
                }else{
                    $cekStatus      = MCoreRefStatus::where('id', @$post["status"])->orderBy('id', 'asc')->first();
                    if (empty($cekStatus->id)) {
                        $result     = false;
                        $pesan[]    = "status, Kode status ".@$post["status"]." Tidak Ditemukan!"; 
                    }
                }
            }
        }

        if ($aksi == "view" ) {    
            // xss clean
            $post["kode"]   = MasXssClean($post["kode"]);
            // end xss clean


            if (empty(@$post["kode"]) ) {
                $result = false;
                $pesan[] = "kode Tidak Boleh Kosong!"; 
            }
        }

        if ($aksi == "created" ) {    
            // xss clean
            $post["owner"]              = MasXssClean($post["owner"]);
            $post["nomor_telepon"]      = str_replace('0', '', MasXssClean($post["nomor_telepon"]));
            $post["nama_perusahaan"]    = MasXssClean($post["nama_perusahaan"]);
            $post["nama_brand"]         = MasXssClean($post["nama_brand"]);
            $post["bidang_usaha"]       = MasXssClean($post["bidang_usaha"]);
            $post["total_pendanaan"]    = MasXssClean($post["total_pendanaan"]);
            $post["deskripsi"]          = MasXssClean($post["deskripsi"]);
            $post["status"]             = MasXssClean($post["status"]);
            // end xss clean

            /*if ( filter_var(@$post["nomor_telepon"], FILTER_VALIDATE_INT) === false ) {
                $result     = false;
                $pesan[]    = "nomor_telepon Hanya Boleh Angka!"; 
            }*/

            if (empty(@$post["nama_perusahaan"]) ) {
                $result = false;
                $pesan[] = "nama_perusahaan Tidak Boleh Kosong!"; 
            }
            if (empty(@$post["nama_brand"]) ) {
                $result = false;
                $pesan[] = "nama_brand Tidak Boleh Kosong!"; 
            }
            if (empty(@$post["bidang_usaha"]) ) {
                $result = false;
                $pesan[] = "bidang_usaha Tidak Boleh Kosong!"; 
            }
            if ( filter_var(@$post["total_pendanaan"], FILTER_VALIDATE_INT) === false ) {
                $result     = false;
                $pesan[]    = "total_pendanaan Hanya Boleh Angka!"; 
            }
            if (empty(@$post["deskripsi"]) ) {
                $result = false;
                $pesan[] = "deskripsi Tidak Boleh Kosong!"; 
            }
			/*if ( filter_var(@$post["bidang_usaha"], FILTER_VALIDATE_INT) === false ) {
                $result     = false;
                $pesan[]    = "bidang_usaha Hanya Boleh Angka!"; 
            }*/
            /*if ( filter_var(@$post["status"], FILTER_VALIDATE_INT) === false ) {
                $result     = false;
                $pesan[]    = "status Hanya Boleh Angka!"; 
            }else{
                $cekStatus      = MCoreRefStatus::where('id', @$post["status"])->orderBy('id', 'asc')->first();
                if (empty($cekStatus->id)) {
                    $result     = false;
                    $pesan[]    = "status, Kode status ".@$post["status"]." Tidak Ditemukan!"; 
                }
            }*/
        }
        

        if ($result==false) {
            MasResponGagal($pesan);
            exit();
        }

        $return = $post;
        
        
        return $return;
    }
    
    
    public function created() {
        $header = $this->input->request_headers();

        $infoPersonal   = MasTokenInfo(@$header['Token'], @$header['Authorization']);
        $post_raw       = $this->input->post();
        $post           = $this->validasi($post_raw,"created");
        
        $token = MasToken(25);
		$arrcheck_duplicate = $this->db->query("SELECt COUNT(*) as jum FROM po_penerbit WHERE (nama_perusahaan = '".$post['nama_perusahaan']."' OR nama_brand = '".$post['nama_brand']."') AND id_personal = '$infoPersonal[id]'")->row_array();
		$arrcheck_duplicate_other = $this->db->query("SELECT COUNT(*) as jum, MAX(id_personal) as id_personal FROM po_penerbit WHERE (nama_perusahaan = '".$post['nama_perusahaan']."' OR nama_brand = '".$post['nama_brand']."') AND id_personal != '$infoPersonal[id]'")->row_array();
		if($arrcheck_duplicate['jum'] == "0" && $arrcheck_duplicate_other['jum'] == "0"){
			$this->db->trans_begin();
			
			$params = array(
				//"id_calon_penerbit" =>@$post['id_calon_penerbit'],
				"id_personal"       =>$infoPersonal['id'],
				"kode"              =>$token,
				
				"nama_perusahaan"   =>$post['nama_perusahaan'],
				"nomor_telepon_perusahaan"     => $post['nomor_telepon_perusahaan'],
				"email_perusahaan"             => $post['email_perusahaan'],
				"nama_brand"        =>$post['nama_brand'],
				"bidang_usaha"      =>$post['bidang_usaha'],
				"provinsi"      =>$post['provinsi'],
				"kode_saham"      =>$post['kode_saham'],
				"total_pendanaan"   =>$post['total_pendanaan'],
				"deskripsi"         =>$post['deskripsi'],
				"status"            =>$post['status'],
			);
			$params['created_by'] = $infoPersonal['user_id'];
			$params['created_at'] = date('Y-m-d H:i:s');
			
			
			$this->db->insert('po_penerbit', $params);
			$id = $this->db->insert_id();
			logs("POST","api/v1/penerbit/created/",$id,"po_penerbit","insert",$infoPersonal['user_id'],"Menambahkan Data Penerbit"); //activity_logs => "method,route,table_id,table_name,table_aksi,id_user,deskripsi"            
			foreach($post['direksi_nama'] as $index => $value){
				$arrcheck_direksi_duplicate = $this->db->query("SELECt COUNT(*) as jum FROM po_penerbit_directors WHERE (direksi_ktp = '".$post['direksi_ktp'][$index]."' OR direksi_nomor_telepon = '".$post['direksi_nomor_telepon'][$index]."' OR direksi_email = '".$post['direksi_email'][$index]."')")->row_array();
				
				if($arrcheck_direksi_duplicate['jum'] != "0"){
					$data   = $params;
					$pesan  = array(1 => "Data penerbit sudah terdaftar di system2");
					$kode   = 411;
					MasRespon($data,$pesan,$kode);
					exit();
				}
				$arrdireksi['penerbit_id'] = $id;
				$arrdireksi['direksi_nama'] = $post['direksi_nama'][$index];
				$arrdireksi['direksi_jabatan'] = $post['direksi_jabatan'][$index];
				$arrdireksi['direksi_ktp'] = $post['direksi_ktp'][$index];
				$arrdireksi['direksi_nomor_telepon'] = $post['direksi_nomor_telepon'][$index];
				$arrdireksi['direksi_email'] = $post['direksi_email'][$index];
				$this->db->insert('po_penerbit_directors', $arrdireksi);
			}
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
	
				$pesan[1] = "Data Tidak Tersimpan";
				MasResponGagal($pesan);
			}
			else{
				$this->db->trans_commit();
			}
	
			//respon balik
			$data   = array(
				"kode" => $token,
			);
			$pesan  = array(1 => "Berhasil Menambahkan Data Penerbit",);
			$kode   = 201;
			MasRespon($data,$pesan,$kode);
			//end respon balik
		}else if($arrcheck_duplicate_other['jum'] > 0){
			$data   = $params;
			$pesan  = array(1 => "Data penerbit sudah terdaftar di system");
			$kode   = 411;
			MasRespon($data,$pesan,$kode);
		}else{
			$data   = $params;
			$pesan  = array(1 => "Data penerbit sudah pernah diinput");
			$kode   = 412;
			MasRespon($data,$pesan,$kode);
		}
        exit();
    }

    public function updated_status() {
        $header = $this->input->request_headers();

        $infoPersonal   = MasTokenInfo(@$header['Token'], @$header['Authorization']);
        $post_raw       = $this->input->post();
        $post           = $this->validasi($post_raw,"updated_status");
        
        $data           = MPoPenerbit::where('kode', @$post['kode'])->orderBy('id', 'asc')->first();
        
        $this->db->trans_begin();    
        $params = array(
            "status"    =>$post['status'],
        );
        
        $params['updated_at'] = date('Y-m-d H:i:s');
        $params['updated_by'] = $infoPersonal['user_id'];
        
        
        $id = $data->id;
        $this->db->update('po_penerbit', $params, array('id' => $id));
        logs("POST","api/v1/Penerbit/updated/",$id,"po_penerbit","update",$infoPersonal['user_id'],"Memperbarui Data Penerbit"); //activity_logs => "method,route,table_id,table_name,table_aksi,id_user,deskripsi"
        
        
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            
            $pesan[1] = "Data Tidak Tersimpan";
            MasResponGagal($pesan);
        }
        else{
            $this->db->trans_commit();
        }
        
        $status           = MCoreRefStatus::where('id', @$post['status'])->orderBy('id', 'asc')->first();
        //respon balik
        $data   = array(
            "kode"              => @$post['kode'],
            "nama_perusahaan"   => @$data->nama_perusahaan,
            "nama_brand"        => @$data->nama_brand,
            "status"            => @$status->name,
        );
        $pesan  = array(1 => "Berhasil Memperbarui Data Penerbit",);
        $kode   = 201;
        MasRespon($data,$pesan,$kode);
        //end respon balik
        exit();
    }
    
    public function view() {
        $header = $this->input->request_headers();
        
        $infoPersonal   = MasTokenInfo(@$header['Token'], @$header['Authorization']);
        $post_raw       = $this->input->post();
        $post           = $this->validasi($post_raw,"view");

        $data           = MPoPenerbit::where('kode', @$post['kode'])->orderBy('id', 'asc')->first();
        $id             = $data->id;
        if (empty($id)) {
            $pesan[1] = "Data Tidak Ditemukan";
            MasResponGagal($pesan);
        }
        
        logs("POST","api/v1/Penerbit/view/",$id,"po_penerbit","view",$infoPersonal['user_id'],"Melihat Data Penerbit"); //activity_logs => "method,route,table_id,table_name,table_aksi,id_user,deskripsi"
        
        $result = array(
            "kode"              => @$data->kode,
            "owner"             => @$data->owner,
            "nomor_telepon"     => @$data->nomor_telepon,
            "email"             => @$data->email,
            "nama_perusahaan"   => @$data->nama_perusahaan,
			"nomor_telepon_perusahaan"     => @$data->nomor_telepon_perusahaan,
            "email_perusahaan"             => @$data->email_perusahaan,
            "nama_brand"        => @$data->nama_brand,
            "bidang_usaha"      => @$data->bidang_usaha,
            "total_pendanaan"   => @$data->total_pendanaan,
            "deskripsi"         => @$data->deskripsi,
            "status"            => @$data->statuse->name,
        );

        //respon balik
        $data   = $result;
        $pesan  = array(1 => "Berhasil Menambahkan Data",);
        $kode   = 200;
        MasRespon($data,$pesan,$kode);
        //end respon balik
    }
	
	public function list_view() {
		$header = $this->input->request_headers();
        $infoPersonal = MasTokenInfo(@$header['Token'], @$header['Authorization']);
		
		
        
        $data = $this->db->query("SELECT A.kode, A.nomor_telepon, A.email, A.nama_perusahaan, A.nomor_telepon_perusahaan, A.email_perusahaan, A.nama_brand, C.nama as bidang_usaha, A.total_pendanaan, A.deskripsi, B.name as status FROM po_penerbit A LEFT JOIN core_ref_status B ON B.id = A.status LEFT JOIN core_ref_jenis_penerbit C ON C.id = A.bidang_usaha WHERE A.id_personal = '".$infoPersonal['id']."'")->result_array();
        $ichild = 1;
		foreach($data as $index => $value){
			$data_child[$ichild] = $value;
			$data_child[$ichild]['report'] = $this->db->query("SELECT jumlah_pemodal, jumlah_user, saham_ditawarkan, saham_dihimpun, jumlah_dividen, saham_syariah_ditawarkan, saham_syariah_dihimpun, jumlah_dividen_syariah, sukuk_ditawarkan, sukuk_dihimpun, obligasi_ditawarkan, obligasi_dihimpun, pembagian_bunga, tenor, tanggal_listed, tanggal_fully_funded, kode_saham FROM po_anggota_data WHERE kode_penerbit = '".$value['kode']."' ORDER BY id DESC LIMIT 1")->row_array();
			$ichild++;
        }
		$i = 1;
		foreach ($data as $key => $value) {
            $result[$i++]     = $value;
        }

        if (empty(@$result)) {
            $pesan[1]   = "Data Penerbit Tidak Ditemukan!";
            MasResponGagal($pesan);
        }

		
        $data   = $result;
	
        $pesan  = array(1 => "Data Ditemukan",);
        $kode   = 200;
        MasRespon($data_child,$pesan,$kode);
        //end respon balik
        exit();
    }

	public function updated() {
        $header = $this->input->request_headers();

        $infoPersonal   = MasTokenInfo(@$header['Token'], @$header['Authorization']);
        $post     = $this->input->post();
        
        $data           = MPoPenerbit::where([
				'kode'     => $post['kode'],
				'id_personal'  => $infoPersonal['id'],
			])->orderBy('id', 'asc')->first();
        
        $this->db->trans_begin();   
		$id = $data->id;
		
		if($id != ""){
			$this->db->query("DELETE FROM po_penerbit_directors WHERE penerbit_id = '$id'");
			foreach($post['direksi_nama'] as $index => $value){
				$arrcheck_direksi_duplicate = $this->db->query("SELECt COUNT(*) as jum FROM po_penerbit_directors WHERE (direksi_ktp = '".$post['direksi_ktp'][$index]."' OR direksi_nomor_telepon = '".$post['direksi_nomor_telepon'][$index]."' OR direksi_email = '".$post['direksi_email'][$index]."')")->row_array();
				
				if($arrcheck_direksi_duplicate['jum'] != "0"){
					$data   = $params;
					$pesan  = array(1 => "Data penerbit sudah terdaftar di system");
					$kode   = 411;
					MasRespon($data,$pesan,$kode);
					exit();
				}
				$arrdireksi['penerbit_id'] = $id;
				$arrdireksi['direksi_nama'] = $post['direksi_nama'][$index];
				$arrdireksi['direksi_jabatan'] = $post['direksi_jabatan'][$index];
				$arrdireksi['direksi_ktp'] = $post['direksi_ktp'][$index];
				$arrdireksi['direksi_nomor_telepon'] = $post['direksi_nomor_telepon'][$index];
				$arrdireksi['direksi_email'] = $post['direksi_email'][$index];
				$this->db->insert('po_penerbit_directors', $arrdireksi);
			}
		}
        
        if(@$post['nama_perusahaan'] != "") $params['nama_perusahaan']   = $post['nama_perusahaan'];
		if(@$post['nomor_telepon_perusahaan'] != "") $params['nomor_telepon_perusahaan']   = $post['nomor_telepon_perusahaan'];
		if(@$post['email_perusahaan'] != "") $params['email_perusahaan']   = $post['email_perusahaan'];
        if(@$post['nama_brand'] != "") $params['nama_brand']        = $post['nama_brand'];
        if(@$post['provinsi'] != "") $params['provinsi']      = $post['provinsi'];
        if(@$post['kode_saham'] != "") $params['kode_saham']      = $post['kode_saham'];
        if(@$post['bidang_usaha'] != "") $params['bidang_usaha']      = $post['bidang_usaha'];
        if(@$post['total_pendanaan'] != "") $params['total_pendanaan']   = $post['total_pendanaan'];
        if(@$post['deskripsi'] != "") $params['deskripsi']         = $post['deskripsi'];
        if(@$post['status'] != "") $params['status']            = $post['status'];
        $params['updated_at'] = date('Y-m-d H:i:s');
        $params['updated_at'] = date('Y-m-d H:i:s');
        $params['updated_by'] = $infoPersonal['user_id'];
       
        
        $id = $data->id;
		if($id == ""){
			$pesan[1] = "Data Penerbit Tidak Ditemukan";
            MasResponGagal($pesan);
		}
        $this->db->update('po_penerbit', $params, array('id' => $id));
        logs("POST","api/v1/Penerbit/updated/",$id,"po_penerbit","update",$infoPersonal['user_id'],"Memperbarui Data Penerbit"); //activity_logs => "method,route,table_id,table_name,table_aksi,id_user,deskripsi"
        
        
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            
            $pesan[1] = "Data Tidak Tersimpan";
            MasResponGagal($pesan);
        }
        else{
            $this->db->trans_commit();
        }
        
        $status           = MCoreRefStatus::where('id', @$post['status'])->orderBy('id', 'asc')->first();
        //respon balik
        $data   = array(
            "kode"              => @$post['kode'],
            "nama_perusahaan"   => @$data->nama_perusahaan,
            "nama_brand"        => @$data->nama_brand,
            "status"            => @$status->name,
        );
        $pesan  = array(1 => "Berhasil Memperbarui Data Penerbit",);
        $kode   = 202;
        MasRespon($data,$pesan,$kode);
        //end respon balik
        exit();
    }

	public function deleted() {
		$header = $this->input->request_headers();

        $infoPersonal   = MasTokenInfo(@$header['Token'], @$header['Authorization']);
        $post     = $this->input->post();
		$data           = MPoPenerbit_test::where([
				'kode'     => $post['kode'],
				'id_personal'  => $infoPersonal['id'],
			])->orderBy('id', 'asc')->first();
		
		$this->db->trans_begin();   
		if(@$post['kode'] != ""){
			$id = $data->id;
			$this->db->query("DELETE FROM po_penerbit_directors_test WHERE penerbit_id = '$id'");
			$this->db->query("DELETE FROM po_penerbit WHERE kode = '$post[kode]' AND id_personal = '$infoPersonal[id]'");
		}
		
		if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            
            $pesan[1] = "Data Tidak Terhapus";
            MasResponGagal($pesan);
        }
        else{
            $this->db->trans_commit();
        }
        
        $status           = MCoreRefStatus::where('id', @$post['status'])->orderBy('id', 'asc')->first();
        //respon balik
        $data   = array(
            "kode"              => @$post['kode']
        );
        $pesan  = array(1 => "Data Pernerbit Berhasil Dihapus",);
        $kode   = 203;
        MasRespon($data,$pesan,$kode);
        //end respon balik
        exit();
	}

}