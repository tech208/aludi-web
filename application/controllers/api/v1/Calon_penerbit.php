<?php
class Calon_penerbit extends CI_Controller {

    public function __construct() {
        parent::__construct();

        
        $this->load->model('MCoreRefStatus');
        $this->load->model('MPoAnggotaData');
        $this->load->model('MPoAnggotaDataJenis');
        $this->load->model('MPoAnggotaDataJenisLog');
        $this->load->model('MPoAnggotaDataLog');
        $this->load->model('MPoAnggotaDataLokasiPemodal');
        $this->load->model('MPoAnggotaDataLokasiPemodalLog');
        $this->load->model('MPoAnggotaDataLokasiPenerbit');
        $this->load->model('MPoAnggotaDataLokasiPenerbitLog');

        $this->load->model('MCoreRefNegara');
        $this->load->model('MCoreRefProvinsi');

        ini_set('date.timezone', 'Asia/Jakarta');
    }

    
    public function index() {
        $header = $this->input->request_headers();
        $header['Token'] = "eyJlIjoiMjAyMTAyMjQwODMyNDAifQ==";
        
        $info = MasTokenInfo($header['Token']);
        MasDebugPree($info,1);



        $result['nama_perusahaan']      = "";
        $result['nama_brand']           = "";
        $result['bidang_usaha']         = "";
        $result['alamat_usaha']         = "";
        $result['jumlah_penerbit']      = "";
        $result['jumlah_pemodal']       = "";
        $result['jumlah_pendanaan']     = "";
        $result['jumlah_dividen']       = "";

        $result['penerbit_kategori']['jumlah']['total']         = "";
        $result['penerbit_kategori']['kategori']['kuliner']     = "";
        $result['penerbit_kategori']['kategori']['tech']        = "";
        $result['penerbit_kategori']['kategori']['enterprice']  = "";
        $result['penerbit_kategori']['kategori']['lainya']      = "";
        
        $result['penerbit_kota']['jumlah']['total']     = "";
        $result['penerbit_kota']['kota']['jakarta']     = "";
        $result['penerbit_kota']['kota']['surabaya']    = "";
        $result['penerbit_kota']['kota']['yogyakarta']  = "";
        $result['penerbit_kota']['kota']['madura']      = "";

        
        $result['penerbit_pemodal']['jumlah']['total']      = "";
        $result['penerbit_pemodal']['jumlah']['kota']       = "";
        $result['penerbit_pemodal']['jumlah']['negara']     = "";
        $result['penerbit_pemodal']['kota']['jakarta']      = "";
        $result['penerbit_pemodal']['kota']['surabaya']     = "";
        $result['penerbit_pemodal']['kota']['yogyakarta']   = "";
        $result['penerbit_pemodal']['kota']['madura']       = "";

        
        $result['penerbit_pendanaan']['total_tersalurkan']  = "";
        $result['penerbit_pendanaan']['total_penerima']     = "";

        $result['penerbit_deviden']['total_deviden']        = "";
        $result['penerbit_deviden']['total_penerima']       = "";


        //respon balik
        $data   = $result;
        $pesan  = array(1 => "Berhasil Menambahkan Data",);
        $kode   = 200;
        MasRespon($data,$pesan,$kode);
        //end respon balik
    }

    private function validasi($post,$aksi) {
        $result = true;
        $pesan  = array();
        
        // xss clean
        $post["nama_pt"]                    = MasXssClean($post["nama_pt"]);
        $post["nama_brand"]                 = MasXssClean($post["nama_brand"]);
        $post["jumlah_penerbit"]            = MasXssClean($post["jumlah_penerbit"]);
        $post["jumlah_penerbit_funded"]     = MasXssClean($post["jumlah_penerbit_funded"]);
        $post["jumlah_investor"]            = MasXssClean($post["jumlah_investor"]);
        $post["jumlah_investor_dividen"]    = MasXssClean($post["jumlah_investor_dividen"]);
        $post["jumlah_dana"]                = MasXssClean($post["jumlah_dana"]);
        $post["jumlah_dividen"]             = MasXssClean($post["jumlah_dividen"]);
        // end xss clean


        if (empty(@$post["nama_pt"]) ) {
            $result = false;
            $pesan[] = "nama_pt Tidak Boleh Kosong!"; 
        }
        
        if (empty(@$post["nama_brand"]) ) {
            $result = false;
            $pesan[] = "nama_brand Tidak Boleh Kosong!"; 
        }
        
        if (empty(@$post["jumlah_penerbit"]) ) {
            $result = false;
            $pesan[] = "jumlah_penerbit Tidak Boleh Kosong!"; 
        }else{
            if ( filter_var(@$post["jumlah_penerbit"], FILTER_VALIDATE_INT) === false ) {
                $result     = false;
                $pesan[]    = "jumlah_penerbit Hanya Boleh Angka!"; 
            }
        }

        if (empty(@$post["jumlah_penerbit_funded"]) ) {
            $result = false;
            $pesan[] = "jumlah_penerbit_funded Tidak Boleh Kosong!"; 
        }else{
            if ( filter_var(@$post["jumlah_penerbit_funded"], FILTER_VALIDATE_INT) === false ) {
                $result     = false;
                $pesan[]    = "jumlah_penerbit_funded Hanya Boleh Angka!"; 
            }
        }

        if (empty(@$post["jumlah_investor"]) ) {
            $result = false;
            $pesan[] = "jumlah_investor Tidak Boleh Kosong!"; 
        }else{
            if ( filter_var(@$post["jumlah_investor"], FILTER_VALIDATE_INT) === false ) {
                $result     = false;
                $pesan[]    = "jumlah_investor Hanya Boleh Angka!"; 
            }
        }

        if (empty(@$post["jumlah_investor_dividen"]) ) {
            $result = false;
            $pesan[] = "jumlah_investor_dividen Tidak Boleh Kosong!"; 
        }else{
            if ( filter_var(@$post["jumlah_investor_dividen"], FILTER_VALIDATE_INT) === false ) {
                $result     = false;
                $pesan[]    = "jumlah_investor_dividen Hanya Boleh Angka!"; 
            }
        }

        if (empty(@$post["jumlah_dana"]) ) {
            $result = false;
            $pesan[] = "jumlah_dana Tidak Boleh Kosong!"; 
        }else{
            if ( filter_var(@$post["jumlah_dana"], FILTER_VALIDATE_INT) === false ) {
                $result     = false;
                $pesan[]    = "jumlah_dana Hanya Boleh Angka!"; 
            }
        }

        if (empty(@$post["jumlah_dividen"]) ) {
            $result = false;
            $pesan[] = "jumlah_dividen Tidak Boleh Kosong!"; 
        }else{
            if ( filter_var(@$post["jumlah_dividen"], FILTER_VALIDATE_INT) === false ) {
                $result     = false;
                $pesan[]    = "jumlah_dividen Hanya Boleh Angka!"; 
            }
        }

        if (empty(@$post["jenis"]) ) {
            $result = false;
            $pesan[] = "jenis Dividen Tidak Boleh Kosong!"; 
        }else {
            foreach (@$post["jenis"] as $key => $value) {
                if ( filter_var($value['jumlah'], FILTER_VALIDATE_INT) === false ) {
                    $result     = false;
                    $pesan[]    = "jenis jumlah Hanya Boleh Angka!"; 
                }

                @$post["jenis"][$key]['jenis'] = MasXssClean($value['jenis']);
            }
        }

        if (empty(@$post["lokasi_pemodal"]) ) {
            $result = false;
            $pesan[] = "lokasi_pemodal Tidak Boleh Kosong!"; 
        }else {
            foreach (@$post["lokasi_pemodal"] as $key => $value) {
                if ( filter_var($value['jumlah'], FILTER_VALIDATE_INT) === false ) {
                    $result     = false;
                    $pesan[]    = "lokasi_pemodal -> jumlah Hanya Boleh Angka!"; 
                }

                if (empty(@$value["id_negara"]) ) {
                    $result     = false;
                    $pesan[]    = "lokasi_pemodal -> id_negara Tidak Boleh Kosong!"; 
                }else{
                    $cekNegara      = MCoreRefNegara::where('kode', @$value["id_negara"])->orderBy('id', 'asc')->first();
                    if (empty($cekNegara->id)) {
                        $result     = false;
                        $pesan[]    = "lokasi_pemodal -> id_negara Salah, Kode Negara ".@$value["id_negara"]." Tidak Ditemukan!"; 
                    }
                }
                
                if (empty(@$value["id_provinsi"]) ) {
                    $result     = false;
                    $pesan[]    = "lokasi_pemodal -> id_provinsi Tidak Boleh Kosong!"; 
                }else{
                    $cekProvinsi      = MCoreRefProvinsi::where('kode', @$value["id_provinsi"])->orderBy('id', 'asc')->first();
                    if (empty($cekProvinsi->id)) {
                        $result     = false;
                        $pesan[]    = "lokasi_pemodal -> id_provinsi Salah, Kode Provinsi ".@$value["id_provinsi"]." Tidak Ditemukan!"; 
                    }
                }

            }
        }

        
        if (empty(@$post["lokasi_penerbit"]) ) {
            $result = false;
            $pesan[] = "lokasi_penerbit Tidak Boleh Kosong!"; 
        }else {
            foreach (@$post["lokasi_penerbit"] as $key => $value) {
                if ( filter_var($value['jumlah'], FILTER_VALIDATE_INT) === false ) {
                    $result     = false;
                    $pesan[]    = "lokasi_penerbit -> jumlah Hanya Boleh Angka!"; 
                }

                if (empty(@$value["id_negara"]) ) {
                    $result     = false;
                    $pesan[]    = "lokasi_penerbit -> id_negara Tidak Boleh Kosong!"; 
                }else{
                    $cekNegara      = MCoreRefNegara::where('kode', @$value["id_negara"])->orderBy('id', 'asc')->first();
                    if (empty($cekNegara->id)) {
                        $result     = false;
                        $pesan[]    = "lokasi_penerbit -> id_negara Salah, Kode Negara ".@$value["id_negara"]." Tidak Ditemukan!"; 
                    }
                }
                
                if (empty(@$value["id_provinsi"]) ) {
                    $result     = false;
                    $pesan[]    = "lokasi_penerbit -> id_provinsi Tidak Boleh Kosong!"; 
                }else{
                    $cekProvinsi      = MCoreRefProvinsi::where('kode', @$value["id_provinsi"])->orderBy('id', 'asc')->first();
                    if (empty($cekProvinsi->id)) {
                        $result     = false;
                        $pesan[]    = "lokasi_penerbit -> id_provinsi Salah, Kode Provinsi ".@$value["id_provinsi"]." Tidak Ditemukan!"; 
                    }
                }

            }
        }


        if ($result==false) {
            MasResponGagal($pesan);
            exit();
        }

        $return = $post;
        
        
        return $return;
    }

    public function pulling() {
        $header         = $this->input->request_headers();
        
        $infoPersonal   = MasTokenInfo(@$header['Token']);
        $post_raw       = $this->input->post();
        $post           = $this->validasi($post_raw,"pulling");
        $data           = MPoAnggotaData::where('id_personal', @$infoPersonal['id'])->orderBy('id', 'asc')->first();

        $this->db->trans_begin();    
        $params = array(
            "id_personal"               =>$infoPersonal['id'],
            // "id_anggota"=>$post[''],
            "nama_pt"                   =>$post['nama_pt'],
            "nama_brand"                =>$post['nama_brand'],
            "jumlah_penerbit"           =>$post['jumlah_penerbit'],
            "jumlah_penerbit_funded"    =>$post['jumlah_penerbit_funded'],
            "jumlah_investor"           =>$post['jumlah_investor'],
            "jumlah_investor_dividen"   =>$post['jumlah_investor_dividen'],
            "jumlah_dana"               =>$post['jumlah_dana'],
            "jumlah_dividen"            =>$post['jumlah_dividen'],
            "status"                    =>2,
        );


        if (!empty(@$data->id)) { //update data
            $params['updated_at'] = date('Y-m-d H:i:s');
            $params['updated_by'] = $infoPersonal['user_id'];
            
            
            $id = $data->id;
            $this->db->update('po_anggota_data', $params, array('id' => $id));
            logs("POST","api/v1/penyelenggara/pulling/",$id,"po_anggota_data","update",$infoPersonal['user_id'],"Memperbarui Pulling Data Penyelenggara"); //activity_logs => "method,route,table_id,table_name,table_aksi,id_user,deskripsi"
            
        }else{ //insert data
            $params['created_at'] = date('Y-m-d H:i:s');
            $params['created_by'] = $infoPersonal['user_id'];
            
            
            $this->db->insert('po_anggota_data', $params);
            $id = $this->db->insert_id();
            logs("POST","api/v1/penyelenggara/pulling/",$id,"po_anggota_data","insert",$infoPersonal['user_id'],"Menambahkan Pulling Data Penyelenggara"); //activity_logs => "method,route,table_id,table_name,table_aksi,id_user,deskripsi"
        }

        //insert log
        $params_log = $params;
        unset($params_log['id_personal']);
        $params_log['id_anggota_data']  = $id;
        $this->db->insert('po_anggota_data_log', $params_log);
        //end insert log

        //penyelenggara jenis
        $params_jenis = array();
        foreach ($post['jenis'] as $key => $value) {
            $params_jenis = array(
                "id_anggota_data"   =>$id,
                "jenis"             =>$value['jenis'],
                "jumlah"            =>$value['jumlah'],
                "status"            =>2,
            );
            
            $cekJenis      = MPoAnggotaDataJenis::where('id_anggota_data', @$id)
            ->where('jenis', @$value["jenis"])
            ->orderBy('id', 'asc')->first();
            if (!empty(@$cekJenis->id)) {
                $params_jenis['updated_at'] = date('Y-m-d H:i:s');
                $params_jenis['updated_by'] = $infoPersonal['user_id'];
            
            
                $this->db->update('po_anggota_data_jenis', $params_jenis, array('id' => @$cekJenis->id));
                $id_jenis = $cekJenis->id;
            }else{
                $params_jenis['created_at'] = date('Y-m-d H:i:s');
                $params_jenis['created_by'] = $infoPersonal['user_id'];
                
                
                $this->db->insert('po_anggota_data_jenis', $params_jenis);
                $id_jenis   = $this->db->insert_id();
            }
            
            //end log
            $params_jenis_log = $params_jenis;
            unset($params_jenis_log['id_anggota_data']);
            $params_jenis_log['id_anggota_data_jenis']  = $id_jenis;
            $this->db->insert('po_anggota_data_jenis_log', $params_jenis_log);
            //end log
        }
        //penyelenggara jenis

        //penyelenggara lokasi_pemodal
        $params_lokasi_pemodal = array();
        foreach ($post['lokasi_pemodal'] as $key => $value) {
            $params_lokasi_pemodal = array(
                "id_anggota_data"   =>$id,
                "id_negara"         =>MasNegara($value['id_negara']),
                "id_provinsi"       =>$value['id_provinsi'],
                "jumlah"            =>$value['jumlah'],
                "status"            =>2,
            );

            
            $cek_lokasi_pemodal      = MPoAnggotaDataLokasiPemodal::where('id_anggota_data', @$id)
            ->where('id_negara', MasNegara(@$value["id_negara"]))
            ->where('id_provinsi', @$value["id_provinsi"])
            ->orderBy('id', 'asc')->first();

            if (!empty(@$cek_lokasi_pemodal->id)) {
                $params_lokasi_pemodal['updated_at'] = date('Y-m-d H:i:s');
                $params_lokasi_pemodal['updated_by'] = $infoPersonal['user_id'];
            
            
                $this->db->update('po_anggota_data_lokasi_pemodal', $params_lokasi_pemodal, array('id' => @$cek_lokasi_pemodal->id));
                $id_lokasi_pemodal = $cek_lokasi_pemodal->id;
            }else{
                $params_lokasi_pemodal['created_at'] = date('Y-m-d H:i:s');
                $params_lokasi_pemodal['created_by'] = $infoPersonal['user_id'];
                
                
                $this->db->insert('po_anggota_data_lokasi_pemodal', $params_lokasi_pemodal);
                $id_lokasi_pemodal   = $this->db->insert_id();
            }


            //end log
            $params_lokasi_pemodal_log = $params_lokasi_pemodal;
            unset($params_lokasi_pemodal_log['id_anggota_data']);
            $params_lokasi_pemodal_log['id_anggota_data_lokasi_pemodal']  = $id_lokasi_pemodal;
            $this->db->insert('po_anggota_data_lokasi_pemodal_log', $params_lokasi_pemodal_log);
            //end log
        }
        //penyelenggara lokasi_pemodal


        //penyelenggara lokasi_penerbit
        $params_lokasi_penerbit = array();
        foreach ($post['lokasi_penerbit'] as $key => $value) {
            $params_lokasi_penerbit = array(
                "id_anggota_data"   =>$id,
                "id_negara"         =>MasNegara($value['id_negara']),
                "id_provinsi"       =>$value['id_provinsi'],
                "jumlah"            =>$value['jumlah'],
                "status"            =>2,
            );

            $cek_lokasi_penerbit      = MPoAnggotaDataLokasiPenerbit::where('id_anggota_data', @$id)
                                ->where('id_negara', MasNegara(@$value["id_negara"]))
                                ->where('id_provinsi', @$value["id_provinsi"])
                                ->orderBy('id', 'asc')->first();

            if (@$cek_lokasi_penerbit->id) {
                $params_lokasi_penerbit['updated_at'] = date('Y-m-d H:i:s');
                $params_lokasi_penerbit['updated_by'] = $infoPersonal['user_id'];
            
            
                $this->db->update('po_anggota_data_lokasi_penerbit', $params_lokasi_penerbit, array('id' => @$cek_lokasi_penerbit->id));
                $id_lokasi_penerbit = @$cek_lokasi_penerbit->id;
            }else{
                $params_lokasi_penerbit['created_at'] = date('Y-m-d H:i:s');
                $params_lokasi_penerbit['created_by'] = $infoPersonal['user_id'];
                
                
                $this->db->insert('po_anggota_data_lokasi_penerbit', $params_lokasi_penerbit);
                $id_lokasi_penerbit     = $this->db->insert_id();
            }

            
            
            //end log
            $params_lokasi_penerbit_log = $params_lokasi_penerbit;
            unset($params_lokasi_penerbit_log['id_anggota_data']);
            $params_lokasi_penerbit_log['id_anggota_data_lokasi_penerbit']  = $id_lokasi_penerbit;
            
            $this->db->insert('po_anggota_data_lokasi_penerbit_log', $params_lokasi_penerbit_log);
            //end log
        }
        //penyelenggara lokasi_penerbit        

        
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();

            $pesan[1] = "Data Tidak Tersimpan";
            MasResponGagal($pesan);
        }
        else{
            $this->db->trans_commit();
        }

        //respon balik
        $data   = array();
        $pesan  = array(1 => "Berhasil Pulling Data Penyelenggara",);
        $kode   = 201;
        MasRespon($data,$pesan,$kode);
        //end respon balik
        exit();
    }

    public function pulling_view() {
        $header = $this->input->request_headers();
        $infoPersonal = MasTokenInfo($header['Token']);

        
        $data           = MPoAnggotaData::where('id_personal', @$infoPersonal['id'])->orderBy('id', 'asc')->first();

        if (empty(@$data->id)) {
            $pesan[1]   = "Data Pulling View Tidak Ditemukan!";
            MasResponGagal($pesan);
        }



        $result['nama_pt']                  = @$data->nama_pt;
        $result['nama_brand']               = @$data->nama_brand;
        // $result['bidang_usaha']         = "";
        // $result['alamat_usaha']         = "";
        $result['jumlah_penerbit']          = @$data->jumlah_penerbit;
        $result['jumlah_penerbit_funded']   = @$data->jumlah_penerbit_funded;
        $result['jumlah_investor']          = @$data->jumlah_investor;
        $result['jumlah_investor_dividen']  = @$data->jumlah_investor_dividen;
        $result['jumlah_dana']              = @$data->jumlah_dana;
        $result['jumlah_dividen']           = @$data->jumlah_dividen;

        $result['jenis']['jumlah']['total']         = $data->jenise->count();
        foreach ($data->jenise as $key => $value) {
            $result['jenis']['data'][$value['jenis']]     = $value['jumlah'];
        }
        
        $result['lokasi_penerbit']['jumlah']['total']         = $data->lokasi_penerbits->count();
        foreach ($data->lokasi_penerbits as $key => $value) {
            $result['lokasi_penerbit']['data'][MasProvinsi($value['id_provinsi'],1)->nama]     = $value['jumlah'];
        }
        
        $result['lokasi_pemodal']['jumlah']['total']         = $data->lokasi_penerbits->count();
        foreach ($data->lokasi_penerbits as $key => $value) {
            $result['lokasi_pemodal']['data'][MasProvinsi($value['id_provinsi'],1)->nama]     = $value['jumlah'];
        }

        
        // $result['penerbit_pemodal']['jumlah']['total']      = "";
        // $result['penerbit_pemodal']['jumlah']['kota']       = "";
        // $result['penerbit_pemodal']['jumlah']['negara']     = "";


        //respon balik
        $data   = $result;
        $pesan  = array(1 => "Data Ditemukan",);
        $kode   = 200;
        MasRespon($data,$pesan,$kode);
        //end respon balik
    }

    public function created() {
        $header = $this->input->request_headers();
        $header['Token'] = "eyJlIjoiMjAyMTAyMjQwODMyNDAifQ==";
        
        $infoPersonal = MasTokenInfo($header['Token']);
        MasDebugPree($infoPersonal,1);
    }
    
    public function updated() {
        $header = $this->input->request_headers();
        $header['Token'] = "eyJlIjoiMjAyMTAyMjQwODMyNDAifQ==";
        
        $infoPersonal = MasTokenInfo($header['Token']);
        MasDebugPree($infoPersonal,1);
    }

    public function deleted() {
        $header = $this->input->request_headers();
        $header['Token'] = "eyJlIjoiMjAyMTAyMjQwODMyNDAifQ==";
        
        $infoPersonal = MasTokenInfo($header['Token']);
        MasDebugPree($infoPersonal,1);
    }

    public function detail() {
        $header = $this->input->request_headers();
        $header['Token'] = "eyJlIjoiMjAyMTAyMjQwODMyNDAifQ==";
        
        $infoPersonal = MasTokenInfo($header['Token']);
        MasDebugPree($infoPersonal,1);
    }
}