<?php
class Auth_api extends CI_Controller {

    public function __construct() {
        parent::__construct();

        
        $this->load->model('MCoreRefStatus');
        $this->load->model('MCoreUser');

        ini_set('date.timezone', 'Asia/Jakarta');
    }

    public function index() {
        $header = $this->input->request_headers();
        
        $info = MasTokenInfo($header['Token']);
        MasDebugPree($info,1);
        $result = array();

        //respon balik
        $data   = $result;
        $pesan  = array(1 => "Berhasil Menambahkan Data",);
        $kode   = 200;
        MasRespon($data,$pesan,$kode);
        //end respon balik
    }

    private function validasi($post,$aksi) {
        $result = true;
        $pesan  = array();
        
        // xss clean
        $post["email"]      = MasXssClean($post["email"]);
        $post["password"]   = MasXssClean($post["password"]);
        // end xss clean

        if (empty(@$post["email"]) ) {
            $result     = false;
            $pesan[]    = "email Tidak Boleh Kosong!"; 
        }

        if (empty(@$post["password"]) ) {
            $result     = false;
            $pesan[]    = "password Tidak Boleh Kosong!"; 
        }
        

        if ($result==false) {
            MasResponGagal($pesan);
            exit();
        }

        $return = $post;
        
        
        return $return;
    }
    
    public function refresh_token() {
        $header = $this->input->request_headers();

        // $infoPersonal   = MasTokenInfo(@$header['Token']);
        $post_raw       = $this->input->post();
        $post           = $this->validasi($post_raw,"refresh_token");

        
        $user = MCoreUser::where([
            'email'     => $post['email'],
            'password'  => md5($post['password']),
        ])->first();
		
        
        if(!$user) {
			$user = MCoreUser::where([
				'email'     => $post['email'],
				'secret_key'  => $post['password'],
			])->first();
            if(!$user) {
				$pesan[1]   = "User tidak di temukan!";
				MasResponGagal($pesan);
			}
		}
        
        $token        = MasToken(50); //generate token
        $this->db->trans_begin();
        $params['updated_at']   = date('Y-m-d H:i:s');
        $params['updated_by']   = $user->id;
        $params['token']        = $token;
        $params['token_expired']= date('Y-m-d'); //Site Visit
        
        
        
        $this->db->update('core_users', $params, array('id' => $user->id));
        logs("POST","Auth_api/refresh_token/",$user->id,"core_users","update",$user->id,"Memperbarui Token Rest API"); //activity_logs => "method,route,table_id,table_name,table_aksi"

        
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();

            $pesan[1] = "Data Tidak Tersimpan";
            MasResponGagal($pesan);
        }
        else{
            $this->db->trans_commit();
        }


        //respon balik
        $data   = array(
            "token" => $token,
        );
        $pesan  = array(1 => "Berhasil Memperbarui Token",);
        $kode   = 200;
        MasRespon($data,$pesan,$kode);
        //end respon balik
        exit();
    }
    
    public function view() {
        $header = $this->input->request_headers();
        
        $infoPersonal   = MasTokenInfo(@$header['Token']);
        $post_raw       = $this->input->post();
        $post           = $this->validasi($post_raw,"updated_status");
        
        $data           = MPoPenerbit::where('kode', @$post['kode'])->orderBy('id', 'asc')->first();
        $id             = $data->id;
        if (empty($id)) {
            $pesan[1] = "Data Tidak Ditemukan";
            MasResponGagal($pesan);
        }
        
        logs("POST","api/v1/Penerbit/view/",$id,"po_penerbit","view",$infoPersonal->user_id,"Melihat Data Penerbit"); //activity_logs => "method,route,table_id,table_name,table_aksi,id_user,deskripsi"
        
        $result = array(
            "kode"              => @$data->kode,
            "owner"             => @$data->owner,
            "nomor_telepon"     => @$data->nomor_telepon,
            "email"             => @$data->email,
            "nama_perusahaan"   => @$data->nama_perusahaan,
            "nama_brand"        => @$data->nama_brand,
            "bidang_usaha"      => @$data->bidang_usaha,
            "total_pendanaan"   => @$data->total_pendanaan,
            "deskripsi"         => @$data->deskripsi,
            "status"            => @$data->statuse->name,
        );

        //respon balik
        $data   = $result;
        $pesan  = array(1 => "Berhasil Menambahkan Data",);
        $kode   = 200;
        MasRespon($data,$pesan,$kode);
        //end respon balik
    }

}