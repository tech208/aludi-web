<?php
class Penyelenggara extends CI_Controller {

    public function __construct() {
        parent::__construct();

        
        $this->load->model('MCoreRefStatus');
        $this->load->model('MPoAnggotaData');
        $this->load->model('MPoAnggotaDataJenis');
        $this->load->model('MPoAnggotaDataJenisLog');
        $this->load->model('MPoAnggotaDataLog');
        $this->load->model('MPoAnggotaDataLokasiPemodal');
        $this->load->model('MPoAnggotaDataLokasiPemodalLog');
        $this->load->model('MPoAnggotaDataLokasiPenerbit');
        $this->load->model('MPoAnggotaDataLokasiPenerbitLog');

        $this->load->model('MCoreRefNegara');
        $this->load->model('MCoreRefProvinsi');
        $this->load->model('MCoreRefKabupaten');
        $this->load->model('MCoreRefJenisPenerbit');
		$this->load->model('MPoPenerbit_test');

        ini_set('date.timezone', 'Asia/Jakarta');
    }

    
    public function index() {
        $header = $this->input->request_headers();
        $header['Token'] = "eyJlIjoiMjAyMTAyMjQwODMyNDAifQ==";
        
        $info = MasTokenInfoTest($header['Token']);
        MasDebugPree($info,1);
    }

    private function validasi($post,$aksi) {
        $result = true;
        $pesan  = array();
        
        // xss clean
        $post["kode_penerbit"]                    = MasXssClean($post["kode_penerbit"]);
        $post["id_provinsi"]                 = MasXssClean($post["id_provinsi"]);
        $post["jumlah_penerbit"]            = MasXssClean($post["jumlah_penerbit"]);
        $post["jumlah_penerbit_funded"]     = MasXssClean($post["jumlah_penerbit_funded"]);
        $post["jumlah_investor"]            = MasXssClean($post["jumlah_investor"]);
        $post["jumlah_investor_dividen"]    = MasXssClean($post["jumlah_investor_dividen"]);
        $post["jumlah_dana"]                = MasXssClean($post["jumlah_dana"]);
        $post["jumlah_dividen"]             = MasXssClean($post["jumlah_dividen"]);
        $post["jumlah_dividen_penerima"]    = MasXssClean($post["jumlah_dividen_penerima"]);
        // end xss clean


        if (empty(@$post["kode_penerbit"]) ) {
            $result = false;
            $pesan[] = "kode_penerbit Tidak Boleh Kosong!"; 
        }
		
		/*if (empty(@$post["id_provinsi"]) ) {
            $result = false;
            $pesan[] = "id_provinsi Tidak Boleh Kosong!"; 
        }
		
		if (empty(@$post["id_industri"]) ) {
            $result = false;
            $pesan[] = "id_industri Tidak Boleh Kosong!"; 
        }*/
		
		if (empty(@$post["kode_saham"]) ) {
            $result = false;
            $pesan[] = "kode_saham Tidak Boleh Kosong!"; 
        }
		if (empty(@$post["saham_ditawarkan"]) ) {
            $result = false;
            $pesan[] = "saham_ditawarkan Tidak Boleh Kosong!"; 
        }
		if (empty(@$post["saham_dihimpun"]) ) {
            $result = false;
            $pesan[] = "saham_dihimpun Tidak Boleh Kosong!"; 
        }
		if (empty(@$post["jumlah_dividen"]) ) {
            $result = false;
            $pesan[] = "jumlah_dividen Tidak Boleh Kosong!"; 
        }
		if (empty(@$post["saham_syariah_ditawarkan"]) ) {
            $result = false;
            $pesan[] = "saham_syariah_ditawarkan Tidak Boleh Kosong!"; 
        }
		if (empty(@$post["saham_syariah_dihimpun"]) ) {
            $result = false;
            $pesan[] = "saham_syariah_dihimpun Tidak Boleh Kosong!"; 
        }
		if (empty(@$post["sukuk_ditawarkan"]) ) {
            $result = false;
            $pesan[] = "sukuk_ditawarkan Tidak Boleh Kosong!"; 
        }
		if (empty(@$post["sukuk_dihimpun"]) ) {
            $result = false;
            $pesan[] = "sukuk_dihimpun Tidak Boleh Kosong!"; 
        }
        
        if (empty(@$post["pembagian_bunga"]) ) {
            $result = false;
            $pesan[] = "pembagian_bunga Tidak Boleh Kosong!"; 
        }
		
		if (empty(@$post["tenor"]) ) {
            $result = false;
            $pesan[] = "tenor Tidak Boleh Kosong!"; 
        }
		
		if (empty(@$post["tanggal_listed"]) ) {
            $result = false;
            $pesan[] = "tanggal_listed Tidak Boleh Kosong!"; 
        }
		
		if (empty(@$post["tanggal_fully_funded"]) ) {
            $result = false;
            $pesan[] = "tanggal_fully_funded Tidak Boleh Kosong!"; 
        }
        
        /*if (empty(@$post["jumlah_penerbit"]) AND @$post["jumlah_penerbit"]<0) {
            // $result = false;
            // $pesan[] = "jumlah_penerbit Tidak Boleh Kosong!"; 
            $post["jumlah_penerbit"] = 0;
        }else{
            if ( filter_var(@$post["jumlah_penerbit"], FILTER_VALIDATE_INT) === false ) {
                $result     = false;
                $pesan[]    = "jumlah_penerbit Hanya Boleh Angka!"; 
            }
        }

        if (empty(@$post["jumlah_penerbit_funded"]) AND @$post["jumlah_penerbit_funded"]<0) {
            // $result = false;
            // $pesan[] = "jumlah_penerbit_funded Tidak Boleh Kosong!"; 
            $post["jumlah_penerbit_funded"] = 0;
        }else{
            if ( filter_var(@$post["jumlah_penerbit_funded"], FILTER_VALIDATE_INT) === false ) {
                $result     = false;
                $pesan[]    = "jumlah_penerbit_funded Hanya Boleh Angka!"; 
            }
        }

        if (empty(@$post["jumlah_investor"]) AND @$post["jumlah_investor"]<0) {
            // $result = false;
            // $pesan[] = "jumlah_investor Tidak Boleh Kosong!"; 
            $post["jumlah_investor"] = 0;
        }else{
            if ( filter_var(@$post["jumlah_investor"], FILTER_VALIDATE_INT) === false ) {
                $result     = false;
                $pesan[]    = "jumlah_investor Hanya Boleh Angka!"; 
            }
        }

        if (empty(@$post["jumlah_investor_dividen"]) AND @$post["jumlah_investor_dividen"]<0) {
            // $result = false;
            // $pesan[] = "jumlah_investor_dividen Tidak Boleh Kosong!"; 
            $post["jumlah_investor_dividen"] = 0;
        }else{
            if ( filter_var(@$post["jumlah_investor_dividen"], FILTER_VALIDATE_INT) === false ) {
                $result     = false;
                $pesan[]    = "jumlah_investor_dividen Hanya Boleh Angka!"; 
            }
        }

        if (empty(@$post["jumlah_dana"]) AND @$post["jumlah_dana"]<0) {
            // $result = false;
            // $pesan[] = "jumlah_dana Tidak Boleh Kosong!"; 
            $post["jumlah_dana"] = 0;
        }else{
            if ( filter_var(@$post["jumlah_dana"], FILTER_VALIDATE_INT) === false ) {
                $result     = false;
                $pesan[]    = "jumlah_dana Hanya Boleh Angka!"; 
            }
        }

        if (empty(@$post["jumlah_dividen"]) AND @$post["jumlah_dividen"]<0) {
            // $result = false;
            // $pesan[] = "jumlah_dividen Tidak Boleh Kosong!";
            $post["jumlah_dividen"] = 0; 
        }else{
            if ( filter_var(@$post["jumlah_dividen"], FILTER_VALIDATE_INT) === false ) {
                $result     = false;
                $pesan[]    = "jumlah_dividen Hanya Boleh Angka!"; 
            }
        }
        
        if (empty(@$post["jumlah_dividen_penerima"]) AND @$post["jumlah_dividen_penerima"]<0) {
            // $result = false;
            // $pesan[] = "jumlah_dividen_penerima Tidak Boleh Kosong!"; 
            $post["jumlah_dividen_penerima"] = 0;
        }else{
            if ( filter_var(@$post["jumlah_dividen_penerima"], FILTER_VALIDATE_INT) === false ) {
                $result     = false;
                $pesan[]    = "jumlah_dividen_penerima Hanya Boleh Angka!"; 
            }
        }

        if (empty(@$post["jenis"]) ) {
            $result = false;
            $pesan[] = "jenis Dividen Tidak Boleh Kosong!"; 
        }else {
            foreach (@$post["jenis"] as $key => $value) {
                if ( filter_var($value['jumlah'], FILTER_VALIDATE_INT) === false ) {
                    $result     = false;
                    $pesan[]    = "jenis jumlah Hanya Boleh Angka!"; 
                }

                @$post["jenis"][$key]['jenis'] = MasXssClean($value['jenis']);

                if (empty(@$value["jenis"]) ) {
                    $result     = false;
                    $pesan[]    = "jenis Tidak Boleh Kosong!"; 
                }else{
                    $cekJenis      = MCoreRefJenisPenerbit::where('id', @$value["jenis"])->orderBy('id', 'asc')->first();
                    if (empty($cekJenis->id)) {
                        $result     = false;
                        $pesan[]    = "Jenis -> jenis Salah, Kode Jenis ".@$value["jenis"]." Tidak Ditemukan!"; 
                    }
                }
            }
        }

        if (empty(@$post["lokasi_pemodal"]) ) {
            $result = false;
            $pesan[] = "lokasi_pemodal Tidak Boleh Kosong!"; 
        }else {
            foreach (@$post["lokasi_pemodal"] as $key => $value) {
                if ( filter_var($value['jumlah'], FILTER_VALIDATE_INT) === false ) {
                    $result     = false;
                    $pesan[]    = "lokasi_pemodal -> jumlah Hanya Boleh Angka!"; 
                }

                if (empty(@$value["id_negara"]) ) {
                    $result     = false;
                    $pesan[]    = "lokasi_pemodal -> id_negara Tidak Boleh Kosong!"; 
                }else{
                    $cekNegara      = MCoreRefNegara::where('kode', @$value["id_negara"])->orderBy('id', 'asc')->first();
                    if (empty($cekNegara->id)) {
                        $result     = false;
                        $pesan[]    = "lokasi_pemodal -> id_negara Salah, Kode Negara ".@$value["id_negara"]." Tidak Ditemukan!"; 
                    }
                }
                

                if (empty(@$value["id_provinsi"]) ) {
                    $result     = false;
                    $pesan[]    = "lokasi_pemodal -> id_provinsi Tidak Boleh Kosong!"; 
                }else{
                    $cekProvinsi      = MCoreRefProvinsi::where('kode', @$value["id_provinsi"])->orderBy('id', 'asc')->first();
                    if (empty($cekProvinsi->id)) {
                        $result     = false;
                        $pesan[]    = "lokasi_pemodal -> id_provinsi Salah, Kode Provinsi ".@$value["id_provinsi"]." Tidak Ditemukan!"; 
                    }
                }

            }
        }

        
        if (empty(@$post["lokasi_penerbit"]) ) {
            $result = false;
            $pesan[] = "lokasi_penerbit Tidak Boleh Kosong!"; 
        }else {
            foreach (@$post["lokasi_penerbit"] as $key => $value) {
                if ( filter_var($value['jumlah'], FILTER_VALIDATE_INT) === false ) {
                    $result     = false;
                    $pesan[]    = "lokasi_penerbit -> jumlah Hanya Boleh Angka!"; 
                }

                if (empty(@$value["id_negara"]) ) {
                    $result     = false;
                    $pesan[]    = "lokasi_penerbit -> id_negara Tidak Boleh Kosong!"; 
                }else{
                    $cekNegara      = MCoreRefNegara::where('kode', @$value["id_negara"])->orderBy('id', 'asc')->first();
                    if (empty($cekNegara->id)) {
                        $result     = false;
                        $pesan[]    = "lokasi_penerbit -> id_negara Salah, Kode Negara ".@$value["id_negara"]." Tidak Ditemukan!"; 
                    }
                }
                
                // if (empty(@$value["id_provinsi"]) ) {
                //     $result     = false;
                //     $pesan[]    = "lokasi_penerbit -> id_provinsi Tidak Boleh Kosong!"; 
                // }else{
                //     $cekProvinsi      = MCoreRefProvinsi::where('kode', @$value["id_provinsi"])->orderBy('id', 'asc')->first();
                //     if (empty($cekProvinsi->id)) {
                //         $result     = false;
                //         $pesan[]    = "lokasi_penerbit -> id_provinsi Salah, Kode Provinsi ".@$value["id_provinsi"]." Tidak Ditemukan!"; 
                //     }
                // }

                
                if (empty(@$value["id_kota"]) ) {
                    $result     = false;
                    $pesan[]    = "lokasi_penerbit -> id_kota Tidak Boleh Kosong!"; 
                }else{
                    $cekProvinsi      = MCoreRefKabupaten::where('id', @$value["id_kota"])->orderBy('id', 'asc')->first();
                    if (empty($cekProvinsi->id)) {
                        $result     = false;
                        $pesan[]    = "lokasi_penerbit -> id_kota Salah, Kode Kota ".@$value["id_kota"]." Tidak Ditemukan!"; 
                    }
                }

            }
        }*/

        // MasDebugPree($pesan,1);


        if ($result==false) {
            MasResponGagal($pesan);
            exit();
        }

        $return = $post;
        
        
        return $return;
    }

    public function pulling() {
        $header         = $this->input->request_headers();
        
        $infoPersonal   = MasTokenInfoTest(@$header['Token'], @$header['Authorization']);
        // $infoPersonal   = array();
        $post_raw       = $this->input->post();
        $post           = $this->validasi($post_raw,"pulling");
        //$data           = MPoAnggotaData::where('id_personal', @$infoPersonal['id'])->orderBy('id', 'asc')->first();
		
		$datax           = MPoPenerbit_test::where([
				'kode'     => $post['kode_penerbit'],
				'id_personal'  => $infoPersonal['id'],
			])->orderBy('id', 'asc')->first();	
		$idx = $datax->id;
		if($idx == ""){
			$pesan[1] = "Data Penerbit Tidak Ditemukan";
            MasResponGagal($pesan);
		}
        
        $this->db->trans_begin();    
        $params = array(
            "id_personal"               =>$infoPersonal['id'],
            "kode_penerbit"                   =>$post['kode_penerbit'],
            "id_provinsi"                =>$post['id_provinsi'],
            "id_industri"           =>$post['id_industri'],
            "kode_saham"    =>$post['kode_saham'],
            "jumlah_pemodal"           =>$post['jumlah_pemodal'],
            "jumlah_user"           =>$post['jumlah_user'],
            "saham_ditawarkan"   =>$post['saham_ditawarkan'],
            "saham_dihimpun"               =>$post['saham_dihimpun'],
            "jumlah_dividen"            =>$post['jumlah_dividen'],
            "saham_syariah_ditawarkan"   =>$post['saham_syariah_ditawarkan'],
            "saham_syariah_dihimpun"   =>$post['saham_syariah_dihimpun'],
			"jumlah_dividen_syariah"            =>$post['jumlah_dividen_syariah'],
            "sukuk_ditawarkan"   =>$post['sukuk_ditawarkan'],
            "sukuk_dihimpun"   =>$post['sukuk_dihimpun'],
			"obligasi_ditawarkan"   =>$post['obligasi_ditawarkan'],
            "obligasi_dihimpun"   =>$post['obligasi_dihimpun'],
            "pembagian_bunga"   =>$post['pembagian_bunga'],
            "tenor"   =>$post['tenor'],
            "tanggal_listed"   =>$post['tanggal_listed'],
            "tanggal_fully_funded"   =>$post['tanggal_fully_funded'],
            "status"                    =>2,
        );
		
        /*if (!empty(@$data->id)) { //update data
            $params['updated_at'] = date('Y-m-d H:i:s');
            $params['updated_by'] = $infoPersonal['user_id'];
            
            
            $id = $data->id;
            $this->db->update('po_anggota_data_test', $params, array('id' => $id));
            logs("POST","api/test/penyelenggara/pulling/",$id,"po_anggota_data_test","update",$infoPersonal['user_id'],"Memperbarui Pulling Data Penyelenggara"); //activity_logs => "method,route,table_id,table_name,table_aksi,id_user,deskripsi"
            
        }else{ //insert data*/
            $params['created_at'] = date('Y-m-d H:i:s');
            $params['created_by'] = $infoPersonal['user_id'];
            
            
            $this->db->insert('po_anggota_data_test', $params);
            $id = $this->db->insert_id();
            logs("POST","api/test/penyelenggara/pulling/",$id,"po_anggota_data_test","insert",$infoPersonal['user_id'],"Menambahkan Pulling Data Penyelenggara"); //activity_logs => "method,route,table_id,table_name,table_aksi,id_user,deskripsi"
        //}

        //insert log
        $params_log = $params;
        unset($params_log['id_personal']);
        $params_log['id_anggota_data']  = $id;
        //$this->db->insert('po_anggota_data_test_log', $params_log);
        //end insert log

        //penyelenggara jenis
        $params_jenis = array();
        foreach ($post['jenis'] as $key => $value) {
            $params_jenis = array(
                "id_anggota_data"   =>$id,
                "jenis"             =>$value['jenis'],
                "jumlah"            =>$value['jumlah'],
                "status"            =>2,
            );
            
            $cekJenis      = MPoAnggotaDataJenis::where('id_anggota_data', @$id)
            ->where('jenis', @$value["jenis"])
            ->orderBy('id', 'asc')->first();
            if (!empty(@$cekJenis->id)) {
                $params_jenis['updated_at'] = date('Y-m-d H:i:s');
                $params_jenis['updated_by'] = $infoPersonal['user_id'];
            
            
                $this->db->update('po_anggota_data_test_jenis', $params_jenis, array('id' => @$cekJenis->id));
                $id_jenis = $cekJenis->id;
            }else{
                $params_jenis['created_at'] = date('Y-m-d H:i:s');
                $params_jenis['created_by'] = $infoPersonal['user_id'];
                
                
                $this->db->insert('po_anggota_data_test_jenis', $params_jenis);
                $id_jenis   = $this->db->insert_id();
            }
            
            //end log
            $params_jenis_log = $params_jenis;
            unset($params_jenis_log['id_anggota_data']);
            $params_jenis_log['id_anggota_data_jenis']  = $id_jenis;
            $this->db->insert('po_anggota_data_test_jenis_log', $params_jenis_log);
            //end log
        }
        //penyelenggara jenis

        //penyelenggara lokasi_pemodal
        $params_lokasi_pemodal = array();
        foreach ($post['lokasi_pemodal'] as $key => $value) {
            $params_lokasi_pemodal = array(
                "id_anggota_data"   =>$id,
                "id_negara"         =>MasNegara($value['id_negara']),
                "id_provinsi"       =>$value['id_provinsi'],
                "jumlah"            =>$value['jumlah'],
                "status"            =>2,
            );

            
            $cek_lokasi_pemodal      = MPoAnggotaDataLokasiPemodal::where('id_anggota_data', @$id)
            ->where('id_negara', MasNegara(@$value["id_negara"]))
            ->where('id_provinsi', @$value["id_provinsi"])
            ->orderBy('id', 'asc')->first();

            if (!empty(@$cek_lokasi_pemodal->id)) {
                $params_lokasi_pemodal['updated_at'] = date('Y-m-d H:i:s');
                $params_lokasi_pemodal['updated_by'] = $infoPersonal['user_id'];
            
            
                $this->db->update('po_anggota_data_test_lokasi_pemodal', $params_lokasi_pemodal, array('id' => @$cek_lokasi_pemodal->id));
                $id_lokasi_pemodal = $cek_lokasi_pemodal->id;
            }else{
                $params_lokasi_pemodal['created_at'] = date('Y-m-d H:i:s');
                $params_lokasi_pemodal['created_by'] = $infoPersonal['user_id'];
                
                
                $this->db->insert('po_anggota_data_test_lokasi_pemodal', $params_lokasi_pemodal);
                $id_lokasi_pemodal   = $this->db->insert_id();
            }


            //end log
            $params_lokasi_pemodal_log = $params_lokasi_pemodal;
            unset($params_lokasi_pemodal_log['id_anggota_data']);
            $params_lokasi_pemodal_log['id_anggota_data_lokasi_pemodal']  = $id_lokasi_pemodal;
            $this->db->insert('po_anggota_data_test_lokasi_pemodal_log', $params_lokasi_pemodal_log);
            //end log
        }
        //penyelenggara lokasi_pemodal


        //penyelenggara lokasi_penerbit
        $params_lokasi_penerbit = array();
        foreach ($post['lokasi_penerbit'] as $key => $value) {
            $params_lokasi_penerbit = array(
                "id_anggota_data"   =>$id,
                "id_negara"         =>MasNegara($value['id_negara']),
                // "id_provinsi"       =>$value['id_provinsi'],
                "id_kota"       =>$value['id_kota'],
                "jumlah"            =>$value['jumlah'],
                "status"            =>2,
            );

            $cek_lokasi_penerbit      = MPoAnggotaDataLokasiPenerbit::where('id_anggota_data', @$id)
                                ->where('id_negara', MasNegara(@$value["id_negara"]))
                                // ->where('id_provinsi', @$value["id_provinsi"])
                                ->where('id_kota', @$value["id_kota"])
                                ->orderBy('id', 'asc')->first();

            if (@$cek_lokasi_penerbit->id) {
                $params_lokasi_penerbit['updated_at'] = date('Y-m-d H:i:s');
                $params_lokasi_penerbit['updated_by'] = $infoPersonal['user_id'];
            
            
                $this->db->update('po_anggota_data_test_lokasi_penerbit', $params_lokasi_penerbit, array('id' => @$cek_lokasi_penerbit->id));
                $id_lokasi_penerbit = @$cek_lokasi_penerbit->id;
            }else{
                $params_lokasi_penerbit['created_at'] = date('Y-m-d H:i:s');
                $params_lokasi_penerbit['created_by'] = $infoPersonal['user_id'];
                
                
                $this->db->insert('po_anggota_data_test_lokasi_penerbit', $params_lokasi_penerbit);
                $id_lokasi_penerbit     = $this->db->insert_id();
            }

            
            
            //end log
            $params_lokasi_penerbit_log = $params_lokasi_penerbit;
            unset($params_lokasi_penerbit_log['id_anggota_data']);
            $params_lokasi_penerbit_log['id_anggota_data_lokasi_penerbit']  = $id_lokasi_penerbit;
            
            $this->db->insert('po_anggota_data_test_lokasi_penerbit_log', $params_lokasi_penerbit_log);
            //end log
        }
        //penyelenggara lokasi_penerbit        

        
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();

            $pesan[1] = "Data Tidak Tersimpan";
            MasResponGagal($pesan);
        }
        else{
            $this->db->trans_commit();
        }

        //respon balik
        $data   = array();
        $pesan  = array(1 => "Berhasil Input Laporan Penerbit",);
        $kode   = 201;
        MasRespon($data,$pesan,$kode);
        //end respon balik
        exit();
    }

    public function pulling_view() {
        $header = $this->input->request_headers();
        $infoPersonal = MasTokenInfoTest(@$header['Token'], @$header['Authorization']);

        
        $data           = MPoAnggotaData::where('id_personal', @$infoPersonal['id'])->orderBy('id', 'asc')->first();

        // MasDebugPree($data,1);

        if (empty(@$data->id)) {
            $pesan[1]   = "Data Pulling View Tidak Ditemukan!";
            MasResponGagal($pesan);
        }

		$arrpo_penerbit_test = $this->db->query("SELECT nama_perusahaan FROM po_penerbit_test WHERE kode = '".@$data->kode_penerbit."'")->row_array();
		$arrcore_ref_provinsi = $this->db->query("SELECT nama FROM core_ref_provinsi WHERE kode = '".@$data->id_provinsi."'")->row_array();
		$arrcore_ref_jenis_penerbit = $this->db->query("SELECT nama FROM core_ref_jenis_penerbit WHERE id = '".@$data->id_industri."'")->row_array();

        $result['penerbit']                  = @$arrpo_penerbit_test['nama_perusahaan'];
        $result['provinsi']               = @$arrcore_ref_provinsi['nama'];
        $result['industri']               = @$arrcore_ref_jenis_penerbit['nama'];
        $result['jumlah_penerbit']          = @$data->jumlah_penerbit;
        $result['jumlah_penerbit_funded']   = @$data->jumlah_penerbit_funded;
        $result['jumlah_investor']          = @$data->jumlah_investor;
        $result['jumlah_investor_dividen']  = @$data->jumlah_investor_dividen;
        $result['jumlah_dana']              = @$data->jumlah_dana;
        $result['jumlah_dividen']           = @$data->jumlah_dividen;

        

        
        // $result['penerbit_pemodal']['jumlah']['total']      = "";
        // $result['penerbit_pemodal']['jumlah']['kota']       = "";
        // $result['penerbit_pemodal']['jumlah']['negara']     = "";


        //respon balik
        $data   = $result;
        $pesan  = array(1 => "Data Ditemukan",);
        $kode   = 200;
        MasRespon($data,$pesan,$kode);
        //end respon balik
    }

    public function pulling_view_all() {
        $header = $this->input->request_headers();
        $infoPersonal = MasTokenInfoTest($header['Token']);

        
        if (@$infoPersonal->role_id != "3") { //Admin OJK
            $pesan[1]   = "Data Pulling View ALL Hanya Untuk OJK!";
            MasResponGagal($pesan);
        }

        $query_data = $this->db->query("SELECT
                SUM(pad.`jumlah_penerbit`) AS 'jumlah_penerbit',
                SUM(pad.`jumlah_penerbit_funded`) AS 'jumlah_penerbit_funded',
                SUM(pad.`jumlah_investor`) AS 'jumlah_investor',
                SUM(pad.`jumlah_investor_dividen`) AS 'jumlah_investor_dividen',
                SUM(pad.`jumlah_dana`) AS 'jumlah_dana',
                SUM(pad.`jumlah_dividen`) AS 'jumlah_dividen',
                SUM(pad.`jumlah_dividen_penerima`) AS 'jumlah_dividen_penerima'
            FROM `po_anggota_data_test` pad
            WHERE pad.`status`=2
        ");
        $data = @$query_data->result_array()[0];


        $result['jumlah_penerbit']          = @$data->jumlah_penerbit;
        $result['jumlah_penerbit_funded']   = @$data->jumlah_penerbit_funded;
        $result['jumlah_investor']          = @$data->jumlah_investor;
        $result['jumlah_investor_dividen']  = @$data->jumlah_investor_dividen;
        $result['jumlah_dana']              = @$data->jumlah_dana;
        $result['jumlah_dividen']           = @$data->jumlah_dividen;
        $result['jumlah_dividen_penerima']  = @$data->jumlah_dividen_penerima;


        $query_data = $this->db->query("SELECT
                `jenis`,
                SUM(`jumlah`) AS jumlah
            FROM `po_anggota_data_test_jenis` padj
            WHERE padj.`status`=2
            GROUP BY padj.`jenis`
        ");
        $data_jenis = @$query_data->result_array();
        $result['jenis']['jumlah']['total']         = count($data_jenis);
        foreach ($data_jenis as $key => $value) {
            $result['jenis']['data'][$value['jenis']]     = $value['jumlah'];
        }
        

        $query_data_lokasi_penerbit = $this->db->query("SELECT
            crp.`nama` AS 'provinsi', 
            `jumlah`
        FROM `po_anggota_data_test_lokasi_pemodal` padlp
        LEFT JOIN core_ref_provinsi crp ON padlp.`id_provinsi`= crp.`id`
        WHERE padlp.`status`=2
        GROUP BY padlp.`id_provinsi`      
        ");
        $data_lokasi_penerbit = @$query_data_lokasi_penerbit->result_array();
        $result['lokasi_penerbit']['jumlah']['total']         = count($data_lokasi_penerbit);
        foreach ($data_lokasi_penerbit as $key => $value) {
            $result['lokasi_penerbit']['data'][$value['provinsi']]     = $value['jumlah'];
        }
        

        $query_data_lokasi_pemodal = $this->db->query("SELECT
            crp.`nama` AS 'provinsi', 
            `jumlah`
        FROM `po_anggota_data_test_lokasi_penerbit` padlpe
        LEFT JOIN core_ref_provinsi crp ON padlpe.`id_provinsi`= crp.`id`
        WHERE padlpe.`status`=2
        GROUP BY padlpe.`id_provinsi`      
        ");
        $data_lokasi_pemodal = @$query_data_lokasi_pemodal->result_array();
        $result['lokasi_pemodal']['jumlah']['total']         = count($data_lokasi_pemodal);
        foreach ($data_lokasi_pemodal as $key => $value) {
            $result['lokasi_pemodal']['data'][$value['provinsi']]     = $value['jumlah'];
        }


        //respon balik
        $data   = $result;
        $pesan  = array(1 => "Data Ditemukan",);
        $kode   = 200;
        MasRespon($data,$pesan,$kode);
        //end respon balik
    }

    
}