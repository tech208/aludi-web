<?php
class Dashboard extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    
    public function index() {
        $header = $this->input->request_headers();
        $header['Token'] = "eyJlIjoiMjAyMTAyMjQwODMyNDAifQ==";
        
        $info = MasTokenInfo($header['Token']);
        MasDebugPree($info,1);



        $result['nama_perusahaan']      = "";
        $result['nama_brand']           = "";
        $result['bidang_usaha']         = "";
        $result['alamat_usaha']         = "";
        $result['jumlah_penerbit']      = "";
        $result['jumlah_pemodal']       = "";
        $result['jumlah_pendanaan']     = "";
        $result['jumlah_dividen']       = "";

        $result['penerbit_kategori']['jumlah']['total']         = "";
        $result['penerbit_kategori']['kategori']['kuliner']     = "";
        $result['penerbit_kategori']['kategori']['tech']        = "";
        $result['penerbit_kategori']['kategori']['enterprice']  = "";
        $result['penerbit_kategori']['kategori']['lainya']      = "";
        
        $result['penerbit_kota']['jumlah']['total']     = "";
        $result['penerbit_kota']['kota']['jakarta']     = "";
        $result['penerbit_kota']['kota']['surabaya']    = "";
        $result['penerbit_kota']['kota']['yogyakarta']  = "";
        $result['penerbit_kota']['kota']['madura']      = "";

        
        $result['penerbit_pemodal']['jumlah']['total']      = "";
        $result['penerbit_pemodal']['jumlah']['kota']       = "";
        $result['penerbit_pemodal']['jumlah']['negara']     = "";
        $result['penerbit_pemodal']['kota']['jakarta']      = "";
        $result['penerbit_pemodal']['kota']['surabaya']     = "";
        $result['penerbit_pemodal']['kota']['yogyakarta']   = "";
        $result['penerbit_pemodal']['kota']['madura']       = "";

        
        $result['penerbit_pendanaan']['total_tersalurkan']  = "";
        $result['penerbit_pendanaan']['total_penerima']     = "";

        $result['penerbit_deviden']['total_deviden']        = "";
        $result['penerbit_deviden']['total_penerima']       = "";


        //respon balik
        $data   = $result;
        $pesan  = array(1 => "Berhasil Menambahkan Data",);
        $kode   = 200;
        MasRespon($data,$pesan,$kode);
        //end respon balik
    }

    public function created() {
        $header = $this->input->request_headers();
        $header['Token'] = "eyJlIjoiMjAyMTAyMjQwODMyNDAifQ==";
        
        $info = MasTokenInfo($header['Token']);
        MasDebugPree($info,1);
    }
    
    public function updated() {
        $header = $this->input->request_headers();
        $header['Token'] = "eyJlIjoiMjAyMTAyMjQwODMyNDAifQ==";
        
        $info = MasTokenInfo($header['Token']);
        MasDebugPree($info,1);
    }

    public function deleted() {
        $header = $this->input->request_headers();
        $header['Token'] = "eyJlIjoiMjAyMTAyMjQwODMyNDAifQ==";
        
        $info = MasTokenInfo($header['Token']);
        MasDebugPree($info,1);
    }

    public function detail() {
        $header = $this->input->request_headers();
        $header['Token'] = "eyJlIjoiMjAyMTAyMjQwODMyNDAifQ==";
        
        $info = MasTokenInfo($header['Token']);
        MasDebugPree($info,1);
    }
}