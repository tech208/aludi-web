<?php

class Po_trails extends sntr_Controller {

    public function __construct() {
        parent::__construct();
        ini_set('max_excecution_time', 0); 
        $this->load->model('MCoreRefStatus');
        $this->load->model('MActivityLog');
    }

    public function index() {
        //$data = MActivityLog::orderBy('id', 'DESC')->get();
       
        $start_date     = "";
        $end_date       = "";
        if (empty(@$_POST)) {
            @$_POST['start_date']   = date('Y-m-d', strtotime(" -2 days"));
            @$_POST['end_date']     = date('Y-m-d');
            
            
            $data = MActivityLog::whereBetween('created_at', [
                $_POST['start_date'],
                $_POST['end_date']
            ])
            ->orderBy('id', 'DESC')->get();

            @$start_date   = @$_POST['start_date'];
            @$end_date     = @$_POST['end_date'];
        }

        
        if (!empty(@$_POST)) {
        
        }


        $this->setParam('start_date', $start_date);
        $this->setParam('end_date', $end_date);
        $this->setParam('data', @$data);
		$this->setParam('url', 'po_trails/get_data/'.@$start_date.'/'.@$end_date);
        $this->setContentView('web/po_trails/index');
        $this->render('Audit Trail');
    }
	
	 public function get_data($start_date = "", $end_date = "") {
      
		/*if($start_date != "" && $end_date != ""){
			$arrwhere[] = "(REPLACE(LEFT(A.created_at, 10), '-', '') BETWEEN REPLACE(LEFT('$start_date', 10), '-', '') AND REPLACE(LEFT('$end_date', 10), '-', ''))";
		}*/
		$arrjoin[] = "LEFT JOIN core_users B ON B.id = A.user_id";
		$arrjoin[] = "LEFT JOIN core_role C ON C.id = B.role_id";
		$arrjoin[] = "LEFT JOIN personals D ON D.user_id = B.id";
		$arrorder[] = "A.id DESC";
		//$arrorder[] = "(account_detail_debit-account_detail_paid) DESC";
		//$arrwhere[] = "A.sales_type = 0";
		$table = "activity_logs A";
		$id = "A.id";
		$id2 = "id";
		$field = array('D.name as personal_name', 'B.email', 'C.name as role_name', 'A.created_at', 'A.route');
		$field2 = array('personal_name', 'email', 'role_name', 'created_at', 'route');
		$url = $this->url_;
		/*if ($_SESSION['role_id'] == "7" || $_SESSION['role_id'] == "12"){
			$action = '<a href="' . site_url($url . "/form_detail/xid") . '" class="btn btn-info"> <i class="fa fa-edit"></i></a> <a href="' . site_url($url . "/table_detail/xid") . '" class="btn btn-warning"> <i class="fa fa-list"></i></a> <a href="' . site_url($url . "/cetak_invoice_d/xid") . '" class="btn btn-danger"> <i class="fa fa-print"></i></a> <a href="' . site_url($url . "/delete_pr/xid") . '" onclick="return confirm(\'Apakah anda yakin menghapus PR ini?\')" class="btn btn-danger"> <i class="fa fa-trash"></i></a>';
		}else{
			$action = '<a href="' . site_url($url . "/table_detail/xid") . '" class="btn btn-warning"> <i class="fa fa-list"></i></a> <a href="' . site_url($url . "/cetak_invoice_d/xid") . '" class="btn btn-danger"> <i class="fa fa-print"></i></a>';
		}*/
		$action = '';
		$jfield = join(', ', $field);
		$start = (@$_GET['start'] == '') ? 0 : $_GET['start'];
		$length = (@$_GET['length'] == '') ? 10 : $_GET['length'];
		$limit = "LIMIT $start, $length";
		$join = "";
		if (@$arrjoin != "") {
			foreach ($arrjoin as $jkey => $jvalue) {
				$arrjoin2[] = $jvalue;
			}
			$join = join(' ', $arrjoin2);
		}
		$where = "";
		$where2 = "";
		$search = $_GET['search']['value'];
		$arrwhere2 = @$arrwhere;
		if (@$arrwhere2 != '') $where2 = 'WHERE ' . join(' AND ', $arrwhere2);
		if (@$search != "") {
			foreach ($field as $key => $value) {
				if ($value == 'D.name as personal_name'){ 
					$arrfield[] = "D.name LIKE '%$search%'";
				}else if($value == 'C.name as role_name'){
					$arrfield[] = "C.name LIKE '%$search%'";
				}else{
					$arrfield[] = "$value LIKE '%$search%'";
				}
			}
			$arrwhere[] = '(' . join(' OR ', $arrfield) . ')';
		}
		if (@$arrwhere != '') $where = 'WHERE ' . join(' AND ', $arrwhere);
		$ix = '0';
		foreach (@$_GET['order'] as $key2 => $value2) {
			if ($value2['column'] != "0") {
				if ($ix == '0') $arrorder = array();
				$ix++;
			}
			$arrorder[] = ($value2['column'] + 1) . ' ' . $value2['dir'];
		}
		$order = 'ORDER BY ' . join(', ', $arrorder);
		$data = array();
		$jum_all = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where2")->row_array();
		$jum_filter = $this->db->query("SELECT COUNT($id) as jum FROM $table $join $where")->row_array();
		$result = $this->db->query("SELECT $id, $jfield FROM $table $join $where $order $limit")->result_array();
		
		$i = $start;
		$dataxy = array();
		foreach ($result as $keyr => $valuer) {
			$i++;
			$datax = array();
			$datax[] = $i;
			foreach ($field2 as $keyfield) {
				if ($keyfield == 'total') {
					if ($valuer[$keyfield] == "LUNAS") {
						$datax[] = $valuer[$keyfield];
					} else {
						$datax[] = "Rp " . number_format(($valuer[$keyfield] == "") ? 0 : $valuer[$keyfield]);
					}
				} else {
					$datax[] = $valuer[$keyfield];
				}
			}
			//$datax[] = str_replace('xid', $valuer[$id2], $action);
			$dataxy[] = $datax;
		}
		$data = array(
			'draw' => $_GET['draw'],
			'recordsTotal' => (int)$jum_all['jum'],
			'recordsFiltered' => (int)$jum_filter['jum'],
			'data' => @$dataxy
		);

		echo json_encode($data);
    }
	
    public function detail($id) {
        $data = MPoUjian::find($id);
        if(!$data) {
            return redirect('po_trails/');
        }
        
        $this->setParam('data', @$data);
        $this->setContentView('web/po_trails/detail');
        $this->render('Detail Audit Trail');
    }


}