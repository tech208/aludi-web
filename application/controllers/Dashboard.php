<?php

use Illuminate\Database\Capsule\Manager as DB;
use Carbon\Carbon;

class Dashboard extends sntr_Controller {

    public function __construct() {
        parent::__construct();
        
        $this->load->model('MPersonal');
        $this->load->model('MPoPendaftaran');
        $this->load->model('MPoPendaftaranAlur');
        $this->load->model('MPoPendaftaranTahapPembayaran');
        $this->load->model('MPoPendaftaranTahapRekomendasi');
        $this->load->model('MPoPendaftaranTahap');
        $this->load->model('MPoPendaftaranTahapDua');
        $this->load->model('MPoPendaftaranTahapSatu');
        $this->load->model('MPoPendaftaranTahapVisit');
        $this->load->model('MPoPendaftaranTahapVisitLangkah');
        $this->load->model('MCoreRefStatus');

        ini_set('date.timezone', 'Asia/Jakarta');
    }

    public function index() {
        if (MasKaryawan()->user->role_id==2) {
            $data           = MPoPendaftaranTahap::where('id_personal', MasKaryawan()->id)->orderBy('id', 'asc')->first();
            $this->setContentView('web/dashboard/index_peserta');
        // }elseif (MasKaryawan()->user->role_id==1) {
        }else{
            $data = new StdClass();

            $data->menunggu_verifikasi_tahap1           = MPoPendaftaranTahap::where('status', 17)->orderBy('id', 'asc')->get();
            $data->menunggu_verifikasi_tahap1_total     = $data->menunggu_verifikasi_tahap1->count();
            $data->menunggu_verifikasi_pembayaran       = MPoPendaftaranTahap::where('status', 21)->orderBy('id', 'asc')->get();
            $data->menunggu_verifikasi_pembayaran_total = $data->menunggu_verifikasi_pembayaran->count();
            $data->menunggu_verifikasi_tahap2           = MPoPendaftaranTahap::where('status', 25)->orderBy('id', 'asc')->get();
            $data->menunggu_verifikasi_tahap2_total     = $data->menunggu_verifikasi_tahap2->count();
            $data->menunggu_verifikasi_visit            = MPoPendaftaranTahap::where('status', 28)->orderBy('id', 'asc')->get();
            $data->menunggu_verifikasi_visit_total      = $data->menunggu_verifikasi_visit->count();
            $data->menunggu_verifikasi_rekomendasi      = MPoPendaftaranTahap::where('status', 30)->orderBy('id', 'asc')->get();
            $data->menunggu_verifikasi_rekomendasi_total= $data->menunggu_verifikasi_rekomendasi->count();


            
            $data->status_tahap1      = MPoPendaftaranTahap::where('status', 30)->orderBy('id', 'asc')->get();
            $data->status_tahap1_total= $data->status_tahap1->count();
            
            $data->status_menunggu_pembayaran      = MPoPendaftaranTahap::where('status', 30)->orderBy('id', 'asc')->get();
            $data->status_menunggu_pembayaran_total= $data->status_menunggu_pembayaran->count();
            
            $data->status_tahap2      = MPoPendaftaranTahap::where('status', 30)->orderBy('id', 'asc')->get();
            $data->status_tahap2_total= $data->status_tahap2->count();

            $data->status_site_visit      = MPoPendaftaranTahap::where('status', 30)->orderBy('id', 'asc')->get();
            $data->status_site_visit_total= $data->status_site_visit->count();
            
            $data->status_rekomendasi_ojk      = MPoPendaftaranTahap::where('status', 30)->orderBy('id', 'asc')->get();
            $data->status_rekomendasi_ojk_total= $data->status_rekomendasi_ojk->count();

            
            // $data->total_member      = MPoPendaftaranTahap::where('status', 31)->orderBy('id', 'asc')->get();
            // $data->total_member      = MPoPendaftaranTahap::orderBy('id', 'asc')->get();
            // $data->total_member_total= $data->total_member->count();

            $total_syariah            = MPoPendaftaranTahapSatu::where('jenis_perusahaan', 'syariah')->orderBy('id', 'asc')->get();
            foreach ($total_syariah as $key => $value) {
                $tahap      = MPoPendaftaranTahap::find(@$value['id_tahap']);
                // if ($tahap->status!=31) {
                //     unset($total_syariah[$key]);
                // }
            }
            $data->total_syariah      = $total_syariah;
            $data->total_syariah_total= ($data->total_syariah->count()<=0 OR empty($data->total_syariah->count()) ) ? 0 : $data->total_syariah->count() ;

            
            $data->tahap           = MPoPendaftaranTahap::where('status', 17)->orderBy('id', 'asc')->get();
            
            $total_konvensional            = MPoPendaftaranTahapSatu::where('jenis_perusahaan', 'konvensional')->orderBy('id', 'asc')->get();
            foreach ($total_konvensional as $key => $value) {
                $tahap      = MPoPendaftaranTahap::find(@$value['id_tahap']);
                // if ($tahap->status!=31) {
                //     unset($total_konvensional[$key]);
                // }
            }
            $data->total_konvensional      = $total_konvensional;
            $data->total_konvensional_total=  ($data->total_konvensional->count()<=0 OR empty($data->total_konvensional->count()) ) ? 0 : $data->total_konvensional->count() ; 
            
            $total_hybrid_total            = MPoPendaftaranTahapSatu::where('jenis_perusahaan', 'hybrid')->orderBy('id', 'asc')->get();
            foreach ($total_hybrid_total as $key => $value) {
                $tahap      = MPoPendaftaranTahap::find(@$value['id_tahap']);
                // if ($tahap->status!=31) {
                //     unset($total_hybrid_total[$key]);
                // }
            }
            $data->total_hybrid_total   = $total_hybrid_total;
            $data->total_hybrid_total   =  ($data->total_hybrid_total->count()<=0 OR empty($data->total_hybrid_total->count()) ) ? 0 : $data->total_hybrid_total->count() ; 
            
            $data->total_member_total= $data->total_syariah_total+$data->total_konvensional_total+$data->total_hybrid_total;
            
            
            $query_status = $this->db->query("SELECT
                    `status`,
                    COUNT(ppt.`id`) AS jumlah,
                    crs.name AS `status_name`
                FROM `po_pendaftaran_tahap` AS ppt
                LEFT JOIN core_ref_status AS crs ON crs.`id`=ppt.`status`
                GROUP BY ppt.`status`
            ");
            $status = @$query_status->result_array();
            $status_jumlah = array();
            foreach ($status as $key => $value) {
                $status_jumlah[$value['status']] = $value['jumlah'];   
            } 
            $data->tahap_1              = (empty($status_jumlah['16'])) ? 0 : $status_jumlah['16'];
            $data->menunggu_pembayaran  = (empty($status_jumlah['21'])) ? 0 : $status_jumlah['21'];
            $data->tahap_2              = (empty($status_jumlah['24'])) ? 0 : $status_jumlah['24'];
            $data->site_visit           = (empty($status_jumlah['29'])) ? 0 : $status_jumlah['29'];
            $data->rekomendasi_ojk      = (empty($status_jumlah['30'])) ? 0 : $status_jumlah['30'];
            

            $query = $this->db->query("SELECT
                    COUNT(id) y,
                    YEAR(created_at)  label
                FROM po_pendaftaran_tahap ppt
                WHERE ppt.status='31'
                GROUP BY YEAR(ppt.created_at)
            ");
            $data->perkembangan = @$query->result_array();


            // CONCAT(DATE_FORMAT(ppt.created_at, %M),' ',YEAR(ppt.created_at)) label
            $query_bulanan = $this->db->query("SELECT
                    COUNT(id) y,
                    MONTH(created_at) bulan,
                    YEAR(created_at) tahun
                FROM po_pendaftaran_tahap ppt
                WHERE ppt.status='31'
                GROUP BY MONTH(ppt.created_at),YEAR(ppt.created_at)          
            ");
            $data->perkembangan_bulanan = @$query_bulanan->result_array();

            // MasDebugPree($data->perkembangan_bulanan,1);
            foreach ($data->perkembangan_bulanan as $key => $value) {
                $data->perkembangan_bulanan[$key]['label'] = MasBulan($value['bulan']).' '.$value['tahun'];
            }
            // $data->perkembangan = array();


            $this->setContentView('web/dashboard/index');
        }

        $this->setParam('data', $data);
        $this->render('Dashboard');
    }
    
    public function detail() {
        $get    = $this->input->get();

        $data   = array();
        $mode   = "";
        if ($get['mode']=="jumlah_member" || $get['mode'] == "") {
            
            $total_syariah            = MPoPendaftaranTahapSatu::where('jenis_perusahaan', 'syariah')->orderBy('id', 'asc')->get();
            foreach ($total_syariah as $key => $value) {
                $tahap      = MPoPendaftaranTahap::find(@$value['id_tahap']);
                // if ($tahap->status!=31) {
                //     unset($total_syariah[$key]);
                // }
            }
            $data['total_syariah']      = $total_syariah;
            $data['total_syariah_total']= ($data['total_syariah']->count()<=0 OR empty($data['total_syariah']->count()) ) ? 0 : $data['total_syariah']->count() ;

            
            $total_konvensional            = MPoPendaftaranTahapSatu::where('jenis_perusahaan', 'konvensional')->orderBy('id', 'asc')->get();
            foreach ($total_konvensional as $key => $value) {
                $tahap      = MPoPendaftaranTahap::find(@$value['id_tahap']);
                // if ($tahap->status!=31) {
                //     unset($total_konvensional[$key]);
                // }
            }
            $data['total_konvensional']      = $total_konvensional;
            $data['total_konvensional_total']=  ($data['total_konvensional']->count()<=0 OR empty($data['total_konvensional']->count()) ) ? 0 : $data['total_konvensional']->count() ; 
            
            $total_hybrid_total            = MPoPendaftaranTahapSatu::where('jenis_perusahaan', 'hybrid')->orderBy('id', 'asc')->get();
            foreach ($total_hybrid_total as $key => $value) {
                $tahap      = MPoPendaftaranTahap::find(@$value['id_tahap']);
                // if ($tahap->status!=31) {
                //     unset($total_hybrid_total[$key]);
                // }
            }
            $data['total_hybrid']           = $total_hybrid_total;
            $data['total_hybrid_total']     = ($data['total_hybrid']->count()<=0 OR empty($data['total_hybrid']->count()) ) ? 0 : $data['total_hybrid']->count() ; 

            $data['jumlah_member']          = $data['total_syariah_total']+$data['total_konvensional_total']+$data['total_hybrid_total'];

            $this->setContentView('web/dashboard/detail_jumlah_member');
            $mode = "Jumlah Member : ".$data['jumlah_member'];
        }

        
        if ($get['mode']=="status_member") {
            
            $data['status_tahap1']                      = MPoPendaftaranTahap::where('status', 16)->orderBy('id', 'asc')->get();
            $data['status_tahap1_total']                = $data['status_tahap1']->count();
            
            $data['status_menunggu_pembayaran']         = MPoPendaftaranTahap::where('status', 21)->orderBy('id', 'asc')->get();
            $data['status_menunggu_pembayaran_total']   = $data['status_menunggu_pembayaran']->count();
            
            $data['status_tahap2']                      = MPoPendaftaranTahap::where('status', 24)->orderBy('id', 'asc')->get();
            $data['status_tahap2_total']                = $data['status_tahap2']->count();

            $data['status_site_visit']                  = MPoPendaftaranTahap::where('status', 29)->orderBy('id', 'asc')->get();
            $data['status_site_visit_total']            = $data['status_site_visit']->count();
            
            $data['status_rekomendasi_ojk']             = MPoPendaftaranTahap::where('status', 30)->orderBy('id', 'asc')->get();
            $data['status_rekomendasi_ojk_total']       = $data['status_rekomendasi_ojk']->count();

            // foreach ($data['status_tahap1'] as $key => $value) {
                // MasDebugPree(@$value->tahap_satu);
                // MasDebugPree(@$value->tahap_satu);
                // MasDebugPree($value->tahap_satu->nama_perusahaan);
            // }
            // MasDebugPree('as',1);
            
            

            $data['jumlah_status_member']               = $data['status_tahap1_total']+$data['status_menunggu_pembayaran_total']+$data['status_tahap2_total']+$data['status_site_visit_total']+$data['status_rekomendasi_ojk_total'];

            $this->setContentView('web/dashboard/detail_status_member');
            $mode = "Status Member : ".$data['jumlah_status_member'];
        }
        
        if ($get['mode']=="jumlah_member_perkembangan_tahunan") {
            $query = $this->db->query("SELECT
                    id,
                    YEAR(created_at)  tahun
                FROM po_pendaftaran_tahap ppt
                WHERE ppt.status='31'
                GROUP BY YEAR(ppt.created_at),id
            ");
            $perkembangan       = @$query->result_array();
            $perkembangan_total = count($perkembangan);

            $query_tahun = $this->db->query("SELECT
                    COUNT(id) total,
                    YEAR(created_at)  tahun
                FROM po_pendaftaran_tahap ppt
                WHERE ppt.status='31'
                GROUP BY YEAR(ppt.created_at)
            ");
            $perkembangan_tahun       = @$query_tahun->result_array();

            $perkembangan_tahun_group = array();
            foreach ($perkembangan_tahun as $key => $value) {
                $perkembangan_tahun_group[$value['tahun']] = $value['total'];
            }


            $perkembangan_group = array();
            foreach ($perkembangan as $key => $value) {
                $perkembangan_group[$value['tahun']][] = $value['id'];
            }
            
            $perkembangan_group_tahunan = array();
            foreach ($perkembangan_group as $key_group => $value_group) {
                $perkembangan_group_tahunan[$key_group] = MPoPendaftaranTahap::find($value_group);
            }
            $data['perkembangan_tahun'] = $perkembangan_tahun_group;
            $data['perkembangan']       = $perkembangan_group_tahunan;

            $this->setContentView('web/dashboard/detail_jumlah_member_perkembangan_tahunan');
            $mode = "Jumlah Member Perkembangan Tahunan : ".$perkembangan_total;
        }

        if ($get['mode']=="jumlah_member_perkembangan_bulanan") {
            $query_bulanan_group_raw = $this->db->query("SELECT
                    COUNT(id) total,
                    MONTH(created_at) bulan,
                    YEAR(created_at) tahun
                FROM po_pendaftaran_tahap ppt
                WHERE ppt.status='31'
                GROUP BY MONTH(ppt.created_at),YEAR(ppt.created_at)          
            ");
            $perkembangan_bulanan_group_raw = @$query_bulanan_group_raw->result_array();

            $perkembangan_bulanan_group_total = array();
            foreach ($perkembangan_bulanan_group_raw as $key => $value) {
                $perkembangan_bulanan_group_total[$value['tahun']][$value['bulan']] = $value['total'];
            }


            $query_bulanan_raw = $this->db->query("SELECT
                    id,
                    MONTH(created_at) bulan,
                    YEAR(created_at) tahun
                FROM po_pendaftaran_tahap ppt
                WHERE ppt.status='31'
                GROUP BY MONTH(ppt.created_at),YEAR(ppt.created_at),id          
            ");
            $perkembangan_bulanan_raw = @$query_bulanan_raw->result_array();

            $perkembangan_bulanan = array();
            foreach ($perkembangan_bulanan_raw as $key_raw => $value_raw) {
                $perkembangan_bulanan[$value_raw['tahun']][$value_raw['bulan']][] = MPoPendaftaranTahap::find($value_raw['id']);
            }

            $data['perkembangan_bulanan_group'] = $perkembangan_bulanan_group_total;
            $data['perkembangan_bulanan']       = $perkembangan_bulanan;

            $this->setContentView('web/dashboard/detail_jumlah_member_perkembangan_bulanan');
            $mode = "Jumlah Member Perkembangan Bulanan ";
        }
        
        $this->setParam('data', $data);
        $this->render('Dashboard '.$mode);
    }
    
    public function save_berkas(){
        $post       = $this->input->post();
        if (empty($post)) { redirect('/dashboard/'); }//bila gak ada inputan

        $this->db->trans_begin();


        $params = array(
            "id_rekomendasi"    => $post['id_rekomendasi'], 
            "nama"              => $post['nama'], 
            "deskripsi"         => $post['deskripsi'], 
            "tanggal"           => $post['tanggal'],
            "status"            => 2, //Aktif
        );

        $params["created_at"]   = date('Y-m-d H:i:s');
        $params["created_by"]   = authUser()->id;
        $this->db->insert('po_pendaftaran_tahap_rekomendasi_log', $params);

        $id = $this->db->insert_id();
        logs("POST","dashboard/save_berkas/",$id,"po_pendaftaran_tahap_rekomendasi_log","insert"); //activity_logs => "method,route,table_id,table_name,table_aksi"
        

        if(!empty($_FILES['files']['name']['rekomendasi_log'])) {//bila ada lampiran rekomendasi_log
            $config['upload_path']          = './uploads/rekomendasi_log/';
            // $config['allowed_types']        = 'jpg|jpeg|png';
            $config['allowed_types']        = '*';
            $config['overwrite']        	= true;
            $config['encrypt_name']        	= true;
            $config['max_size']             = 10120;

            $this->load->library('upload', $config);

            
            $_FILES['rekomendasi_log']['name']       = $_FILES['files']['name']['rekomendasi_log'];
            $_FILES['rekomendasi_log']['type']       = $_FILES['files']['type']['rekomendasi_log'];
            $_FILES['rekomendasi_log']['tmp_name']   = $_FILES['files']['tmp_name']['rekomendasi_log'];
            $_FILES['rekomendasi_log']['error']      = $_FILES['files']['error']['rekomendasi_log'];
            $_FILES['rekomendasi_log']['size']       = $_FILES['files']['size']['rekomendasi_log'];

            
            if (!$this->upload->do_upload('rekomendasi_log')) {
                // MasDebugPree($this->upload->display_errors(),1);
                set_flashdata('error', 'Data Tidak Berhasil Disimpan, Pastikan File jpg|jpeg|png Dan ukuran Maximal 10mb');
                redirect('/po_tahap_pendaftaran/tahap2');
            }

            $upload_data        = $this->upload->data();
            $params_rekomendasi_log['nama_file']    = $upload_data['file_name'];
    
            $this->db->update('po_pendaftaran_tahap_rekomendasi_log', $params_rekomendasi_log, array('id' => $id));
        }

    
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }
        else{
            $this->db->trans_commit();
        }

        set_flashdata('success', 'Data berhasil disimpan');
        redirect('/dashboard');
    
    }

}