<?php

class Po_tahap_pendaftaran extends sntr_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('MPoPendaftaran');
        $this->load->model('MPoPendaftaranAlur');
        $this->load->model('MPoPendaftaranTahapPembayaran');
        $this->load->model('MPoPendaftaranTahapRekomendasi');
        $this->load->model('MPoPendaftaranTahap');
        $this->load->model('MPoPendaftaranTahapDua');
        $this->load->model('MPoPendaftaranTahapSatu');
        $this->load->model('MPoPendaftaranTahapVisit');
        $this->load->model('MPoPendaftaranTahapVisitHasil');
        $this->load->model('MPoPendaftaranTahapVisitLangkah');
        $this->load->model('MCoreRefStatus');
        
        $this->load->model('MPoUjian');
        $this->load->model('MCoreRefProvinsi');
        $this->load->model('MCoreRefKabupaten');
        $this->load->model('MCoreRefKecamatan');
        $this->load->model('MCoreRefDesa');
        
        ini_set('date.timezone', 'Asia/Jakarta');
    }

    public function index() {

        // MasDebugPree(MasKaryawan()->user->role_id,1);
        
        if (MasKaryawan()->user->role_id==2) {
            $data_tahap = MPoPendaftaranTahap::where('id_personal', MasKaryawan()->id)->orderBy('id', 'asc')->first();
            // MasDebugPree($data_tahap,1);
            $this->tahap($data_tahap->statuse->grup); //untuk cek pendaftaran
        }
        $data = new \stdClass; 

        @$data->step = 'index';
        $this->setParam('data', $data);
        $this->setContentView('web/po_tahap_pendaftaran/index');
        $this->render(' ');
    }

    public function tahap($grup_status) {
        if ($grup_status==6) { //status grup Tahap 1
            redirect('po_tahap_pendaftaran/tahap1');
        }
        elseif ($grup_status==7) { //status grup Pembayaran
            redirect('po_tahap_pendaftaran/pembayaran');
        }
        elseif ($grup_status==8) { //status grup Tahap 2
            redirect('po_tahap_pendaftaran/tahap2');
        }
        elseif ($grup_status==9) { //status grup Site Visit
            redirect('po_tahap_pendaftaran/visit');
        }
        elseif ($grup_status==10) { //status grup Menunggu Proses Rekomendasi
            redirect('po_tahap_pendaftaran/rekomendasi');
        }
        elseif ($grup_status==11) { //status grup Selesai
            redirect('po_tahap_pendaftaran/selesai');
        }
    }
    
    public function tahap1($id='',$mode='') {
        
        $data = new \stdClass;

        if(MasKaryawan()->user->role_id==2){ //jika peserta
            $data = MPoPendaftaranTahapSatu::where('id_personal', MasKaryawan()->id)->orderBy('id', 'asc')->first();
        }

        if (!empty($id) AND empty($data->id)) {
            $data = MPoPendaftaranTahapSatu::find($id);
        }

        // MasDebugPree($data,1);

        $refProvinsi      = MCoreRefProvinsi::
                            orderBy('nama', 'asc')->get();

        if (!empty(@$data->id_provinsi)) {
            $refKabupaten      = MCoreRefKabupaten::
                                where("id_provinsi",@$data->id_provinsi)->orderBy('nama', 'asc')->get();
        }
        else {
            $refKabupaten      = new \stdClass;
        }

                            
        @$data->step = $this->uri->segment(2);
        $this->setParam('refKabupaten', $refKabupaten);
        $this->setParam('refProvinsi', $refProvinsi);
        $this->setParam('data', $data);
        $this->setParam('mode', $mode);
        $this->setContentView('web/po_tahap_pendaftaran/index_tahap1');
        $this->render(' ');
    }

    public function saveTahap1(){
        ini_set('date.timezone', 'Asia/Jakarta');
        $data       = $this->input->post();
        if (empty($data)) { redirect('/po_tahap_pendaftaran/tahap1/'); }//bila gak ada inputan

        $this->db->trans_begin();

        $id = @$data['id'];
        
        $data_tahap = MPoPendaftaranTahap::where('id_personal', $data['id_personal'])->orderBy('id', 'asc')->first();
        $params_tahap["updated_at"]   = date('Y-m-d H:i:s');
        $params_tahap["updated_by"]   = authUser()->id;
        $params_tahap["status"]       = 17;

        $this->db->update('po_pendaftaran_tahap', $params_tahap, array('id' => $data_tahap->id));

        

        $params = array(
            "id_personal"       => $data['id_personal'],
            "id_tahap"          => $data_tahap['id'],
            "nama_platform"     => $data['nama_platform'],
            "nama_perusahaan"   => $data['nama_perusahaan'],
            "tahun_berdiri"     => $data['tahun_berdiri'],
            "tahun_beroperasi"  => $data['tahun_beroperasi'],
            "paid_up_capital"   => $data['paid_up_capital'],
            "alamat_perusahaan" => $data['alamat_perusahaan'],
            "id_negara"         => $data['id_negara'],
            "id_provinsi"       => $data['id_provinsi'],
            "id_kota"           => $data['id_kota'],
            "kode_pos"          => $data['kode_pos'],
            "nomor_telepon_perusahaan"=> $data['nomor_telepon_perusahaan'],
            "email_perusahaan"  => $data['email_perusahaan'],
            "website"           => $data['website'],
            "nomor_kbli"        => $data['nomor_kbli'],
            "jenis_perusahaan"  => $data['jenis_perusahaan'],
            "instagram"         => $data['instagram'],
            "status"            => 17, //Menunggu Verifikasi Tahap 1
        );

        if (empty($id)) { //insert

            $params["created_at"]   = date('Y-m-d H:i:s');
            $params["created_by"]   = authUser()->id;
            $this->db->insert('po_pendaftaran_tahap_satu', $params);
    
            $id = $this->db->insert_id();
            logs("POST","po_tahap_pendaftaran/saveTahap1/",$id,"po_pendaftaran_tahap_satu","insert"); //activity_logs => "method,route,table_id,table_name,table_aksi"
        }else{ //update
            $params["updated_at"]   = date('Y-m-d H:i:s');
            $params["updated_by"]   = authUser()->id;
    
            $this->db->update('po_pendaftaran_tahap_satu', $params, array('id' => $id));
            logs("POST","po_tahap_pendaftaran/saveTahap1/",$id,"po_pendaftaran_tahap_satu","update"); //activity_logs => "method,route,table_id,table_name,table_aksi"
        }


        $data_tahap = MPoPendaftaranTahapSatu::find($id); //data tahap

        if(!empty($_FILES['files']['name']['akta_pendirian'])) {//bila ada lampiran akta_pendirian
            if (!empty(@$data_tahap->akta_pendirian)) {
                $path = './uploads/akta_pendirian/'.@$data_tahap->akta_pendirian;
                unlink($path);
            }

            $config = array();
            $config['upload_path']          = './uploads/akta_pendirian/';
            $config['allowed_types']        = 'jpg|jpeg|gif|png|doc|docx|pdf|xls|xlsx';
            $config['overwrite']        	= true;
            //$config['encrypt_name']        	= true;
            $config['max_size']             = 10120;

            // $this->load->library('upload', $config);
            $this->load->library('upload', $config, 'akta_pendirian'); // Create custom object for cover upload
            $this->akta_pendirian->initialize($config);

            
            $_FILES['akta_pendirian']['name']       = $_FILES['files']['name']['akta_pendirian'];
            $_FILES['akta_pendirian']['type']       = $_FILES['files']['type']['akta_pendirian'];
            $_FILES['akta_pendirian']['tmp_name']   = $_FILES['files']['tmp_name']['akta_pendirian'];
            $_FILES['akta_pendirian']['error']      = $_FILES['files']['error']['akta_pendirian'];
            $_FILES['akta_pendirian']['size']       = $_FILES['files']['size']['akta_pendirian'];

            
            if (!$this->akta_pendirian->do_upload('akta_pendirian')) {
                set_flashdata('success', 'Data Tidak Berhasil Disimpan, Pastikan File jpg|jpeg|gif|png|doc|docx|pdf|xls|xlsx Dan ukuran Maximal 10mb');
                redirect('/po_tahap_pendaftaran/pembayaran');
            }

            $upload_data        = $this->akta_pendirian->data();
            $params['akta_pendirian']  = $upload_data['file_name'];
    
            $this->db->update('po_pendaftaran_tahap_satu', $params, array('id' => $id));
        }
        
        if(!empty($_FILES['files']['name']['akta_perubahan'])) {//bila ada lampiran akta_perubahan
            if (!empty(@$data_tahap->akta_perubahan)) {
                $path = './uploads/akta_pendirian/'.@$data_tahap->akta_perubahan;
                unlink($path);
            }

            $config = array();
            $config['upload_path']          = './uploads/akta_perubahan/';
            $config['allowed_types']        = 'jpg|jpeg|gif|png|doc|docx|pdf|xls|xlsx';
            $config['overwrite']        	= true;
            //$config['encrypt_name']        	= true;
            $config['max_size']             = 10120;

            // $this->load->library('upload', $config);
            $this->load->library('upload', $config, 'akta_perubahan'); // Create custom object for cover upload
            $this->akta_perubahan->initialize($config);

            
            $_FILES['akta_perubahan']['name']       = $_FILES['files']['name']['akta_perubahan'];
            $_FILES['akta_perubahan']['type']       = $_FILES['files']['type']['akta_perubahan'];
            $_FILES['akta_perubahan']['tmp_name']   = $_FILES['files']['tmp_name']['akta_perubahan'];
            $_FILES['akta_perubahan']['error']      = $_FILES['files']['error']['akta_perubahan'];
            $_FILES['akta_perubahan']['size']       = $_FILES['files']['size']['akta_perubahan'];

            
            if (!$this->akta_perubahan->do_upload('akta_perubahan')) {
                set_flashdata('success', 'Data Tidak Berhasil Disimpan, Pastikan File jpg|jpeg|gif|png|doc|docx|pdf|xls|xlsx Dan ukuran Maximal 10mb');
                redirect('/po_tahap_pendaftaran/pembayaran');
            }

            $upload_data        = $this->akta_perubahan->data();
            $params['akta_perubahan']  = $upload_data['file_name'];
    
            $this->db->update('po_pendaftaran_tahap_satu', $params, array('id' => $id));
        }
        
        if(!empty($_FILES['files']['name']['sk_kemenkumham_akta_pendirian'])) {//bila ada lampiran sk_kemenkumham_akta_pendirian
            if (!empty(@$data_tahap->sk_kemenkumham_akta_pendirian)) {
                $path = './uploads/sk_kemenkumham_akta_pendirian/'.@$data_tahap->sk_kemenkumham_akta_pendirian;
                unlink($path);
            }

            $config = array();
            $config['upload_path']          = './uploads/sk_kemenkumham_akta_pendirian/';
            $config['allowed_types']        = 'jpg|jpeg|gif|png|doc|docx|pdf|xls|xlsx';
            $config['overwrite']        	= true;
            //$config['encrypt_name']        	= true;
            $config['max_size']             = 10120;

            // $this->load->library('upload', $config);
            $this->load->library('upload', $config, 'sk_kemenkumham_akta_pendirian'); // Create custom object for cover upload
            $this->sk_kemenkumham_akta_pendirian->initialize($config);

            
            $_FILES['sk_kemenkumham_akta_pendirian']['name']       = $_FILES['files']['name']['sk_kemenkumham_akta_pendirian'];
            $_FILES['sk_kemenkumham_akta_pendirian']['type']       = $_FILES['files']['type']['sk_kemenkumham_akta_pendirian'];
            $_FILES['sk_kemenkumham_akta_pendirian']['tmp_name']   = $_FILES['files']['tmp_name']['sk_kemenkumham_akta_pendirian'];
            $_FILES['sk_kemenkumham_akta_pendirian']['error']      = $_FILES['files']['error']['sk_kemenkumham_akta_pendirian'];
            $_FILES['sk_kemenkumham_akta_pendirian']['size']       = $_FILES['files']['size']['sk_kemenkumham_akta_pendirian'];

            
            if (!$this->sk_kemenkumham_akta_pendirian->do_upload('sk_kemenkumham_akta_pendirian')) {
                set_flashdata('success', 'Data Tidak Berhasil Disimpan, Pastikan File jpg|jpeg|gif|png|doc|docx|pdf|xls|xlsx Dan ukuran Maximal 10mb');
                redirect('/po_tahap_pendaftaran/pembayaran');
            }

            $upload_data        = $this->sk_kemenkumham_akta_pendirian->data();
            $params['sk_kemenkumham_akta_pendirian']  = $upload_data['file_name'];
    
            $this->db->update('po_pendaftaran_tahap_satu', $params, array('id' => $id));
        }
        
        if(!empty($_FILES['files']['name']['salinan_dokumen_npwp_perusahaan'])) {//bila ada lampiran salinan_dokumen_npwp_perusahaan
            if (!empty(@$data_tahap->salinan_dokumen_npwp_perusahaan)) {
                $path = './uploads/salinan_dokumen_npwp_perusahaan/'.@$data_tahap->salinan_dokumen_npwp_perusahaan;
                unlink($path);
            }

            $config = array();
            $config['upload_path']          = './uploads/salinan_dokumen_npwp_perusahaan/';
            $config['allowed_types']        = 'jpg|jpeg|gif|png|doc|docx|pdf|xls|xlsx';
            $config['overwrite']        	= true;
            //$config['encrypt_name']        	= true;
            $config['max_size']             = 10120;

            // $this->load->library('upload', $config);
            $this->load->library('upload', $config, 'salinan_dokumen_npwp_perusahaan'); // Create custom object for cover upload
            $this->salinan_dokumen_npwp_perusahaan->initialize($config);

            
            $_FILES['salinan_dokumen_npwp_perusahaan']['name']       = $_FILES['files']['name']['salinan_dokumen_npwp_perusahaan'];
            $_FILES['salinan_dokumen_npwp_perusahaan']['type']       = $_FILES['files']['type']['salinan_dokumen_npwp_perusahaan'];
            $_FILES['salinan_dokumen_npwp_perusahaan']['tmp_name']   = $_FILES['files']['tmp_name']['salinan_dokumen_npwp_perusahaan'];
            $_FILES['salinan_dokumen_npwp_perusahaan']['error']      = $_FILES['files']['error']['salinan_dokumen_npwp_perusahaan'];
            $_FILES['salinan_dokumen_npwp_perusahaan']['size']       = $_FILES['files']['size']['salinan_dokumen_npwp_perusahaan'];

            
            if (!$this->salinan_dokumen_npwp_perusahaan->do_upload('salinan_dokumen_npwp_perusahaan')) {
                set_flashdata('success', 'Data Tidak Berhasil Disimpan, Pastikan File jpg|jpeg|gif|png|doc|docx|pdf|xls|xlsx Dan ukuran Maximal 10mb');
                redirect('/po_tahap_pendaftaran/pembayaran');
            }

            $upload_data        = $this->salinan_dokumen_npwp_perusahaan->data();
            $params['salinan_dokumen_npwp_perusahaan']  = $upload_data['file_name'];
    
            $this->db->update('po_pendaftaran_tahap_satu', $params, array('id' => $id));
        }

        if(!empty($_FILES['files']['name']['struktur_organisasi_perusahaan'])) {//bila ada lampiran struktur_organisasi_perusahaan
            if (!empty(@$data_tahap->struktur_organisasi_perusahaan)) {
                $path = './uploads/struktur_organisasi_perusahaan/'.@$data_tahap->struktur_organisasi_perusahaan;
                unlink($path);
            }

            $config = array();
            $config['upload_path']          = './uploads/struktur_organisasi_perusahaan/';
            $config['allowed_types']        = 'jpg|jpeg|gif|png|doc|docx|pdf|xls|xlsx';
            $config['overwrite']        	= true;
            //$config['encrypt_name']        	= true;
            $config['max_size']             = 10120;

            // $this->load->library('upload', $config);
            $this->load->library('upload', $config, 'struktur_organisasi_perusahaan'); // Create custom object for cover upload
            $this->struktur_organisasi_perusahaan->initialize($config);

            
            $_FILES['struktur_organisasi_perusahaan']['name']       = $_FILES['files']['name']['struktur_organisasi_perusahaan'];
            $_FILES['struktur_organisasi_perusahaan']['type']       = $_FILES['files']['type']['struktur_organisasi_perusahaan'];
            $_FILES['struktur_organisasi_perusahaan']['tmp_name']   = $_FILES['files']['tmp_name']['struktur_organisasi_perusahaan'];
            $_FILES['struktur_organisasi_perusahaan']['error']      = $_FILES['files']['error']['struktur_organisasi_perusahaan'];
            $_FILES['struktur_organisasi_perusahaan']['size']       = $_FILES['files']['size']['struktur_organisasi_perusahaan'];

            
            if (!$this->struktur_organisasi_perusahaan->do_upload('struktur_organisasi_perusahaan')) {
                set_flashdata('success', 'Data Tidak Berhasil Disimpan, Pastikan File jpg|jpeg|gif|png|doc|docx|pdf|xls|xlsx Dan ukuran Maximal 10mb');
                redirect('/po_tahap_pendaftaran/pembayaran');
            }

            $upload_data        = $this->struktur_organisasi_perusahaan->data();
            $params['struktur_organisasi_perusahaan']  = $upload_data['file_name'];
    
            $this->db->update('po_pendaftaran_tahap_satu', $params, array('id' => $id));
        }
        
        if(!empty($_FILES['files']['name']['surat_pernyataan_kepatuhan'])) {//bila ada lampiran surat_pernyataan_kepatuhan
            if (!empty(@$data_tahap->surat_pernyataan_kepatuhan)) {
                $path = './uploads/surat_pernyataan_kepatuhan/'.@$data_tahap->surat_pernyataan_kepatuhan;
                unlink($path);
            }

            $config = array();
            $config['upload_path']          = './uploads/surat_pernyataan_kepatuhan/';
            $config['allowed_types']        = 'jpg|jpeg|gif|png|doc|docx|pdf|xls|xlsx';
            $config['overwrite']        	= true;
            //$config['encrypt_name']        	= true;
            $config['max_size']             = 10120;

            // $this->load->library('upload', $config);
            $this->load->library('upload', $config, 'surat_pernyataan_kepatuhan'); // Create custom object for cover upload
            $this->surat_pernyataan_kepatuhan->initialize($config);

            
            $_FILES['surat_pernyataan_kepatuhan']['name']       = $_FILES['files']['name']['surat_pernyataan_kepatuhan'];
            $_FILES['surat_pernyataan_kepatuhan']['type']       = $_FILES['files']['type']['surat_pernyataan_kepatuhan'];
            $_FILES['surat_pernyataan_kepatuhan']['tmp_name']   = $_FILES['files']['tmp_name']['surat_pernyataan_kepatuhan'];
            $_FILES['surat_pernyataan_kepatuhan']['error']      = $_FILES['files']['error']['surat_pernyataan_kepatuhan'];
            $_FILES['surat_pernyataan_kepatuhan']['size']       = $_FILES['files']['size']['surat_pernyataan_kepatuhan'];

            
            if (!$this->surat_pernyataan_kepatuhan->do_upload('surat_pernyataan_kepatuhan')) {
                set_flashdata('success', 'Data Tidak Berhasil Disimpan, Pastikan File jpg|jpeg|gif|png|doc|docx|pdf|xls|xlsx Dan ukuran Maximal 10mb');
                redirect('/po_tahap_pendaftaran/pembayaran');
            }

            $upload_data        = $this->surat_pernyataan_kepatuhan->data();
            $params['surat_pernyataan_kepatuhan']  = $upload_data['file_name'];
    
            $this->db->update('po_pendaftaran_tahap_satu', $params, array('id' => $id));
        }

    
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }
        else{
            $this->db->trans_commit();

            
            if(MasKaryawan()->user->role_id==2){ //jika peserta
                //send email
                $tahap = MPoPendaftaranTahap::where('id_personal', MasKaryawan()->id)->orderBy('id', 'asc')->first();
                $dataEmail = array(
                    'email'         => $tahap->personals->personal_email,
                    'nama'          => $tahap->tahap_satu->nama_perusahaan,
                    'url_detail'    => site_url('po_pendaftaran/verifikasi/'.$id),
                );

                $this->load->library('mailgun');
                $dataSend = array(
                    'to'        => $tahap->personals->personal_email,
                    'subject'   => "Pendaftaran Tahap 1- Platform ALUDI",
                    'html'      => $this->load->view('email/pendaftaran_tahap1_pengisian_formulir',$dataEmail,true),  
                );
                // $this->mailgun::send($dataSend);
                //end send email
            }
        }
		
        set_flashdata('success', 'Data berhasil disimpan');
        if(@$_SESSION['user']['role_id'] == 1 || @$_SESSION['user']['role_id'] == 4){
			redirect('/po_peserta/detail/'.$id);
		}else{
			redirect('/po_tahap_pendaftaran/tahap1/'.$id.'/detail');
		}
    }

    public function tahap1_logo_delete($id){
        ini_set('date.timezone', 'Asia/Jakarta');

        $this->db->trans_begin();


        
        $data = MPoPendaftaranTahapSatu::find($id);
        $params["updated_at"]   = date('Y-m-d H:i:s');
        $params["updated_by"]   = authUser()->id;

        $params['logo_perusahaan']  = '';
        $this->db->update('po_pendaftaran_tahap_satu', $params, array('id' => $id));
        logs("POST","po_tahap_pendaftaran/saveTahap1/",$id,"po_pendaftaran_tahap_satu","update"); //activity_logs => "method,route,table_id,table_name,table_aksi"
        

        @unlink("./uploads/anggota/".$data->logo_perusahaan);
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }
        else{
            $this->db->trans_commit();
        }

        set_flashdata('success', 'Data berhasil disimpan');
        redirect('/po_tahap_pendaftaran/tahap1/'.$id.'/detail');
    }

    public function saveTahap1_logo(){
        ini_set('date.timezone', 'Asia/Jakarta');
        $data       = $this->input->post();
        if (empty($data)) { redirect('/po_tahap_pendaftaran/tahap1/'); }//bila gak ada inputan

        $this->db->trans_begin();

        $id = @$data['id'];
        

        if (empty($id)) { //insert

        }else{ //update
            $params["updated_at"]   = date('Y-m-d H:i:s');
            $params["updated_by"]   = authUser()->id;
    
            $this->db->update('po_pendaftaran_tahap_satu', $params, array('id' => $id));
            logs("POST","po_tahap_pendaftaran/saveTahap1/",$id,"po_pendaftaran_tahap_satu","update"); //activity_logs => "method,route,table_id,table_name,table_aksi"
        }

        if(!empty($_FILES['files']['name']['anggota'])) {//bila ada lampiran anggota
            $config['upload_path']          = './uploads/anggota/';
            $config['allowed_types']        = 'jpg|jpeg|gif|png';
            $config['overwrite']        	= true;
            $config['encrypt_name']        	= true;
            $config['max_size']             = 10120;

            $this->load->library('upload', $config);

            
            $_FILES['anggota']['name']       = $_FILES['files']['name']['anggota'];
            $_FILES['anggota']['type']       = $_FILES['files']['type']['anggota'];
            $_FILES['anggota']['tmp_name']   = $_FILES['files']['tmp_name']['anggota'];
            $_FILES['anggota']['error']      = $_FILES['files']['error']['anggota'];
            $_FILES['anggota']['size']       = $_FILES['files']['size']['anggota'];

            
            if (!$this->upload->do_upload('anggota')) {
                set_flashdata('success', 'Data Tidak Berhasil Disimpan, Pastikan File jpg|jpeg|gif|png|doc|docx|pdf|xls|xlsx Dan ukuran Maximal 10mb');
                redirect('/po_tahap_pendaftaran/tahap1_logo');
            }

            $upload_data        = $this->upload->data();
            $params['logo_perusahaan']  = $upload_data['file_name'];
    
            $this->db->update('po_pendaftaran_tahap_satu', $params, array('id' => $id));
        }
        
    
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }
        else{
            $this->db->trans_commit();
        }

        set_flashdata('success', 'Data berhasil disimpan');
        redirect('/po_tahap_pendaftaran/tahap1/'.$id.'/detail');
    
    }
    
    public function pembayaran($id='',$mode='') {
        $data = new \stdClass; 

        if(MasKaryawan()->user->role_id==2){ //jika peserta
            $data = MPoPendaftaranTahapPembayaran::where('id_personal', MasKaryawan()->id)->orderBy('id', 'asc')->first();
        }

        if (!empty($id) AND empty($data->id)) {
            $data = MPoPendaftaranTahapPembayaran::find($id);
        }

        @$data->step = $this->uri->segment(2);

        if(empty(@$data->status)){
            @$data->status   = 20;
            @$data->statuse  = MCoreRefStatus::find(20); //pembayaran
        }

        $this->setParam('data', $data);
        $this->setParam('mode', $mode);
        $this->setContentView('web/po_tahap_pendaftaran/index_pembayaran');
        $this->render(' ');
    }

    public function pembayaran_bukti_invoice() {
        $data = MPoPendaftaranTahap::where('id_personal', MasKaryawan()->id)->orderBy('id', 'asc')->first();
        
        // MasDebugPree($data,1);
        if (empty($data->id)) {
            return redirect('/pembayaran/');
        }
        

        $result['tahap_pembayaran'] = $data->tahap_pembayaran;
        $result['tahap_satu']       = $data->tahap_satu;

        return $this->load->view('laporan/pembayaran_bukti_invoice',$result);  
    }

    public function pembayaran_bukti_pelunasan() {
        $data = MPoPendaftaranTahap::where('id_personal', MasKaryawan()->id)->orderBy('id', 'asc')->first();
        
        // MasDebugPree($data,1);
        if (empty($data->id)) {
            return redirect('/pembayaran/');
        }
        

        $result['tahap_pembayaran'] = $data->tahap_pembayaran;
        $result['tahap_satu']       = $data->tahap_satu;

        return $this->load->view('laporan/pembayaran_bukti_pelunasan',$result);  
    }

    public function pembayaran_bukti_sertifikat() {
        $data = MPoPendaftaranTahapPembayaran::where('id_personal', MasKaryawan()->id)->orderBy('id', 'asc')->first();
        
        if (empty($data->id)) {
            return redirect('/pembayaran/');
        }
        
        
        if($data->sertifikat == "" OR empty($data->sertifikat)){
            $dataTahap1 = MPoPendaftaranTahapSatu::where('id_personal', MasKaryawan()->id)->orderBy('id', 'asc')->first();


            $bulan_depan = date("F", strtotime("+1 month", strtotime($data->created_at)));
            $tahun_depan = date("Y", strtotime("+1 year", strtotime($data->created_at)));
            
            $bulan_saat_ini = date("F", strtotime($data->created_at));
            $tahun_saat_ini = date("Y", strtotime($data->created_at));

            $saat_ini = date("d F Y");

            $title          = $dataTahap1->nama_perusahaan;
            $sut_title      = "As a valued member for period ".$bulan_saat_ini." ".$tahun_saat_ini." - ".$bulan_depan." ".$tahun_depan;
            $tanggal        = "JAKARTA, ".$saat_ini;
            $no_registrasi  = $data->id."/REG/ALUDI/SER";
    
            $nama_file = MasRandom(50);
            $this->save_images_sertifikat($nama_file,$title,$no_registrasi,$sut_title,$tanggal);

            $params["updated_at"]   = date('Y-m-d H:i:s');
            $params["updated_by"]   = authUser()->id;
            $params["sertifikat"]   = $nama_file.".jpeg";
            $params["sertifikat_no"]= $no_registrasi;
            $params["periode"]      = $bulan_saat_ini." ".$tahun_saat_ini." - ".$bulan_depan." ".$tahun_depan;
    
            $this->db->update('po_pendaftaran_tahap_pembayaran', $params, array('id' => $data->id));
            logs("POST","po_tahap_pendaftaran/pembayaran_bukti_sertifikat/",$data->id,"po_pendaftaran_tahap_pembayaran","update"); //activity_logs => "method,route,table_id,table_name,table_aksi"

            return redirect('/po_tahap_pendaftaran/pembayaran_bukti_sertifikat');
        }else {
            $result['sertifikat'] = $data->sertifikat;
            return $this->load->view('laporan/pembayaran_bukti_sertifikat',$result);  
        }



    }

    public function sertifikat() {
        
        $data = MPoPendaftaranTahapPembayaran::get();

        foreach ($data as $key => $value) {
            $nama_file      = $value->sertifikat;
            $title          = $value->nama;
            $no_registrasi  = $value->sertifikat_no;
            $sut_title      = "As a valued member for period ".$value->periode;
            $tanggal        ="JAKARTA, ".$value->tanggal;
            
            $this->save_images_sertifikat($nama_file,$title,$no_registrasi,$sut_title,$tanggal);

            // MasDebugPree($nama_file);
            // MasDebugPree($title);
            // MasDebugPree($no_registrasi);
            // MasDebugPree($sut_title);
            // MasDebugPree($tanggal,1);
            // MasDebug();
        }



    
    }

    public function save_images_sertifikat($nama_file,$title,$no_registrasi,$sut_title,$tanggal) {
        // ========================================================== tanggal ================
        // Memuat dan menciptakan gambar
		$gambar = imagecreatefrompng('./uploads/pembayaran_sertifikat/template.png');

		// Mengalokasikan warna untuk teks, masukkan nilai RGB
		$warna_putih = imagecolorallocate($gambar, 255, 255, 255);

		// Menetapkan file path font
		$font_path = realpath('./app-assets/fonts/Montserrat-Light.otf');

		// Mendapatkan isi teks dari input form untuk dicetak ke gambar
		$isiteks = $tanggal;

		$ukuran	= 12;
		$angle	= 0;
		$kiri	= 150;
		$atas	= 370;

		// Cetak teks ke gambar
		imagettftext($gambar, $ukuran,$angle,$kiri,$atas, $warna_putih, $font_path, $isiteks);
        imagejpeg($gambar, './uploads/pembayaran_sertifikat/'.$nama_file.'.jpeg');
        // ========================================================== end tanggal ================


        // ========================================================== untuk sub title ================
        // Memuat dan menciptakan gambar
        $gambar = imagecreatefromjpeg('./uploads/pembayaran_sertifikat/'.$nama_file.'.jpeg');

		// // Mengalokasikan warna untuk teks, masukkan nilai RGB
		$warna_putih = imagecolorallocate($gambar, 255, 255, 255);

		// Menetapkan file path font
		$font_path = realpath('./app-assets/fonts/Montserrat-Light.otf');

		// Mendapatkan isi teks dari input form untuk dicetak ke gambar
		$isiteks = $sut_title;

		$ukuran	= 12;
		$angle	= 0;
		$kiri	= 40;
		$atas	= 310;

		// Cetak teks ke gambar
		imagettftext($gambar, $ukuran,$angle,$kiri,$atas, $warna_putih, $font_path, $isiteks);
        imagejpeg($gambar, './uploads/pembayaran_sertifikat/'.$nama_file.'.jpeg');
        // ========================================================== end untuk sub title ================

        // ========================================================== untuk title ================
        $gambar = imagecreatefromjpeg('./uploads/pembayaran_sertifikat/'.$nama_file.'.jpeg');

		// Mengalokasikan warna untuk teks, masukkan nilai RGB
		$warna_putih = imagecolorallocate($gambar, 255, 255, 255);

		// Menetapkan file path font
		$font_path = realpath('./app-assets/fonts/Montserrat-Bold.otf');

		// Mendapatkan isi teks dari input form untuk dicetak ke gambar
		$isiteks = $title;

		$ukuran	= 25;
		$angle	= 0;
		$kiri	= 40;
		$atas	= 250;

		// Cetak teks ke gambar
		imagettftext($gambar, $ukuran,$angle,$kiri,$atas, $warna_putih, $font_path, $isiteks);
        imagejpeg($gambar, './uploads/pembayaran_sertifikat/'.$nama_file.'.jpeg');
        // ========================================================== end untuk title ================
        
        // ========================================================== untuk no registrasi ================
        $gambar = imagecreatefromjpeg('./uploads/pembayaran_sertifikat/'.$nama_file.'.jpeg');

		// Mengalokasikan warna untuk teks, masukkan nilai RGB
		$warna_putih = imagecolorallocate($gambar, 255, 255, 255);

		// Menetapkan file path font
		$font_path = realpath('./app-assets/fonts/Montserrat-Bold.otf');

		// Mendapatkan isi teks dari input form untuk dicetak ke gambar
		$isiteks = $no_registrasi;

		$ukuran	= 18;
		$angle	= 0;
		$kiri	= 40;
		$atas	= 280;

		// Cetak teks ke gambar
		imagettftext($gambar, $ukuran,$angle,$kiri,$atas, $warna_putih, $font_path, $isiteks);
        imagejpeg($gambar, './uploads/pembayaran_sertifikat/'.$nama_file.'.jpeg');
        // ========================================================== end untuk title ================

    }

    public function savePembayaran(){
        ini_set('date.timezone', 'Asia/Jakarta');
        $data       = $this->input->post();
        if (empty($data)) { redirect('/po_tahap_pendaftaran/pembayaran/'); }//bila gak ada inputan

        $this->db->trans_begin();

        $id = @$data['id'];
        
        $data_tahap = MPoPendaftaranTahap::where('id_personal', $data['id_personal'])->orderBy('id', 'asc')->first();
        $params_tahap["updated_at"]   = date('Y-m-d H:i:s');
        $params_tahap["updated_by"]   = authUser()->id;
        $params_tahap["status"]       = 21;

        $this->db->update('po_pendaftaran_tahap', $params_tahap, array('id' => $data_tahap->id));
        

        $params = array(    
            "id_personal"       => $data['id_personal'],
            "id_tahap"          => $data_tahap['id'],
            "nama"              => $data['nama'],
            "status"            => 21, //Menunggu Verifikasi Tahap 1
        );

        if (empty($id)) { //insert
            $params["created_at"]   = date('Y-m-d H:i:s');
            $params["created_by"]   = authUser()->id;
            $this->db->insert('po_pendaftaran_tahap_pembayaran', $params);
    
            $id = $this->db->insert_id();
            
            logs("POST","po_tahap_pendaftaran/savePembayaran/",$id,"po_pendaftaran_tahap_pembayaran","insert"); //activity_logs => "method,route,table_id,table_name,table_aksi"
        }else{ //update
            $params["updated_at"]   = date('Y-m-d H:i:s');
            $params["updated_by"]   = authUser()->id;
    
            $this->db->update('po_pendaftaran_tahap_pembayaran', $params, array('id' => $id));
            logs("POST","po_tahap_pendaftaran/savePembayaran/",$id,"po_pendaftaran_tahap_pembayaran","update"); //activity_logs => "method,route,table_id,table_name,table_aksi"
        }
        

        if(!empty($_FILES['files']['name']['bukti'])) {//bila ada lampiran bukti
            $config['upload_path']          = './uploads/bukti/';
            $config['allowed_types']        = 'jpg|jpeg|png';
            $config['overwrite']        	= true;
            $config['encrypt_name']        	= true;
            $config['max_size']             = 10120;

            $this->load->library('upload', $config);

            
            $_FILES['bukti']['name']       = $_FILES['files']['name']['bukti'];
            $_FILES['bukti']['type']       = $_FILES['files']['type']['bukti'];
            $_FILES['bukti']['tmp_name']   = $_FILES['files']['tmp_name']['bukti'];
            $_FILES['bukti']['error']      = $_FILES['files']['error']['bukti'];
            $_FILES['bukti']['size']       = $_FILES['files']['size']['bukti'];

            
            if (!$this->upload->do_upload('bukti')) {
                // var_dump($this->upload->display_errors());
                // exit();
                set_flashdata('error', 'Data Tidak Berhasil Disimpan, Pastikan File jpg|jpeg|png Dan ukuran Maximal 10mb');
                redirect('/po_tahap_pendaftaran/pembayaran');
            }

            $upload_data        = $this->upload->data();
            $params_bukti['bukti']    = $upload_data['file_name'];
    
            $this->db->update('po_pendaftaran_tahap_pembayaran', $params_bukti, array('id' => $id));
        }
        
    
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }
        else{
            $this->db->trans_commit();
        }

        set_flashdata('success', 'Data berhasil disimpan');
        redirect('/po_tahap_pendaftaran/pembayaran/'.$id.'/detail');
    
    }

    public function tahap2($id='',$mode='') {
        $data = new \stdClass; 

        
        if(MasKaryawan()->user->role_id==2){ //jika peserta
            $data = MPoPendaftaranTahapDua::where('id_personal', MasKaryawan()->id)->orderBy('id', 'asc')->first();
        }

        if (!empty($id) AND empty($data->id)) {
            $data = MPoPendaftaranTahapDua::find($id);
        }

        if(empty(@$data->status)){
            @$data->status   = 20;
            @$data->statuse  = MCoreRefStatus::find(20); //pembayaran
        }

        @$data->step = $this->uri->segment(2);
        $this->setParam('mode', $mode);
        $this->setParam('data', $data);
        $this->setContentView('web/po_tahap_pendaftaran/index_tahap2');
        $this->render(' ');
    }

    public function saveTahap2(){
        ini_set('date.timezone', 'Asia/Jakarta');
        $data       = $this->input->post();
        if (empty($data)) { redirect('/po_tahap_pendaftaran/tahap2/'); }//bila gak ada inputan

        $this->db->trans_begin();

        $id = @$data['id'];
        
        $data_tahap = MPoPendaftaranTahap::where('id_personal', $data['id_personal'])->orderBy('id', 'asc')->first();
        $params_tahap["updated_at"]   = date('Y-m-d H:i:s');
        $params_tahap["updated_by"]   = authUser()->id;
        $params_tahap["status"]       = 25;

        $this->db->update('po_pendaftaran_tahap', $params_tahap, array('id' => $data_tahap->id));

        $params = array(
            "id_personal"       => $data['id_personal'],
            "id_tahap"          => $data_tahap['id'], 
            "nama_platform"     => $data['nama_platform'], 
            "tanggal"           => $data['tanggal'], 
            "domain_nama"       => $data['domain_nama'], 
            "logo_ojk_nama"     => $data['logo_ojk_nama'], 
            "domain_usia_nama"  => $data['domain_usia_nama'], 
            "server_lokasi_nama"=> $data['server_lokasi_nama'], 
            "platform_ssl_nama" => $data['platform_ssl_nama'], 
            "ssl_detail_nama"   => $data['ssl_detail_nama'], 
            "akses_camera_nama" => $data['akses_camera_nama'],
            "technical_apps_ssl_nama"   => $data['technical_apps_ssl_nama'],
            "customer_service_nama"     => $data['customer_service_nama'],
            "sop_keamanan_nama" => $data['sop_keamanan_nama'],
            "validasi_sms_nama" => $data['validasi_sms_nama'],
            "validasi_pin_nama" => $data['validasi_pin_nama'],
            "kyc_nama"          => $data['kyc_nama'], 
            "sla_nama"          => $data['sla_nama'], 
            "sertifikat_drc_nama"  => $data['sertifikat_drc_nama'],
            "iso_nama"          => $data['iso_nama'], 
            "validasi_kominfo_nama"     => $data['validasi_kominfo_nama'],
            "test_penetrasi_nama"       => $data['test_penetrasi_nama'], 
            "nama_saya"         => $data['nama_saya'],
            "status"            => 25, //Menunggu Verifikasi Tahap 2
        );


        if (empty($id)) { //insert
            $params["created_at"]   = date('Y-m-d H:i:s');
            $params["created_by"]   = authUser()->id;
            $this->db->insert('po_pendaftaran_tahap_dua', $params);

            $id = $this->db->insert_id();
            logs("POST","po_tahap_pendaftaran/saveTahap2/",$id,"po_pendaftaran_tahap_dua","insert"); //activity_logs => "method,route,table_id,table_name,table_aksi"
        }else{ //update
            $params["updated_at"]   = date('Y-m-d H:i:s');
            $params["updated_by"]   = authUser()->id;
    
            $this->db->update('po_pendaftaran_tahap_dua', $params, array('id' => $id));
            logs("POST","po_tahap_pendaftaran/saveTahap2/",$id,"po_pendaftaran_tahap_dua","update"); //activity_logs => "method,route,table_id,table_name,table_aksi"
        }


        
        $data_tahap = MPoPendaftaranTahapDua::find($id); //data tahap

        if(!empty($_FILES['files']['name']['domain_bukti'])) {//bila ada lampiran domain_bukti
            if (!empty(@$data_tahap->domain_bukti)) {
                $path = './uploads/domain_bukti/'.@$data_tahap->domain_bukti;
                unlink($path);
            }

            $config = array();
            $config['upload_path']          = './uploads/domain_bukti/';
            $config['allowed_types']        = 'jpg|jpeg|png|pdf|xls|xlsx|doc|docx';
            $config['overwrite']        	= true;
            //$config['encrypt_name']        	= true;
            $config['max_size']             = 10120;

            // $this->load->library('upload', $config);
            $this->load->library('upload', $config, 'domain_bukti'); // Create custom object for cover upload
            $this->domain_bukti->initialize($config);

            
            $_FILES['domain_bukti']['name']       = $_FILES['files']['name']['domain_bukti'];
            $_FILES['domain_bukti']['type']       = $_FILES['files']['type']['domain_bukti'];
            $_FILES['domain_bukti']['tmp_name']   = $_FILES['files']['tmp_name']['domain_bukti'];
            $_FILES['domain_bukti']['error']      = $_FILES['files']['error']['domain_bukti'];
            $_FILES['domain_bukti']['size']       = $_FILES['files']['size']['domain_bukti'];

            
            if (!$this->domain_bukti->do_upload('domain_bukti')) {
                // MasDebugPree("domain_bukti".$this->domain_bukti->display_errors(),1);
                set_flashdata('error', 'Data Tidak Berhasil Disimpan, Pastikan File jpg|jpeg|png|pdf|xls|xlsx|doc|docx Dan ukuran Maximal 10mb');
                redirect('/po_tahap_pendaftaran/tahap2');
            }

            $upload_data        = $this->domain_bukti->data();
            $params_domain_bukti['domain_bukti']    = $upload_data['file_name'];
    
            $this->db->update('po_pendaftaran_tahap_dua', $params_domain_bukti, array('id' => $id));
        }

        
        if(!empty($_FILES['files']['name']['logo_ojk_bukti'])) {//bila ada lampiran logo_ojk_bukti
            if (!empty(@$data_tahap->logo_ojk_bukti)) {
                $path = './uploads/logo_ojk_bukti/'.@$data_tahap->logo_ojk_bukti;
                unlink($path);
            }

            $config = array();
            $config['upload_path']          = './uploads/logo_ojk_bukti/';
            $config['allowed_types']        = 'jpg|jpeg|png|pdf|xls|xlsx|doc|docx';
            $config['overwrite']        	= true;
            //$config['encrypt_name']        	= true;
            $config['max_size']             = 10120;

            // $this->load->library('upload', $config);
            $this->load->library('upload', $config, 'logo_ojk_bukti'); // Create custom object for cover upload
            $this->logo_ojk_bukti->initialize($config);

            
            $_FILES['logo_ojk_bukti']['name']       = $_FILES['files']['name']['logo_ojk_bukti'];
            $_FILES['logo_ojk_bukti']['type']       = $_FILES['files']['type']['logo_ojk_bukti'];
            $_FILES['logo_ojk_bukti']['tmp_name']   = $_FILES['files']['tmp_name']['logo_ojk_bukti'];
            $_FILES['logo_ojk_bukti']['error']      = $_FILES['files']['error']['logo_ojk_bukti'];
            $_FILES['logo_ojk_bukti']['size']       = $_FILES['files']['size']['logo_ojk_bukti'];

            
            if (!$this->logo_ojk_bukti->do_upload('logo_ojk_bukti')) {
                // MasDebugPree("logo_ojk_bukti".$this->logo_ojk_bukti->display_errors(),1);
                set_flashdata('error', 'Data Tidak Berhasil Disimpan, Pastikan File jpg|jpeg|png|pdf|xls|xlsx|doc|docx Dan ukuran Maximal 10mb');
                redirect('/po_tahap_pendaftaran/tahap2');
            }

            $upload_data        = $this->logo_ojk_bukti->data();
            $params_logo_ojk_bukti['logo_ojk_bukti']    = $upload_data['file_name'];
    
            $this->db->update('po_pendaftaran_tahap_dua', $params_logo_ojk_bukti, array('id' => $id));
        }
        
        if(!empty($_FILES['files']['name']['domain_usia_bukti'])) {//bila ada lampiran domain_usia_bukti
            if (!empty(@$data_tahap->domain_usia_bukti)) {
                $path = './uploads/domain_usia_bukti/'.@$data_tahap->domain_usia_bukti;
                unlink($path);
            }

            $config = array();
            $config['upload_path']          = './uploads/domain_usia_bukti/';
            $config['allowed_types']        = 'jpg|jpeg|png|pdf|xls|xlsx|doc|docx';
            $config['overwrite']        	= true;
            //$config['encrypt_name']        	= true;
            $config['max_size']             = 10120;

            // $this->load->library('upload', $config);
            $this->load->library('upload', $config, 'domain_usia_bukti'); // Create custom object for cover upload
            $this->domain_usia_bukti->initialize($config);

            
            $_FILES['domain_usia_bukti']['name']       = $_FILES['files']['name']['domain_usia_bukti'];
            $_FILES['domain_usia_bukti']['type']       = $_FILES['files']['type']['domain_usia_bukti'];
            $_FILES['domain_usia_bukti']['tmp_name']   = $_FILES['files']['tmp_name']['domain_usia_bukti'];
            $_FILES['domain_usia_bukti']['error']      = $_FILES['files']['error']['domain_usia_bukti'];
            $_FILES['domain_usia_bukti']['size']       = $_FILES['files']['size']['domain_usia_bukti'];

            
            if (!$this->domain_usia_bukti->do_upload('domain_usia_bukti')) {
                // MasDebugPree("domain_usia_bukti".$this->domain_usia_bukti->display_errors(),1);
                set_flashdata('error', 'Data Tidak Berhasil Disimpan, Pastikan File jpg|jpeg|png|pdf|xls|xlsx|doc|docx Dan ukuran Maximal 10mb');
                redirect('/po_tahap_pendaftaran/tahap2');
            }

            $upload_data        = $this->domain_usia_bukti->data();
            $params_domain_usia_bukti['domain_usia_bukti']    = $upload_data['file_name'];
    
            $this->db->update('po_pendaftaran_tahap_dua', $params_domain_usia_bukti, array('id' => $id));
        }
        
        
        if(!empty($_FILES['files']['name']['ketentuan_pojk_bukti'])) {//bila ada lampiran ketentuan_pojk_bukti
            if (!empty(@$data_tahap->ketentuan_pojk_bukti)) {
                $path = './uploads/ketentuan_pojk_bukti/'.@$data_tahap->ketentuan_pojk_bukti;
                unlink($path);
            }

            $config = array();
            $config['upload_path']          = './uploads/ketentuan_pojk_bukti/';
            $config['allowed_types']        = 'jpg|jpeg|png|pdf|xls|xlsx|doc|docx';
            $config['overwrite']        	= true;
            //$config['encrypt_name']        	= true;
            $config['max_size']             = 10120;

            // $this->load->library('upload', $config);
            $this->load->library('upload', $config, 'ketentuan_pojk_bukti'); // Create custom object for cover upload
            $this->ketentuan_pojk_bukti->initialize($config);

            
            $_FILES['ketentuan_pojk_bukti']['name']       = $_FILES['files']['name']['ketentuan_pojk_bukti'];
            $_FILES['ketentuan_pojk_bukti']['type']       = $_FILES['files']['type']['ketentuan_pojk_bukti'];
            $_FILES['ketentuan_pojk_bukti']['tmp_name']   = $_FILES['files']['tmp_name']['ketentuan_pojk_bukti'];
            $_FILES['ketentuan_pojk_bukti']['error']      = $_FILES['files']['error']['ketentuan_pojk_bukti'];
            $_FILES['ketentuan_pojk_bukti']['size']       = $_FILES['files']['size']['ketentuan_pojk_bukti'];

            
            if (!$this->ketentuan_pojk_bukti->do_upload('ketentuan_pojk_bukti')) {
                // MasDebugPree("ketentuan_pojk_bukti".$this->ketentuan_pojk_bukti->display_errors(),1);
                set_flashdata('error', 'Data Tidak Berhasil Disimpan, Pastikan File jpg|jpeg|png|pdf|xls|xlsx|doc|docx Dan ukuran Maximal 10mb');
                redirect('/po_tahap_pendaftaran/tahap2');
            }

            $upload_data        = $this->ketentuan_pojk_bukti->data();
            $params_ketentuan_pojk_bukti['ketentuan_pojk_bukti']    = $upload_data['file_name'];
    
            $this->db->update('po_pendaftaran_tahap_dua', $params_ketentuan_pojk_bukti, array('id' => $id));
        }
        
        if(!empty($_FILES['files']['name']['server_lokasi_bukti'])) {//bila ada lampiran server_lokasi_bukti
            if (!empty(@$data_tahap->server_lokasi_bukti)) {
                $path = './uploads/server_lokasi_bukti/'.@$data_tahap->server_lokasi_bukti;
                unlink($path);
            }

            $config = array();
            $config['upload_path']          = './uploads/server_lokasi_bukti/';
            $config['allowed_types']        = 'jpg|jpeg|png|pdf|xls|xlsx|doc|docx';
            $config['overwrite']        	= true;
            //$config['encrypt_name']        	= true;
            $config['max_size']             = 10120;

            // $this->load->library('upload', $config);
            $this->load->library('upload', $config, 'server_lokasi_bukti'); // Create custom object for cover upload
            $this->server_lokasi_bukti->initialize($config);

            
            $_FILES['server_lokasi_bukti']['name']       = $_FILES['files']['name']['server_lokasi_bukti'];
            $_FILES['server_lokasi_bukti']['type']       = $_FILES['files']['type']['server_lokasi_bukti'];
            $_FILES['server_lokasi_bukti']['tmp_name']   = $_FILES['files']['tmp_name']['server_lokasi_bukti'];
            $_FILES['server_lokasi_bukti']['error']      = $_FILES['files']['error']['server_lokasi_bukti'];
            $_FILES['server_lokasi_bukti']['size']       = $_FILES['files']['size']['server_lokasi_bukti'];

            
            if (!$this->server_lokasi_bukti->do_upload('server_lokasi_bukti')) {
                // MasDebugPree("server_lokasi_bukti".$this->server_lokasi_bukti->display_errors(),1);
                set_flashdata('error', 'Data Tidak Berhasil Disimpan, Pastikan File jpg|jpeg|png|pdf|xls|xlsx|doc|docx Dan ukuran Maximal 10mb');
                redirect('/po_tahap_pendaftaran/tahap2');
            }

            $upload_data        = $this->server_lokasi_bukti->data();
            $params_server_lokasi_bukti['server_lokasi_bukti']    = $upload_data['file_name'];
    
            $this->db->update('po_pendaftaran_tahap_dua', $params_server_lokasi_bukti, array('id' => $id));
        }
        
        if(!empty($_FILES['files']['name']['platform_ssl_bukti'])) {//bila ada lampiran platform_ssl_bukti
            if (!empty(@$data_tahap->platform_ssl_bukti)) {
                $path = './uploads/platform_ssl_bukti/'.@$data_tahap->platform_ssl_bukti;
                unlink($path);
            }

            $config = array();
            $config['upload_path']          = './uploads/platform_ssl_bukti/';
            $config['allowed_types']        = 'jpg|jpeg|png|pdf|xls|xlsx|doc|docx';
            $config['overwrite']        	= true;
            //$config['encrypt_name']        	= true;
            $config['max_size']             = 10120;

            // $this->load->library('upload', $config);
            $this->load->library('upload', $config, 'platform_ssl_bukti'); // Create custom object for cover upload
            $this->platform_ssl_bukti->initialize($config);

            
            $_FILES['platform_ssl_bukti']['name']       = $_FILES['files']['name']['platform_ssl_bukti'];
            $_FILES['platform_ssl_bukti']['type']       = $_FILES['files']['type']['platform_ssl_bukti'];
            $_FILES['platform_ssl_bukti']['tmp_name']   = $_FILES['files']['tmp_name']['platform_ssl_bukti'];
            $_FILES['platform_ssl_bukti']['error']      = $_FILES['files']['error']['platform_ssl_bukti'];
            $_FILES['platform_ssl_bukti']['size']       = $_FILES['files']['size']['platform_ssl_bukti'];

            
            if (!$this->platform_ssl_bukti->do_upload('platform_ssl_bukti')) {
                // MasDebugPree("platform_ssl_bukti".$this->platform_ssl_bukti->display_errors(),1);
                set_flashdata('error', 'Data Tidak Berhasil Disimpan, Pastikan File jpg|jpeg|png|pdf|xls|xlsx|doc|docx Dan ukuran Maximal 10mb');
                redirect('/po_tahap_pendaftaran/tahap2');
            }

            $upload_data        = $this->platform_ssl_bukti->data();
            $params_platform_ssl_bukti['platform_ssl_bukti']    = $upload_data['file_name'];
    
            $this->db->update('po_pendaftaran_tahap_dua', $params_platform_ssl_bukti, array('id' => $id));
        }
        
        if(!empty($_FILES['files']['name']['ssl_detail_bukti'])) {//bila ada lampiran ssl_detail_bukti
            if (!empty(@$data_tahap->ssl_detail_bukti)) {
                $path = './uploads/ssl_detail_bukti/'.@$data_tahap->ssl_detail_bukti;
                unlink($path);
            }

            $config = array();
            $config['upload_path']          = './uploads/ssl_detail_bukti/';
            $config['allowed_types']        = 'jpg|jpeg|png|pdf|xls|xlsx|doc|docx';
            $config['overwrite']        	= true;
            //$config['encrypt_name']        	= true;
            $config['max_size']             = 10120;

            // $this->load->library('upload', $config);
            $this->load->library('upload', $config, 'ssl_detail_bukti'); // Create custom object for cover upload
            $this->ssl_detail_bukti->initialize($config);

            
            $_FILES['ssl_detail_bukti']['name']       = $_FILES['files']['name']['ssl_detail_bukti'];
            $_FILES['ssl_detail_bukti']['type']       = $_FILES['files']['type']['ssl_detail_bukti'];
            $_FILES['ssl_detail_bukti']['tmp_name']   = $_FILES['files']['tmp_name']['ssl_detail_bukti'];
            $_FILES['ssl_detail_bukti']['error']      = $_FILES['files']['error']['ssl_detail_bukti'];
            $_FILES['ssl_detail_bukti']['size']       = $_FILES['files']['size']['ssl_detail_bukti'];

            
            if (!$this->ssl_detail_bukti->do_upload('ssl_detail_bukti')) {
                // MasDebugPree("ssl_detail_bukti".$this->ssl_detail_bukti->display_errors(),1);
                set_flashdata('success', 'Data Tidak Berhasil Disimpan, Pastikan File jpg|jpeg|png|pdf|xls|xlsx|doc|docx Dan ukuran Maximal 10mb');
                redirect('/po_tahap_pendaftaran/tahap2');
            }

            $upload_data        = $this->ssl_detail_bukti->data();
            $params_ssl_detail_bukti['ssl_detail_bukti']    = $upload_data['file_name'];
    
            $this->db->update('po_pendaftaran_tahap_dua', $params_ssl_detail_bukti, array('id' => $id));
        }
        
        if(!empty($_FILES['files']['name']['akses_camera_bukti'])) {//bila ada lampiran akses_camera_bukti
            if (!empty(@$data_tahap->akses_camera_bukti)) {
                $path = './uploads/akses_camera_bukti/'.@$data_tahap->akses_camera_bukti;
                unlink($path);
            }

            $config = array();
            $config['upload_path']          = './uploads/akses_camera_bukti/';
            $config['allowed_types']        = 'jpg|jpeg|png|pdf|xls|xlsx|doc|docx';
            $config['overwrite']        	= true;
            //$config['encrypt_name']        	= true;
            $config['max_size']             = 10120;

            // $this->load->library('upload', $config);
            $this->load->library('upload', $config, 'akses_camera_bukti'); // Create custom object for cover upload
            $this->akses_camera_bukti->initialize($config);

            
            $_FILES['akses_camera_bukti']['name']       = $_FILES['files']['name']['akses_camera_bukti'];
            $_FILES['akses_camera_bukti']['type']       = $_FILES['files']['type']['akses_camera_bukti'];
            $_FILES['akses_camera_bukti']['tmp_name']   = $_FILES['files']['tmp_name']['akses_camera_bukti'];
            $_FILES['akses_camera_bukti']['error']      = $_FILES['files']['error']['akses_camera_bukti'];
            $_FILES['akses_camera_bukti']['size']       = $_FILES['files']['size']['akses_camera_bukti'];

            
            if (!$this->akses_camera_bukti->do_upload('akses_camera_bukti')) {
                // MasDebugPree("akses_camera_bukti".$this->akses_camera_bukti->display_errors(),1);
                set_flashdata('error', 'Data Tidak Berhasil Disimpan, Pastikan File jpg|jpeg|png|pdf|xls|xlsx|doc|docx Dan ukuran Maximal 10mb');
                redirect('/po_tahap_pendaftaran/tahap2');
            }

            $upload_data        = $this->akses_camera_bukti->data();
            $params_akses_camera_bukti['akses_camera_bukti']    = $upload_data['file_name'];
    
            $this->db->update('po_pendaftaran_tahap_dua', $params_akses_camera_bukti, array('id' => $id));
        }
        
        if(!empty($_FILES['files']['name']['technical_apps_ssl_bukti'])) {//bila ada lampiran technical_apps_ssl_bukti
            if (!empty(@$data_tahap->technical_apps_ssl_bukti)) {
                $path = './uploads/technical_apps_ssl_bukti/'.@$data_tahap->technical_apps_ssl_bukti;
                unlink($path);
            }

            $config = array();
            $config['upload_path']          = './uploads/technical_apps_ssl_bukti/';
            $config['allowed_types']        = 'jpg|jpeg|png|pdf|xls|xlsx|doc|docx';
            $config['overwrite']        	= true;
            //$config['encrypt_name']        	= true;
            $config['max_size']             = 10120;

            // $this->load->library('upload', $config);
            $this->load->library('upload', $config, 'technical_apps_ssl_bukti'); // Create custom object for cover upload
            $this->technical_apps_ssl_bukti->initialize($config);

            
            $_FILES['technical_apps_ssl_bukti']['name']       = $_FILES['files']['name']['technical_apps_ssl_bukti'];
            $_FILES['technical_apps_ssl_bukti']['type']       = $_FILES['files']['type']['technical_apps_ssl_bukti'];
            $_FILES['technical_apps_ssl_bukti']['tmp_name']   = $_FILES['files']['tmp_name']['technical_apps_ssl_bukti'];
            $_FILES['technical_apps_ssl_bukti']['error']      = $_FILES['files']['error']['technical_apps_ssl_bukti'];
            $_FILES['technical_apps_ssl_bukti']['size']       = $_FILES['files']['size']['technical_apps_ssl_bukti'];

            
            if (!$this->technical_apps_ssl_bukti->do_upload('technical_apps_ssl_bukti')) {
                // MasDebugPree("technical_apps_ssl_bukti".$this->technical_apps_ssl_bukti->display_errors(),1);
                set_flashdata('error', 'Data Tidak Berhasil Disimpan, Pastikan File jpg|jpeg|png|pdf|xls|xlsx|doc|docx Dan ukuran Maximal 10mb');
                redirect('/po_tahap_pendaftaran/tahap2');
            }

            $upload_data        = $this->technical_apps_ssl_bukti->data();
            $params_technical_apps_ssl_bukti['technical_apps_ssl_bukti']    = $upload_data['file_name'];
    
            $this->db->update('po_pendaftaran_tahap_dua', $params_technical_apps_ssl_bukti, array('id' => $id));
        }

        if(!empty($_FILES['files']['name']['sop_keamanan_bukti'])) {//bila ada lampiran sop_keamanan_bukti
            if (!empty(@$data_tahap->sop_keamanan_bukti)) {
                $path = './uploads/sop_keamanan_bukti/'.@$data_tahap->sop_keamanan_bukti;
                unlink($path);
            }

            $config = array();
            $config['upload_path']          = './uploads/sop_keamanan_bukti/';
            $config['allowed_types']        = 'jpg|jpeg|png|pdf|xls|xlsx|doc|docx';
            $config['overwrite']        	= true;
            //$config['encrypt_name']        	= true;
            $config['max_size']             = 10120;

            // $this->load->library('upload', $config);
            $this->load->library('upload', $config, 'sop_keamanan_bukti'); // Create custom object for cover upload
            $this->sop_keamanan_bukti->initialize($config);

            
            $_FILES['sop_keamanan_bukti']['name']       = $_FILES['files']['name']['sop_keamanan_bukti'];
            $_FILES['sop_keamanan_bukti']['type']       = $_FILES['files']['type']['sop_keamanan_bukti'];
            $_FILES['sop_keamanan_bukti']['tmp_name']   = $_FILES['files']['tmp_name']['sop_keamanan_bukti'];
            $_FILES['sop_keamanan_bukti']['error']      = $_FILES['files']['error']['sop_keamanan_bukti'];
            $_FILES['sop_keamanan_bukti']['size']       = $_FILES['files']['size']['sop_keamanan_bukti'];

            
            if (!$this->sop_keamanan_bukti->do_upload('sop_keamanan_bukti')) {
                MasDebugPree("sop_keamanan_bukti".$this->sop_keamanan_bukti->display_errors(),1);
                set_flashdata('error', 'Data Tidak Berhasil Disimpan, Pastikan File jpg|jpeg|png|pdf|xls|xlsx|doc|docx Dan ukuran Maximal 10mb');
                redirect('/po_tahap_pendaftaran/tahap2');
            }

            $upload_data        = $this->sop_keamanan_bukti->data();
            $params_sop_keamanan_bukti['sop_keamanan_bukti']    = $upload_data['file_name'];
    
            $this->db->update('po_pendaftaran_tahap_dua', $params_sop_keamanan_bukti, array('id' => $id));
        }

        if(!empty($_FILES['files']['name']['validasi_sms_bukti'])) {//bila ada lampiran validasi_sms_bukti
            if (!empty(@$data_tahap->validasi_sms_bukti)) {
                $path = './uploads/validasi_sms_bukti/'.@$data_tahap->validasi_sms_bukti;
                unlink($path);
            }

            $config = array();
            $config['upload_path']          = './uploads/validasi_sms_bukti/';
            $config['allowed_types']        = 'jpg|jpeg|png|pdf|xls|xlsx|doc|docx';
            $config['overwrite']        	= true;
            //$config['encrypt_name']        	= true;
            $config['max_size']             = 10120;

            // $this->load->library('upload', $config);
            $this->load->library('upload', $config, 'validasi_sms_bukti'); // Create custom object for cover upload
            $this->validasi_sms_bukti->initialize($config);

            
            $_FILES['validasi_sms_bukti']['name']       = $_FILES['files']['name']['validasi_sms_bukti'];
            $_FILES['validasi_sms_bukti']['type']       = $_FILES['files']['type']['validasi_sms_bukti'];
            $_FILES['validasi_sms_bukti']['tmp_name']   = $_FILES['files']['tmp_name']['validasi_sms_bukti'];
            $_FILES['validasi_sms_bukti']['error']      = $_FILES['files']['error']['validasi_sms_bukti'];
            $_FILES['validasi_sms_bukti']['size']       = $_FILES['files']['size']['validasi_sms_bukti'];

            
            if (!$this->validasi_sms_bukti->do_upload('validasi_sms_bukti')) {
                // MasDebugPree("validasi_sms_bukti".$this->validasi_sms_bukti->display_errors(),1);
                set_flashdata('error', 'Data Tidak Berhasil Disimpan, Pastikan File jpg|jpeg|png|pdf|xls|xlsx|doc|docx Dan ukuran Maximal 10mb');
                redirect('/po_tahap_pendaftaran/tahap2');
            }

            $upload_data        = $this->validasi_sms_bukti->data();
            $params_validasi_sms_bukti['validasi_sms_bukti']    = $upload_data['file_name'];
    
            $this->db->update('po_pendaftaran_tahap_dua', $params_validasi_sms_bukti, array('id' => $id));
        }
        
        if(!empty($_FILES['files']['name']['validasi_pin_bukti'])) {//bila ada lampiran validasi_pin_bukti
            if (!empty(@$data_tahap->validasi_pin_bukti)) {
                $path = './uploads/validasi_pin_bukti/'.@$data_tahap->validasi_pin_bukti;
                unlink($path);
            }

            $config = array();
            $config['upload_path']          = './uploads/validasi_pin_bukti/';
            $config['allowed_types']        = 'jpg|jpeg|png|pdf|xls|xlsx|doc|docx';
            $config['overwrite']        	= true;
            //$config['encrypt_name']        	= true;
            $config['max_size']             = 10120;

            // $this->load->library('upload', $config);
            $this->load->library('upload', $config, 'validasi_pin_bukti'); // Create custom object for cover upload
            $this->validasi_pin_bukti->initialize($config);

            
            $_FILES['validasi_pin_bukti']['name']       = $_FILES['files']['name']['validasi_pin_bukti'];
            $_FILES['validasi_pin_bukti']['type']       = $_FILES['files']['type']['validasi_pin_bukti'];
            $_FILES['validasi_pin_bukti']['tmp_name']   = $_FILES['files']['tmp_name']['validasi_pin_bukti'];
            $_FILES['validasi_pin_bukti']['error']      = $_FILES['files']['error']['validasi_pin_bukti'];
            $_FILES['validasi_pin_bukti']['size']       = $_FILES['files']['size']['validasi_pin_bukti'];

            
            if (!$this->validasi_pin_bukti->do_upload('validasi_pin_bukti')) {
                // MasDebugPree("validasi_pin_bukti".$this->validasi_pin_bukti->display_errors(),1);
                set_flashdata('error', 'Data Tidak Berhasil Disimpan, Pastikan File jpg|jpeg|png|pdf|xls|xlsx|doc|docx Dan ukuran Maximal 10mb');
                redirect('/po_tahap_pendaftaran/tahap2');
            }

            $upload_data        = $this->validasi_pin_bukti->data();
            $params_validasi_pin_bukti['validasi_pin_bukti']    = $upload_data['file_name'];
    
            $this->db->update('po_pendaftaran_tahap_dua', $params_validasi_pin_bukti, array('id' => $id));
        }
        
        if(!empty($_FILES['files']['name']['validasi_email_bukti'])) {//bila ada lampiran validasi_email_bukti
            if (!empty(@$data_tahap->validasi_email_bukti)) {
                $path = './uploads/validasi_email_bukti/'.@$data_tahap->validasi_email_bukti;
                unlink($path);
            }

            $config = array();
            $config['upload_path']          = './uploads/validasi_email_bukti/';
            $config['allowed_types']        = 'jpg|jpeg|png|pdf|xls|xlsx|doc|docx';
            $config['overwrite']        	= true;
            //$config['encrypt_name']        	= true;
            $config['max_size']             = 10120;

            // $this->load->library('upload', $config);
            $this->load->library('upload', $config, 'validasi_email_bukti'); // Create custom object for cover upload
            $this->validasi_email_bukti->initialize($config);

            
            $_FILES['validasi_email_bukti']['name']       = $_FILES['files']['name']['validasi_email_bukti'];
            $_FILES['validasi_email_bukti']['type']       = $_FILES['files']['type']['validasi_email_bukti'];
            $_FILES['validasi_email_bukti']['tmp_name']   = $_FILES['files']['tmp_name']['validasi_email_bukti'];
            $_FILES['validasi_email_bukti']['error']      = $_FILES['files']['error']['validasi_email_bukti'];
            $_FILES['validasi_email_bukti']['size']       = $_FILES['files']['size']['validasi_email_bukti'];

            
            if (!$this->validasi_email_bukti->do_upload('validasi_email_bukti')) {
                // MasDebugPree("validasi_email_bukti".$this->validasi_email_bukti->display_errors(),1);
                set_flashdata('error', 'Data Tidak Berhasil Disimpan, Pastikan File jpg|jpeg|png|pdf|xls|xlsx|doc|docx Dan ukuran Maximal 10mb');
                redirect('/po_tahap_pendaftaran/tahap2');
            }

            $upload_data        = $this->validasi_email_bukti->data();
            $params_validasi_email_bukti['validasi_email_bukti']    = $upload_data['file_name'];
    
            $this->db->update('po_pendaftaran_tahap_dua', $params_validasi_email_bukti, array('id' => $id));
        }
        
        if(!empty($_FILES['files']['name']['kyc_bukti'])) {//bila ada lampiran kyc_bukti
            if (!empty(@$data_tahap->kyc_bukti)) {
                $path = './uploads/kyc_bukti/'.@$data_tahap->kyc_bukti;
                unlink($path);
            }

            $config = array();
            $config['upload_path']          = './uploads/kyc_bukti/';
            $config['allowed_types']        = 'jpg|jpeg|png|pdf|xls|xlsx|doc|docx';
            $config['overwrite']        	= true;
            //$config['encrypt_name']        	= true;
            $config['max_size']             = 10120;

            // $this->load->library('upload', $config);
            $this->load->library('upload', $config, 'kyc_bukti'); // Create custom object for cover upload
            $this->kyc_bukti->initialize($config);

            
            $_FILES['kyc_bukti']['name']       = $_FILES['files']['name']['kyc_bukti'];
            $_FILES['kyc_bukti']['type']       = $_FILES['files']['type']['kyc_bukti'];
            $_FILES['kyc_bukti']['tmp_name']   = $_FILES['files']['tmp_name']['kyc_bukti'];
            $_FILES['kyc_bukti']['error']      = $_FILES['files']['error']['kyc_bukti'];
            $_FILES['kyc_bukti']['size']       = $_FILES['files']['size']['kyc_bukti'];

            
            if (!$this->kyc_bukti->do_upload('kyc_bukti')) {
                // MasDebugPree("kyc_bukti".$this->kyc_bukti->display_errors(),1);
                set_flashdata('error', 'Data Tidak Berhasil Disimpan, Pastikan File jpg|jpeg|png|pdf|xls|xlsx|doc|docx Dan ukuran Maximal 10mb');
                redirect('/po_tahap_pendaftaran/tahap2');
            }

            $upload_data        = $this->kyc_bukti->data();
            $params_kyc_bukti['kyc_bukti']    = $upload_data['file_name'];
    
            $this->db->update('po_pendaftaran_tahap_dua', $params_kyc_bukti, array('id' => $id));
        }
        
        if(!empty($_FILES['files']['name']['sla_bukti'])) {//bila ada lampiran sla_bukti
            if (!empty(@$data_tahap->sla_bukti)) {
                $path = './uploads/sla_bukti/'.@$data_tahap->sla_bukti;
                unlink($path);
            }

            $config = array();
            $config['upload_path']          = './uploads/sla_bukti/';
            $config['allowed_types']        = 'jpg|jpeg|png|pdf|xls|xlsx|doc|docx';
            $config['overwrite']        	= true;
           //$config['encrypt_name']        	= true;
            $config['max_size']             = 10120;

            // $this->load->library('upload', $config);
            $this->load->library('upload', $config, 'sla_bukti'); // Create custom object for cover upload
            $this->sla_bukti->initialize($config);

            
            $_FILES['sla_bukti']['name']       = $_FILES['files']['name']['sla_bukti'];
            $_FILES['sla_bukti']['type']       = $_FILES['files']['type']['sla_bukti'];
            $_FILES['sla_bukti']['tmp_name']   = $_FILES['files']['tmp_name']['sla_bukti'];
            $_FILES['sla_bukti']['error']      = $_FILES['files']['error']['sla_bukti'];
            $_FILES['sla_bukti']['size']       = $_FILES['files']['size']['sla_bukti'];

            
            if (!$this->sla_bukti->do_upload('sla_bukti')) {
                // MasDebugPree("sla_bukti".$this->sla_bukti->display_errors(),1);
                set_flashdata('error', 'Data Tidak Berhasil Disimpan, Pastikan File jpg|jpeg|png|pdf|xls|xlsx|doc|docx Dan ukuran Maximal 10mb');
                redirect('/po_tahap_pendaftaran/tahap2');
            }

            $upload_data        = $this->sla_bukti->data();
            $params_sla_bukti['sla_bukti']    = $upload_data['file_name'];
    
            $this->db->update('po_pendaftaran_tahap_dua', $params_sla_bukti, array('id' => $id));
        }

        if(!empty($_FILES['files']['name']['sertifikat_drc_bukti'])) {//bila ada lampiran sertifikat_drc_bukti
            if (!empty(@$data_tahap->sertifikat_drc_bukti)) {
                $path = './uploads/sertifikat_drc_bukti/'.@$data_tahap->sertifikat_drc_bukti;
                unlink($path);
            }

            $config = array();
            $config['upload_path']          = './uploads/sertifikat_drc_bukti/';
            $config['allowed_types']        = 'jpg|jpeg|png|pdf|xls|xlsx|doc|docx';
            $config['overwrite']        	= true;
            //$config['encrypt_name']        	= true;
            $config['max_size']             = 10120;

            // $this->load->library('upload', $config);
            $this->load->library('upload', $config, 'sertifikat_drc_bukti'); // Create custom object for cover upload
            $this->sertifikat_drc_bukti->initialize($config);

            
            $_FILES['sertifikat_drc_bukti']['name']       = $_FILES['files']['name']['sertifikat_drc_bukti'];
            $_FILES['sertifikat_drc_bukti']['type']       = $_FILES['files']['type']['sertifikat_drc_bukti'];
            $_FILES['sertifikat_drc_bukti']['tmp_name']   = $_FILES['files']['tmp_name']['sertifikat_drc_bukti'];
            $_FILES['sertifikat_drc_bukti']['error']      = $_FILES['files']['error']['sertifikat_drc_bukti'];
            $_FILES['sertifikat_drc_bukti']['size']       = $_FILES['files']['size']['sertifikat_drc_bukti'];

            
            if (!$this->sertifikat_drc_bukti->do_upload('sertifikat_drc_bukti')) {
                // MasDebugPree("sertifikat_drc_bukti".$this->sertifikat_drc_bukti->display_errors(),1);
                set_flashdata('error', 'Data Tidak Berhasil Disimpan, Pastikan File jpg|jpeg|png|pdf|xls|xlsx|doc|docx Dan ukuran Maximal 10mb');
                redirect('/po_tahap_pendaftaran/tahap2');
            }

            $upload_data        = $this->sertifikat_drc_bukti->data();
            $params_sertifikat_drc_bukti['sertifikat_drc_bukti']    = $upload_data['file_name'];
    
            $this->db->update('po_pendaftaran_tahap_dua', $params_sertifikat_drc_bukti, array('id' => $id));
        }

        if(!empty($_FILES['files']['name']['iso_bukti'])) {//bila ada lampiran iso_bukti
            if (!empty(@$data_tahap->iso_bukti)) {
                $path = './uploads/iso_bukti/'.@$data_tahap->iso_bukti;
                unlink($path);
            }

            $config = array();
            $config['upload_path']          = './uploads/iso_bukti/';
            $config['allowed_types']        = 'jpg|jpeg|png|pdf|xls|xlsx|doc|docx';
            $config['overwrite']        	= true;
            //$config['encrypt_name']        	= true;
            $config['max_size']             = 10120;

            // $this->load->library('upload', $config);
            $this->load->library('upload', $config, 'iso_bukti'); // Create custom object for cover upload
            $this->iso_bukti->initialize($config);

            
            $_FILES['iso_bukti']['name']       = $_FILES['files']['name']['iso_bukti'];
            $_FILES['iso_bukti']['type']       = $_FILES['files']['type']['iso_bukti'];
            $_FILES['iso_bukti']['tmp_name']   = $_FILES['files']['tmp_name']['iso_bukti'];
            $_FILES['iso_bukti']['error']      = $_FILES['files']['error']['iso_bukti'];
            $_FILES['iso_bukti']['size']       = $_FILES['files']['size']['iso_bukti'];

            
            if (!$this->iso_bukti->do_upload('iso_bukti')) {
                // MasDebugPree("iso_bukti".$this->iso_bukti->display_errors(),1);
                set_flashdata('error', 'Data Tidak Berhasil Disimpan, Pastikan File jpg|jpeg|png|pdf|xls|xlsx|doc|docx Dan ukuran Maximal 10mb');
                redirect('/po_tahap_pendaftaran/tahap2');
            }

            $upload_data        = $this->iso_bukti->data();
            $params_iso_bukti['iso_bukti']    = $upload_data['file_name'];
    
            $this->db->update('po_pendaftaran_tahap_dua', $params_iso_bukti, array('id' => $id));
        }

        if(!empty($_FILES['files']['name']['validasi_kominfo_bukti'])) {//bila ada lampiran validasi_kominfo_bukti
            if (!empty(@$data_tahap->validasi_kominfo_bukti)) {
                $path = './uploads/validasi_kominfo_bukti/'.@$data_tahap->validasi_kominfo_bukti;
                unlink($path);
            }

            $config = array();
            $config['upload_path']          = './uploads/validasi_kominfo_bukti/';
            $config['allowed_types']        = 'jpg|jpeg|png|pdf|xls|xlsx|doc|docx';
            $config['overwrite']        	= true;
            //$config['encrypt_name']        	= true;
            $config['max_size']             = 10120;

            // $this->load->library('upload', $config);
            $this->load->library('upload', $config, 'validasi_kominfo_bukti'); // Create custom object for cover upload
            $this->validasi_kominfo_bukti->initialize($config);

            
            $_FILES['validasi_kominfo_bukti']['name']       = $_FILES['files']['name']['validasi_kominfo_bukti'];
            $_FILES['validasi_kominfo_bukti']['type']       = $_FILES['files']['type']['validasi_kominfo_bukti'];
            $_FILES['validasi_kominfo_bukti']['tmp_name']   = $_FILES['files']['tmp_name']['validasi_kominfo_bukti'];
            $_FILES['validasi_kominfo_bukti']['error']      = $_FILES['files']['error']['validasi_kominfo_bukti'];
            $_FILES['validasi_kominfo_bukti']['size']       = $_FILES['files']['size']['validasi_kominfo_bukti'];

            
            if (!$this->validasi_kominfo_bukti->do_upload('validasi_kominfo_bukti')) {
                // MasDebugPree("validasi_kominfo_bukti".$this->validasi_kominfo_bukti->display_errors(),1);
                set_flashdata('error', 'Data Tidak Berhasil Disimpan, Pastikan File jpg|jpeg|png|pdf|xls|xlsx|doc|docx Dan ukuran Maximal 10mb');
                redirect('/po_tahap_pendaftaran/tahap2');
            }

            $upload_data        = $this->validasi_kominfo_bukti->data();
            $params_validasi_kominfo_bukti['validasi_kominfo_bukti']    = $upload_data['file_name'];
    
            $this->db->update('po_pendaftaran_tahap_dua', $params_validasi_kominfo_bukti, array('id' => $id));
        }
        
        if(!empty($_FILES['files']['name']['test_penetrasi_bukti'])) {//bila ada lampiran test_penetrasi_bukti
            if (!empty(@$data_tahap->test_penetrasi_bukti)) {
                $path = './uploads/test_penetrasi_bukti/'.@$data_tahap->test_penetrasi_bukti;
                unlink($path);
            }

            $config = array();
            $config['upload_path']          = './uploads/test_penetrasi_bukti/';
            $config['allowed_types']        = 'jpg|jpeg|png|pdf|xls|xlsx|doc|docx';
            $config['overwrite']        	= true;
            //$config['encrypt_name']        	= true;
            $config['max_size']             = 10120;

            // $this->load->library('upload', $config);
            $this->load->library('upload', $config, 'test_penetrasi_bukti'); // Create custom object for cover upload
            $this->test_penetrasi_bukti->initialize($config);

            
            $_FILES['test_penetrasi_bukti']['name']       = $_FILES['files']['name']['test_penetrasi_bukti'];
            $_FILES['test_penetrasi_bukti']['type']       = $_FILES['files']['type']['test_penetrasi_bukti'];
            $_FILES['test_penetrasi_bukti']['tmp_name']   = $_FILES['files']['tmp_name']['test_penetrasi_bukti'];
            $_FILES['test_penetrasi_bukti']['error']      = $_FILES['files']['error']['test_penetrasi_bukti'];
            $_FILES['test_penetrasi_bukti']['size']       = $_FILES['files']['size']['test_penetrasi_bukti'];

            
            if (!$this->test_penetrasi_bukti->do_upload('test_penetrasi_bukti')) {
                // MasDebugPree("test_penetrasi_bukti".$this->test_penetrasi_bukti->display_errors(),1);
                set_flashdata('error', 'Data Tidak Berhasil Disimpan, Pastikan File jpg|jpeg|png|pdf|xls|xlsx|doc|docx Dan ukuran Maximal 10mb');
                redirect('/po_tahap_pendaftaran/tahap2');
            }

            $upload_data        = $this->test_penetrasi_bukti->data();
            $params_test_penetrasi_bukti['test_penetrasi_bukti']    = $upload_data['file_name'];
    
            $this->db->update('po_pendaftaran_tahap_dua', $params_test_penetrasi_bukti, array('id' => $id));
        }
        
        if(!empty($_FILES['files']['name']['ttd_perusahaan'])) {//bila ada lampiran ttd_perusahaan
            if (!empty(@$data_tahap->ttd_perusahaan)) {
                $path = './uploads/ttd_perusahaan/'.@$data_tahap->ttd_perusahaan;
                unlink($path);
            }

            $config = array();
            $config['upload_path']          = './uploads/ttd_perusahaan/';
            $config['allowed_types']        = 'jpg|jpeg|png';
            $config['overwrite']        	= true;
            //$config['encrypt_name']        	= true;
            $config['max_size']             = 10120;

            // $this->load->library('upload', $config);
            $this->load->library('upload', $config, 'ttd_perusahaan'); // Create custom object for cover upload
            $this->ttd_perusahaan->initialize($config);

            
            $_FILES['ttd_perusahaan']['name']       = $_FILES['files']['name']['ttd_perusahaan'];
            $_FILES['ttd_perusahaan']['type']       = $_FILES['files']['type']['ttd_perusahaan'];
            $_FILES['ttd_perusahaan']['tmp_name']   = $_FILES['files']['tmp_name']['ttd_perusahaan'];
            $_FILES['ttd_perusahaan']['error']      = $_FILES['files']['error']['ttd_perusahaan'];
            $_FILES['ttd_perusahaan']['size']       = $_FILES['files']['size']['ttd_perusahaan'];

            
            if (!$this->ttd_perusahaan->do_upload('ttd_perusahaan')) {
                // MasDebugPree("ttd_perusahaan".$this->ttd_perusahaan->display_errors(),1);
                set_flashdata('error', 'Data Tidak Berhasil Disimpan, Tanda Tangan Pastikan File jpg|jpeg|png Dan ukuran Maximal 10mb');
                redirect('/po_tahap_pendaftaran/tahap2');
            }

            $upload_data        = $this->ttd_perusahaan->data();
            $params_ttd_perusahaan['ttd_perusahaan']    = $upload_data['file_name'];
    
            $this->db->update('po_pendaftaran_tahap_dua', $params_ttd_perusahaan, array('id' => $id));
        }

    
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }
        else{
            $this->db->trans_commit();
        }

        set_flashdata('success', 'Data berhasil disimpan');
        redirect('/po_tahap_pendaftaran/tahap2/'.$id.'/detail');
    
    }

    public function visit($id='',$mode=''){
        $data = new \stdClass; 

        
        if(MasKaryawan()->user->role_id==2){ //jika peserta
            $data           = MPoPendaftaranTahap::where('id_personal', MasKaryawan()->id)->orderBy('id', 'asc')->first();
            $data->visits   = MPoPendaftaranTahapVisit::where('id_personal', MasKaryawan()->id)->orderBy('id', 'asc')->get();
            @$data->visits_hasil   = MPoPendaftaranTahapVisitHasil::where('id_personal', MasKaryawan()->id)->orderBy('id', 'asc')->get();            
        }

        $data->ujian           = MPoUjian::where('status', 2)->orderBy('id', 'asc')->get();

        if(empty(@$data->status)){
            @$data->status   = 20;
            @$data->statuse  = MCoreRefStatus::find(20); //pembayaran
        }
        // MasDebugPree($data,1);
        @$data->step = $this->uri->segment(2);
        $this->setParam('mode', $mode);
        $this->setParam('data', $data);
        $this->setContentView('web/po_tahap_pendaftaran/index_visit');
        $this->render(' ');
    }
    
    public function rekomendasi($id='',$mode='') {
        $data = new \stdClass; 

        
        if(MasKaryawan()->user->role_id==2){ //jika peserta
            $data = MPoPendaftaranTahapVisit::where('id_personal', MasKaryawan()->id)->orderBy('id', 'asc')->first();
        }

        if (!empty($id) AND empty($data->id)) {
            $data = MPoPendaftaranTahapVisit::find($id);
        }

        if(empty(@$data->status)){
            @$data->status   = 20;
            @$data->statuse  = MCoreRefStatus::find(20); //pembayaran
        }
        @$data->step = $this->uri->segment(2);
        $this->setParam('mode', $mode);
        $this->setParam('data', $data);
        $this->setContentView('web/po_tahap_pendaftaran/index_rekomendasi');
        $this->render(' ');
    }

    public function selesai($id='',$mode='') {
        $data = new \stdClass; 

        
        if(MasKaryawan()->user->role_id==2){ //jika peserta
            $data = MPoPendaftaranTahapVisit::where('id_personal', MasKaryawan()->id)->orderBy('id', 'asc')->first();
        }

        if (!empty($id) AND empty($data->id)) {
            $data = MPoPendaftaranTahapVisit::find($id);
        }

        if(empty(@$data->status)){
            @$data->status   = 20;
            @$data->statuse  = MCoreRefStatus::find(20); //pembayaran
        }
        @$data->step = $this->uri->segment(2);
        $this->setParam('mode', $mode);
        $this->setParam('data', $data);
        $this->setContentView('web/po_tahap_pendaftaran/index_rekomendasi');
        $this->render(' ');
    }

    public function form($id='',$mode='') {
        $this->load->model('MPoArtikel');
        $this->load->model('MPoRefKategori');
        $this->load->model('MCoreRefStatus');

        if (!empty($id)) {
            $data = MPoArtikel::where('id', $id)->orderBy('id', 'asc')->first();
        }
        
        $refKategori    = MPoRefKategori::orderBy('name', 'asc')->get();
        $refStatus      = MCoreRefStatus::
                            whereIn('id', explode(',', '1,3,4'))
                            ->orderBy('name', 'asc')->get();

        $this->setParam('refStatus', $refStatus);
        $this->setParam('refKategori', $refKategori);
        $this->setParam('mode', $mode);
        $this->setParam('data', @$data);
        $this->setContentView('web/po_artikel/form');
        $this->render('Form Artikel');
    }

    public function save() {
        $this->load->model('MPoArtikel');
        $this->load->model('MPoRefKategori');
        
        $post   = $this->input->post();
        $id     = !empty($post['id'])?$post['id']:null;

        
        $result = true;
        // validasi
        $msg = '';
        // end validasi

        if($result==true){
            $this->db->trans_begin();

            $params = array(
                "id_kategori"   => $post['id_kategori'],
                "tanggal"       => $post['tanggal'],
                "name"          => $post['name'],
                "deskripsi"     => $post['deskripsi'],
                "is_karyawan"   => !empty($post['is_karyawan'])?$post['is_karyawan']:0,
                "status"        => $post['status'],
            );

            if(isset($_FILES['lampiran']) && !empty($_FILES['lampiran']['name'])) {//bila ada lampiran
                $config['upload_path']          = './uploads/artikel/';
                $config['allowed_types']        = '*';
                $config['overwrite']        	= true;
                $config['max_size']             = 5120;

                $this->load->library('upload', $config);

                $this->upload->do_upload('lampiran');
                
                $upload_data        = $this->upload->data();
                $params['lampiran']  = $upload_data['file_name'];
            }

            if (empty($id)) { //untuk insert
                $params['created_at'] = date('Y-m-d H:i:s');
                $params['created_by'] = authUser()->id;

                $this->db->insert('po_artikel', $params);
                $insert_id = $this->db->insert_id();
                logs("POST","po_artikel/save/",$insert_id,"po_artikel","insert"); //activity_logs => "method,route,table_id,table_name,table_aksi"   
            }else { //untuk update
                $params['updated_at'] = date('Y-m-d H:i:s');
                $params['updated_by'] = authUser()->id;

                $this->db->update('po_artikel', $params, array('id' => $id));
                logs("POST","po_artikel/save/",$id,"po_artikel","update"); //activity_logs => "method,route,table_id,table_name,table_aksi"
            }

            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
            }
            else{
                $this->db->trans_commit();
            }
            set_flashdata('success', 'Data berhasil disimpan');
            redirect('/po_artikel/');
            
        }
        else{
            set_flashdata('error', 'Data Tidak berhasil disimpan. '.$msg);
            redirect('/po_artikel/');
        }
    }

    public function delete($id) {
        $this->load->model('MPoArtikel');
        $data = MPoArtikel::find($id);
        if(!$data) {
            return redirect('po_artikel/');
        }

        $this->db->trans_begin();
        $this->db->delete('po_artikel_file', array('id_artikel' => $id));  
        $this->db->delete('po_artikel', array('id' => $id)); 
        logs("POST","po_artikel/delete/",$id,"po_artikel","delete"); //activity_logs => "method,route,table_id,table_name,table_aksi"
        
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }
        else{
            $this->db->trans_commit();
        }
        set_flashdata('success', 'Data berhasil disimpan');
        redirect('/po_artikel/');
    }
    
    public function detail($id) {
        $this->load->model('MPoArtikel');
        $data = MPoArtikel::find($id);
        if(!$data) {
            return redirect('po_artikel/');
        }
        
        $this->setParam('data', @$data);
        $this->setContentView('web/po_artikel/detail');
        $this->render('Detail Artikel');
    }

    public function getReferensi() {
        $post   = $this->input->post();
        $id     = !empty($post['id'])?$post['id']:null;

        if ($post['data']=='kabupaten') {
            $data    = MCoreRefKabupaten::where("id_provinsi",$id)->orderBy('nama', 'asc')->get()->toArray();
        }elseif ($post['data']=='kecamatan') {
            $data    = MCoreRefKecamatan::where("id_kabupaten",$id)->orderBy('nama', 'asc')->get()->toArray();
        }else{
            $data    = MCoreRefDesa::where("id_kecamatan",$id)->orderBy('nama', 'asc')->get()->toArray();
        }

        foreach ($data as $key => $value) {
            echo "<option value='$value[id]'>$value[nama]</option>";
        }
    }

    public function rekomendasiSuratOjk() {
        $data['tahap'] = MPoPendaftaranTahap::where('id_personal', MasKaryawan()->id)->orderBy('id', 'asc')->first();
        
        if (empty($data['tahap']->id)) {
            return redirect('/po_tahap_pendaftaran/');
        }
        
        // MasDebugPree($data['tahap']->tahap_satu,1);

        // $this->load->view('laporan/rekomendasi_surat_ojk',$data);  
    
        $this->load->library('pdf');
    
        $this->pdf->setPaper('A4', 'potrait');
        $this->pdf->filename = "laporan-rekomendasi.pdf";
        $this->pdf->load_view('laporan/rekomendasi_surat_ojk', $data);

        // $result['tahap_satu'] = $data->tahap_satu;
        // MasDebugPree($data->tahap_satu,1);

        // return $this->load->view('laporan/rekomendasi_surat_ojk',$result);  
    }

    public function tahap1_logo() {
        $tahap = MPoPendaftaranTahap::where('id_personal', MasKaryawan()->id)->orderBy('id', 'asc')->first();
        
        $data = $tahap->tahap_satu;
        $this->setParam('data', @$data);
        $this->setContentView('web/po_tahap_pendaftaran/index_tahap1_logo');
        $this->render(' ');
    }
}