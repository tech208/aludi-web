<?php
class Core_home extends CI_Controller {
    public function __construct() {
        parent::__construct();
        ini_set('date.timezone', 'Asia/Jakarta');
        
        $this->load->model('MPoMedia');
        $this->load->model('MPoAnggota');
        $this->load->model('MPoArtikel');
        $this->load->model('MPoBerita');
        $this->load->model('MPoBanner');
        $this->load->model('MCoreRefStatus');
        $this->load->model('MPoPendaftaranTahapPembayaran');
        $this->load->model('MPoPendaftaranTahapRekomendasi');
        $this->load->model('MPoPendaftaranTahap');
        $this->load->model('MPoPendaftaranTahapDua');
        $this->load->model('MPoPendaftaranTahapSatu');
        $this->load->model('MPoPendaftaranTahapVisit');
        $this->load->model('MPoPendaftaranTahapVisitLangkah');
    }

    public function index($language="") {
		if($language == ""){
			$language = 'ind';
		}
		$this->session->set_userdata(['language' => $language]);
		$arrlanguageear = $this->db->query("SELECT * FROM m_language")->result_array();
        foreach($arrlanguageear as $index => $value){
			$data['arrlanguage'][$value['language_code']] = $value['language_'.$_SESSION['language']];
		}
		$data['banner'] = MPoBanner::
        where('status', 2)
        ->orderBy('id', 'desc')
        ->get()->toArray();

        $data['nama_platform'] = MPoAnggota::
        orderBy('nama', 'asc')
        ->get()->toArray();

        $data['anggota'] = MPoAnggota::
        orderBy('nama', 'asc')
        ->get();
        
        $data['media'] = MPoMedia::
        where('status', 2)
        ->orderBy('name', 'asc')
        ->get();

        $this->load->view('core_home/index',$data);
    }
    
    public function info_terbaru() {
        $data['artikel'] = MPoArtikel::
        where('status', 1)
        ->orderBy('id', 'desc')
        ->get();

		 $data['berita'] = MPoBerita::
        where('status', 1)
        ->orderBy('id', 'desc')
        ->get();
        $this->load->view('core_home/index_info',$data);
    }
    
    public function info_terbaru_detail($id) {
        $data['data'] = MPoArtikel::find($id);
        $this->load->view('core_home/index_info_detail',$data);
    }
	
	public function info_terbaru_detail2($id) {
        $data['data'] = MPoBerita::find($id);
        $this->load->view('core_home/index_info_detail2',$data);
    }

    public function selengkapnya($language = "") {
        if($language == ""){
			$language = 'ind';
		}
		$this->session->set_userdata(['language' => $language]);
		$arrlanguageear = $this->db->query("SELECT * FROM m_language")->result_array();
        foreach($arrlanguageear as $index => $value){
			$data['arrlanguage'][$value['language_code']] = $value['language_'.$_SESSION['language']];
		}
        $this->load->view('core_home/index_selengkapnya',$data);
    }

    public function selengkapnya_anggota() {
        $data['anggota'] = MPoAnggota::
        orderBy('nama', 'asc')
        ->get();

        $this->load->view('core_home/index_selengkapnya_anggota',@$data);
    }

    public function layanan() {
        $data = "";
        $this->load->view('core_home/index_layanan',$data);
    }

    public function masuk() {
        $data = "";
        $this->load->view('core_home/index_masuk',$data);
    }

    
}
