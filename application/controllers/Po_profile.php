<?php

class Po_profile extends sntr_Controller {

    public function __construct() {
        parent::__construct();
        ini_set('date.timezone', 'Asia/Jakarta');
        
        $this->load->model('MPoProfil');
        
    }

    public function index() {
        $data = MPoProfil::
                where('status', 2)
                ->orderBy('id', 'desc')
                ->get();

        $this->setParam('data', $data);
        $this->setContentView('web/po_profile/index');
        $this->render('Profil');
    }



    public function form($id,$detail='') {       

        $data = MPoProfil::find($id);
        
        
        $this->setParam('detail', $detail);
        $this->setParam('data', $data);
        $this->setContentView('web/po_profile/form');
        $this->render('Profil');
    }

    public function save() {
        $data   = $this->input->post();

        $this->db->trans_begin();

        $id = @$data['id'];
    
        $params = array(
            'nama'          => $data['nama'],
            'deskripsi'     => $data['deskripsi'],
            'updated_at'    => date('Y-m-d H:i:s'),
            'updated_by'    => authUser()->id
        );

        $this->db->update('po_profil', $params, array('id' => $id));
        logs("POST","po_profil/save/",$id,"po_profil","update"); //activity_logs => "method,route,table_id,table_name,table_aksi"

        
        if(!empty($_FILES['files']['name']['lampiran'])) {//bila ada lampiran 
            $config['upload_path']          = './uploads/profil_lampiran/';
            $config['allowed_types']        = 'jpg|jpeg|png';
            $config['overwrite']        	= true;
            $config['encrypt_name']        	= true;
            $config['max_size']             = 10120;

            $this->load->library('upload', $config);

            
            $_FILES['lampiran']['name']       = $_FILES['files']['name']['lampiran'];
            $_FILES['lampiran']['type']       = $_FILES['files']['type']['lampiran'];
            $_FILES['lampiran']['tmp_name']   = $_FILES['files']['tmp_name']['lampiran'];
            $_FILES['lampiran']['error']      = $_FILES['files']['error']['lampiran'];
            $_FILES['lampiran']['size']       = $_FILES['files']['size']['lampiran'];

            
            if (!$this->upload->do_upload('lampiran')) {
                // MasDebugPree($this->upload->display_errors(),1);
                set_flashdata('error', 'Data Tidak Berhasil Disimpan, Pastikan File jpg|jpeg|png Dan ukuran Maximal 10mb');
                redirect('/po_tahap_pendaftaran/tahap2');
            }

            $upload_data        = $this->upload->data();
            $params_lampiran['lampiran']    = $upload_data['file_name'];
    
            $this->db->update('po_profil', $params_lampiran, array('id' => $id));
        }

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }
        else{
            $this->db->trans_commit();
        }
        
        
        set_flashdata('success', 'Data berhasil disimpan');
        redirect('/po_profile/form/'.$id);

    }

    public function delete($id) {

    }

}