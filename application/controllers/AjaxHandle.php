<?php

class AjaxHandle extends CI_Controller {

    public function get_job_title($divisi_id = null) {
        $this->load->model('MJobTitle');
        $job_titles = MJobTitle::orderBy('id', 'desc');

        if ($divisi_id) {
            $job_titles->where('division_id', $divisi_id);
        }

        $job_titles = $job_titles->get();

        echo json_encode($job_titles->toArray());
    }

    public function getDivisiByDepartment($department_id) {
        $this->load->model('MDivision');
        $divisions = MDivision::where('departement_id', $department_id)->get();

        echo json_encode($divisions->toArray());
    }
}