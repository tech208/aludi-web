<?php
	use PhpOffice\PhpSpreadsheet\Spreadsheet;
    use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
    use Mailgun\Mailgun;

class Verified extends CI_Controller {

    public function index() {
        if(cekSetting('perawatan')==1){ redirect('/verified/perawatan');} //cek perawatan
        if(cekIp(getIp())>0 AND cekSetting('cek_ip')==1){ redirect('/');} //cek ip
        if(cekSetting('cek_ip')==0){ redirect('/');} //cek ip
        $this->load->view('web/verified/daftar_ip');
    }

    public function cekExport() {

        
        $inputFileType = 'Xls';
        $inputFileName = 'uploads/template/surat_tugas.xls';

        /**  Create a new Reader of the type defined in $inputFileType  **/
        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
        /**  Advise the Reader that we only want to load cell data  **/
        $reader->setReadDataOnly(true);
        /**  Load $inputFileName to a Spreadsheet Object  **/
        $spreadsheet = $reader->load($inputFileName);
        // $value = $spreadsheet->getActiveSheet()->getCell('A')->getCalculatedValue();
        $worksheet = $spreadsheet->getActiveSheet();
        $rows = [];
        foreach ($worksheet->getRowIterator() AS $row) {
            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(FALSE); // This loops through all cells,
            $cells = [];
            foreach ($cellIterator as $cell) {
                $cells[] = $cell->getValue();
            }
            $rows[] = $cells;
        }

        MasDebugPree($rows,1);

        // echo "sasa";
        // // die();
        // $spreadsheet = new Spreadsheet();
        // $sheet = $spreadsheet->getActiveSheet();
        // $sheet->setCellValue('A1', 'No');

        
        // $writer = new Xlsx($spreadsheet);
        // $filename = 'laporan-siswa';
        
        // // header('Content-Type: application/vnd.ms-excel');
        // header('Content-Disposition: attachment;filename="'. $filename .'.xlsx"'); 
        // // header('Cache-Control: max-age=0');

        // $writer->save('php://output');
    }

    public function perawatan() {
        if(cekSetting('perawatan')==0){ redirect('/');}
        $this->load->view('web/verified/perawatan');
    }

    public function saveIp() {
        ini_set('date.timezone', 'Asia/Jakarta');
        $data = $this->input->post();

        $this->load->model('MUser');
        $pengawai = MUser::where('email',$data['email'])->first();

        $token = getToken(25);

        if($pengawai) { //bila pegawai
            try {
                $this->db->trans_begin();
                $params = array(
                    "ip"        => getIp(),
                    "created_at"=> date('Y-m-d H:i:s'),
                    "created_by"=> $data['email'],
                    "token"     => $token,
                    "status"    => 3,
                );
                $this->db->insert('core_ip', $params);
    
                if ($this->db->trans_status() === FALSE){
                    $this->db->trans_rollback();
                }
                else{
                    $this->db->trans_commit();
    
                    //send email
                    $this->load->library('mailgun');
    
                    $dataEmail = array(
                        'name'          => $pengawai->email,
                        'action_url'    => site_url('verified/saveStatus/'.$token),
                    );
                    
                    $dataSend = array(
                        'to'        => $data['email'],
                        'subject'   => "Akses  - PT. Santara Daya Inspiratama",
                        'html'      => $this->load->view('email/verified_pegawai',$dataEmail,true),   
                    );
                    $this->mailgun::send($dataSend);
                    //end send email
                }
                
                $this->load->view('web/verified/notice');
            } catch(\Exception $e) {
                $this->session->set_flashdata('error_verified', 'Terjadi kesalahan.');
            }
            
        }else{//bukan pegawai

            try {
                $this->db->trans_begin();
                $params = array(
                    "ip"        => getIp(),
                    "created_at"=> date('Y-m-d H:i:s'),
                    "created_by"=> $data['email'],
                    "token"     => $token,
                    "status"    => 3,
                );
                $this->db->insert('core_ip', $params);
    
                if ($this->db->trans_status() === FALSE){
                    $this->db->trans_rollback();
                }
                else{
                    $this->db->trans_commit();
    
                    //send email
                    $this->load->library('mailgun');
    
                    $dataEmail = array(
                        'name'          => $data['email'],
                        'ip'            => getIp(),
                        'action_url'    => site_url('verified/saveStatusNon/'.$token),
                    );
                    
                    $dataSend = array(
                        'to'        => "dinnu.shobirin@santara.co.id",
                        // 'cc'        => "human.capital@santara.co.id,yoga.pasramakrisnan@santara.co.id",
                        'cc'        => "dinnu.shobirin@santara.co.id", //data tes
                        'subject'   => "Akses  - PT. Santara Daya Inspiratama",
                        'html'      => $this->load->view('email/verified_pegawai_non',$dataEmail,true),   
                    );
                    $this->mailgun::send($dataSend);
                    //end send email
                }
                
                $this->load->view('web/verified/notice');
            } catch(\Exception $e) {
                $this->session->set_flashdata('error_verified', 'Terjadi kesalahan.');
            }
            
        }
    }

    public function saveStatus($token) {
        ini_set('date.timezone', 'Asia/Jakarta');
        try {
            $this->db->trans_begin();
            $params = array(
                "updated_at"=> date('Y-m-d H:i:s'),
                "status"    => 2,
            );
            
            $this->db->update('core_ip', $params, array('token' => $token));
            logs("POST","verified/saveStatus/".$token,$token,"core_ip","update"); //activity_logs => "method,route,table_id,table_name,table_aksi"

            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
            }
            else{
                $this->db->trans_commit();
            }
            
            redirect('/');
        } catch(\Exception $e) {
            set_flashdata('error', 'Terjadi kesalahan.');
            redirect('/');
        }
    }

    public function saveStatusNon($token) {
        ini_set('date.timezone', 'Asia/Jakarta');
        $this->load->model('MCoreIp');
        $data_raw = MCoreIp::where('token',$token)->first();

        try {
            $this->db->trans_begin();
            $params = array(
                "updated_at"=> date('Y-m-d H:i:s'),
                "status"    => 2,
            );
            
            $this->db->update('core_ip', $params, array('token' => $token));
            logs("POST","verified/saveStatusNon/".$token,$token,"core_device","update"); //activity_logs => "method,route,table_id,table_name,table_aksi"

            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
            }
            else{
                $this->db->trans_commit();

                //send email
                $this->load->library('mailgun');
    
                $dataEmail = array(
                    'name'          => $data_raw->created_by,
                    'ip'            => $data_raw->ip,
                    'action_url'    => site_url('verified/saveStatusNon/'.$token),
                );
                
                $dataSend = array(
                    'to'        => $data_raw->created_by,
                    'subject'   => "Di Beri Akses  - PT. Santara Daya Inspiratama",
                    'html'      => $this->load->view('email/verified_pegawai_non_notice',$dataEmail,true),   
                );
                $this->mailgun::send($dataSend);
                //end send email
            }
            
            redirect('/');
        } catch(\Exception $e) {
            set_flashdata('error', 'Terjadi kesalahan.');
            redirect('/');
        }
    }

    public function saveStatusAuth($token) {
        ini_set('date.timezone', 'Asia/Jakarta');
        $this->load->model('MCoreDevice');
        $this->load->model('MUser');
        
        $data_raw = MCoreDevice::where('token',$token)->first();
        if(!$data_raw){
            set_flashdata('error', 'Terjadi kesalahan.');
            redirect('/');
        }


        try {
            $this->db->trans_begin();
            $params = array(
                "updated_at"=> date('Y-m-d H:i:s'),
                "status"    => 2,
            );
            
            $this->db->update('core_device', $params, array('token' => $token));
            logs("POST","verified/saveStatusAuth/".$token,$token,"core_device","update"); //activity_logs => "method,route,table_id,table_name,table_aksi"

            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
            }
            else{
                $this->db->trans_commit();

                $mac_address = getMac();
                $data_device = MCoreDevice::where([
                    'name'      => $data_raw->name,
                    'device'    => $mac_address->agent,
                    'token'     => $token,
                ])->first();

                if (!empty($data_device->name)) {
                    $user = MUser::where([
                        'email' => $data_raw->name,
                    ])->first();
                    $this->session->set_userdata(['user' => $user->toArray()]);
                    redirect('dashboard');
                    return;
                }else {
                    $this->session->set_flashdata('error_device', 'Hai '.$data_raw->name.'!!. Anda Sudah Bisa Login Dengan Device <b>'.$data_raw->device.'</b>');
                    $this->session->set_flashdata('error', 'Device Berbeda!! silahkan login kembali');
                    redirect('auth/login');
                    return;
                }


            }
        } catch(\Exception $e) {
            set_flashdata('error', 'Terjadi kesalahan.');
            redirect('/');
        }
    }
        
    public function cekIsi() {
        echo cekIp(getIp());
    }

    public function cekCuti($id_personal) {
        ini_set('date.timezone', 'Asia/Jakarta');

        $date = date("Y-m-d");
        $query = $this->db->query("SELECT
                        a.*
                    FROM leave_histories a
                    WHERE '$date' BETWEEN a.start_date AND a.end_date
                        AND personal_id='$id_personal'
                    ");
        $cuti = $query->result_array();

        $result = 1;
        if (!empty($cuti)) {
            foreach ($cuti as $key => $value) {
                if ($value['leave_id'] != 16 ) { //bukan teleworking
                    $result = 0;
                } 
            }
        }
        return $result;
    }

    public function cekIn() {
        ini_set('date.timezone', 'Asia/Jakarta');
        $this->load->model('MPersonal');


        if(date('N')==6 OR date('N')==7){ //bila hari libur sabtu minggu
            return true;
        }

        $data = MPersonal::
        where('status', 1)
        ->orderBy('id', 'asc')
        ->get();

        
        // $rand = rand(1, 4);
        

        foreach ($data as $key => $value) {
            if ($value->id=='34') { //bila mas aves
                unset($data[$key]);
            }
            
            if (!empty($value->checkIn)) { //bila check in
                unset($data[$key]);
            }
            
            $cekCuti = $this->cekCuti($value->id);
            if ($cekCuti==0 AND empty($value->checkIn)) { //bila blm check ing ada cuti selain teleworking
                unset($data[$key]);
            }

            // dummy data
            if ($value->id!='82') { //hanya dinnu
                unset($data[$key]);
            } 
            // end dummy data 
        }


        foreach ($data as $key => $value) {
           $dataEmail = array(
               'url_detail'        => site_url('dashboard'),

               'karyawan_nama'     => $value->name,
           );

           $this->load->library('mailgun');
           $dataSend = array(
               'to'        => $value->user->email,
               'subject'   => "Anda Belum CHECK IN - PT. Santara Daya Inspiratama",
               'html'      => $this->load->view('email/presensi_check_in',$dataEmail,true),   
           );
           $this->mailgun::send($dataSend);
        }

    }

    public function cekOut() {
        ini_set('date.timezone', 'Asia/Jakarta');
        $this->load->model('MPersonal');


        if(date('N')==6 OR date('N')==7){ //bila hari libur sabtu minggu
            return true;
        }

        $data = MPersonal::
        where('status', 1)
        ->orderBy('id', 'asc')
        ->get();

        
        // $rand = rand(1, 4);
        

        foreach ($data as $key => $value) {
            if ($value->id=='34') { //bila mas aves
                unset($data[$key]);
            }
            
            if (!empty($value->checkOut)) { //bila check Out
                unset($data[$key]);
            }
            
            $cekCuti = $this->cekCuti($value->id);
            if ($cekCuti==0 AND empty($value->checkOut)) { //bila blm check ing ada cuti selain teleworking
                unset($data[$key]);
            }

            // dummy data
            if ($value->id!='82') { //hanya dinnu
                unset($data[$key]);
            } 
            // end dummy data 
        }


        foreach ($data as $key => $value) {
           $dataEmail = array(
               'url_detail'        => site_url('dashboard'),

               'karyawan_nama'     => $value->name,
           );

           $this->load->library('mailgun');
           $dataSend = array(
               'to'        => $value->user->email,
               'subject'   => "CHECK OUT - PT. Santara Daya Inspiratama",
               'html'      => $this->load->view('email/presensi_check_out',$dataEmail,true),   
           );
           $this->mailgun::send($dataSend);
        }

    }

    public function mailCek() {
        // var_dump("teas", Mailgun::class);
        // $m = new Mailgun;
        var_dump('aaa');
        $api_key = "0408d83f1d8622adfaebdaf4d05a16ff-e470a504-2bf79c85";
        $api_base_url = "https://api.eu.mailgun.net/v3/pages.santara.co.id";
        // $api_base_url = "https://api.eu.mailgun.net/v3/";
        # Instantiate the client.
        $mgClient = Mailgun::create($api_key, $api_base_url);
        $mgClient = Mailgun::create($api_key);
        $domain = "pages.santara.co.id";
        $params = array(
        'from'    => 'Excited User <dinnushobirin94@gmail.com>',
        'to'      => 'dinnu.shobirin@santara.co.id',
        'subject' => 'Hello',
        'text'    => 'Testing some Mailgun awesomness!'
        );

        // # Make the call to the client.
        $mgClient->messages()->send($domain, $params);





        // ini_set('date.timezone', 'Asia/Jakarta');

        // $token = base64_encode(json_encode([
        //     'e' => date('YmdHis'),
        //     'i' => 1,
        // ]));


        //         $ch =     curl_init();

        //   curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        //   curl_setopt($ch, CURLOPT_USERPWD, 'api:PRIVATE_API_KEY');
        //   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        //   curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        //   curl_setopt($ch, CURLOPT_URL, MESSAGE_STORAGE_URL);
        //   curl_setopt($ch, CURLOPT_POSTFIELDS, array(
        //       'to'=> 'dinnu.shobirin@santara.co.id'
        //       )
        //   );

        //   $result = curl_exec($ch);
        //   curl_close($ch);

        //   return $result;

        //send email
        // $dataEmail = array(
        //     'manager_name'  => "adit",
        //     'cuti_type'     => "main",
        //     'cuti_date'     => hari_tgl_indo("2020-07-06"),
        //     'cuti_date_end' => hari_tgl_indo("2020-07-07"),
        //     'total_hari'    => 1,
        //     'employee_name' => "dinnu",
        //     'cuti_note'     => "alasasn",
        //     'action_url'    => site_url('selfService/detail_cuti_approval/1?t='.$token),
        // );
        
        // $this->load->library('mailgun');

        // $dataSend = array(
        //     'to'        => "dinnu.shobirin@santara.co.id",
        //     'subject'   => "Leave Request - PT. Santara Daya Inspiratama",
        //     'html'      => $this->load->view('email/manager_cuti_notif',$dataEmail,true),   
        // );
        // $this->mailgun::send($dataSend);


        // $this->load->library('mailgun');
        // $this->mailgun::send([
        //     'from'      => "Human Capital team <human.capital@santara.co.id>",
        //     'to'        => "dinnu.shobirin@santara.co.id",
        //     'subject'   => "Welcome to Example.com",
        //     'html'      => $this->load->view('email/reset_password','',true),   
        // ]);

    }

    
}