<?php

class Po_berita extends sntr_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->model('MPoBerita');

        $data = MPoBerita::orderBy('id', 'DESC')->get();

        $this->setParam('data', $data);
        $this->setContentView('web/po_berita/index');
        $this->render('Berita');
    }

    public function form($id='',$mode='') {
        $this->load->model('MPoBerita');
        $this->load->model('MPoRefKategori');
        $this->load->model('MCoreRefStatus');

        if (!empty($id)) {
            $data = MPoBerita::where('id', $id)->orderBy('id', 'asc')->first();
        }
        
        $refKategori    = MPoRefKategori::orderBy('name', 'asc')->get();
        $refStatus      = MCoreRefStatus::
                            whereIn('id', explode(',', '1,3,4'))
                            ->orderBy('name', 'asc')->get();

        $this->setParam('refStatus', $refStatus);
        $this->setParam('refKategori', $refKategori);
        $this->setParam('mode', $mode);
        $this->setParam('data', @$data);
        $this->setContentView('web/po_berita/form');
        $this->render('Form Berita');
    }

    public function save() {
        $this->load->model('MPoBerita');
        $this->load->model('MPoRefKategori');
        
        $post   = $this->input->post();
        $id     = !empty($post['id'])?$post['id']:null;

        
        $result = true;
        // validasi
        $msg = '';
        // end validasi

        if($result==true){
            $this->db->trans_begin();

            $params = array(
                "id_kategori"   => $post['id_kategori'],
                "tanggal"       => $post['tanggal'],
                "name"          => $post['name'],
                "deskripsi"     => $post['deskripsi'],
                "is_karyawan"   => !empty($post['is_karyawan'])?$post['is_karyawan']:0,
                "status"        => $post['status'],
                "grup"          => @$post['grup'],
            );

            if (empty($id)) { //untuk insert
                $params['created_at'] = date('Y-m-d H:i:s');
                $params['created_by'] = authUser()->id;

                $this->db->insert('po_berita', $params);
                $id = $this->db->insert_id();
                logs("POST","po_berita/save/",$id,"po_berita","insert"); //activity_logs => "method,route,table_id,table_name,table_aksi"   
            }else { //untuk update
                $params['updated_at'] = date('Y-m-d H:i:s');
                $params['updated_by'] = authUser()->id;

                $this->db->update('po_berita', $params, array('id' => $id));
                logs("POST","po_berita/save/",$id,"po_berita","update"); //activity_logs => "method,route,table_id,table_name,table_aksi"
            }

            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
            }
            else{
                $this->db->trans_commit();
            }
            set_flashdata('success', 'Data berhasil disimpan');
            redirect('/po_berita/form/'.$id);
            
        }
        else{
            set_flashdata('error', 'Data Tidak berhasil disimpan. '.$msg);
            redirect('/po_berita/');
        }
    }

    public function delete($id) {
        $this->load->model('MPoBerita');
        $data = MPoBerita::find($id);
        if(!$data) {
            return redirect('po_berita/');
        }
 
        $this->db->trans_begin();
        $this->db->delete('po_berita', array('id' => $id)); 
        logs("POST","po_berita/delete/",$id,"po_berita","delete"); //activity_logs => "method,route,table_id,table_name,table_aksi"
        
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }
        else{
            $this->db->trans_commit();
        }
        set_flashdata('success', 'Data berhasil disimpan');
        redirect('/po_berita/');
    }
    
    public function detail($id) {
        $this->load->model('MPoBerita');
        $data = MPoBerita::find($id);
        if(!$data) {
            return redirect('po_artikel/');
        }
        
        $this->setParam('data', @$data);
        $this->setContentView('web/po_berita/detail');
        $this->render('Detail Berita');
    }

    public function send_mail($id) {
        $this->load->model('MPoArtikel');
        $this->load->model('MPersonal');

        $data = MPoArtikel::find($id);
        if(!$data OR $data->is_karyawan==0) {
            set_flashdata('success', 'Data Tidak Ditemukan Atau Tidak Di Share ke Pegawai');
            return redirect('po_artikel/');
        }
        
        //send email
        $personals = MPersonal::where("status",1)->get();
        
        $to_email = array();
        // $no = 1;
        foreach ($personals as $key => $value) {
            if (!empty(@$value->user->email)) {
                $to_email[] = @$value->user->email;
            }
        }
        $to_email = implode(",",$to_email);
        
        //dumy email/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $to_email = "internal@santara.co.id";
        // $to_email = "rizky.widowati@santara.co.id,enggar.leksono@santara.co.id,gangsar.adi@santara.co.id,dinnu.shobirin@santara.co.id,aditya.putri@santara.co.id,theresia.widyadara@santara.co.id,christella.kusumastuti@santara.co.id,gagas.baskara@santara.co.id";
        // $to_email = "dinnu.shobirin@santara.co.id"; internal@santara.co.id
        //end dumy email/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        

        $deksripsi  = substr(strip_tags($data->deskripsi),0,300);  
        $detail     = "<a href='".site_url('po_artikel/detail/'.$data->id)."' target='_blank'>selengkapnya...</a>";
        $dataEmail  = array(
            'name'          => $data->name,
            'tanggal'       => tgl_indo($data->tanggal),
            'kategori'      => $data->kategoris->name,
            'deksripsi'     => $deksripsi,
            'lampiran'      => base_url("/uploads/artikel/".$data->lampiran),
            'detail'        => $detail,
        );
        
        // MasDebugPree($dataEmail,1);
        
        $this->load->library('mailgun');
        $dataSend = array(
            'to'        => $to_email,
            'subject'   => $data->kategoris->name." - PT. Santara Daya Inspira",
            'html'      => $this->load->view('email/artikel_notif',$dataEmail,true),   
        );
        $this->mailgun::send($dataSend);
        //end send email
        set_flashdata('success', 'Data berhasil disimpan');
        redirect('/po_artikel/');
    }

}