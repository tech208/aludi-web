<?php

class Po_peserta extends sntr_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('MPoPendaftaran');
        $this->load->model('MPoPendaftaranAlur');
        $this->load->model('MPoPendaftaranTahapPembayaran');
        $this->load->model('MPoPendaftaranTahapRekomendasi');
        $this->load->model('MPoPendaftaranTahap');
        $this->load->model('MPoPendaftaranTahapDua');
        $this->load->model('MPoPendaftaranTahapSatu');
        $this->load->model('MPoPendaftaranTahapVisit');
        $this->load->model('MPoPendaftaranTahapVisitHasil');
        $this->load->model('MPoPendaftaranTahapVisitLangkah');
        $this->load->model('MCoreRefStatus');
        $this->load->model('MCoreUser');
        $this->load->model('MCoreRefProvinsi');
        $this->load->model('MCoreRefKabupaten');
        
        ini_set('date.timezone', 'Asia/Jakarta');
    }

    public function index() {
        $data = MPoPendaftaranTahap::orderBy('id', 'desc')->get();
        
        $status     = "";
        if (empty(@$_POST)) {
            @$_POST['status'] = "Aktif";
        }

        if (!empty(@$_POST)) {

            if (@$_POST['status'] AND @$_POST['status']!="All") {
                foreach ($data as $key => $value) {
                    if($value->statuse->grup_status != @$_POST['status']){
                        unset($data[$key]);
                    }
                }
            }
            
            $status     = @$_POST['status'];
        }

        $refStatus = array(
            0 => array("id"=>"Aktif","name"=>"Aktif"),
            1 => array("id"=>"Pasif","name"=>"Pasif"),
            2 => array("id"=>"Blacklist","name"=>"Blacklist"),
            3 => array("id"=>"All","name"=>"All"),
        );

        
        $this->setParam('status', $status);
        $this->setParam('refStatus', $refStatus);
        $this->setParam('data', $data);
        $this->setContentView('web/po_peserta/index');
        $this->render('Penyelenggara');
    }

    public function form($id='',$mode='') {
        $this->load->model('MPoArtikel');
        $this->load->model('MPoRefKategori');
        $this->load->model('MCoreRefStatus');

        if (!empty($id)) {
            $data = MPoArtikel::where('id', $id)->orderBy('id', 'asc')->first();
        }
        
        $refKategori    = MPoRefKategori::orderBy('name', 'asc')->get();
        $refStatus      = MCoreRefStatus::
                            whereIn('id', explode(',', '1,3,4'))
                            ->orderBy('name', 'asc')->get();

        $this->setParam('refStatus', $refStatus);
        $this->setParam('refKategori', $refKategori);
        $this->setParam('mode', $mode);
        $this->setParam('data', @$data);
        $this->setContentView('web/po_artikel/form');
        $this->render('Form Artikel');
    }

    public function save() {
        $this->load->model('MPoArtikel');
        $this->load->model('MPoRefKategori');
        
        $post   = $this->input->post();
        $id     = !empty($post['id'])?$post['id']:null;

        
        $result = true;
        // validasi
        $msg = '';
        // end validasi

        if($result==true){
            $this->db->trans_begin();

            $params = array(
                "id_kategori"   => $post['id_kategori'],
                "tanggal"       => $post['tanggal'],
                "name"          => $post['name'],
                "deskripsi"     => $post['deskripsi'],
                "is_karyawan"   => !empty($post['is_karyawan'])?$post['is_karyawan']:0,
                "status"        => $post['status'],
            );

            if(isset($_FILES['lampiran']) && !empty($_FILES['lampiran']['name'])) {//bila ada lampiran
                $config['upload_path']          = './uploads/artikel/';
                $config['allowed_types']        = '*';
                $config['overwrite']        	= true;
                $config['max_size']             = 5120;

                $this->load->library('upload', $config);

                $this->upload->do_upload('lampiran');
                
                $upload_data        = $this->upload->data();
                $params['lampiran']  = $upload_data['file_name'];
            }

            if (empty($id)) { //untuk insert
                $params['created_at'] = date('Y-m-d H:i:s');
                $params['created_by'] = authUser()->id;

                $this->db->insert('po_artikel', $params);
                $insert_id = $this->db->insert_id();
                logs("POST","po_artikel/save/",$insert_id,"po_artikel","insert"); //activity_logs => "method,route,table_id,table_name,table_aksi"   
            }else { //untuk update
                $params['updated_at'] = date('Y-m-d H:i:s');
                $params['updated_by'] = authUser()->id;

                $this->db->update('po_artikel', $params, array('id' => $id));
                logs("POST","po_artikel/save/",$id,"po_artikel","update"); //activity_logs => "method,route,table_id,table_name,table_aksi"
            }

            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
            }
            else{
                $this->db->trans_commit();
            }
            set_flashdata('success', 'Data berhasil disimpan');
            redirect('/po_artikel/');
            
        }
        else{
            set_flashdata('error', 'Data Tidak berhasil disimpan. '.$msg);
            redirect('/po_artikel/');
        }
    }

    public function delete($id) {
        $this->load->model('MPoArtikel');
        $data = MPoArtikel::find($id);
        if(!$data) {
            return redirect('po_artikel/');
        }

        $this->db->trans_begin();
        $this->db->delete('po_artikel_file', array('id_artikel' => $id));  
        $this->db->delete('po_artikel', array('id' => $id)); 
        logs("POST","po_artikel/delete/",$id,"po_artikel","delete"); //activity_logs => "method,route,table_id,table_name,table_aksi"
        
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }
        else{
            $this->db->trans_commit();
        }
        set_flashdata('success', 'Data berhasil disimpan');
        redirect('/po_artikel/');
    }
    
    public function detail($id) {
        $data = MPoPendaftaranTahap::find($id);
        $data_hasil = MPoPendaftaranTahapVisitHasil::where('id_tahap',$id)
                      ->orderby('id','desc')
                      ->get();


        if(!$data OR empty($id)) {
            return redirect('Po_peserta/');
        }


        
        $refProvinsi      = MCoreRefProvinsi::
                            where("id",@$data->tahap_satu->id_provinsi)
                            ->orderBy('nama', 'asc')->get();

        $refKabupaten      = MCoreRefKabupaten::
                            where("id",@$data->tahap_satu->id_kota)->orderBy('nama', 'asc')->get();


        $this->setParam('refKabupaten', $refKabupaten);
        $this->setParam('refProvinsi', $refProvinsi);
        $this->setParam('data_hasil', @$data_hasil);        
        $this->setParam('data', @$data);
        $this->setContentView('web/po_peserta/detail');
        $this->render('Detail Peserta');
    }

    
    public function save_images_sertifikat($nama_file,$title,$no_registrasi,$sut_title,$tanggal) {
        // ========================================================== tanggal ================
        // Memuat dan menciptakan gambar
		$gambar = imagecreatefrompng('./uploads/pembayaran_sertifikat/template.png');

		// Mengalokasikan warna untuk teks, masukkan nilai RGB
		$warna_putih = imagecolorallocate($gambar, 255, 255, 255);

		// Menetapkan file path font
		$font_path = realpath('./app-assets/fonts/Montserrat-Light.otf');

		// Mendapatkan isi teks dari input form untuk dicetak ke gambar
		$isiteks = $tanggal;

		$ukuran	= 12;
		$angle	= 0;
		$kiri	= 150;
		$atas	= 370;

		// Cetak teks ke gambar
		imagettftext($gambar, $ukuran,$angle,$kiri,$atas, $warna_putih, $font_path, $isiteks);
        imagejpeg($gambar, './uploads/pembayaran_sertifikat/'.$nama_file.'.jpeg');
        // ========================================================== end tanggal ================


        // ========================================================== untuk sub title ================
        // Memuat dan menciptakan gambar
        $gambar = imagecreatefromjpeg('./uploads/pembayaran_sertifikat/'.$nama_file.'.jpeg');

		// // Mengalokasikan warna untuk teks, masukkan nilai RGB
		$warna_putih = imagecolorallocate($gambar, 255, 255, 255);

		// Menetapkan file path font
		$font_path = realpath('./app-assets/fonts/Montserrat-Light.otf');

		// Mendapatkan isi teks dari input form untuk dicetak ke gambar
		$isiteks = $sut_title;

		$ukuran	= 12;
		$angle	= 0;
		$kiri	= 40;
		$atas	= 310;

		// Cetak teks ke gambar
		imagettftext($gambar, $ukuran,$angle,$kiri,$atas, $warna_putih, $font_path, $isiteks);
        imagejpeg($gambar, './uploads/pembayaran_sertifikat/'.$nama_file.'.jpeg');
        // ========================================================== end untuk sub title ================

        // ========================================================== untuk title ================
        $gambar = imagecreatefromjpeg('./uploads/pembayaran_sertifikat/'.$nama_file.'.jpeg');

		// Mengalokasikan warna untuk teks, masukkan nilai RGB
		$warna_putih = imagecolorallocate($gambar, 255, 255, 255);

		// Menetapkan file path font
		$font_path = realpath('./app-assets/fonts/Montserrat-Bold.otf');

		// Mendapatkan isi teks dari input form untuk dicetak ke gambar
		$isiteks = $title;

		$ukuran	= 25;
		$angle	= 0;
		$kiri	= 40;
		$atas	= 250;

		// Cetak teks ke gambar
		imagettftext($gambar, $ukuran,$angle,$kiri,$atas, $warna_putih, $font_path, $isiteks);
        imagejpeg($gambar, './uploads/pembayaran_sertifikat/'.$nama_file.'.jpeg');
        // ========================================================== end untuk title ================
        
        // ========================================================== untuk no registrasi ================
        $gambar = imagecreatefromjpeg('./uploads/pembayaran_sertifikat/'.$nama_file.'.jpeg');

		// Mengalokasikan warna untuk teks, masukkan nilai RGB
		$warna_putih = imagecolorallocate($gambar, 255, 255, 255);

		// Menetapkan file path font
		$font_path = realpath('./app-assets/fonts/Montserrat-Bold.otf');

		// Mendapatkan isi teks dari input form untuk dicetak ke gambar
		$isiteks = $no_registrasi;

		$ukuran	= 18;
		$angle	= 0;
		$kiri	= 40;
		$atas	= 280;

		// Cetak teks ke gambar
		imagettftext($gambar, $ukuran,$angle,$kiri,$atas, $warna_putih, $font_path, $isiteks);
        imagejpeg($gambar, './uploads/pembayaran_sertifikat/'.$nama_file.'.jpeg');
        // ========================================================== end untuk title ================

    }
    
    public function status($id_status,$id_tahap,$tipe_tahap) {
        if($tipe_tahap=="tahap1"){
            $data = MPoPendaftaranTahapSatu::find($id_tahap);
        }

        elseif($tipe_tahap=="pembayaran"){
            $data = MPoPendaftaranTahapPembayaran::find($id_tahap);
            
            if ($id_status=='22') { //Verifikasi Pembayaran Diterima
                if($data->sertifikat == "" OR empty($data->sertifikat)){
                    $dataTahap1 = MPoPendaftaranTahapSatu::where('id_personal', $data->id_personal)->orderBy('id', 'asc')->first();
        
        
                    $bulan_depan = date("F", strtotime("+1 month", strtotime($data->created_at)));
                    $tahun_depan = date("Y", strtotime("+1 year", strtotime($data->created_at)));
                    
                    $bulan_saat_ini = date("F", strtotime($data->created_at));
                    $tahun_saat_ini = date("Y", strtotime($data->created_at));
        
                    $saat_ini = date("d F Y");
        
                    $title          = $dataTahap1->nama_perusahaan;
                    $sut_title      = "As a valued member for period ".$bulan_saat_ini." ".$tahun_saat_ini." - ".$bulan_depan." ".$tahun_depan;
                    $tanggal        = "JAKARTA, ".$saat_ini;
                    $no_registrasi  = $data->id."/REG/ALUDI/SER";
            
                    $nama_file = MasRandom(50);
                    $this->save_images_sertifikat($nama_file,$title,$no_registrasi,$sut_title,$tanggal);
        
                    $params["updated_at"]   = date('Y-m-d H:i:s');
                    $params["updated_by"]   = authUser()->id;
                    $params["sertifikat"]   = $nama_file.".jpeg";
                    $params["sertifikat_no"]= $no_registrasi;
                    $params["periode"]      = $bulan_saat_ini." ".$tahun_saat_ini." - ".$bulan_depan." ".$tahun_depan;
            
                    $this->db->update('po_pendaftaran_tahap_pembayaran', $params, array('id' => $data->id));
                    logs("POST","po_tahap_pendaftaran/pembayaran_bukti_sertifikat/",$data->id,"po_pendaftaran_tahap_pembayaran","update"); //activity_logs => "method,route,table_id,table_name,table_aksi"
                }
            }
        }

        elseif($tipe_tahap=="tahap2"){
            $data = MPoPendaftaranTahapDua::find($id_tahap);
        }

        elseif($tipe_tahap=="rekomendasi"){
            $data               = MPoPendaftaranTahap::find($id_tahap);
            @$data->id_tahap    = $data->id;
        }

        elseif($tipe_tahap=="selesai"){
            $data               = MPoPendaftaranTahap::find($id_tahap);
            @$data->id_tahap    = $data->id;
        }

        if(empty($data->id)) {
            return redirect('Po_peserta/');
        }

        
        $params['updated_at']   = date('Y-m-d H:i:s');
        $params['updated_by']   = authUser()->id;
        $params['status']       = $id_status;
        
        
        $params_utama['updated_at']   = date('Y-m-d H:i:s');
        $params_utama['updated_by']   = authUser()->id;
        $params_utama['status']       = $id_status; //sesuai inputan
        if ($id_status=='18') { //Verifikasi Tahap 1 Diterima
            $params_utama['status']       = 20; //Pembayaran
        }
        elseif ($id_status=='22') { //Verifikasi Pembayaran Diterima
            $params_utama['status']       = 24; //Tahap 2
        }
        elseif ($id_status=='26') { //Verifikasi Tahap 2 Diterima
            $params_utama['status']       = 28; //Tahap 2
        }

        if($tipe_tahap=="tahap1"){
            $this->db->trans_begin();
            
            $this->db->update('po_pendaftaran_tahap', $params_utama, array('id' => $data->id_tahap));
            logs("POST","po_peserta/status/",$data->id_tahap,"po_pendaftaran_tahap","update"); //activity_logs => "method,route,table_id,table_name,table_aksi"

            $this->db->update('po_pendaftaran_tahap_satu', $params, array('id' => $data->id));
            logs("POST","po_peserta/status/",$data->id,"po_pendaftaran_tahap_satu","update"); //activity_logs => "method,route,table_id,table_name,table_aksi"

            
            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
            }
            else{
                $this->db->trans_commit();

                if ($id_status=='18') { //Verifikasi Tahap 1 Diterima
                    //send email
                    $tahap = MPoPendaftaranTahap::where('id_personal', $data->id_personal)->orderBy('id', 'asc')->first();
                    $dataEmail = array(
                        'email'         => $tahap->personals->personal_email,
                        'nama'          => $tahap->tahap_satu->nama_perusahaan,
                        'url_detail'    => site_url('/po_tahap_pendaftaran/tahap1/'.$data->id_tahap.'/detail'),
                    );
    
                    $this->load->library('mailgun');
                    $dataSend = array(
                        'to'        => $tahap->personals->personal_email,
                        'subject'   => "Tahap 1 Berhasil Diverifikasi - Platform ALUDI",
                        'html'      => $this->load->view('email/pendaftaran_tahap1_berhasil_diverifikasi',$dataEmail,true),  
                    );
                    $this->mailgun::send($dataSend);
                    //end send email
                }

            }
            set_flashdata('success', 'Data berhasil disimpan');
            
        }
        
        if($tipe_tahap=="pembayaran"){
            $this->db->trans_begin();
            $this->db->update('po_pendaftaran_tahap', $params_utama, array('id' => $data->id_tahap));
            logs("POST","po_peserta/status/",$data->id_tahap,"po_pendaftaran_tahap","update"); //activity_logs => "method,route,table_id,table_name,table_aksi"

            $this->db->update('po_pendaftaran_tahap_pembayaran', $params, array('id' => $data->id));
            logs("POST","po_peserta/status/",$data->id,"po_pendaftaran_tahap_pembayaran","update"); //activity_logs => "method,route,table_id,table_name,table_aksi"

            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
            }
            else{
                $this->db->trans_commit();
                
                if ($id_status=='22') { //Verifikasi Pembayaran Diterima
                    //send email
                    $tahap = MPoPendaftaranTahap::where('id_personal', $data->id_personal)->orderBy('id', 'asc')->first();
                    $dataEmail = array(
                        'email'         => $tahap->personals->personal_email,
                        'nama'          => $tahap->tahap_satu->nama_perusahaan,
                        'url_detail'    => site_url('/po_tahap_pendaftaran/pembayaran/'.$data->id_tahap.'/detail'),
                    );
    
                    $this->load->library('mailgun');
                    $dataSend = array(
                        'to'        => $tahap->personals->personal_email,
                        'subject'   => "Pembayaran Berhasil Diverifikasi - Platform ALUDI",
                        'html'      => $this->load->view('email/pendaftaran_pembayaran_berhasil_diverifikasi',$dataEmail,true),  
                    );
                    $this->mailgun::send($dataSend);
                    //end send email
                }
            }
            set_flashdata('success', 'Data berhasil disimpan');
        }

        if($tipe_tahap=="tahap2"){
            $this->db->trans_begin();
            $this->db->update('po_pendaftaran_tahap', $params_utama, array('id' => $data->id_tahap));
            logs("POST","po_peserta/status/",$data->id_tahap,"po_pendaftaran_tahap","update"); //activity_logs => "method,route,table_id,table_name,table_aksi"

            $this->db->update('po_pendaftaran_tahap_dua', $params, array('id' => $data->id));
            logs("POST","po_peserta/status/",$data->id,"po_pendaftaran_tahap_dua","update"); //activity_logs => "method,route,table_id,table_name,table_aksi"

            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
            }
            else{
                $this->db->trans_commit();

                if ($id_status=='26') { //Verifikasi Tahap 2 Diterima
                    //send email
                    $tahap = MPoPendaftaranTahap::where('id_personal', $data->id_personal)->orderBy('id', 'asc')->first();
                    $dataEmail = array(
                        'email'         => $tahap->personals->personal_email,
                        'nama'          => $tahap->tahap_satu->nama_perusahaan,
                        'url_detail'    => site_url('/po_tahap_pendaftaran/tahap2/'.$data->id_tahap.'/detail'),
                    );
    
                    $this->load->library('mailgun');
                    $dataSend = array(
                        'to'        => $tahap->personals->personal_email,
                        'subject'   => "Tahap 2 Telah Diverifikasi - Platform ALUDI",
                        'html'      => $this->load->view('email/pendaftaran_tahap2_telah_diverifikasi',$dataEmail,true),  
                    );
                    $this->mailgun::send($dataSend);
                    //end send email
                }
            }
            set_flashdata('success', 'Data berhasil disimpan');
        }

        if($tipe_tahap=="rekomendasi"){
            $this->db->trans_begin();
            $this->db->update('po_pendaftaran_tahap', $params_utama, array('id' => $data->id_tahap));
            logs("POST","po_peserta/status/",$data->id_tahap,"po_pendaftaran_tahap","update"); //activity_logs => "method,route,table_id,table_name,table_aksi"

            
            $params_rekomendasi['nama']         = "Sedang Proses Di Rekomendasikan"; 
            $params_rekomendasi['deskripsi']    = "direkomendasikan Oleh :".MasKaryawan()->name;
            $params_rekomendasi['id_personal']  = $data->id_personal; //Menunggu Proses Rekomendasi
            $params_rekomendasi['id_tahap']     = $data->id_tahap; //Menunggu Proses Rekomendasi
            $params_rekomendasi['status']       = 30; //Menunggu Proses Rekomendasi
            $params_rekomendasi['created_at']   = date('Y-m-d H:i:s');
            $params_rekomendasi['created_by']   = authUser()->id;

            $this->db->insert('po_pendaftaran_tahap_rekomendasi', $params_rekomendasi);
            $insert_id = $this->db->insert_id();
            logs("POST","po_artikel/save/",$insert_id,"po_artikel","insert"); //activity_logs => "method,route,table_id,table_name,table_aksi"   

            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
            }
            else{
                $this->db->trans_commit();
            }
            set_flashdata('success', 'Data berhasil disimpan');
        }


        // MasDebugPree($data->tahap_rekomendasi,1);
        if($tipe_tahap=="selesai" AND !empty(@$data->tahap_rekomendasi->id)){

            $this->db->trans_begin();
            $this->db->update('po_pendaftaran_tahap', $params_utama, array('id' => $data->id_tahap));
            logs("POST","po_peserta/status/",$data->id_tahap,"po_pendaftaran_tahap","update"); //activity_logs => "method,route,table_id,table_name,table_aksi"

            $this->db->update('po_pendaftaran_tahap_rekomendasi', $params, array('id' => $data->tahap_rekomendasi->id));
            logs("POST","po_peserta/status/",$data->id,"po_pendaftaran_tahap_rekomendasi","update"); //activity_logs => "method,route,table_id,table_name,table_aksi"

            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
            }
            else{
                $this->db->trans_commit();
            }
            set_flashdata('success', 'Data berhasil disimpan');
        }
        

        return redirect('po_peserta/detail/'.$data->id_tahap);
    }

    
    public function saveVisitHasil() {
        $post       = $this->input->post();
        if (empty($post)) { redirect('/po_peserta/'); }//bila gak ada inputan

        
        $this->db->trans_begin();
        // $params['updated_at']   = date('Y-m-d H:i:s');
        // $params['updated_by']   = authUser()->id;
        // $params['status']       = 29; //Site Visit
        
        
        //$this->db->update('po_pendaftaran_tahap', $params, array('id' => $post['id_tahap']));
        // logs("POST","po_peserta/status/",$post['id_tahap'],"po_pendaftaran_tahap","update"); //activity_logs => "method,route,table_id,table_name,table_aksi"

        
        $data = MPoPendaftaranTahap::find($post['id_tahap']);

        $params_visit['id_tahap']               = $post['id_tahap'];
        $params_visit['id_personal']            = $data->id_personal;
        $params_visit['nama']                   = $post['nama'];
        // $params_visit['lokasi']                 = $post['lokasi'];
        // $params_visit['tanggal_mulai']          = $post['tanggal_mulai'];
        // $params_visit['tanggal_mulai_jam']      = $post['tanggal_mulai_jam'];
        // $params_visit['tanggal_selesai']        = $post['tanggal_selesai'];
        // $params_visit['tanggal_selesai_jam']    = $post['tanggal_selesai_jam'];
        $params_visit['created_at']             = date('Y-m-d H:i:s');
        $params_visit['created_by']             = authUser()->id;
        $params_visit['status']                 = "2"; //Site Visit
        
        if(isset($_FILES['lampiran']) && !empty($_FILES['lampiran']['name'])) {//bila ada lampiran
            $config['upload_path']          = './uploads/visit_hasil/';
            $config['allowed_types']        = '*';
            $config['overwrite']        	= true;
            $config['encrypt_name']        	= true;            
            $config['max_size']             = 5120;

            $this->load->library('upload', $config);

            $this->upload->do_upload('lampiran');
            
            $upload_data        = $this->upload->data();
            $params_visit['lampiran']  = $upload_data['file_name'];
        }        

        // masDebugPree($params_visit);
        // die();

        $this->db->insert('po_pendaftaran_tahap_visit_hasil', $params_visit);

        // $id_visit = $this->db->insert_id();
        // logs("POST","po_peserta/saveVisit/",$id_visit,"po_pendaftaran_tahap_visit","insert"); //activity_logs => "method,route,table_id,table_name,table_aksi"

        // $visitor = $post['visitor'];
        // if (!empty($visitor)) {
        //     foreach ($visitor as $key => $value) {
        //         if (!empty($value["nama"]) AND !empty($value["jabatan"])) {
        //             $params_visit_visitor['id_visit']   = $id_visit;
        //             $params_visit_visitor['nama']       = $value['nama'];
        //             $params_visit_visitor['jabatan']    = $value['jabatan'];
        //             $params_visit_visitor['created_at'] = date('Y-m-d H:i:s');
        //             $params_visit_visitor['created_by'] = authUser()->id;
        //             $params_visit_visitor['status']     = 29; //Site Visit
        //             $this->db->insert('po_pendaftaran_tahap_visit_visitor', $params_visit_visitor);
        //         }
        //     }
        // }

        
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }
        else{
            $this->db->trans_commit();
        }
        set_flashdata('success', 'Data berhasil disimpan');
        

        
        return redirect('po_peserta/detail/'.$data->id);
    }
    


    public function saveVisit() {
        $post       = $this->input->post();
        if (empty($post)) { redirect('/po_peserta/'); }//bila gak ada inputan

        
        $this->db->trans_begin();
        $params['updated_at']   = date('Y-m-d H:i:s');
        $params['updated_by']   = authUser()->id;
        $params['status']       = 29; //Site Visit
        
        
        $this->db->update('po_pendaftaran_tahap', $params, array('id' => $post['id_tahap']));
        logs("POST","po_peserta/status/",$post['id_tahap'],"po_pendaftaran_tahap","update"); //activity_logs => "method,route,table_id,table_name,table_aksi"

        
        $data = MPoPendaftaranTahap::find($post['id_tahap']);

        $params_visit['id_tahap']               = $post['id_tahap'];
        $params_visit['id_personal']            = $data->id_personal;
        $params_visit['nama']                   = $post['nama'];
        $params_visit['lokasi']                 = $post['lokasi'];
        $params_visit['tanggal_mulai']          = $post['tanggal_mulai'];
        $params_visit['tanggal_mulai_jam']      = $post['tanggal_mulai_jam'];
        $params_visit['tanggal_selesai']        = $post['tanggal_selesai'];
        $params_visit['tanggal_selesai_jam']    = $post['tanggal_selesai_jam'];
        $params_visit['created_at']             = date('Y-m-d H:i:s');
        $params_visit['created_by']             = authUser()->id;
        $params_visit['status']                 = 29; //Site Visit
        
        $this->db->insert('po_pendaftaran_tahap_visit', $params_visit);

        $id_visit = $this->db->insert_id();
        logs("POST","po_peserta/saveVisit/",$id_visit,"po_pendaftaran_tahap_visit","insert"); //activity_logs => "method,route,table_id,table_name,table_aksi"

        $visitor = $post['visitor'];
        if (!empty($visitor)) {
            foreach ($visitor as $key => $value) {
                if (!empty($value["nama"]) AND !empty($value["jabatan"])) {
                    $params_visit_visitor['id_visit']   = $id_visit;
                    $params_visit_visitor['nama']       = $value['nama'];
                    $params_visit_visitor['jabatan']    = $value['jabatan'];
                    $params_visit_visitor['created_at'] = date('Y-m-d H:i:s');
                    $params_visit_visitor['created_by'] = authUser()->id;
                    $params_visit_visitor['status']     = 29; //Site Visit
                    $this->db->insert('po_pendaftaran_tahap_visit_visitor', $params_visit_visitor);
                }
            }
        }

        
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }
        else{
            $this->db->trans_commit();
        }
        set_flashdata('success', 'Data berhasil disimpan');
        

        
        return redirect('po_peserta/detail/'.$data->id);
    }
    
    public function delete_visit($id) {
        $data = MPoPendaftaranTahapVisit::find($id);
        if(!$data) {
            return redirect('po_peserta/');
        }

        $this->db->trans_begin();
        $this->db->delete('po_pendaftaran_tahap_visit_visitor', array('id_visit' => $id));  
        $this->db->delete('po_pendaftaran_tahap_visit', array('id' => $id)); 
        logs("POST","po_peserta/delete_visit/",$id,"po_pendaftaran_tahap_visit","delete"); //activity_logs => "method,route,table_id,table_name,table_aksi"
        
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }
        else{
            $this->db->trans_commit();
        }
        set_flashdata('success', 'Data berhasil disimpan');
        return redirect('po_peserta/detail/'.$data->id_tahap);
    }

    public function save_rekomendasi_ojk() {

        $post   = $this->input->post();


        if(!empty($_FILES['files']['name']['rekomendasi_ojk'])) {//bila ada lampiran rekomendasi_ojk
            $config['upload_path']          = './uploads/rekomendasi_ojk/';
            $config['allowed_types']        = 'jpg|jpeg|png|pdf';
            $config['overwrite']        	= true;
            $config['encrypt_name']        	= true;
            $config['max_size']             = 10120;

            $this->load->library('upload', $config);

            
            $_FILES['rekomendasi_ojk']['name']       = $_FILES['files']['name']['rekomendasi_ojk'];
            $_FILES['rekomendasi_ojk']['type']       = $_FILES['files']['type']['rekomendasi_ojk'];
            $_FILES['rekomendasi_ojk']['tmp_name']   = $_FILES['files']['tmp_name']['rekomendasi_ojk'];
            $_FILES['rekomendasi_ojk']['error']      = $_FILES['files']['error']['rekomendasi_ojk'];
            $_FILES['rekomendasi_ojk']['size']       = $_FILES['files']['size']['rekomendasi_ojk'];

            
            if (!$this->upload->do_upload('rekomendasi_ojk')) {
                // MasDebugPree($this->upload->display_errors(),1);
                set_flashdata('error', 'Data Tidak Berhasil Disimpan, Pastikan File jpg|jpeg|png|pdf Dan ukuran Maximal 10mb');
               return redirect('/po_peserta/detail/'.$post['id_rekomendasi']);
            }


            $upload_data        = $this->upload->data();
            $params['nama_file']            = $upload_data['file_name'];        
            $params['id_rekomendasi']       = $post['id_rekomendasi'];        
            $params['nama']                 = "File Rekomendasi";        
            $params['status']               = 2;        
            $params['created_at']           = date('Y-m-d H:i:s');
            $params['created_by']           = authUser()->id;

            $this->db->insert('po_pendaftaran_tahap_rekomendasi_ojk_file', $params);
            $insert_id = $this->db->insert_id();
            logs("POST","po_peserta/save_rekomendasi_ojk/",$insert_id,"po_artikel","insert"); //activity_logs => "method,route,table_id,table_name,table_aksi" 
              
        }
        set_flashdata('success', 'Data berhasil disimpan');
        return redirect('/po_peserta/detail/'.$post['id']);
    }
    
    public function printxlx() {
        $data = MPoPendaftaranTahap::get();
        
        $result['data']         = $data;
        // return $this->load->view('laporan/peserta_list',$result);  
        
        ini_set('memory_limit', '256M');
        ini_set('max_execution_time', 0);
        header('Content-Type: application/vnd.ms-excel');
        header("Content-Disposition: attachment;Filename=report.xls");
        header('Cache-Control: max-age=0');
        $result = $this->load->view('laporan/peserta_list',$result,true);
        echo $result;
        exit;
    }
}