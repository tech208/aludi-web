<?php

class Po_banner extends sntr_Controller {

    public function __construct() {
        parent::__construct();
        ini_set('date.timezone', 'Asia/Jakarta');
        
        $this->load->model('MPoBanner');
        $this->load->model('MCoreRefStatus');

    }

    public function index() {
        $data = MPoBanner::
                where('status', 2)
                ->orderBy('id', 'desc')
                ->get();
            
        $this->setParam('data', $data);
        $this->setContentView('web/po_banner/index');
        $this->render('Banner');
    }



    public function form($id='',$detail='') {       

        $data = MPoBanner::find($id);

        $ref_status= MCoreRefStatus::
                     where('grup',1)
                     ->orderBy('id','asc')
                    ->get();
		$this->setParam('ref_jenis', array('1' => 'Gambar',
									   '2' => 'Video'));
        $this->setParam('ref_status', $ref_status);        
        $this->setParam('detail', $detail);
        $this->setParam('data', $data);
        $this->setContentView('web/po_banner/form');
        $this->render('Banner');
    }

    public function save() {
        $data   = $this->input->post();

        $this->db->trans_begin();

        $id = @$data['id'];
    
        $params = array(
            'nama'           => $data['nama'],
            'url'            => $data['url'],
            'status'         => $data['status'],        
            'status'         => $data['status'],        
            'jenis'         => $data['jenis'],        
        );
        if (empty($id)) { //insert

            $params["created_at"]   = date('Y-m-d H:i:s');
            $params["created_by"]   = authUser()->id;
            $this->db->insert('po_banner', $params);
            $id = $this->db->insert_id();

        }else{ //update
            $params["updated_at"]   = date('Y-m-d H:i:s');
            $params["updated_by"]   = authUser()->id;
    
            $this->db->update('po_banner', $params, array('id' => $id));
        }
        
        if(!empty($_FILES['files']['name']['lampiran'])) {//bila ada lampiran 
            $config['upload_path']          = './uploads/banner_lampiran/';
            $config['allowed_types']        = 'jpg|jpeg|png|mp4|mpg|avi|webm|mkv|gif|wmv|gifv|m4v';
            $config['overwrite']        	= true;
            $config['encrypt_name']        	= true;
            $config['max_size']             = 101200;

            $this->load->library('upload', $config);

            
            $_FILES['lampiran']['name']       = $_FILES['files']['name']['lampiran'];
            $_FILES['lampiran']['type']       = $_FILES['files']['type']['lampiran'];
            $_FILES['lampiran']['tmp_name']   = $_FILES['files']['tmp_name']['lampiran'];
            $_FILES['lampiran']['error']      = $_FILES['files']['error']['lampiran'];
            $_FILES['lampiran']['size']       = $_FILES['files']['size']['lampiran'];

            
            if (!$this->upload->do_upload('lampiran')) {
                MasDebugPree($this->upload->display_errors(),1);
              
				set_flashdata('error', 'Data Tidak Berhasil Disimpan, Pastikan File jpg|jpeg|png Dan ukuran Maximal 10mb');
                redirect('/po_banner/');
            }

            $upload_data        = $this->upload->data();
            $params_lampiran['lampiran']    = $upload_data['file_name'];
    
            $this->db->update('po_banner', $params_lampiran, array('id' => $id));
        }

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }
        else{
            $this->db->trans_commit();
        }
        
        
        set_flashdata('success', 'Data berhasil disimpan');
        redirect('/po_banner');

    }

    public function delete($id) {

        // $data = MHrPengajuan::find($id);

        // if(!$data) {
        //     return redirect('po_banner/');
        // }

        $this->db->trans_begin();
        $this->db->delete('po_banner', array('id' => $id));  

//        logs("POST","hr_pengajuan/delete/",$id,"hr_pengajuan","delete"); //activity_logs => "method,route,table_id,table_name,table_aksi"
        
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }
        else{
            $this->db->trans_commit();
        }
        set_flashdata('success', 'Data berhasil dihapus');
        redirect('/po_banner/');
    }

}