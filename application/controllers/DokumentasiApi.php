<?php

class DokumentasiApi extends sntr_Controller {

    public function __construct() {
        parent::__construct();
        
        $this->load->model('MPoAduan');
        $this->load->model('MCoreRefStatus');
        
        $this->load->model('MCoreRefNegara');
        $this->load->model('MCoreRefProvinsi');
        $this->load->model('MCoreRefKabupaten');
        $this->load->model('MCoreRefJenisPenerbit');

        ini_set('date.timezone', 'Asia/Jakarta');
    }

    public function index() {
        $this->setParam('data', array());
        $this->setContentView('web/api/index');
        $this->render('Dokumentasi Rest API');
    }

    public function update_token() {
        // $post       = $this->input->post();
        // if (empty($post)) { redirect('/dokumentasiApi/'); }//bila gak ada inputan

        $this->db->trans_begin();
        $params['updated_at']   = date('Y-m-d H:i:s');
        $params['updated_by']   = authUser()->id;
        $params['token']        = MasToken(50); //generate token
        $params['token_expired']= date('Y-m-d'); //Site Visit
        
        
        
        $this->db->update('core_users', $params, array('id' => authUser()->id));
        logs("POST","DokumentasiApi/update_token/",authUser()->id,"core_users","update",authUser()->id,"Memperbarui Token Rest API"); //activity_logs => "method,route,table_id,table_name,table_aksi"

        
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }
        else{
            $this->db->trans_commit();
        }
        set_flashdata('success', 'Data berhasil disimpan');
        

        
        return redirect('dokumentasiApi/');
    }
	
	public function update_token_test() {
        // $post       = $this->input->post();
        // if (empty($post)) { redirect('/dokumentasiApi/'); }//bila gak ada inputan

        $this->db->trans_begin();
        $params['updated_at']   = date('Y-m-d H:i:s');
        $params['updated_by']   = authUser()->id;
        $params['token_test']        = MasToken(50); //generate token
        $params['token_expired_test']= date('Y-m-d'); //Site Visit
        
        
        
        $this->db->update('core_users', $params, array('id' => authUser()->id));
        logs("POST","DokumentasiApi/update_token_test/",authUser()->id,"core_users","update",authUser()->id,"Memperbarui Token Test Rest API"); //activity_logs => "method,route,table_id,table_name,table_aksi"

        
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }
        else{
            $this->db->trans_commit();
        }
        set_flashdata('success', 'Data berhasil disimpan');
        

        
        return redirect('dokumentasiApi/');
    }
	
	public function update_secret_key() {
        // $post       = $this->input->post();
        // if (empty($post)) { redirect('/dokumentasiApi/'); }//bila gak ada inputan

        $this->db->trans_begin();
        $params['updated_at']   = date('Y-m-d H:i:s');
        $params['updated_by']   = authUser()->id;
        $params['secret_key']        = MasToken(50); //generate token
        
        
        
        $this->db->update('core_users', $params, array('id' => authUser()->id));
        logs("POST","DokumentasiApi/update_secret_key/",authUser()->id,"core_users","update",authUser()->id,"Memperbarui Secret Key"); //activity_logs => "method,route,table_id,table_name,table_aksi"

        
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }
        else{
            $this->db->trans_commit();
        }
        set_flashdata('success', 'Data berhasil disimpan');
        

        
        return redirect('dokumentasiApi/');
    }

    
    public function referensi() {
        $data["negara"] = MCoreRefNegara::
                orderBy('nama', 'asc')
                ->get();

        $data["provinsi"] = MCoreRefProvinsi::
                orderBy('nama', 'asc')
                ->get();

        $data["kabupaten"] = MCoreRefKabupaten::
                orderBy('nama', 'asc')
                ->get();

        $data["status"] = MCoreRefStatus::
                where("is_public","yes")
                ->orderBy('name', 'asc')
                ->get();

        $data["jenis_penerbit"] = MCoreRefJenisPenerbit::
                orderBy('nama', 'asc')
                ->get();

        // MasDebugPree($dataNegara,1);
        $this->setParam('data', $data);
        $this->setContentView('web/api/referensi');
        $this->render('Referensi Data Rest API');
    }

}