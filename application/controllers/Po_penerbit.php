<?php

class Po_penerbit extends sntr_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('MPoPendaftaran');
        $this->load->model('MCoreRefStatus');
        $this->load->model('MCoreUser');
        $this->load->model('MCoreRefProvinsi');
        $this->load->model('MCoreRefKabupaten');
        
        $this->load->model('MPoCalonPenerbit');
        $this->load->model('MPoPenerbit');
        ini_set('date.timezone', 'Asia/Jakarta');
    }

    public function index() {
        if (MasKaryawan()->user->role_id=='1') {
            $data = MPoPenerbit::orderBy('id', 'desc')->get();
		}else if (MasKaryawan()->user->role_id=='7') {
			$arrpersonals = $this->db->query("SELECT * FROM personals A WHERE A.user_id = '".MasKaryawan()->user->id."'")->row_array();
			$data = $this->db->query("SELECT * FROM po_penerbit A WHERE A.id_personal = '".$arrpersonals['id']."'")->result_array();
			print_r($data);
			die();
            $data = MPoPenerbit::where("id_personal",$arrpersonals['id'])
            ->orderBy('id', 'desc')->get();
			echo $this->db->last_query();
			die();
        }else{
            $data = MPoPenerbit::where("created_by",MasKaryawan()->user->id)
            ->orderBy('id', 'desc')->get();
        }
        //filter
        $status     = "";
        if (@$_POST['status']!="" AND @$_POST['status']!="All") {
            if (MasKaryawan()->user->role_id=='1') {
                $data = MPoPenerbit::
                where('status', $_POST['status'])
                ->orderBy('id', 'desc')->get();
            }else{
                $data = MPoPenerbit::where("created_by",MasKaryawan()->user->id)
                ->where('status', $_POST['status'])
                ->orderBy('id', 'desc')->get();
            }
            
            $status     = @$_POST['status'];
        }
        //end filter

        $refStatus  = MCoreRefStatus::
        where('grup', 14)
        ->orderBy('id', 'desc')
        ->get();
        
        $this->setParam('refStatus', $refStatus);
        $this->setParam('status', $status);
        $this->setParam('data', $data);
        $this->setContentView('web/po_penerbit/index');
        $this->render('Penerbit');
    }

    public function form($id='',$mode='') {
        if (!empty($id)) {
            $data = MPoPenerbit::where('id', $id)->orderBy('id', 'asc')->first();
        }
        
        $refCalonPenerbit   = MPoCalonPenerbit::orderBy('nama_perusahaan', 'asc')->get();
        foreach ($refCalonPenerbit as $key => $value) {
            $cekCalonPenerbit   = MPoPenerbit::where('id', @$value->id)->first();

            if (@$cekCalonPenerbit->id AND empty(@$id)) {
                unset($refCalonPenerbit[$key]);
            }
        }

        $refStatus          = MCoreRefStatus::
                            where('grup','14')
                            ->orderBy('name', 'asc')->get();

        $this->setParam('refStatus', $refStatus);
        $this->setParam('refCalonPenerbit', $refCalonPenerbit);
        $this->setParam('mode', $mode);
        $this->setParam('data', @$data);
        $this->setContentView('web/po_penerbit/form');
        $this->render('Form Penerbit');
    }

    public function save() {        
        $post   = $this->input->post();

        $id     = !empty($post['id'])?$post['id']:null;

        
        $result = true;
        // validasi
        $msg = '';
        // end validasi

        if($result==true){
            $this->db->trans_begin();

            $params = array(
                "id_personal"       => MasKaryawan()->id,
                "kode"              => MasToken(25),
                "id_calon_penerbit" => @$post['id_calon_penerbit'],
                "owner"             => @$post['owner'],
                "email"             => @$post['email'],
                "nama_perusahaan"   => @$post['nama_perusahaan'],
                "nama_brand"        => @$post['nama_brand'],
                "bidang_usaha"      => @$post['bidang_usaha'],
                "lokasi"            => @$post['lokasi'],
                "total_pendanaan"   => @$post['total_pendanaan'],
                "deskripsi"         => @$post['deskripsi'],
                "status"            => @$post['status'],
            );

            MasDebugPree($params,1);

            if(isset($_FILES['lampiran']) && !empty($_FILES['lampiran']['name'])) {//bila ada lampiran
                $config['upload_path']          = './uploads/penerbit/';
                $config['allowed_types']        = '*';
                $config['overwrite']        	= true;
                $config['max_size']             = 5120;

                $this->load->library('upload', $config);

                $this->upload->do_upload('lampiran');
                
                $upload_data        = $this->upload->data();
                $params['lampiran']  = $upload_data['file_name'];
            }

            if (empty($id)) { //untuk insert
                $params['created_at'] = date('Y-m-d H:i:s');
                $params['created_by'] = authUser()->id;

                $this->db->insert('po_penerbit', $params);
                $insert_id = $this->db->insert_id();
                logs("POST","po_penerbit/save/",$insert_id,"po_penerbit","insert"); //activity_logs => "method,route,table_id,table_name,table_aksi"   
            }else { //untuk update
                $params['updated_at'] = date('Y-m-d H:i:s');
                $params['updated_by'] = authUser()->id;

                $this->db->update('po_penerbit', $params, array('id' => $id));
                logs("POST","po_penerbit/save/",$id,"po_penerbit","update"); //activity_logs => "method,route,table_id,table_name,table_aksi"
            }

            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
            }
            else{
                $this->db->trans_commit();
            }
            set_flashdata('success', 'Data berhasil disimpan');
            redirect('/po_penerbit/');
            
        }
        else{
            set_flashdata('error', 'Data Tidak berhasil disimpan. '.$msg);
            redirect('/po_penerbit/');
        }
    }

    public function delete($id) {
        $this->load->model('MPoArtikel');
        $data = MPoArtikel::find($id);
        if(!$data) {
            return redirect('po_penerbit/');
        }

        $this->db->trans_begin();
        $this->db->delete('po_penerbit_file', array('id_artikel' => $id));  
        $this->db->delete('po_penerbit', array('id' => $id)); 
        logs("POST","po_penerbit/delete/",$id,"po_penerbit","delete"); //activity_logs => "method,route,table_id,table_name,table_aksi"
        
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }
        else{
            $this->db->trans_commit();
        }
        set_flashdata('success', 'Data berhasil disimpan');
        redirect('/po_penerbit/');
    }
    
    public function detail($id) {
        $data = MPoPendaftaranTahap::find($id);
        $data_hasil = MPoPendaftaranTahapVisitHasil::where('id_tahap',$id)
                      ->orderby('id','desc')
                      ->get();

        // MasDebugPree(@$data->tahap_rekomendasi->rekomendasis[0]->nama_file,1);
        // MasDebugPree($data->tahap_rekomendasi,1);

        if(!$data OR empty($id)) {
            return redirect('po_calon_penerbit/');
        }


        
        $refProvinsi      = MCoreRefProvinsi::
                            where("id",@$data->tahap_satu->id_provinsi)
                            ->orderBy('nama', 'asc')->get();

        $refKabupaten      = MCoreRefKabupaten::
                            where("id",@$data->tahap_satu->id_kota)->orderBy('nama', 'asc')->get();


        // MasDebugPree($data->tahap_rekomendasi->rekomendasis->nama_file,1);
        // MasDebugPree($data->tahap_satu,1);
        $this->setParam('refKabupaten', $refKabupaten);
        $this->setParam('refProvinsi', $refProvinsi);
        $this->setParam('data_hasil', @$data_hasil);        
        $this->setParam('data', @$data);
        $this->setContentView('web/po_calon_penerbit/detail');
        $this->render('Detail Peserta');
    }
    
    
    public function printxlx() {
        $data = MPoPendaftaranTahap::get();
        
        $result['data']         = $data;
        // return $this->load->view('laporan/peserta_list',$result);  
        
        ini_set('memory_limit', '256M');
        ini_set('max_execution_time', 0);
        header('Content-Type: application/vnd.ms-excel');
        header("Content-Disposition: attachment;Filename=report.xls");
        header('Cache-Control: max-age=0');
        $result = $this->load->view('laporan/peserta_list',$result,true);
        echo $result;
        exit;
    }
}