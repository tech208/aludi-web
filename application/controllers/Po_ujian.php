<?php

class Po_ujian extends sntr_Controller {

    public function __construct() {
        parent::__construct();
        
        $this->load->model('MCoreRefStatus');
        $this->load->model('MPoUjian');
    }

    public function index() {
        $data = MPoUjian::orderBy('id', 'DESC')->get();

        $this->setParam('data', $data);
        $this->setContentView('web/po_ujian/index');
        $this->render('Ujian Site Visit');
    }

    public function form($id='',$mode='') {
        if (!empty($id)) {
            $data = MPoUjian::where('id', $id)->orderBy('id', 'asc')->first();
        }
        
        $refStatus      = MCoreRefStatus::
                            where('grup', '1')
                            // whereIn('id', explode(',', '1,3,4'))
                            ->orderBy('name', 'asc')->get();

        $this->setParam('refStatus', $refStatus);
        $this->setParam('mode', $mode);
        $this->setParam('data', @$data);
        $this->setContentView('web/po_ujian/form');
        $this->render('Form Ujian Site Visit');
    }

    public function save() {        
        $post   = $this->input->post();
        $id     = !empty($post['id'])?$post['id']:null;

        
        $result = true;
        // validasi
        $msg = '';
        // end validasi

        if($result==true){
            $this->db->trans_begin();

            $params = array(
                "name"          => $post['name'],
                "deskripsi"     => $post['deskripsi'],
                "url"           => $post['url'],
                "status"        => $post['status'],
            );


            if (empty($id)) { //untuk insert
                $params['created_at'] = date('Y-m-d H:i:s');
                $params['created_by'] = authUser()->id;

                $this->db->insert('po_ujian', $params);
                $id = $this->db->insert_id();
                logs("POST","po_ujian/save/",$id,"po_ujian","insert",authUser()->id,"Menambahkan Ujian Site Ujian"); //activity_logs => "method,route,table_id,table_name,table_aksi"   
            }else { //untuk update
                $params['updated_at'] = date('Y-m-d H:i:s');
                $params['updated_by'] = authUser()->id;

                $this->db->update('po_ujian', $params, array('id' => $id));
                logs("POST","po_ujian/save/",$id,"po_ujian","update",authUser()->id,"Merubah Ujian Site Ujian"); //activity_logs => "method,route,table_id,table_name,table_aksi"
            }

            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
            }
            else{
                $this->db->trans_commit();
            }
            set_flashdata('success', 'Data berhasil disimpan');
            redirect('/po_ujian/form/'.$id);
            
        }
        else{
            set_flashdata('error', 'Data Tidak berhasil disimpan. '.$msg);
            redirect('/po_ujian/');
        }
    }

    public function delete($id) {
        $this->load->model('MPoUjian');
        $data = MPoUjian::find($id);
        if(!$data) {
            return redirect('po_ujian/');
        }

        $this->db->trans_begin();
        $this->db->delete('po_ujian', array('id' => $id)); 
        logs("POST","po_ujian/delete/",$id,"po_ujian","delete"); //activity_logs => "method,route,table_id,table_name,table_aksi"
        
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }
        else{
            $this->db->trans_commit();
        }
        set_flashdata('success', 'Data berhasil disimpan');
        redirect('/po_ujian/');
    }
    
    public function detail($id) {
        $this->load->model('MPoUjian');
        $data = MPoUjian::find($id);
        if(!$data) {
            return redirect('po_ujian/');
        }
        
        $this->setParam('data', @$data);
        $this->setContentView('web/po_ujian/detail');
        $this->render('Detail Ujian Site Visit');
    }


}