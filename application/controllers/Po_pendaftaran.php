<?php
class Po_pendaftaran extends CI_Controller {

    public function __construct() {
        parent::__construct();
        
        $this->load->model('MPersonal');
        $this->load->model('MCoreUser');
        $this->load->model('MPoPendaftaran');
    }

    public function index($cek_email='1') {
        $data["cek_email"] = $cek_email;
        $this->load->view('po_pendaftaran/index_pendaftaran',@$data);
    }


    public function save() {
        
        $post   = $this->input->post();
        $id     = !empty($post['id'])?$post['id']:null;

        
        $result = true;
        // validasi
        $msg = '';
        if ($post['password']!=$post['password_ulangi']) {
            $result = false;
            $msg    = 'Password tidak sama!';
        }
        // end validasi

        //google captcha
        $recaptchaResponse = trim($this->input->post('g-recaptcha-response'));
        $userIp = $this->input->ip_address();
        $secret = $this->config->item('google_secret');
   
        $url="https://www.google.com/recaptcha/api/siteverify?secret=".$secret."&response=".$recaptchaResponse."&remoteip=".$userIp;

        $ch     = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, $url); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        $output = curl_exec($ch); 
        curl_close($ch);      

        $status= json_decode($output, true);

        if (!@$status['success'] OR @$status['success']!='1') {
            set_flashdata('error', 'Sorry Google Recaptcha Unsuccessful!!. '.$msg);
            redirect('/');
            return;
        }
        //end google captcha


        
        $data_cek = MPoPendaftaran::where('email',$post['email'])->first();

        if (!empty($data_cek->id)) {
            set_flashdata('error', 'Email anda Sudah Terdaftar. '.$msg);
            redirect('/po_pendaftaran/index/0');
        }


        if($result==true){
            $this->db->trans_begin();

            $params = array(
                "email"         => $post['email'],
                "nama"          => MasXssClean($post['nama']),
                "nomor_handphone"=> MasXssClean($post['nomor_handphone']),
                "password"      => md5($post['password']),
                "setuju"        => !empty($post['setuju'])?$post['setuju']:0,
                "status"        => 9, //Terdaftar
            );


            
            $data = MPoPendaftaran::where('email',$post['email'])
                    ->where('status',9)
                    ->orderBy('id', 'DESC')->first();


            if (empty($data->id)) { //untuk insert
                $params['created_at'] = date('Y-m-d H:i:s');
                $params['created_by'] = $post['email'];

                $this->db->insert('po_pendaftaran', $params);
                $id = $this->db->insert_id();
                logs("POST","po_pendaftaran/save/",$id,"po_pendaftaran","insert"); //activity_logs => "method,route,table_id,table_name,table_aksi"   
            }else { //untuk update
                $params['updated_at'] = date('Y-m-d H:i:s');
                $params['updated_by'] = $post['email'];

                $id = $data->id;
                $this->db->update('po_pendaftaran', $params, array('id' => $id));
                logs("POST","po_pendaftaran/save/",$id,"po_pendaftaran","update"); //activity_logs => "method,route,table_id,table_name,table_aksi"
            }

            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
            }
            else{
                $this->db->trans_commit();

                //send email
                $dataEmail = array(
                    'email'         => $post['email'],
                    'url_detail'    => site_url('po_pendaftaran/verifikasi/'.$id),
                );

                $this->load->library('mailgun');
                $dataSend = array(
                    'to'        => $post['email'],
                    'subject'   => "Pendaftaran - Platform ALUDI",
                    'html'      => $this->load->view('email/pendaftaran_verifikasi',$dataEmail,true),  
                );
                $this->mailgun::send($dataSend);
                //end send email
            }
            set_flashdata('success', 'Data berhasil disimpan. Silahkan Cek Email Untuk melanjutkan!');
            redirect('/po_pendaftaran/');
            
        }
        else{
            set_flashdata('error', 'Data Tidak berhasil disimpan. '.$msg);
            redirect('/');
        }
    }

    public function verifikasi($id){
        /*if (!authUser()) {
            redirect('auth/login?redirect='. site_url(uri_string()));
            return;
        }*/
        
        $result = true;
        // validasi
        $msg = '';
        
        $data = MPoPendaftaran::find($id);
        if (empty($data->id)) {
            $result = false;
            $msg    = 'Pendaftaran tidak ditemukan!';
        }
        // end validasi
        if($result==true){
            $this->db->trans_begin();
            

            $params['updated_at'] = date('Y-m-d H:i:s');
            $params['updated_by'] = $data->email;

            $id = $data->id;
            $this->db->update('po_pendaftaran', $params, array('id' => $id));
            logs("POST","po_pendaftaran/save/",$id,"po_pendaftaran","update"); //activity_logs => "method,route,table_id,table_name,table_aksi"

            //insert core_users
            $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyz';
            $token = substr(str_shuffle($permitted_chars), 0, 10);
            $params_core_users['created_at']    = date('Y-m-d H:i:s');
            $params_core_users['created_by']    = $data->email;
            $params_core_users['uuid']          = $token;
            $params_core_users['email']         = $data->email;
            $params_core_users['password']      = $data->password;
            $params_core_users['role_id']       = 2;
            
            $this->db->insert('core_users', $params_core_users);
            $id_user = $this->db->insert_id();
            //end insert core_users

            //insert personals
            $params_personals['created_at']         = date('Y-m-d H:i:s');
            $params_personals['created_by']         = $id_user;
            $params_personals['user_id']            = $id_user;
            $params_personals['id_pendaftaran']     = $id;
            $params_personals['name']               = $data->nama;
            $params_personals['phone']              = $data->nomor_handphone;
            $params_personals['personal_email']     = $data->email;
            
            $this->db->insert('personals', $params_personals);
            $id_personal = $this->db->insert_id();
            //end insert personals
            
            //insert personals
            $params_tahap['created_at']     = date('Y-m-d H:i:s');
            $params_tahap['created_by']     = $id_user;
            $params_tahap['id_personal']    = $id_personal;
            $params_tahap['id_alur']        = 2;
            $params_tahap['status']         = 16; //Tahap 1
            
            $this->db->insert('po_pendaftaran_tahap', $params_tahap);
            //end insert personals
           
            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
            }
            else{
                $this->db->trans_commit();

            }
            $user = MCoreUser::where([
                'email' => $data->email,
            ])->first();
            $this->session->set_userdata(['user' => $user->toArray()]);
            set_flashdata('success', 'Data berhasil disimpan. Silahkan melengkapi data!');
            redirect('/po_tahap_pendaftaran');
        }
        else{
            set_flashdata('error', 'Data Tidak berhasil disimpan. '.$msg);
            redirect('/po_pendaftaran');
        }
    }

    public function save_aduan_aludi(){
        $data = $this->input->post();
        if (empty($data)) {
            redirect('/');
            return;
        }

        $result = true;


        if($result==true){
            $this->db->trans_begin();
            $params = array(
                "nama"               => MasXssClean($data['nama']),
                "no_kontak"          => MasXssClean($data['no_kontak']),
                "email"              => MasXssClean($data['email']),
                "id_bentuk_aduan"    => MasXssClean(@$data['id_bentuk_aduan']),
                "id_nama_platform"   => MasXssClean($data['id_nama_platform']),
                "kronologis"         => MasXssClean($data['kronologis']),
                "status"             => "5",
                "created_at"         => date('Y-m-d H:i:s'),
                "created_by"         => $data['email'],

            );

            if(isset($_FILES['lampiran']) && !empty($_FILES['lampiran']['name'])) {//bila ada lampiran
                
				$config['upload_path']          = './uploads/pengaduan_lampiran/';
                $config['allowed_types']        = 'gif|jpg|jpeg|png|pdf|word';
                $config['overwrite']        	= true;
                $config['encrypt_name']        	= true;
                $config['max_size']             = 5120;
				$this->load->library('upload', $config);
				if($this->upload->do_upload('lampiran')) { 
					$upload_data        = $this->upload->data();                
					$params['lampiran']  = $upload_data['file_name'];
				} else { 
					set_flashdata('error', 'Format file tidak sesuai dengan ketentuan.');
					redirect('/');
					//echo $this->upload->display_errors(); 
				} 
                

     
                
                
            }
			if($params['lampiran'] != ""){
				$this->db->insert('po_aduan_draft', $params);
				$insert_id = $this->db->insert_id();
            
				logs("POST","po_pendaftaran/save_aduan_aludi/",$insert_id,"po_aduan","insert"); //activity_logs => "method,route,table_id,table_name,table_aksi"   
			}
            
            
            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
				set_flashdata('error', 'Data Tidak berhasil disimpan. '.$msg);
				redirect('/');
            }
            else{
                $this->db->trans_commit();
             $dataEmail = array(
                    'email'         => $post['email'],
                    'url_detail'    => site_url('auth/report_done_activation/'.$insert_id),
                );

                $this->load->library('mailgun');
                $dataSend = array(
                    'to'        => $params['email'],
                    'subject'   => "Pendaftaran - Platform ALUDI",
                    'html'      => $this->load->view('email/complaint_verification',$dataEmail,true),  
                );
                $this->mailgun::send($dataSend);
                //end send email
				set_flashdata('success', 'Aduan berhasil disimpan. Silahkan Cek Email Untuk verifikasi!');
				redirect('/');
            }
            
   

            
            
        }
        else{
            set_flashdata('error', 'Data Tidak berhasil disimpan. '.$msg);
            redirect('/');
        }
    }
    
}