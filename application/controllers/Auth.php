<?php

class Auth extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }
    public function login() {
        ini_set('date.timezone', 'Asia/Jakarta');
        if(cekSetting('perawatan')==1){ redirect('/verified/perawatan');} //cek perawatan
        if(cekIp(getIp())==0 AND cekSetting('cek_ip')==1){ redirect('/verified/');} //cek ip
        
        if (strtolower($this->input->method()) == 'get') {
            if(authUser()) {
                redirect('dashboard');
            }
            return $this->load->view('auth/login');
        }


        
        
        //google captcha
        $recaptchaResponse = trim($this->input->post('g-recaptcha-response'));
        $userIp = $this->input->ip_address();
        $secret = $this->config->item('google_secret');
   
        $url="https://www.google.com/recaptcha/api/siteverify?secret=".$secret."&response=".$recaptchaResponse."&remoteip=".$userIp;

        $ch     = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, $url); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        $output = curl_exec($ch); 
        curl_close($ch);      

        $status= json_decode($output, true);

        if (!@$status['success'] OR @$status['success']!='1') {
            $this->session->set_flashdata('error', 'Sorry Google Recaptcha Unsuccessful!!');
            redirect('auth/login');
            return;
        }
        //end google captcha

        $this->load->model('MCoreUser');
        $this->load->model('MCoreDevice');
        $this->load->library('form_validation');


        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('password', 'password', 'required');
        if(!$this->form_validation->run()) {
            return $this->load->view('auth/login');
        }

        $data = $this->input->post();

        $user = MCoreUser::where([
            'email' => $data['email'],
            'password' => md5($data['password']),
        ])->first();

        if(!$user) {
            $this->session->set_flashdata('error', 'The combination of email and password is invalid');
            redirect('auth/login');
            return;
        }

        if (!$user->personal) {
            $this->session->set_flashdata('error', 'Your personal data is not complete, ask HR to complete your data');
            redirect('auth/login');
            return;
        }

        if ($user->personal->status == 9) {
            $this->session->set_flashdata('error', 'Anda Belum Menverifikasi email');
            redirect('auth/login');
            return;
        }
        

        $mac_address    = getMac();
        $format_device  = $mac_address->platform.'|'.$mac_address->browser;
        $data_device = MCoreDevice::where([
            'name'      => $data['email'],
            'device'    => $format_device,
        ])->first();

        $cekDevice  = false; 
        $id         = "";
        if ($data_device) {
            $cekDevice  = true;
            if($data_device->status==3){ //status tidak aktif
                $cekDevice  = false;
            }
            $id         = $data_device->id;
        }
            
        // if (!$cekDevice AND cekSetting('cek_device')==1) {
        if (!$cekDevice) {
            $token  = getToken(50);
            
            $this->db->trans_begin();
            $params = array(
                "name"      => $data['email'],
                "device"    => $format_device,
                "token"     => $token,
                "status"    => 3,
            );
            if(!empty($id)){
                $params["updated_at"] = date('Y-m-d H:i:s');
                $params["updated_by"] = $data['email'];
                $this->db->update('core_device', $params, array('id' => $id));
                
                logs("POST","auth/login",$id,"core_device","update"); //activity_logs => "method,route,table_id,table_name,table_aksi"
            }else {
                $params["created_at"] = date('Y-m-d H:i:s');
                $params["created_by"] = $data['email'];
                $this->db->insert('core_device', $params);
                $insertId = $this->db->insert_id();

                logs("POST","auth/login",$insertId,"core_device","insert"); //activity_logs => "method,route,table_id,table_name,table_aksi"
            }

            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
            }
            else{
                $this->db->trans_commit();

                if (cekSetting('cek_device')==1) {
                    //send email
                    $this->load->library('mailgun');

                    $dataEmail = array(
                        'name'          => $data['email'],
                        'waktu'         => date('Y-m-d H:i:s'),
                        "device"        => $format_device,
                        'action_url'    => site_url('verified/saveStatusAuth/'.$token),
                    );
                    
                    $dataSend = array(
                        'to'        => $data['email'],
                        'subject'   => "Login  - PT. Santara Daya Inspiratama",
                        'html'      => $this->load->view('email/verified_auth',$dataEmail,true),   
                    );
                    $this->mailgun::send($dataSend);
                    //end send email
                }
                
            }
            
            
            if (cekSetting('cek_device')==1) {
                $this->session->set_flashdata('error_device', 'silahkan cek email anda, untuk melanjutkan masuk.');
                redirect('auth/login');
                return;
            }
        }
        
        $this->session->set_userdata(['user' => $user->toArray()]);

        if(isset($data['redirect']) && !empty($data['redirect'])) {
            return redirect(MasXssClean($data['redirect']));
        }

        redirect('dashboard');
    }

    public function forgotpassword() {
        if(cekSetting('perawatan')==1){ redirect('/verified/perawatan');} //cek perawatan
        $this->load->view('auth/forgotpassword');
    }
	
	public function report_done_activation($id) {
        $arraduan_draft = $this->db->query("SELECT * FROM po_aduan_draft WHERE id = '$id'")->row_array();
		$this->db->insert('po_aduan', $arraduan_draft);
		$this->db->query("UPDATE po_aduan_draft SET status = 99 WHERE id = '$id'");
		 
		set_flashdata('success', 'Aduan anda telah berhasil diverifikasi. '.$msg);
				redirect('/');
    }
	
	public function report_done() {
        if(cekSetting('perawatan')==1){ redirect('/verified/perawatan');} //cek perawatan
        $this->load->view('auth/report_done');
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect('auth/login');
    }

    public function reset_password() {
        if(cekSetting('perawatan')==1){ redirect('/verified/perawatan');} //cek perawatan
        if (strtolower($this->input->method()) == 'get') {
            return redirect('auth/login');
        }
        $email = $this->input->post('email');
        $this->load->model('MCoreUser');
        $user = MCoreUser::where('email', $email)->first();
        
        if($user) {
            $token = base64_encode(json_encode([
                'e' => date('YmdHis'),
                'i' => $user->id,
            ]));

            
            $data = array(
                'token' => $token        
            );
            
            $this->load->library('mailgun');
            $this->mailgun::send([
                'to'        => $email,
                'subject'   => "Reset Password Requets",
                'html'      => $this->load->view('email/reset_password',$data,true),   
            ]);
        }

        $this->load->view('auth/forgotresult');
    }

    public function new_password() {
        if(cekSetting('perawatan')==1){ redirect('/verified/perawatan');} //cek perawatan
        $token = $this->input->get('t');
        $token = json_decode(base64_decode($token), true);

        $now = \Carbon\Carbon::now('Asia/Jakarta');
        $exp = \Carbon\Carbon::createFromFormat('YmdHis', $token['e'], 'Asia/Jakarta')->addHours(24);
        if($exp->lt($now)) {
            $this->session->set_flashdata('error', 'Reset password link is expire');
            redirect('auth/login');
            return;
        }

        $this->load->view('auth/new_password', $token);
    }

    public function update_password() {
        if(cekSetting('perawatan')==1){ redirect('/verified/perawatan');} //cek perawatan
        $data = $this->input->post();

        if ($data['password'] != $data['c_password']) {
            $this->session->set_flashdata('error', 'Confirm password not match');
            redirect('auth/new_password?t='. $data['token']);
            return;
        }

        $token = json_decode(base64_decode($data['token']), true);
    
        $this->load->model('MCoreUser');
        $user = MCoreUser::where('id', $token['i'])->first();
        $user->password = md5($data['password']);
        $user->save();

        redirect('auth/login');
    }
}