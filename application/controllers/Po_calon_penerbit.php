<?php

class Po_calon_penerbit extends sntr_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('MPoPendaftaran');
        $this->load->model('MCoreRefStatus');
        $this->load->model('MCoreUser');
        $this->load->model('MCoreRefProvinsi');
        $this->load->model('MCoreRefKabupaten');
        
        $this->load->model('MPoCalonPenerbit');
        ini_set('date.timezone', 'Asia/Jakarta');
    }

    public function index() {
        
        if (MasKaryawan()->user->role_id=='1') {
            $data = MPoCalonPenerbit::orderBy('id', 'desc')->get();
        }else{
            $data = MPoCalonPenerbit::where("created_by",MasKaryawan()->user->id)
            ->orderBy('id', 'desc')->get();
        }

        //filter
        $status     = "";
        if (@$_POST['status']!="" AND @$_POST['status']!="All") {
            if (MasKaryawan()->user->role_id=='1') {
                $data = MPoCalonPenerbit::
                where('status', $_POST['status'])
                ->orderBy('id', 'desc')->get();
                
            }else{
                $data = MPoCalonPenerbit::where("created_by",MasKaryawan()->user->id)
                ->where('status', $_POST['status'])
                ->orderBy('id', 'desc')->get();
            }

            $status     = @$_POST['status'];
        }
        //end filter

        $refStatus  = MCoreRefStatus::
        where('grup', 13)
        ->orderBy('id', 'desc')
        ->get();
        
        $this->setParam('refStatus', $refStatus);
        $this->setParam('status', $status);
        $this->setParam('data', $data);
        $this->setContentView('web/po_calon_penerbit/index');
        $this->render('Calon Penerbit');
    }

    public function form($id='',$mode='') {
        if (!empty($id)) {
            $data = MPoCalonPenerbit::where('id', $id)->orderBy('id', 'asc')->first();
        }
        
        $refStatus      = MCoreRefStatus::
                            // whereIn('id', explode(',', '1,3,4'))
                            where('grup', '13')
                            ->orderBy('name', 'asc')->get();

        $this->setParam('refStatus', $refStatus);
        $this->setParam('mode', @$mode);
        $this->setParam('data', @$data);
        $this->setContentView('web/po_calon_penerbit/form');
        $this->render('Form Calon Penerbit');
    }

    public function save() {        
        $post   = $this->input->post();
        $id     = !empty($post['id'])?$post['id']:null;

        
        $result = true;
        // validasi
        $msg = '';
        // end validasi

        if($result==true){
            $this->db->trans_begin();

            $params = array(
                "id_personal"       => MasKaryawan()->id,
                "kode"              => MasToken(25),
                "owner"             => $post['owner'],
                "nomor_telepon"     => $post['nomor_telepon'],
                "email"             => $post['email'],
                "nama_perusahaan"   => $post['nama_perusahaan'],
                "nama_brand"        => $post['nama_brand'],
                "bidang_usaha"      => $post['bidang_usaha'],
                "lokasi"            => $post['lokasi'],
                "total_pendanaan"   => $post['total_pendanaan'],
                "deskripsi"         => $post['deskripsi'],
                "status"            => $post['status'],
            );

            
           

            if (empty($id)) { //untuk insert
                $params['created_at'] = date('Y-m-d H:i:s');
                $params['created_by'] = authUser()->id;

                $this->db->insert('po_calon_penerbit', $params);
                $id = $this->db->insert_id();
                logs("POST","po_calon_penerbit/save/",$id,"po_calon_penerbit","insert",authUser()->id,"Memambahkan Calon Penerbit"); //activity_logs => "method,route,table_id,table_name,table_aksi"   
            }else { //untuk update
                $params['updated_at'] = date('Y-m-d H:i:s');
                $params['updated_by'] = authUser()->id;

                $this->db->update('po_calon_penerbit', $params, array('id' => $id));
                logs("POST","po_calon_penerbit/save/",$id,"po_calon_penerbit","update",authUser()->id,"Merubah Calon Penerbit"); //activity_logs => "method,route,table_id,table_name,table_aksi"
            }


            $data = MPoCalonPenerbit::find($id); //data 

            if(!empty($_FILES['files']['name']['foto'])) {//bila ada lampiran foto
                if (!empty(@$data->foto)) {
                    $path = './uploads/calon_penerbit/'.@$data->foto;
                    unlink($path);
                }
    
                $config = array();
                $config['upload_path']          = './uploads/calon_penerbit/';
                $config['allowed_types']        = 'jpg|jpeg|gif|png';
                $config['overwrite']        	= true;
                //$config['encrypt_name']        	= true;
                $config['max_size']             = 10120;
    
                // $this->load->library('upload', $config);
                $this->load->library('upload', $config, 'foto'); // Create custom object for cover upload
                $this->foto->initialize($config);
    
                
                $_FILES['foto']['name']       = $_FILES['files']['name']['foto'];
                $_FILES['foto']['type']       = $_FILES['files']['type']['foto'];
                $_FILES['foto']['tmp_name']   = $_FILES['files']['tmp_name']['foto'];
                $_FILES['foto']['error']      = $_FILES['files']['error']['foto'];
                $_FILES['foto']['size']       = $_FILES['files']['size']['foto'];
    
                
                if (!$this->foto->do_upload('foto')) {
                    set_flashdata('success', 'Data Tidak Berhasil Disimpan, Pastikan File jpg|jpeg|gif|png|doc|docx|pdf|xls|xlsx Dan ukuran Maximal 10mb');
                    redirect('/po_tahap_pendaftaran/pembayaran');
                }
    
                $upload_data        = $this->foto->data();
                $params['foto']  = $upload_data['file_name'];
        
                $this->db->update('po_calon_penerbit', $params, array('id' => $id));
            }

            
            if(!empty($_FILES['files']['name']['lampiran'])) {//bila ada lampiran 
                if (!empty(@$data->lampiran)) {
                    $path = './uploads/calon_penerbit/'.@$data->lampiran;
                    unlink($path);
                }
    
                $config = array();
                $config['upload_path']          = './uploads/calon_penerbit/';
                $config['allowed_types']        = 'jpg|jpeg|gif|png|doc|docx|pdf|xls|xlsx';
                $config['overwrite']        	= true;
                //$config['encrypt_name']        	= true;
                $config['max_size']             = 10120;
    
                // $this->load->library('upload', $config);
                $this->load->library('upload', $config, 'lampiran'); // Create custom object for cover upload
                $this->lampiran->initialize($config);
    
                
                $_FILES['lampiran']['name']       = $_FILES['files']['name']['lampiran'];
                $_FILES['lampiran']['type']       = $_FILES['files']['type']['lampiran'];
                $_FILES['lampiran']['tmp_name']   = $_FILES['files']['tmp_name']['lampiran'];
                $_FILES['lampiran']['error']      = $_FILES['files']['error']['lampiran'];
                $_FILES['lampiran']['size']       = $_FILES['files']['size']['lampiran'];
    
                
                if (!$this->lampiran->do_upload('lampiran')) {
                    set_flashdata('success', 'Data Tidak Berhasil Disimpan, Pastikan File jpg|jpeg|gif|png|doc|docx|pdf|xls|xlsx Dan ukuran Maximal 10mb');
                    redirect('/po_calon_penerbit/form');
                }
    
                $upload_data        = $this->lampiran->data();
                $params['lampiran']  = $upload_data['file_name'];
        
                $this->db->update('po_calon_penerbit', $params, array('id' => $id));
            }

            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
            }
            else{
                $this->db->trans_commit();
            }
            set_flashdata('success', 'Data berhasil disimpan');
            redirect('/po_calon_penerbit/');
            
        }
        else{
            set_flashdata('error', 'Data Tidak berhasil disimpan. '.$msg);
            redirect('/po_calon_penerbit/');
        }
    }

    public function delete($id) {
        $data = MPoCalonPenerbit::find($id);
        if(!$data) {
            return redirect('po_calon_penerbit/');
        }

        $this->db->trans_begin();
        $this->db->delete('po_calon_penerbit_file', array('id_artikel' => $id));  
        $this->db->delete('po_calon_penerbit', array('id' => $id)); 
        logs("POST","po_calon_penerbit/delete/",$id,"po_calon_penerbit","delete"); //activity_logs => "method,route,table_id,table_name,table_aksi"
        
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }
        else{
            $this->db->trans_commit();
        }
        set_flashdata('success', 'Data berhasil disimpan');
        redirect('/po_calon_penerbit/');
    }
    
    public function detail($id) {
        $data = MPoCalonPenerbit::find($id);


        if(!$data OR empty($id)) {
            return redirect('po_calon_penerbit/');
        }


        
        $refProvinsi      = MCoreRefProvinsi::
                            where("id",@$data->tahap_satu->id_provinsi)
                            ->orderBy('nama', 'asc')->get();

        $refKabupaten      = MCoreRefKabupaten::
                            where("id",@$data->tahap_satu->id_kota)->orderBy('nama', 'asc')->get();


        // MasDebugPree($data->tahap_rekomendasi->rekomendasis->nama_file,1);
        // MasDebugPree($data->tahap_satu,1);
        $this->setParam('refKabupaten', $refKabupaten);
        $this->setParam('refProvinsi', $refProvinsi);
        $this->setParam('data_hasil', @$data_hasil);        
        $this->setParam('data', @$data);
        $this->setContentView('web/po_calon_penerbit/detail');
        $this->render('Detail Peserta');
    }
    
    
    public function printxlx() {
        $data = MPoCalonPenerbit::get();
        
        $result['data']         = $data;
        // return $this->load->view('laporan/peserta_list',$result);  
        
        ini_set('memory_limit', '256M');
        ini_set('max_execution_time', 0);
        header('Content-Type: application/vnd.ms-excel');
        header("Content-Disposition: attachment;Filename=report.xls");
        header('Cache-Control: max-age=0');
        $result = $this->load->view('laporan/peserta_list',$result,true);
        echo $result;
        exit;
    }
}