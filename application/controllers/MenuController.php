<?php

use App\Constants\Menu;

class MenuController extends sntr_Controller {

    private $menu_id = 1;
    private $permission_id = 1;
    private $child_menu_id = 1;

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->model('MMenu');
        $this->load->model('MMenuChild');
        $this->load->model('MPermission');

        MMenu::truncate();
        MMenuChild::truncate();
        MPermission::truncate();

        $menus = Menu::getMenu();

        foreach($menus as $menu) {
            $m = new MMenu;
            $m->id = $this->menu_id;
            $m->title = $menu['title'];
            $m->route = $menu['route'];
            $m->icon = $menu['icon'];
            $m->save();

            foreach($menu['permissions'] as $permission) {
                $p = new MPermission;
                $p->id = $this->permission_id;
                $p->menu_id = $this->menu_id;
                $p->permission = $permission;
                $p->save();
                $this->permission_id += 1;
            }

            foreach ($menu['child'] as $child) {
                $mc = new MMenuChild;
                $mc->id = $this->child_menu_id;
                $mc->title = $child['title'];
                $mc->route = $child['route'];
                $mc->icon = $child['icon'];
                $mc->menu_id = $this->menu_id;

                if(!empty($child['access_permission']) && isset($child['access_permission'])) {
                    $mc->access_permission = $child['access_permission'];
                }

                $mc->save();

                $this->child_menu_id += 1;
            }

            $this->menu_id += 1;
        }
    }
}