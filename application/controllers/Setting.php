<?php

class Setting extends sntr_Controller {

    public function role() {
        $this->load->model('MCoreRole');
        $roles = MCoreRole::orderBy('id', 'desc')->get();
        // MasDebugPree($roles,1);
        $this->setParam('roles', $roles);
        $this->setContentView('web/setting/index');
        $this->render('Settings');
    }

    public function add_role() {
        $this->load->model('MCoreMenu');
        $menus = MCoreMenu::orderBy('id', 'asc')->get();
        $this->setParam('menus', $menus);
        $this->load->model('MCoreRole');
        $this->setContentView('web/setting/create');
        $this->render('Settings');
    }

    public function insert_role() {
        if(!has_permission('CREATE_ROLE')) {
            return redirect('setting/role');
        }
        $this->load->model('MCoreRole');
        $this->load->library('uuid');
        $data = $this->input->post();
        $role = new MCoreRole;
        $role->uuid = $this->uuid->v4();
        $role->name = $data['name'];
        $role->permissions = implode(',', $data['access']);
        $role->save();

        $this->load->model('MCoreRolePerm');
        foreach($data['permissions'] as $perm) {
            $rp = new MCoreRolePerm;
            $rp->role_id = $role->id;
            $rp->permission = $perm;
            $rp->save();
        }

        set_flashdata('success', 'Data berhasil disimpan');
        redirect('setting/role');
    }

    public function edit_role($id) {
        $this->load->model('MCoreRole');
        $this->load->model('MCoreMenu');
        $menus = MCoreMenu::orderBy('id', 'asc')->get();
        $role = MCoreRole::where('id', $id)->first();
        $this->setParam('menus', $menus);
        $this->setParam('role', $role);
        $this->load->model('MCoreRole');
        $this->setContentView('web/setting/edit');
        $this->render('Settings');
    }

    public function update_role($id) {
        if(!has_permission('UPDATE_ROLE')) {
            return redirect('setting/role');
        }
        $this->load->model('MCoreRole');
        $data = $this->input->post();
        $role = MCoreRole::find($id);
        $role->name = $data['name'];
        $role->permissions = implode(',', $data['access']);
        $role->save();

        $role->role_permissions()->delete();
        $this->load->model('MCoreRolePerm');
        foreach($data['permissions'] as $perm) {
            $rp = new MCoreRolePerm;
            $rp->role_id = $role->id;
            $rp->permission = $perm;
            $rp->save();
        }

        set_flashdata('success', 'Data berhasil disimpan');
        redirect('setting/role');
    }

    public function account() {
        $this->load->model('MCoreUser');
        $accounts = MCoreUser::where('role_id','!=','2')
            ->orderBy('id', 'desc')
            ->get();
        $this->setParam('accounts', $accounts);
        $this->setContentView('web/setting/account/index');
        $this->render('Settings');
    }

    public function add_account() {
        $this->load->model('MCoreRole');
        $roles = MCoreRole::where('id','!=','2')
            ->orderBy('id', 'desc')
            ->get(); //disini sudah dibenerin
        $this->setParam('roles', $roles);

        $this->load->model('MPersonal');
        $candidats = MPersonal::whereNull('user_id')->orderBy('id', 'desc')->get();
        $this->setParam('candidats', $candidats);

        $this->setContentView('web/setting/account/create');
        $this->render('Settings');
    }

    public function insert_account() {
        if(!has_permission('CREATE_ACCOUNT')) {
            return redirect('setting/role');
        }
        $this->load->model('MPoPendaftaran');
        $this->load->model('MCoreUser');

        $data = $this->input->post();
        $id     = !empty($post['id'])?$post['id']:null;


        
        $result = true;        

        // cek email yang sudah terdaftar
        $data_cek = MPoPendaftaran::where('email',$data['email'])->first();

        if (!empty($data_cek->id)) {
            set_flashdata('error', 'Email anda Sudah Terdaftar. '.$msg);
            redirect('/po_pendaftaran/index/0');
        }
        /////////////////////////////////////

        if($result==true){
            $this->db->trans_begin();
            

            // $params['updated_at'] = date('Y-m-d H:i:s');
            // $params['updated_by'] = $data->email;

            // $id = $data->id;
            //$this->db->update('po_pendaftaran', $params, array('id' => $id));
            //logs("POST","po_pendaftaran/save/",$id,"po_pendaftaran","update"); //activity_logs => "method,route,table_id,table_name,table_aksi"

            //insert core_users
            $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyz';
            $token = substr(str_shuffle($permitted_chars), 0, 10);
            $params_core_users['created_at']    = date('Y-m-d H:i:s');
            $params_core_users['created_by']    = authUser()->id;
            $params_core_users['uuid']          = $token;
            $params_core_users['email']         = $data['email'];
            $params_core_users['password']      =  md5($data['password']);
            $params_core_users['role_id']       = $data['role'];
            
            $this->db->insert('core_users', $params_core_users);
            $id_user = $this->db->insert_id();
            //end insert core_users

            //insert personals
            $params_personals['created_at']         = date('Y-m-d H:i:s');
            $params_personals['created_by']         = authUser()->id;
            $params_personals['user_id']            = $id_user;
            $params_personals['id_pendaftaran']     = $id;
            $params_personals['name']               = $data['candidat'];
//            $params_personals['phone']              = $data->nomor_handphone;
            $params_personals['personal_email']     = $data['email'];
            $params_personals['join_date']          = date('Y-m-d');
            $params_personals['status']             = "2";
            
            $this->db->insert('personals', $params_personals);
            $id_personal = $this->db->insert_id();
            //end insert personals
            
            //insert personals
            // $params_tahap['created_at']     = date('Y-m-d H:i:s');
            // $params_tahap['created_by']     = $id_user;
            // $params_tahap['id_personal']    = $id_personal;
            // $params_tahap['id_alur']        = 2;
            // $params_tahap['status']         = 16; //Tahap 1
            
            // $this->db->insert('po_pendaftaran_tahap', $params_tahap);
            //end insert personals
           
            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
            }
            else{
                $this->db->trans_commit();

            }

            // $user = MCoreUser::where([
            //     'email' => $data->email,
            // ])->first();
            // $this->session->set_userdata(['user' => $user->toArray()]);

            set_flashdata('success', 'Data berhasil disimpan. Silahkan melengkapi data!');
            redirect('setting/account');
        }
        else{
            set_flashdata('error', 'Data Tidak berhasil disimpan. '.$msg);
            redirect('/setting/account');
        }       
        
        // $account = new MCoreUser;
        // $this->load->library('uuid');
        // $account->uuid = $this->uuid->v4();
        // $account->email = $data['email'];
        // $account->role_id = $data['role'];
        // $account->password = md5($data['password']);

        // $account->save();

        // set_flashdata('success', 'Data berhasil disimpan');
        // redirect('setting/account');
    }

    public function edit_account($id) {
        $this->load->model('MCoreUser');
        $account = MCoreUser::find($id);
        $this->setParam('account', $account);


        $this->load->model('MCoreRole');
        $roles = MCoreRole::orderBy('id', 'desc')->get();
        $this->setParam('roles', $roles);

        $this->setContentView('web/setting/account/edit');
        $this->render('Settings');
    }

    public function update_account($id) {
        if(!has_permission('UPDATE_ACCOUNT')) {
            return redirect('setting/role');
        }
        $data = $this->input->post();
        $this->load->model('MCoreUser');

        $account = MCoreUser::find($id);
        $account->email = $data['email'];
        $account->role_id = $data['role'];

        if(isset($data['password'])) {
            if ($data['password']) {
                $account->password = md5($data['password']);
            }
        }

        $account->save();

        set_flashdata('success', 'Data berhasil disimpan');
        redirect('setting/account');
    }
}